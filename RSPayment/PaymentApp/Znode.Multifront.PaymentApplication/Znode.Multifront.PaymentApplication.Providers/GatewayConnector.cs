﻿using System;
using System.Diagnostics;
using System.Linq;
using Znode.Multifront.PaymentApplication.Data;
using Znode.Multifront.PaymentApplication.Helpers;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Providers
{
    public class GatewayConnector : BaseConnector
    {
        #region Public Methods
        /// <summary>
        /// Execute and get Credit card processing and returns the GUID.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel GetResponse(PaymentModel paymentModel)
        {
            try
            {
                TransactionService repository = new TransactionService();

                if (!string.IsNullOrEmpty(paymentModel.TransactionId))
                {
                    ZnodeTransaction transactionDetails = repository.GetPayment(paymentModel.TransactionId);
                    if (!Equals(transactionDetails, null))
                    {
                        paymentModel.CustomerProfileId = transactionDetails.CustomerProfileId;
                        paymentModel.CustomerPaymentProfileId = transactionDetails.CustomerPaymentId;
                        paymentModel.TransactionId = transactionDetails.TransactionId;
                        paymentModel.GatewayCurrencyCode = transactionDetails.CurrencyCode;
                        paymentModel.CardDataToken = transactionDetails.Custom1;
                        paymentModel.OrderId = transactionDetails.Custom1;
                        paymentModel.GUID = Convert.ToString(transactionDetails.GUID);
                    }
                }

                GatewayResponseModel response = ProcessCreditCard(paymentModel);

                if (Equals(response, null))
                    return new GatewayResponseModel { HasError = true, ErrorMessage = "Authorization failed" };

                if (response.IsSuccess)
                {
                    if (string.IsNullOrEmpty(paymentModel.TransactionId))
                    {
                        paymentModel.CustomerProfileId = response.CustomerProfileId;
                        paymentModel.CustomerPaymentProfileId = response.CustomerPaymentProfileId;
                    }

                    paymentModel.TransactionId = response.TransactionId;
                    paymentModel.ResponseText = response.GatewayResponseData;
                    paymentModel.ResponseCode = response.ResponseCode;
                    paymentModel.CardDataToken = response.Token;
                    paymentModel.PaymentStatusId = (int)response.PaymentStatus;
                    paymentModel.GUID = string.IsNullOrEmpty(paymentModel.GUID) ? repository.AddPayment(paymentModel) : repository.UpdatePayment(paymentModel);

                    if (paymentModel.Subscriptions.Any() && paymentModel.GatewayType != Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.TWOCHECKOUT.ToString())).ToString())
                    {
                        GetSubscriptionResponse(paymentModel);
                    }
                    //NIVI CODE Added ,CardAuthCode = response.CardAuthCode
                    return new GatewayResponseModel { Token = paymentModel.TransactionId, IsGatewayPreAuthorize = response.IsGatewayPreAuthorize,CardAuthCode = response.CardAuthCode };
                }
                else
                {
                    string message = string.Empty;
                    if (!Equals(response.GatewayResponseData, null))
                    {
                        message = response.GatewayResponseData.Replace("<br>", string.Empty);
                        Logging.LogMessage(message, Logging.Components.Payment.ToString(), TraceLevel.Error);
                    }
                    else if (!string.IsNullOrEmpty(response.ResponseCode) || !string.IsNullOrEmpty(response.ResponseText))
                    {
                        message = $"{response.ResponseCode} {response.ResponseText}";
                        Logging.LogMessage(message, Logging.Components.Payment.ToString(), TraceLevel.Error);
                    }
                    else
                    {
                        message = "Unable to contact payment provider.";
                        Logging.LogMessage(message, Logging.Components.Payment.ToString(), TraceLevel.Error);
                    }

                    LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, "Transaction failed", message);
                    if (!string.IsNullOrEmpty(response.CustomerProfileId) && !string.IsNullOrEmpty(response.CustomerPaymentProfileId))
                        DeleteSavedCCDetails(response.CustomerProfileId, response.CustomerPaymentProfileId);

                    return new GatewayResponseModel { HasError = true, ErrorMessage = message };
                }
            }
            catch (Exception ex)
            {
                LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, ex.Message);
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                return new GatewayResponseModel { HasError = true, ErrorMessage = ex.Message };
            }
        }

        /// <summary>
        /// To create customer profile using Payment API
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel GetCustomerResponse(PaymentModel paymentModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(paymentModel.PaymentCode))
                {
                    PaymentSettingsService service = new PaymentSettingsService();
                    paymentModel.PaymentApplicationSettingId = service.GetPaymentSettingIdByCode(paymentModel.PaymentCode);
                }
                return ProcessCreditCard(paymentModel);
            }
            catch (Exception ex)
            {
                LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, ex.Message);
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);

                GatewayResponseModel errorModel = new GatewayResponseModel();
                errorModel.IsSuccess = false;
                errorModel.ResponseText = ex.Message;

                return errorModel;
            }
        }

        /// <summary>
        /// Execute and Get Capture status from gateway provider
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public BooleanModel GetCaptureResponse(string token)
        {
            TransactionService repository = new TransactionService();
            PaymentModel paymentModel = new PaymentModel { TransactionId = token, IsCapture = true };

            if (!string.IsNullOrEmpty(token))
            {
                ZnodeTransaction transactionDetails = repository.GetPayment(token);
                if (!Equals(transactionDetails, null))
                {
                    paymentModel.CustomerProfileId = transactionDetails.CustomerProfileId;
                    paymentModel.CustomerPaymentProfileId = transactionDetails.CustomerPaymentId;
                    paymentModel.PaymentApplicationSettingId = transactionDetails.PaymentSettingId.Value;
                    paymentModel.TransactionId = transactionDetails.TransactionId;
                    paymentModel.Total = (Math.Round(Convert.ToDecimal(transactionDetails.Amount), 2)).ToString();
                    paymentModel.GatewayCurrencyCode = transactionDetails.CurrencyCode;
                    paymentModel.CardDataToken = transactionDetails.Custom1;
                    paymentModel.OrderId = transactionDetails.Custom1;
                    paymentModel.CaptureTransactionId = transactionDetails.CaptureTransactionId;
                    paymentModel.GUID = Convert.ToString(transactionDetails.GUID);
                    GatewayResponseModel response = ProcessCreditCard(paymentModel);

                    if (response.IsSuccess)
                    {
                        paymentModel.CaptureTransactionId = response.TransactionId;
                        paymentModel.PaymentStatusId = (int)response.PaymentStatus;
                        paymentModel.ResponseText = response.GatewayResponseData;
                        paymentModel.ResponseCode = response.ResponseCode;
                        paymentModel.CardDataToken = response.Token;
                        repository.UpdatePayment(paymentModel);
                        LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, "Transaction Captured");
                        return new BooleanModel { IsSuccess = true };
                    }
                    else
                    {
                        string message = response.GatewayResponseData?.Replace("<br>", "");
                        LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, "Transaction Capture failed", message);
                        Logging.LogMessage(message, Logging.Components.Payment.ToString(), TraceLevel.Error);
                        return new BooleanModel { HasError = true, ErrorMessage = message };
                    }
                }
            }
            return new BooleanModel { HasError = true, ErrorMessage = "Invalid Payment Transaction Token" };
        }

        /// <summary>
        /// Get Refund/Void status from gateway provider
        /// </summary>
        /// <param name="token"></param>
        /// <param name="amount"></param>
        /// <param name="isVoid"></param>
        /// <returns></returns>
        public BooleanModel GetRefundVoidResponse(string token, decimal amount, bool isCompleteOrderRefund, bool isVoid)
        {
            TransactionService repository = new TransactionService();

            if (!string.IsNullOrEmpty(token))
            {
                ZnodeTransaction transactionDetails = repository.GetPayment(token);
                if (!Equals(transactionDetails, null))
                {
                    PaymentModel paymentModel = new PaymentModel();
                    paymentModel.CustomerProfileId = transactionDetails.CustomerProfileId;
                    paymentModel.CustomerPaymentProfileId = transactionDetails.CustomerPaymentId;
                    paymentModel.PaymentApplicationSettingId = transactionDetails.PaymentSettingId.Value;
                    paymentModel.TransactionId = transactionDetails.TransactionId;
                    paymentModel.GatewayCurrencyCode = transactionDetails.CurrencyCode;
                    paymentModel.CardDataToken = transactionDetails.Custom1;
                    paymentModel.OrderId = transactionDetails.Custom1;
                    paymentModel.GUID = Convert.ToString(transactionDetails.GUID);  
                    paymentModel.CaptureTransactionId = transactionDetails.CaptureTransactionId;
                    if (!isVoid)
                    {                        
                        paymentModel.Total = isCompleteOrderRefund ? Convert.ToString(transactionDetails.Amount - (transactionDetails.RefundAmount ?? 0m)) : amount.ToString();
                        //check if the refund amount should not be greater than order total and previous refunds.
                        if (CheckRefundAmoutWithOrderTotal(amount, transactionDetails))
                        {
                            string message = "Refund amount exceed the order total";
                            Logging.LogMessage(message, Logging.Components.Payment.ToString(), TraceLevel.Info);
                            return new BooleanModel { HasError = true, ErrorMessage = message };
                        }
                    }

                    GatewayResponseModel response = this.ProcessRefundVoid(paymentModel, isVoid);

                    if (response.IsSuccess)
                    {
                        paymentModel.RefundTransactionId = response.TransactionId;

                        paymentModel.ResponseText = response.GatewayResponseData;
                        paymentModel.ResponseCode = response.ResponseCode;
                        paymentModel.PaymentStatusId = (int)response.PaymentStatus;
                        if (!isVoid)
                            paymentModel.RefundAmount = amount;
                        repository.UpdatePayment(paymentModel);
                        LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, isVoid ? "Void transaction" : "Refund transaction");
                        return new BooleanModel { IsSuccess = true };
                    }
                    else
                    {
                        string message = response.GatewayResponseData?.Replace("<br>", "");
                        LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, isVoid ? "Transaction Void failed" : "Transaction Refund failed", message);
                        Logging.LogMessage(message, Logging.Components.Payment.ToString(), TraceLevel.Error);
                        return new BooleanModel { HasError = true, ErrorMessage = message };
                    }
                }
            }
            return new BooleanModel { HasError = true, ErrorMessage = "Invalid Payment Transaction Token" };
        }

        /// <summary>
        /// Get the subscription response(Subscription Created/Failed)
        /// </summary>
        /// <param name="paymentModel">Model of payment</param>
        /// <returns>Response in the form of string</returns>
        public string GetSubscriptionResponse(PaymentModel paymentModel)
        {
            TransactionService repository = new TransactionService();
            foreach (SubscriptionModel subscriptionModel in paymentModel.Subscriptions)
            {
                paymentModel.Subscription = subscriptionModel;
                GatewayResponseModel response = this.ProcessSubscription(paymentModel);
                if (response.IsSuccess)
                {
                    paymentModel.TransactionId = response.TransactionId;

                    repository.UpdatePayment(paymentModel);
                    LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, "Subscription Created");
                    // return "Success";
                }
                else
                {
                    string message = response.GatewayResponseData.Replace("<br>", "");
                    LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, "Subscription failed", message);
                    Logging.LogMessage(message, Logging.Components.Payment.ToString(), TraceLevel.Error);
                    // return string.Format("Message={0}", message);
                }

            }
            return "";
        }

        /// <summary>
        /// Execute and Get Capture status from gateway provider
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public GatewayResponseModel GetPaypalResponse(PaymentModel paymentModel)
        {
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            paymentModel = GetPaymentSettingsModel(paymentModel);
            TransactionService repository = new TransactionService();
            Log4NetHelper.ReplaceLog4NetDLL(Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.PAYPALEXPRESS.ToString())).ToString());
            var paypal = new Znode.Libraries.Paypal.PaypalGateway(paymentModel);

            paypal.PaymentActionTypeCode = "Sale";

            Znode.Libraries.Paypal.PaypalResponse response = paypal.DoPaypalExpressCheckout(paymentModel);

            if (!Equals(response.ResponseCode, "0"))//  return response.ResponseText;
            {
                gatewayResponse.IsSuccess = false;
                gatewayResponse.ResponseText = response.ResponseText;
                gatewayResponse.ResponseCode = response.ResponseCode;
                gatewayResponse.PaymentToken = response.PayalToken;
            }
            else//  return response.HostUrl;// Redirect to paypal server
            {
                gatewayResponse.IsSuccess = true;
                gatewayResponse.ResponseText = response.HostUrl;
                gatewayResponse.ResponseCode = response.ResponseCode;
                gatewayResponse.PaymentToken = response.PayalToken;
            }
            return gatewayResponse;
        }

        public GatewayResponseModel GetFinalizedPaypalResponse(PaymentModel paymentModel)
        {
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            paymentModel = GetPaymentSettingsModel(paymentModel);
            TransactionService repository = new TransactionService();
            Log4NetHelper.ReplaceLog4NetDLL(Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.PAYPALEXPRESS.ToString())).ToString());
            var paypal = new Znode.Libraries.Paypal.PaypalGateway(paymentModel);

            paypal.PaymentActionTypeCode = "Sale";
            LoggingService.LogActivity(paymentModel?.PaymentApplicationSettingId, $"{paymentModel?.PaymentApplicationSettingId} GetFinalizedPaypalResponse called"); Znode.Libraries.Paypal.PaypalResponse response = new Znode.Libraries.Paypal.PaypalResponse();

            response = paypal.GetExpressCheckoutDetails(paymentModel?.PaymentToken.ToString());
            gatewayResponse = paypal.DoExpressCheckoutPayment(paymentModel?.PaymentToken.ToString(), response.PayerID);
            if ((gatewayResponse?.IsSuccess).GetValueOrDefault())
            {
                paymentModel.TransactionId = gatewayResponse.TransactionId;
                repository.AddPayment(paymentModel);
            }
            LoggingService.LogActivity(paymentModel?.PaymentApplicationSettingId, $"{paymentModel?.PaymentApplicationSettingId} after GetFinalizedPaypalResponse called");
            return gatewayResponse;
        }

        /// <summary>
        /// To save customer details in payment database
        /// </summary>
        /// <param name="paymentModel">Model of payment</param>
        /// <returns> returns true/false</returns>
        public bool SaveCustomerDetails(PaymentModel paymentModel)
        {
            bool isSuccess = true;
            try
            {
                //Save cc details in vault.
                if (string.IsNullOrEmpty(paymentModel.CustomerGUID) && string.IsNullOrEmpty(paymentModel.PaymentToken))
                {
                    if (AddCustomer(paymentModel) && AddAddress(paymentModel) && AddPayment(paymentModel))
                    {
                        paymentModel.CustomerProfileId = paymentModel.CustomerProfileId;
                        paymentModel.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId; ;
                    }
                    else
                        paymentModel.ResponseText = "Unable to create customer please try again.";
                }
                else if (!string.IsNullOrEmpty(paymentModel.CustomerGUID) && string.IsNullOrEmpty(paymentModel.PaymentToken))
                {
                    //Save data in ZnodePaymentAddress and ZnodePaymentMethod
                    if (AddAddress(paymentModel) && AddPayment(paymentModel))
                        paymentModel.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                    paymentModel.PaymentToken = paymentModel.PaymentToken;

                }
                else if (!string.IsNullOrEmpty(paymentModel.CustomerGUID) && !string.IsNullOrEmpty(paymentModel.PaymentToken))
                {
                    //Get CustomerProfileId & token from ZnodePaymentMethods table having GUID = CustomerGUID and paymentsettingId = PaymentSettingId
                    PaymentMethodsService repository = new PaymentMethodsService();
                    ZnodePaymentMethod payment = repository.GetPaymentMethod(paymentModel.PaymentApplicationSettingId, paymentModel.CustomerGUID, paymentModel.PaymentToken);
                    if (!Equals(payment, null))
                    {
                        paymentModel.CustomerProfileId = payment.CustomerProfileId;
                        paymentModel.CustomerPaymentProfileId = payment.Token;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, ex.Message);
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                isSuccess = false;
            }
            return isSuccess;
        }

        /// <summary>
        /// To save payment details in payment database for anonymousUser
        /// </summary>
        /// <param name="paymentModel">Model of payment</param>
        /// <returns>Boolean Value true/false</returns>
        public bool SavePaymentDetails(PaymentModel paymentModel)
        {
            bool isSuccess = true;
            try
            {
                if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId) && !string.IsNullOrEmpty(paymentModel.CustomerPaymentProfileId))
                {
                    if (AddPayment(paymentModel))
                    {
                        paymentModel.CustomerProfileId = paymentModel.CustomerProfileId;
                        paymentModel.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                    }
                    else
                        paymentModel.ResponseText = "Unable to create customer please try again.";
                }
            }
            catch (Exception ex)
            {
                LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, ex.Message);
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                isSuccess = false;
            }
            return isSuccess;
        }

        public TransactionDetailsModel GetTransactionDetails(string transactionId)
        {
            TransactionService repository = new TransactionService();
            PaymentModel paymentModel = new PaymentModel();
            if (!string.IsNullOrEmpty(transactionId))
            {
                var transactionDetails = repository.GetPayment(transactionId);
                if (!Equals(transactionDetails, null))
                {
                    //PaymentModel paymentModel = new PaymentModel();
                    paymentModel.CustomerProfileId = transactionDetails.CustomerProfileId;
                    paymentModel.CustomerPaymentProfileId = transactionDetails.CustomerPaymentId;
                    paymentModel.PaymentApplicationSettingId = transactionDetails.PaymentSettingId.Value;
                    paymentModel.TransactionId = transactionDetails.TransactionId;
                    paymentModel.GatewayCurrencyCode = transactionDetails.CurrencyCode;
                    paymentModel.CardDataToken = transactionDetails.Custom1;
                    paymentModel.OrderId = transactionDetails.Custom1;
                    paymentModel.GUID = Convert.ToString(transactionDetails.GUID);
                }
            }
            IPaymentProviders _provider = GetPaymentProviderObject(paymentModel);
            if (!Equals(_provider, null))
                return _provider.GetTransactionDetails(paymentModel);

            return null;
        }
        #endregion

        #region Private Methods
        //Get Payment Settings details from DB and assigned to Payment Model.
        private PaymentModel GetPaymentSettingsModel(PaymentModel paymentModel)
        {
            PaymentSettingsService paymentrepository = new PaymentSettingsService();
            if (!Equals(paymentModel, null))
            {
                PaymentSettingsModel paymentSetting = !string.IsNullOrEmpty(paymentModel.PaymentCode) ? paymentrepository.GetPaymentSettingWithCredentials(paymentModel.PaymentCode) : paymentrepository.GetPaymentSettingWithCredentials(paymentModel.PaymentApplicationSettingId);
                if (!Equals(paymentSetting, null))
                {
                    paymentModel.GatewayTestMode = paymentSetting.TestMode;
                    paymentModel.GatewayPreAuthorize = paymentSetting.PreAuthorize;
                    paymentModel.GatewayLoginName = paymentSetting.GatewayUsername;
                    paymentModel.GatewayLoginPassword = paymentSetting.GatewayPassword;
                    paymentModel.GatewayTransactionKey = paymentSetting.TransactionKey;
                    paymentModel.GatewayPreAuthorize = paymentSetting.PreAuthorize;
                    paymentModel.GatewayType = Equals(paymentSetting.PaymentGatewayId, null) ? string.Empty : paymentSetting.PaymentGatewayId.Value.ToString();
                    paymentModel.Vendor = paymentSetting.Vendor;
                    paymentModel.Partner = paymentSetting.Partner;
                    paymentModel.PaymentApplicationSettingId = paymentSetting.PaymentSettingId;

                    if (paymentSetting.PaymentTypeId == 5)
                        paymentModel.RefundTransactionId = paymentrepository.GetAmazonUpdateTrnasactionId(paymentModel.TransactionId);
                }
            }
            else
            {
                paymentModel = new PaymentModel();
            }
            return paymentModel;
        }

        //Process Credit card based on Gateway type.
        private GatewayResponseModel ProcessCreditCard(PaymentModel paymentModel)
        {
            IPaymentProviders _provider = GetPaymentProviderObject(paymentModel);
            if (!Equals(_provider, null))
                return _provider.ValidateCreditcard(paymentModel);
            return null;
        }

        //Process Refund/Void status based on Gateway type.
        private GatewayResponseModel ProcessRefundVoid(PaymentModel paymentModel, bool isVoid = false)
        {
            IPaymentProviders _provider = GetPaymentProviderObject(paymentModel);
            if (!Equals(_provider, null))
                return isVoid ? _provider.Void(paymentModel) : _provider.Refund(paymentModel);

            return null;
        }

        //Process the subscription based on Gateway.
        private GatewayResponseModel ProcessSubscription(PaymentModel paymentModel)
        {
            IPaymentProviders _provider = GetPaymentProviderObject(paymentModel);
            if (!Equals(_provider, null))
                return _provider.Subscription(paymentModel);

            return null;
        }

        private IPaymentProviders GetPaymentProviderObject(PaymentModel paymentModel)
        {
            paymentModel = GetPaymentSettingsModel(paymentModel);

            if (!string.IsNullOrEmpty(paymentModel.GatewayType))
            {
                IPaymentProviders _provider = GetProvider(GetGatewayClassName(Convert.ToInt32(paymentModel.GatewayType)));
                Log4NetHelper.ReplaceLog4NetDLL(paymentModel.GatewayType);
                if (!Equals(_provider, null))
                    return _provider;
            }
            else
            {
                IPaymentProviders _provider = GetProvider(GetPaymentTypeClassName(Convert.ToInt32(paymentModel.PaymentApplicationSettingId)));
                if (!Equals(_provider, null))
                    return _provider;
            }
            return null;
        }

        //Add new customer to PaymentCustomers table and return customer guid back in model.
        private bool AddCustomer(PaymentModel model)
        {
            PaymentCustomersService repository = new PaymentCustomersService();
            SetCardHolderName(model);
            string customerId = repository.AddPaymentCustomers(model);
            if (!string.IsNullOrEmpty(customerId))
            {
                model.CustomerGUID = customerId;
                return true;
            }
            return false;
        }

        //Add new customer to PaymentAddress table and return addressId guid back in model.
        private bool AddAddress(PaymentModel model)
        {
            PaymentAddressService repository = new PaymentAddressService();
            SetCardHolderName(model);
            string addressId = repository.AddPaymentAddress(model);
            if (!string.IsNullOrEmpty(addressId))
            {
                model.AddressId = addressId;
                return true;
            }
            return false;
        }

        //Add new payment  to PaymentMethod table and return PaymentGUID guid back in model.
        private bool AddPayment(PaymentModel model)
        {
            model.CreditCardLastFourDigit = model.CardNumber.Substring(model.CardNumber.Length - 4, 4);
            PaymentMethodsService repository = new PaymentMethodsService();
            string paymentToken = repository.AddPaymentMethods(model);
            if (!string.IsNullOrEmpty(paymentToken))
            {
                model.PaymentToken = paymentToken;
                return true;
            }
            return false;
        }

        //Set Card Holder First Name & Last Name in payment model from cardholder name field.
        private void SetCardHolderName(PaymentModel model)
        {
            if (!string.IsNullOrEmpty(model.CardHolderName))
            {
                string[] cardHolderName = model.CardHolderName.Split(' ');
                model.CardHolderFirstName = cardHolderName[0].Trim();
                if (cardHolderName.Length > 1)
                    model.CardHolderLastName = cardHolderName[1].Trim();
            }
        }

        //Delete PaymentMethod and PaymentAddress if transaction fails and data is saved in PaymentMethod and PaymentAddress table.
        private void DeleteSavedCCDetails(string customerProfileId, string CustomerPaymentProfileId)
        {
            if (!string.IsNullOrEmpty(customerProfileId) && !string.IsNullOrEmpty(CustomerPaymentProfileId))
            {
                TransactionService transactionRepository = new TransactionService();
                PaymentMethodsService paymentMethodsRepository = new PaymentMethodsService();
                if (!transactionRepository.IsTransactionPresent(customerProfileId, CustomerPaymentProfileId))
                    paymentMethodsRepository.DeletePaymentMethods(customerProfileId, CustomerPaymentProfileId);

                Logging.LogMessage("Saved card details deleted.", Logging.Components.Payment.ToString(), TraceLevel.Info);
            }
        }


        //Check the Refund amount should be smaller than Order total plus previous refund amount
        private bool CheckRefundAmoutWithOrderTotal(decimal amount, ZnodeTransaction transactionDetails)
        {
            bool isGreater = false;
            decimal dbRefundAmount = transactionDetails.RefundAmount.HasValue ? transactionDetails.RefundAmount.Value : 0.0M;

            if ((dbRefundAmount + amount) > transactionDetails.Amount.Value)
                isGreater = true;

            return isGreater;
        }

        #endregion

        #region Amazon Pay

        /// <summary>
        /// Execute and Get Capture status from gateway provider
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public GatewayResponseModel GetAmazonPayAddress(PaymentModel paymentModel)
        {
            GatewayResponseModel gatewayResponse = new GatewayResponseModel { PaymentModel = new PaymentModel() };
            paymentModel = GetPaymentSettingsModel(paymentModel);
            IPaymentProviders _provider = new AmazonPayProvider();
            GatewayResponseModel response = _provider.Subscription(paymentModel);

            if (response.HasError)
            {
                gatewayResponse.IsSuccess = false;
                gatewayResponse.ResponseText = response.ResponseText;
                gatewayResponse.ResponseCode = response.ResponseCode;
                gatewayResponse.PaymentToken = response.PaymentToken;
            }
            else
            {
                gatewayResponse.IsSuccess = true;
                gatewayResponse.ResponseText = response.HostUrl;
                gatewayResponse.ResponseCode = response.ResponseCode;
            }
            gatewayResponse.PaymentModel = response.PaymentModel;
            return gatewayResponse;
        }
        #endregion
    }
}
