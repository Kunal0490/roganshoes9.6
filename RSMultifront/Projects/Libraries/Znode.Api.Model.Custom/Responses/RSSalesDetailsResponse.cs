﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models.Responses;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Sample.Api.Model.Responses
{
    public class RSSalesDetailsResponse : BaseResponse
    {
        public List<RSSalesDetailsModel> SalesDetails { get; set; }
    }
}
