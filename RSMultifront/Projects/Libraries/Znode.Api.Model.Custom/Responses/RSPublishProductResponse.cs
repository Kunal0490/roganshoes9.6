﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
   public class RSPublishProductResponse : PublishProductResponse
    {
        public DataTable StoreLocations { get; set; }
    }
}
