﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.RSCustomizedFormModel
{
    public class RSSalesDetailsModel : BaseModel
    {
        public string SEOUrl { get; set; }
        public string SKU { get; set; }
        public string Brand { get; set; }
        public int ProductId { get; set; }
    }
}
