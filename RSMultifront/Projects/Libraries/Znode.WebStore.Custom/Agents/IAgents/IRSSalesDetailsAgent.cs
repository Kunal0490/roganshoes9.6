﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IRSSalesDetailsAgent
    {
        List<RSSalesDetailsViewModel> GetSalesDetails();
    }
}
