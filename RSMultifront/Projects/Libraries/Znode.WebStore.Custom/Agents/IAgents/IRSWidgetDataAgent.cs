﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.ViewModels;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IRSWidgetDataAgent
    {
        /// <summary>
        /// Get sub total of cart.
        /// </summary>
        /// <returns>Sub total of cart.</returns>

        CartViewModel GetCart();
        List<RSCategoryViewModel> GetSubCategories(WidgetParameter widgetparameter);
        string GetDefaultStoreDetailsFromCookie();
    }
}
