﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;


namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSWidgetDataAgent : WidgetDataAgent, IRSWidgetDataAgent
    {
        private readonly IShoppingCartClient _shoppingCartsClient;
        private readonly ICartAgent _cartAgent;
        public RSWidgetDataAgent(IWebStoreWidgetClient widgetClient, IPublishProductClient productClient, IPublishCategoryClient publishCategoryClient, IBlogNewsClient blogNewsClient, IContentPageClient contentPageClient, ISearchClient searchClient, ICMSPageSearchClient cmsPageSearchClient) :
            base( widgetClient, productClient,publishCategoryClient,blogNewsClient, contentPageClient,searchClient,cmsPageSearchClient)
        {
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _shoppingCartsClient = GetClient<ShoppingCartClient>();
        }

        public CartViewModel GetCart()
        {
            //Get Shopping cart from session or cookie.
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                        _cartAgent.GetCartFromCookie();

            if (shoppingCartModel!=null)
            {
                return new CartViewModel()
                {
                    HasError = true,
                    ErrorMessage = WebStore_Resources.OutofStockMessage
                };
            }

            if (shoppingCartModel?.ShoppingCartItems?.Count == 0)
                return new CartViewModel();

            //Remove Shipping and Tax calculation From Cart.

            //Calculate cart
            shoppingCartModel = CalculateCart(shoppingCartModel);

            //Set currency details.
            return SetCartCurrency(shoppingCartModel);
        }

        //Set currecy details for shopping cart and cart items.
        private CartViewModel SetCartCurrency(ShoppingCartModel cartModel)
        {
            if (cartModel != null)
            {
                CartViewModel cartViewModel = cartModel.ToViewModel<CartViewModel>();
                cartViewModel.ShoppingCartItems?.ForEach(item => item.GroupProducts.ForEach(y => y.Quantity = (y.Quantity)));
                string currencyCode = PortalAgent.CurrentPortal.CurrencyCode;
                cartViewModel.CurrencyCode = currencyCode;
                cartViewModel.ShoppingCartItems.Select(x => { x.CurrencyCode = currencyCode; return x; })?.ToList();
                return cartViewModel;
            }
            return new CartViewModel();
        }
        //Calculate cart.
        private ShoppingCartModel CalculateCart(ShoppingCartModel shoppingCartModel)
        {
            if (shoppingCartModel != null)
            {
                shoppingCartModel.LocaleId = PortalAgent.LocaleId;
                string billingEmail = shoppingCartModel.BillingEmail;
                int shippingId = shoppingCartModel.ShippingId;
                int billingAddressId = shoppingCartModel.BillingAddressId;
                int shippingAddressId = shoppingCartModel.ShippingAddressId;
                int selectedAccountUserId = shoppingCartModel.SelectedAccountUserId;
                shoppingCartModel = _shoppingCartsClient.Calculate(shoppingCartModel);
                //Bind required data to ShoppingCartModel.
                BindShoppingCartData(shoppingCartModel, billingEmail, shippingId, shippingAddressId, billingAddressId, selectedAccountUserId);
            }

            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            return shoppingCartModel;
        }
        //Bind required data to ShoppingCartModel.
        private void BindShoppingCartData(ShoppingCartModel shoppingCartModel, string billingEmail, int shippingId, int shippingAddressId, int billingAddressId, int selectedAccountUserId)
        {
            shoppingCartModel.BillingEmail = billingEmail;
            shoppingCartModel.ShippingId = shippingId;
            shoppingCartModel.ShippingAddressId = shippingAddressId;
            shoppingCartModel.BillingAddressId = billingAddressId;
            shoppingCartModel.SelectedAccountUserId = selectedAccountUserId;
        }
        //Get sub categories of selected category
        public virtual new List<RSCategoryViewModel> GetSubCategories(WidgetParameter widgetparameter)
        {
            return new List<RSCategoryViewModel>();
        }
            //    // ZnodeLogging.LogMessage("RSWIDGETDATAAGENT GetDefaultStoreDetailsFromCookie() GetSubCategories called", "WIDGET", TraceLevel.Error);
            //    IPublishCategoryClient _categoryClient = new PublishCategoryClient();
            //    //Sorting For Category List.
            //    SortCollection sorts = new SortCollection();
            //    sorts.Add(SortKeys.DisplayOrder, SortDirections.Ascending);
            //    List<RSCategoryViewModel> rsCategoryList = new List<RSCategoryViewModel>();
            //    ZnodeLogging.LogMessage("RSWIDGETDATAAGENT _categoryClient.GetPublishCategoryList begins at" + DateTime.Now, "WIDGET", TraceLevel.Error);
            //    PublishCategoryListModel model = _categoryClient.GetPublishCategoryList(new ExpandCollection { ZnodeConstant.SEO }, GetFilterForSubCategory(widgetparameter), null);
            //    ZnodeLogging.LogMessage("RSWIDGETDATAAGENT model?.PublishCategories?.Count()=" + model?.PublishCategories?.Count(), "WIDGET", TraceLevel.Error);
            //    if (model?.PublishCategories?.Count > 0)
            //    {
            //        ZnodeLogging.LogMessage("RSWIDGETDATAAGENT foreach start time=" + DateTime.Now, "WIDGET", TraceLevel.Error);
            //        foreach (PublishCategoryModel pcm in model.PublishCategories)
            //        {
            //            RSCategoryViewModel rcvm = new RSCategoryViewModel();
            //            rcvm.CategoryId = pcm.PublishCategoryId;
            //            rcvm.CategoryName = pcm.Name;
            //            rcvm.SEOUrl = pcm.SEODetails?.SEOUrl;
            //            rcvm.ProductIdCount = pcm.ProductIds.Count();
            //            rsCategoryList.Add(rcvm);
            //        }
            //        model.PublishCategories.ForEach(d =>
            //        {
            //            RSCategoryViewModel rcvm = new RSCategoryViewModel();
            //            rcvm.CategoryId = d.PublishCategoryId;
            //            rcvm.CategoryName = d.Name;
            //            rcvm.SEOUrl = d.SEODetails?.SEOUrl;
            //            rcvm.ProductIdCount = d.ProductIds.Count();
            //            rsCategoryList.Add(rcvm);
            //        });

            //        ZnodeLogging.LogMessage("RSWIDGETDATAAGENT foreach end time=" + DateTime.Now, "WIDGET", TraceLevel.Error);
            //    }
            //    if (rsCategoryList.Count > 0 && widgetparameter.WidgetKey == "123")
            //    {
            //        int ProductCount = rsCategoryList.Sum(x => x.ProductIds.Count());
            //        rsCategoryList.RemoveRange(1, rsCategoryList.Count() - 1);
            //        rsCategoryList[0].CategoryName = widgetparameter.properties["CategoryName"].ToString();
            //        rsCategoryList[0].CategoryId = Convert.ToInt32(widgetparameter.properties["CategoryId"]);
            //        rsCategoryList[0].SEOUrl = widgetparameter.properties["SEO"].ToString();
            //        rsCategoryList[0].ProductIdCount = ProductCount;

            //        rsCategoryList[0].ParentCategoryName = widgetparameter.properties["PCategoryName"].ToString();
            //        rsCategoryList[0].ParentCategoryId = Convert.ToInt32(widgetparameter.properties["CategoryId"]);
            //        rsCategoryList[0].ParentSEODetails = widgetparameter.properties["SEO"].ToString();
            //        widgetparameter.CMSMappingId = Convert.ToInt32(widgetparameter.properties["PCategoryId"]);
            //        widgetparameter.DisplayName = widgetparameter.properties["PCategoryName"].ToString();
            //        PublishCategoryListModel Parentmodel = _categoryClient.GetPublishCategoryList(new ExpandCollection { ZnodeConstant.SEO }, GetFilterForSubCategory(widgetparameter), null);
            //        List<RSCategoryViewModel> ParentCategoryList;
            //        ParentCategoryList = Parentmodel?.PublishCategories?.Count > 0 ? Parentmodel.PublishCategories.ToViewModel<RSCategoryViewModel>().ToList() : new List<RSCategoryViewModel>();
            //        rsCategoryList[0].ParentProductIdCount = ParentCategoryList.Sum(x => x.ProductIds.Count());

            //    }
            //    //ZnodeLogging.LogMessage("RSWIDGETDATAAGENT rsCategoryList.Count="++""+, "WIDGET", TraceLevel.Error);
            //    if (rsCategoryList.Count > 0 && widgetparameter.WidgetKey == "122")
            //    {
            //        rsCategoryList[0].ParentCategoryName = widgetparameter.properties["CategoryName"].ToString();
            //        rsCategoryList[0].ParentCategoryId = Convert.ToInt32(widgetparameter.properties["CategoryId"]);
            //        rsCategoryList[0].ParentSEODetails = Convert.ToString(widgetparameter.properties["SEO"]);
            //        rsCategoryList[0].ParentProductIdCount = rsCategoryList.Sum(x => x.ProductIdCount);
            //    }
            //    return rsCategoryList;
            //}
            ////Get Filter For SubCategory.
            //private FilterCollection GetFilterForSubCategory(WidgetParameter widgetparameter)
            //{
            //    FilterCollection filters = GetRequiredFilters();
            //    filters.Add(WebStoreEnum.ZnodeParentCategoryIds.ToString(), FilterOperators.Contains, widgetparameter.CMSMappingId.ToString());
            //    return filters;
            //}

            public string GetDefaultStoreDetailsFromCookie()
        {
            string defaultStore = GetFromCookie("DefaultSelectedStore");
            //ZnodeLogging.LogMessage("RSWIDGETDATAAGENT GetDefaultStoreDetailsFromCookie() GetFromCookie-" + defaultStore, "WIDGET", TraceLevel.Error);
            string[] storeValues = defaultStore.Split('*');
            string storeName = "";
            if (storeValues.Length > 1)
            {
                storeName = storeValues[1];
            }
            if (storeName == "")
                storeName = "Locate A Store";
            return storeName;
        }

        public override  ProductListViewModel GetCategoryProducts(WidgetParameter widgetparameter, int pageNum = 1, int pageSize = 40, int sortValue = 0)
        {
            /*NIVI CODE*/
            if (pageSize == 16)
                pageSize = 40;

            if ((Convert.ToInt32(widgetparameter.properties[WebStoreConstants.PageSize])) == 16)
                widgetparameter.properties[WebStoreConstants.PageSize] = 40;

            //  ZnodeLogging.LogMessage("RSWIDGETDATAAGENT base.GetCategoryProducts Begins at" + DateTime.Now, "PLP", TraceLevel.Error);
            ProductListViewModel productlist = base.GetCategoryProducts(widgetparameter, pageNum, pageSize, sortValue);
           // ZnodeLogging.LogMessage("RSWIDGETDATAAGENT base.GetCategoryProducts Ends at" + DateTime.Now, "PLP", TraceLevel.Error);
            return productlist;
        }
    }
}
