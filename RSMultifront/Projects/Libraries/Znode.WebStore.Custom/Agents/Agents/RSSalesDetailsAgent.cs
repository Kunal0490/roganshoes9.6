﻿using System.Collections.Generic;
using System.Linq;
using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.WebStore;
using Znode.Sample.Api.Model.RSCustomizedFormModel;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSSalesDetailsAgent : BaseAgent, IRSSalesDetailsAgent
    {
        private readonly IRSSalesDetailsClient _RSSalesDetailsClient;
        public RSSalesDetailsAgent() {
            _RSSalesDetailsClient = new RSSalesDetailsClient();
        }

        public virtual List<RSSalesDetailsViewModel> GetSalesDetails()
        {
            List<RSSalesDetailsModel> salesDetails = _RSSalesDetailsClient.GetSalesDetails();
            List<RSSalesDetailsViewModel> list = salesDetails.ToViewModel<RSSalesDetailsViewModel>().ToList();
            return list;
        }
        
    }
}
