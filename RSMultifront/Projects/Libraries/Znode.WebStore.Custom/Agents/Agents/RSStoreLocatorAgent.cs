﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSStoreLocatorAgent : StoreLocatorAgent, IRSStoreLocatorAgent
    {
        double cookiesexpiretime = Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]);
        #region Public Constructor
        public RSStoreLocatorAgent(IWebStoreLocatorClient webStoreLocatorClient) : base(webStoreLocatorClient)
        {

        }
        #endregion

        public void SaveInCookie(string storeId, string storeName, string storeAddress)
        {
            RemoveCookie("DefaultSelectedStore");
            string defaultSelectedStoreDetails = storeId + "*" + storeName + "*" + storeAddress;
            SaveInCookie("DefaultSelectedStore", defaultSelectedStoreDetails, cookiesexpiretime);
        }

        public string GetCookie()
        {
            string _cookie = GetFromCookie("DefaultSelectedStore");
            return _cookie;
        }
    }
}
