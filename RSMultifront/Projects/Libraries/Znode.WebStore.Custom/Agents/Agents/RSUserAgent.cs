﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSUserAgent : UserAgent, IUserAgent, IRSUserAgent
    {
        private readonly IOrderClient _orderClient;
        private readonly IUserClient _userClient;
        private readonly IShippingClient _shippingClient;
        public RSUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient,
          IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :
            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient,
                orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            _orderClient = GetClient<IOrderClient>(orderClient);
            _userClient = GetClient<IUserClient>(userClient);
            _shippingClient = GetClient<IShippingClient>(shippingClient);
        }

        public override LoginViewModel Login(LoginViewModel model)
        {
            LoginViewModel loginViewModel;
            double cookiesexpiretime = Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]);
            try
            {
                model.PortalId = PortalAgent.CurrentPortal?.PortalId;
                //Authenticate the user credentials.
                UserModel userModel = _userClient.Login(UserViewModelMap.ToLoginModel(model), GetExpands());
                if (HelperUtility.IsNotNull(userModel))
                {
                    //Check of Reset password Condition.
                    if (HelperUtility.IsNotNull(userModel.User) && !string.IsNullOrEmpty(userModel.User.PasswordToken))
                    {
                        loginViewModel = UserViewModelMap.ToLoginViewModel(userModel);
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword, true, loginViewModel);
                    }
                    // Check user associated profiles.                
                    if (!userModel.Profiles.Any() && !userModel.IsAdminUser)
                    {
                        return ReturnErrorModel(WebStore_Resources.ProfileLoginFailedErrorMessage);
                    }
                    SetLoginUserProfile(userModel);
                    //Save the User Details in Session.
                    SaveInSession(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                    /*NIVI CODE START: TO SAVE DEFAULT STORE FOR USER*/

                    string defaultStore = GetFromCookie("DefaultSelectedStore");
                    string[] storeValues = defaultStore.Split('*');
                    if (storeValues.Length > 1)
                    {
                        userModel.Custom1 = storeValues[0];
                        userModel.Custom2 = storeValues[1];
                        userModel.Custom3 = storeValues[2];
                        this.UpdateProfile(userModel.ToViewModel<UserViewModel>(), true);
                        SaveInSession(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                    }
                    else
                    {
                        string defaultstorestring = userModel.Custom1 + "*" + userModel.Custom2 + "*" + userModel.Custom3;
                        RemoveCookie("DefaultSelectedStore");
                        SaveInCookie("DefaultSelectedStore", defaultstorestring, cookiesexpiretime);

                    }
                    HttpContext.Current.Session["FirstName"] = userModel.FirstName;
                    /*NIVI CODE END: TO SAVE DEFAULT STORE FOR USER*/
                    return UserViewModelMap.ToLoginViewModel(userModel);
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case 2://Error Code to Reset the Password for the first time login.
                        return ReturnErrorModel(ex.ErrorMessage, true);
                    case ErrorCodes.AccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorAccountLocked);
                    case ErrorCodes.LoginFailed:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                    case ErrorCodes.TwoAttemptsToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorTwoAttemptsRemain);
                    case ErrorCodes.OneAttemptToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorOneAttemptRemain);
                    default:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
            }
            return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
        }

        private LoginViewModel ReturnErrorModel(string errorMessage, bool hasResetPassword = false, LoginViewModel model = null)
        {
            if (HelperUtility.IsNull(model))
            {
                model = new LoginViewModel();
            }

            //Set Model Properties.
            model.HasError = true;
            model.IsResetPassword = hasResetPassword;
            model.ErrorMessage = string.IsNullOrEmpty(errorMessage) ? WebStore_Resources.InvalidUserNamePassword : errorMessage;
            return model;
        }

        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.GiftCardHistory,
                };
        }

        public override UserViewModel UpdateProfile(UserViewModel model, bool webStoreUser)
        {
            UserViewModel userViewModel = GetUserViewModelFromSession();

            if (HelperUtility.IsNotNull(userViewModel))
            {
                MapUserProfileData(model, userViewModel);
            }
            else
            {
                userViewModel = model;
            }

            try
            {
                _userClient.UpdateUserAccountData(userViewModel.ToModel<UserModel>(), webStoreUser);
                SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                userViewModel.SuccessMessage = WebStore_Resources.SuccessProfileUpdated;
                Znode.Engine.WebStore.Helper.ClearCache($"UserAccountAddressList{userViewModel.UserId}");
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
            }
            return userViewModel;
        }

        private static void MapUserProfileData(UserViewModel model, UserViewModel userViewModel)
        {
            userViewModel.Email = model.Email;
            userViewModel.EmailOptIn = model.EmailOptIn;
            userViewModel.FirstName = model.FirstName;
            userViewModel.LastName = model.LastName;
            userViewModel.PhoneNumber = model.PhoneNumber;
            userViewModel.UserName = model.UserName;
            userViewModel.ExternalId = model.ExternalId;
            userViewModel.Custom1 = model.Custom1;
            userViewModel.Custom2 = model.Custom2;
            userViewModel.Custom3 = model.Custom3;
            userViewModel.Custom4 = model.Custom4;
            userViewModel.Custom5 = model.Custom5;
        }

        public string GetDefaultStoreDetailsFromCookie()
        {
            string defaultStore = GetFromCookie("DefaultSelectedStore");
            return defaultStore;
        }

        public void SetDefaultStoreDetailsInCookie()
        {
            string defaultStore = GetFromCookie("DefaultSelectedStore");

        }

        //Get order details to generate the order reciept.
        /*Nivi Code- Overridden just to call SetTrackingURL method which is Private
         In which we need to set Speedy Tracking URL*/
        public override OrdersViewModel GetOrderDetails(int orderId, int portalId = 0)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());

            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (HelperUtility.IsNotNull(userViewModel) && orderId > 0)
            {
                OrderModel orderModel = portalId > 0 ? GetOrderBasedOnPortalId(orderId, portalId) : _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);

               if (orderModel.OmsOrderId > 0 || userViewModel.UserId == orderModel.UserId || ((orderModel.IsQuoteOrder || userViewModel?.AccountId.GetValueOrDefault() > 0)))
                {
                    List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

                    //Create new order line item model.
                    CreateSingleOrderLineItem(orderModel, orderLineItemListModel);

                    orderModel.OrderLineItems = orderLineItemListModel;
                    /*Here*/
                    orderModel.TrackingNumber = SetTrackingURL(orderModel);
                    if (orderModel?.OrderLineItems?.Count > 0)
                    {
                        OrdersViewModel orderDetails = orderModel?.ToViewModel<OrdersViewModel>();
                        orderDetails.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                        orderDetails.CouponCode = orderDetails.CouponCode?.Replace("<br/>", ", ");
                        orderDetails?.OrderLineItems?.ForEach(x => x.UOM = orderModel?.ShoppingCartModel?.ShoppingCartItems?.Where(y => y.SKU == x.Sku).Select(y => y.UOM).FirstOrDefault());
                        /*Here*/
                        orderDetails?.OrderLineItems?.ForEach(x => x.TrackingNumber = SetTrackingUrl(x.TrackingNumber, orderModel.ShippingId));
                        return orderDetails;
                    }
                }
                return null;
            }
            return new OrdersViewModel();
        }

        //private OrderModel GetOrderBasedOnPortalId(int orderId, int portalId)
        //{
        //    FilterCollection filters = new FilterCollection();
        //    filters.Add(new FilterTuple(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, portalId.ToString()));
        //    return _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);
        //}
        ////Set Expands For Order Receipt.
        //private static ExpandCollection SetExpandsForReceipt()
        //{
        //    ExpandCollection expands = new ExpandCollection();
        //    expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString());
        //    expands.Add(ExpandKeys.Store);
        //    expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString());
        //    expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString());
        //    expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString());
        //    expands.Add(ExpandKeys.ZnodeShipping);
        //    expands.Add(ExpandKeys.IsFromOrderReceipt);
        //    expands.Add(ExpandKeys.PortalTrackingPixel);
        //    expands.Add(ExpandKeys.IsWebStoreOrderReciept);
        //    return expands;
        //}
        private string SetTrackingUrl(string trackingNo, int shippingId)
        {
            string trackingUrl = GetTrackingUrlByShippingId(shippingId);
            return string.IsNullOrEmpty(trackingUrl) ? trackingNo : "<a target=_blank href=" + GetTrackingUrlByShippingId(shippingId) + trackingNo + ">" + trackingNo + "</a>";
        }

        private string GetTrackingUrlByShippingId(int shippingId)
            => _shippingClient.GetShipping(shippingId)?.TrackingUrl;
        /*nivi code start*/
        private string SetTrackingURL(OrderModel ordermodel)
        {
            string TrackingNumbers = "";
            try
            {
                //Get shipping type name based on provided shipping id
                string SPEEDY = Convert.ToString(ConfigurationManager.AppSettings["SPEEDYTrackingUrl"]);

                string[] shipping = ordermodel.Custom1?.Split(',');
                string[] TrackNumbers = ordermodel.TrackingNumber?.Split(',');
                //string shippingType = GetShippingType(ordermodel.ShippingId);
                int icount = 0;
                if (TrackNumbers?.Count() > 0)
                {
                    foreach (string shippingname in shipping)
                    {
                        if (ZnodeConstant.UPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.UPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.FedEx == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.FedExTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.USPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.USPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if ("SPEEDEE" == shippingname)
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ SPEEDY + TrackNumbers[icount]}>{TrackNumbers[icount]} </a >" + ",";
                        icount = icount + 1;
                    }
                    if (TrackingNumbers != "")
                    {
                        TrackingNumbers = TrackingNumbers.Remove(TrackingNumbers.Length - 1, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Shipping=" + Convert.ToString(ordermodel.Custom1) + " TrackingNo=" + ordermodel.TrackingNumber + "error =" + ex.Message + " stacktrace =" + ex.StackTrace, string.Empty, TraceLevel.Error);
            }
            return TrackingNumbers;
        }
        /*nivi code end*/
    }
}
