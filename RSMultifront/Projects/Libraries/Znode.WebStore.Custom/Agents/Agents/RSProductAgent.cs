﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Engine.WebStore;
using Znode.Libraries.Resources;
using Znode.Engine.Api.Client.Expands;
using Znode.WebStore.Custom.Helper;
using Znode.Engine.WebStore.Models;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSProductAgent : ProductAgent, IProductAgent
    {
        #region Private Variables
        private readonly IPublishProductClient _productClient;
        private readonly IPublishCategoryClient _publishCategoryClient;

        #endregion

        #region Public Constructor
        public RSProductAgent(ICustomerReviewClient reviewClient, IPublishProductClient productClient, IWebStoreProductClient webstoreProductClient, ISearchClient searchClient, IHighlightClient highlightClient, IPublishCategoryClient publishCategoryClient):
            base(reviewClient,productClient,webstoreProductClient,searchClient,highlightClient,publishCategoryClient)
        {            
            _productClient = GetClient<IPublishProductClient>(productClient);
            _publishCategoryClient = GetClient<IPublishCategoryClient>(publishCategoryClient);
        }
        #endregion

        //public override ProductViewModel GetConfigurableProduct(ParameterProductModel model)
        //{
        //    try
        //    {              
        //        ProductViewModel viewModel = base.GetConfigurableProduct(model);            
        //        if (viewModel != null)
        //        {
        //            string[] codes = model.Codes.Split(',');
        //            int colorIndex = Array.IndexOf(codes, "Color");
        //            string selectedcolor = model.Values.Split(',')?[colorIndex];
        //            int sizeIndex = Array.IndexOf(codes, "Size");
        //            string selectedsize = "";
        //            if(sizeIndex!=-1)
        //            selectedsize = model.Values.Split(',')?[sizeIndex];
        //            if (viewModel.ConfigurableData.SwatchImages == null)
        //                viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

        //            List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();

        //            foreach (string color in _colors)
        //            {

        //                SwatchImageViewModel svm = new SwatchImageViewModel();
        //                svm.AttributeCode = "Color";
        //                svm.AttributeValues = color;
        //                svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
        //                viewModel.ConfigurableData.SwatchImages.Add(svm);

        //                SwatchImageViewModel colorsize = new SwatchImageViewModel();
        //                colorsize.AttributeCode = "ColorSize";
        //                List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
        //                colorsize.AttributeValues = color + "-";
        //                colorsize.Custom1 = color;
        //                colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });

        //                List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == selectedcolor && Convert.ToString(x.Custom1) == selectedsize).Select(o => o.Custom2).Distinct().ToList();
        //                selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });

        //                viewModel.ConfigurableData.SwatchImages.Add(colorsize);
        //            }
        //            //TO BE UNCOMMENTED FOR PICKUP/SHIP
        //            viewModel = SetDefaultStoreAddressDetails(viewModel);
        //        }

        //        return viewModel;
        //    }
        //    catch(Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("RSProductAgent-Error-" + ex.Message + "- Stack Trace - " + ex.StackTrace, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
        //        return new ProductViewModel();
        //    }
        //}

        //public override ProductViewModel GetProduct(int productID)
        //{           
        //    ProductViewModel viewModel = base.GetProduct(productID);
        //     if (viewModel != null)
        //    {
        //        string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size").AttributeValues;
        //        string colorval = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Color").SelectedAttributeValue[0];

        //        if (viewModel?.ConfigurableData?.SwatchImages == null)
        //            viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

        //        List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
        //        List<object> _colorSizeWidthList = viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList();
        //        foreach (string color in _colors)
        //        {                   
        //            SwatchImageViewModel svm = new SwatchImageViewModel();
        //            svm.AttributeCode = "Color";
        //            svm.AttributeValues = color;
        //            svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
        //            viewModel.ConfigurableData.SwatchImages.Add(svm);

        //            SwatchImageViewModel colorsize = new SwatchImageViewModel();
        //            colorsize.AttributeCode = "ColorSize";
        //            List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
        //            colorsize.AttributeValues = color + "-";
        //            colorsize.Custom1 = color;                   
        //            colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });
        //            List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == colorval && Convert.ToString(x.Custom1)==size).Select(o => o.Custom2).Distinct().ToList();
        //            selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });
        //            viewModel.ConfigurableData.SwatchImages.Add(colorsize);
        //        }
        //        //TO BE UNCOMMENTED FOR PICKUP/SHIP
        //        viewModel = SetDefaultStoreAddressDetails(viewModel);

        //        viewModel.ShowAddToCart = true;
        //        viewModel.InventoryMessage = "";
        //        viewModel.Custom4 = _colorSizeWidthList;
        //        viewModel.Custom5 = "FROMPLP";
        //    }            
        //    return viewModel;
        //}

        public override ProductViewModel GetConfigurableProduct(ParameterProductModel model)
        {
            try
            {
                ProductViewModel viewModel = base.GetConfigurableProduct(model);
                if (viewModel != null)
                {
                    string[] codes = model.Codes.Split(',');
                    int colorIndex = Array.IndexOf(codes, "Color");
                    string selectedcolor = model.Values.Split(',')?[colorIndex];
                    int sizeIndex = Array.IndexOf(codes, "Size");
                    string selectedsize = "";
                    if (sizeIndex > 0)
                        selectedsize = model.Values.Split(',')?[sizeIndex];
                    if (viewModel.ConfigurableData.SwatchImages == null)
                        viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

                    List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
                    List<string> _colorSizeWidthList = viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList();
                  
                    foreach (string color in _colors)
                    {

                        SwatchImageViewModel svm = new SwatchImageViewModel();
                        svm.AttributeCode = "Color";
                        svm.AttributeValues = color;
                        svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
                        /*Custom3 - GalleryImages*/
                        svm.Custom3 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorCode;

                        viewModel.ConfigurableData.SwatchImages.Add(svm);

                        SwatchImageViewModel colorsize = new SwatchImageViewModel();
                        colorsize.AttributeCode = "ColorSize";
                        List<string> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
                        colorsize.AttributeValues = color + "-";
                        colorsize.Custom1 = color;
                        colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });

                        List<string> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == selectedcolor && Convert.ToString(x.Custom1) == selectedsize).Select(o => o.Custom2).Distinct().ToList();
                        selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });

                        viewModel.ConfigurableData.SwatchImages.Add(colorsize);
                    }
                    //TO BE UNCOMMENTED FOR PICKUP/SHIP
                    viewModel = SetDefaultStoreAddressDetails(viewModel);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("RSProductAgent-Error-" + ex.Message + "- Stack Trace - " + ex.StackTrace, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                return new ProductViewModel();
            }
        }
        //private void GetConfigurableValues(ProductViewModel viewModel)
        //{
        //    viewModel.ConfigurableData = new ConfigurableAttributeViewModel();
        //    //Select Is Configurable Attributes list
        //    //viewModel.ConfigurableData.ConfigurableAttributes = viewModel.Attributes.Where(x => x.IsConfigurable && x.ConfigurableAttribute?.Count > 0).ToList();
        //    viewModel.ConfigurableData.ConfigurableAttributes = viewModel.Attributes.Where(x => x.IsConfigurable).ToList();
        //    //Assign select attribute values.
        //    //viewModel.ConfigurableData.ConfigurableAttributes.ForEach(x => x.SelectedAttributeValue = new[] {x.ConfigurableAttribute?.FirstOrDefault()?.SelectValues[0].Code });
        //    viewModel.ConfigurableData.ConfigurableAttributes.ForEach(x => x.SelectedAttributeValue = new[] { x.AttributeValues });
        //}

        public override ProductViewModel GetProduct(int productID)
        {
            ProductViewModel viewModel = base.GetProduct(productID);
            if (viewModel != null)
            {
               // AttributesViewModel cnt = viewModel.Attributes?.Find(x => x.ConfigurableAttribute.Count > 0);
                //if (cnt == null)
                //    GetConfigurableValues(viewModel);
                //string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size").SelectedAttributeValue[0];
                string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size").AttributeValues;
                string colorval = "";
                try
                {
                    colorval = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Color").SelectedAttributeValue[0];
                }
                catch(Exception ex)
                {
                    
                }
                if (viewModel?.ConfigurableData?.SwatchImages == null)
                    viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

                List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
                //List<string> _colorSizeWidthList = viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList();
                //_colorSizeWidthList.Add(",");
                string _colorSizeWidthList = string.Join(",", viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList());
                _colorSizeWidthList = _colorSizeWidthList + ",";
                foreach (string color in _colors)
                {
                    SwatchImageViewModel svm = new SwatchImageViewModel();
                    svm.AttributeCode = "Color";
                    svm.AttributeValues = color;
                    svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
                    /*Custom3 - GalleryImages*/
                    svm.Custom2 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorSwatchText;
                    svm.Custom3 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorCode;
                    viewModel.ConfigurableData.SwatchImages.Add(svm);

                    SwatchImageViewModel colorsize = new SwatchImageViewModel();
                    colorsize.AttributeCode = "ColorSize";
                    List<string> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
                    colorsize.AttributeValues = color + "-";
                    colorsize.Custom1 = color;
                    colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });
                    colorsize.AttributeValues = colorsize.AttributeValues + ",";
                    List<string> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == colorval && Convert.ToString(x.Custom1) == size).Select(o => o.Custom2).Distinct().ToList();
                    selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });
                    colorsize.Custom2 = colorsize.Custom2 + ",";
                    viewModel.ConfigurableData.SwatchImages.Add(colorsize);
                }
                //TO BE UNCOMMENTED FOR PICKUP/SHIP
                viewModel = SetDefaultStoreAddressDetails(viewModel);
                viewModel.ShowAddToCart = true;
                viewModel.InventoryMessage = "";
                viewModel.Custom4 = _colorSizeWidthList;
                viewModel.Custom5 = "FROMPLP";
            }
            return viewModel;
        }
        private ProductViewModel SetDefaultStoreAddressDetails(ProductViewModel viewModel)
        {
            try
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length == 3)
                {
                    viewModel.Custom1 = storeValues[0];
                    viewModel.Custom2 = storeValues[1];
                    viewModel.Custom3 = storeValues[2];
                }
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            }
            return viewModel;
        }

        private ShortProductViewModel SetDefaultStoreAddressDetails(ShortProductViewModel viewModel)
        {
            try
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length == 3)
                {
                    viewModel.Custom1 = storeValues[0];
                    viewModel.Custom2 = storeValues[1];
                    viewModel.Custom3 = storeValues[2];
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            }
            return viewModel;
        }

       

        public override ProductReviewViewModel GetProductForReview(int productID, string productName, decimal? rating)
        {
            _productClient.SetProfileIdExplicitly(RSWebstoreHelper.GetProfileId().GetValueOrDefault());
            PublishProductModel model = _productClient.GetPublishProduct(productID, GetRequiredFilters(), new ExpandCollection { ExpandKeys.SEO });
            if (HelperUtility.IsNotNull(model))
            {
                /*nivi code start: check null condition*/
                if(GetFromSession<List<RecentViewModel>>(ZnodeConstant.RecentViewProduct)!=null)
                { 
               // Updated rating added againest that product.
                List<RecentViewModel> recentViewProductCookie = GetFromSession<List<RecentViewModel>>(ZnodeConstant.RecentViewProduct);
                foreach (RecentViewModel recentViewModel in recentViewProductCookie.Where(x => x.PublishProductId == productID))
                {
                    recentViewModel.Rating = Convert.ToDecimal(rating);
                }
                SaveInSession(ZnodeConstant.RecentViewProduct, recentViewProductCookie);
                }
                /*nivi code end*/
                ProductReviewViewModel viewModel = model.ToViewModel<ProductReviewViewModel>();
                viewModel.PublishProductId = model.ConfigurableProductId > 0 ? model.ConfigurableProductId : model.PublishProductId;
                return viewModel;
            }
            else
                return new ProductReviewViewModel { PublishProductId = productID, ProductName = productName };
        }
        /*Nivi Code- Instead of LinkHomeIcon we require LinkTextHome*/
        public override string GetBreadCrumb(int categoryId, string[] productAssociatedCategoryIds, bool checkFromSession)
        {
            if (checkFromSession)
            {
                categoryId = GetFromSession<int>(string.Format(WebStoreConstants.LastSelectedCategoryForPortal, PortalAgent.CurrentPortal.PortalId));
                if ((!productAssociatedCategoryIds?.Contains(categoryId.ToString())).GetValueOrDefault())
                    categoryId = Convert.ToInt32(productAssociatedCategoryIds[0]);
                else if (HelperUtility.IsNull(productAssociatedCategoryIds))
                    /*here*/
                    return $"<a href='/'>{WebStore_Resources.LinkTextHome}</a>";
            }
            FilterCollection filters = GetRequiredFilters();
            filters.Add(WebStoreEnum.IsGetParentCategory.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add(WebStoreEnum.IsBindImage.ToString(), FilterOperators.Equals, ZnodeConstant.FalseValue);
            CategoryViewModel category = _publishCategoryClient.GetPublishCategory(categoryId, filters, new ExpandCollection { ZnodeConstant.SEO })?.ToViewModel<CategoryViewModel>();
            /*here*/
            return $"<a href='/'>{WebStore_Resources.LinkTextHome}</a> / {GetBreadCrumbHtml(category)}";
        }

        private string GetBreadCrumbHtml(CategoryViewModel category, bool isParent = false)
        {
            if (HelperUtility.IsNotNull(category))
            {
                string breadCrumb = $"<a href='/{(string.IsNullOrEmpty(category.SEODetails?.SEOUrl) ? "category/" + category.CategoryId : category.SEODetails.SEOUrl)}'>{category.CategoryName}</a>";
                if (category.ParentCategory?.Count > 0)
                    breadCrumb = GetBreadCrumbHtml(category.ParentCategory[0], true) + " / " + breadCrumb;
                return breadCrumb;
            }
            return string.Empty;
        }

        /*Nivi Code- To show Call for pricing products Price when admin comes through Impersonation*/
        protected override void GetProductFinalPrice(ProductViewModel viewModel, List<AddOnViewModel> addOns, decimal minQuantity, string addOnIds)
        {
            if (!viewModel.IsCallForPricing || (viewModel.IsCallForPricing && SessionHelper.GetDataFromSession<ImpersonationModel>(WebStoreConstants.ImpersonationSessionKey) != null))
            {
                viewModel.UnitPrice = viewModel.SalesPrice > 0 ? viewModel.SalesPrice : viewModel.RetailPrice;
                //Apply tier price if any.
                if (viewModel.TierPriceList?.Count > 0 && viewModel.TierPriceList.Where(x => minQuantity >= x.MinQuantity)?.Count() > 0)
                    viewModel.ProductPrice = viewModel.TierPriceList.FirstOrDefault(x => minQuantity >= x.MinQuantity && minQuantity < x.MaxQuantity)?.Price;
                else
                    viewModel.ProductPrice = (minQuantity > 0 && HelperUtility.IsNotNull(viewModel.SalesPrice)) ? viewModel.SalesPrice * minQuantity : viewModel.PromotionalPrice > 0 ? viewModel.PromotionalPrice * minQuantity : viewModel.RetailPrice;

                if (addOns?.Count > 0)
                {
                    decimal? addonPrice = 0.00M;

                    if (!string.IsNullOrEmpty(addOnIds))
                    {
                        foreach (string addOn in addOnIds.Split(','))
                        {
                            AddOnValuesViewModel addOnValue = addOns.SelectMany(
                                       y => y.AddOnValues.Where(x => x.SKU == addOn))?.FirstOrDefault();
                            if (HelperUtility.IsNotNull(addOnValue))
                                addonPrice = addonPrice + (HelperUtility.IsNotNull(addOnValue.SalesPrice) ? addOnValue.SalesPrice : addOnValue.RetailPrice);

                        }
                    }
                    viewModel.ProductPrice = addonPrice > 0 ? viewModel.ProductPrice + addonPrice : viewModel.ProductPrice;

                    //Check add on price.
                    if (HelperUtility.IsNull(addonPrice))
                    {
                        viewModel.ShowAddToCart = false;
                        viewModel.InventoryMessage = Convert.ToBoolean(viewModel?.Attributes?.Value(ZnodeConstant.CallForPricing)) ? string.Empty : WebStore_Resources.ErrorAddOnPrice;
                    }
                }
                //Check product final price.
                if (HelperUtility.IsNull(viewModel.ProductPrice) && (!Equals(viewModel.ProductType, ZnodeConstant.GroupedProduct)))
                {
                    viewModel.ShowAddToCart = false;
                    viewModel.InventoryMessage = Convert.ToBoolean(viewModel?.Attributes?.Value(ZnodeConstant.CallForPricing)) ? string.Empty : WebStore_Resources.ErrorPriceNotAssociate;
                }
            }
            else
                viewModel.ShowAddToCart = false;
        }
    }
}
