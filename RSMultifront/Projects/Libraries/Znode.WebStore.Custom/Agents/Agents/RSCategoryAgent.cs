﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Agents.Agents
{
   public class RSCategoryAgent : CategoryAgent,ICategoryAgent
    {
        private readonly IWebStoreCategoryClient _categoryClient;
        private readonly IPublishCategoryClient _publishCategoryClient;
        private readonly IPublishBrandClient _brandClient;

        public RSCategoryAgent(IWebStoreCategoryClient categoryClient, IPublishCategoryClient publishCategoryClient, IPublishBrandClient brandClient) : base (categoryClient, publishCategoryClient, brandClient)
        {
            _categoryClient = GetClient<IWebStoreCategoryClient>(categoryClient);
            _publishCategoryClient = GetClient<IPublishCategoryClient>(publishCategoryClient);
            _brandClient = GetClient<IPublishBrandClient>(brandClient);
        }
        public override string GetBreadCrumb(int categoryId)
        {
            FilterCollection filters = GetRequiredFilters();
            filters.Add(WebStoreEnum.IsGetParentCategory.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add(WebStoreEnum.IsBindImage.ToString(), FilterOperators.Equals, ZnodeConstant.FalseValue);
            PublishCategoryModel category = _publishCategoryClient.GetPublishCategory(categoryId, filters, new ExpandCollection { ZnodeConstant.SEO });

            if (HelperUtility.IsNull(category))
            {
                throw new ZnodeException(ErrorCodes.NotFound, WebStore_Resources.ErrorCategoryNotFound);
            }

            CategoryViewModel categoryViewModel = category?.ToViewModel<CategoryViewModel>();

            return $"<a href='/'>{WebStore_Resources.LinkTextHome}</a> / {GetBreadCrumbHtml(categoryViewModel)}";
        }
        private string GetBreadCrumbHtml(CategoryViewModel category, bool isParentCategory = false)
        {
            if (HelperUtility.IsNotNull(category))
            {
                string breadCrumb = string.Empty;

                breadCrumb = (isParentCategory)
                        ? $"<a href='/{(string.IsNullOrEmpty(category.SEODetails?.SEOUrl) ? "category/" + category.CategoryId : category.SEODetails.SEOUrl)}'>{category.CategoryName}</a>"
                        : category.CategoryName;

                if (category?.ParentCategory?.Count > 0)
                {
                    breadCrumb = GetBreadCrumbHtml(category.ParentCategory[0], true) + " / " + breadCrumb;
                }

                return breadCrumb;
            }
            return string.Empty;
        }
    }
}
