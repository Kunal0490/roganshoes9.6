﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Helper;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;



namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSCheckoutAgent : CheckoutAgent
    {
        #region Private Variables
        private readonly IUserAgent _userAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IPortalClient _portalClient;
        private readonly IShoppingCartClient _shoppingCartClient;
        private readonly IOrderClient _orderClient;
        private readonly IShippingClient _shippingsClient;
        private const string FedEx_Ground = "FEDEX_GROUND";

        #endregion

        public RSCheckoutAgent(IShippingClient shippingsClient, IPaymentClient paymentClient, IPortalProfileClient profileClient, ICustomerClient customerClient, IUserClient userClient, IOrderClient orderClient, IAccountClient accountClient, IWebStoreUserClient webStoreAccountClient, IPortalClient portalClient, IShoppingCartClient shoppingCartClient, IAddressClient addressClient) :
            base(shippingsClient, paymentClient, profileClient, customerClient, userClient, orderClient, accountClient, webStoreAccountClient, portalClient, shoppingCartClient, addressClient)
        {
            _shippingsClient = GetClient<IShippingClient>(shippingsClient);
            _userAgent = new UserAgent(GetClient<CountryClient>(), GetClient<WebStoreUserClient>(), GetClient<WishLishClient>(), GetClient<UserClient>(), GetClient<PublishProductClient>(), GetClient<CustomerReviewClient>(), GetClient<OrderClient>(), GetClient<GiftCardClient>(), GetClient<AccountClient>(), GetClient<AccountQuoteClient>(), GetClient<OrderStateClient>(), GetClient<PortalCountryClient>(), GetClient<ShippingClient>(), GetClient<PaymentClient>(), GetClient<CustomerClient>(), GetClient<StateClient>(), GetClient<PortalProfileClient>());
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _portalClient = GetClient<IPortalClient>(portalClient);
            _shoppingCartClient = GetClient<IShoppingCartClient>(shoppingCartClient);
            _orderClient = GetClient<IOrderClient>(orderClient);
        }
        #region Pickup/Ship
       
      

        public override OrdersViewModel SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            //Get cart from session or by cookie.
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                      _cartAgent.GetCartFromCookie();
            //cartModel.Custom1 = (submitOrderViewModel.Custom1 != null) ? ((string[])submitOrderViewModel.Custom1)[0] : null;
            //cartModel.Custom2 = (submitOrderViewModel.Custom2 != null) ? ((string[])submitOrderViewModel.Custom2)[0] : null;
            //cartModel.Custom3 = (submitOrderViewModel.Custom3 != null) ? ((string[])submitOrderViewModel.Custom3)[0] : null;
            //cartModel.Custom5 = (submitOrderViewModel.Custom5 != null) ? ((string[])submitOrderViewModel.Custom5)[0] : null;
            cartModel.Custom1 = (submitOrderViewModel.Custom1 != null) ? submitOrderViewModel.Custom1 : null;
            cartModel.Custom2 = (submitOrderViewModel.Custom2 != null) ? submitOrderViewModel.Custom2 : null;
            cartModel.Custom3 = (submitOrderViewModel.Custom3 != null) ? submitOrderViewModel.Custom3 : null;
            cartModel.Custom5 = (submitOrderViewModel.Custom5 != null) ? submitOrderViewModel.Custom5 : null;
            // Set shoppingcart details like shipping.payment setting, etc.
            OrdersViewModel ordersViewModel = CheckInventoryAndMinMaxQuantity(cartModel);
            if (!ordersViewModel.IsInventoryAndMinMaxQuantityAvailable)
            {
                return ordersViewModel;
            }
            OrdersViewModel ovm = base.SubmitOrder(submitOrderViewModel);
            /*NIVI CODE TO SAVE DEFAULT STORE FOR USER*/
            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            UserViewModel user = userViewModel ?? GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey);
            string defaultStore = GetFromCookie("DefaultSelectedStore");
            string[] storeValues = defaultStore.Split('*');
            if (storeValues.Length > 1)
            {
                if (userViewModel != null && userViewModel.Custom1 != storeValues[0])
                {
                    userViewModel.Custom1 = storeValues[0];
                    userViewModel.Custom2 = storeValues[1];
                    userViewModel.Custom3 = storeValues[2];
                    _userAgent.UpdateProfile(userViewModel, true);
                }
            }
            /*NIVI CODE TO SAVE DEFAULT STORE FOR USER*/
            return ovm;
          

        }
        #endregion
        /*THIS METHOD IS OVERRIDEN JUST TO CALL GetShippingListAndRates METHOD Defined in this class*/
        public override ShippingOptionListViewModel GetShippingOptions(string shippingTypeName = null, bool isQuote = false)
        {
            bool isB2BUser = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.AccountId > 0;

            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                         _cartAgent.GetCartFromCookie();

            if (IsNotNull(cartModel) && !string.IsNullOrEmpty(shippingTypeName))
            {
                SetShippingTypeNameToModel(shippingTypeName, cartModel);
            }

            int omsQuoteId = (cartModel?.OmsQuoteId).GetValueOrDefault();

            string customerServicePhone = _portalClient.GetPortal(cartModel.PortalId, null).CustomerServicePhoneNumber;

            //Get address associated to the cart, If it is not available then get address from user address book.
            AddressListViewModel addressList = GetCartAddressList(cartModel);
            if (addressList?.ShippingAddress == null || addressList?.ShippingAddress?.AddressId == 0)
            {
                addressList = _userAgent.GetAddressList();
            }

            if (!IsValidShippingAddress(addressList))
            {
                return new ShippingOptionListViewModel() { IsB2BUser = isB2BUser, OmsQuoteId = omsQuoteId };
            }

            List<ShippingOptionViewModel> shippingOptions;
            try
            {
                cartModel.BillingAddress = addressList?.BillingAddress?.ToModel<AddressModel>();
                cartModel.Payment = new PaymentModel { ShippingAddress = addressList?.ShippingAddress?.ToModel<AddressModel>() };
                /*NIVI OVERRIDE JUST FOR THIS*/
                shippingOptions = GetShippingListAndRates(addressList?.ShippingAddress?.PostalCode, cartModel)?.ShippingOptions;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                shippingOptions = new List<ShippingOptionViewModel>();
            }

            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            if (shoppingCart?.ShoppingCartItems?.Count > 0)
            {
                shippingOptions.Where(x => x.ShippingId == shoppingCart.ShippingId)?.Select(y => { y.IsSelected = true; shoppingCart.Shipping.ShippingId = y.ShippingId; return y; }).FirstOrDefault();
                if (shoppingCart.ShoppingCartItems.Any(x => x.Quantity > 500 && shippingOptions?.Count() == 0))
                {
                    SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, shoppingCart);
                    return new ShippingOptionListViewModel() { ShippingOptions = shippingOptions, IsB2BUser = isB2BUser, OmsQuoteId = omsQuoteId, ErrorMessage = Admin_Resources.ErrorShippingExceeded, HasError = true };
                }
            }
            SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, shoppingCart);
            return new ShippingOptionListViewModel() { ShippingOptions = shippingOptions, IsB2BUser = isB2BUser, OmsQuoteId = omsQuoteId };
        }

        #region Private Methods

        private void SetShippingTypeNameToModel(string shippingTypeName, ShoppingCartModel cartModel)
        {
            if (IsNull(cartModel?.Shipping))
            {
                cartModel.Shipping = new OrderShippingModel();
            }

            cartModel.Shipping.ShippingTypeName = shippingTypeName;
        }

        private bool IsValidShippingAddress(AddressListViewModel addressList)
        {
            bool isValid = true;
            string shippingCountryCode = addressList?.ShippingAddress?.CountryName ?? string.Empty;
            string shippingstateCode = addressList?.ShippingAddress?.StateName ?? string.Empty;
            string shippingZipCode = addressList?.ShippingAddress?.PostalCode ?? string.Empty;

            //if user shipping CountryCode, state and zipcode is null then no shipping option will available for that user address
            if (string.IsNullOrEmpty(shippingCountryCode) || string.IsNullOrEmpty(shippingstateCode) || string.IsNullOrEmpty(shippingZipCode))
            {
                isValid = false;
            }
            return isValid;
        }

        private ShippingOptionListViewModel GetShippingListAndRates(string postalCode, ShoppingCartModel cartModel)
        {
            /*Original Code*/
            //    string zipCode = postalCode;
            //    ShippingOptionListViewModel listViewModel = new ShippingOptionListViewModel { ShippingOptions = _shoppingCartClient.GetShippingEstimates(zipCode, cartModel)?.ShippingList?.ToViewModel<ShippingOptionViewModel>()?.ToList() };
            //    string currencyCode = PortalAgent.CurrentPortal.CurrencyCode;
            //    listViewModel?.ShippingOptions?.ToList().ForEach(x => x.FormattedShippingRate = HelperMethods.FormatPriceWithCurrency(x.ShippingRate, currencyCode));
            //return listViewModel?.ShippingOptions?.Count > 0 ? listViewModel : new ShippingOptionListViewModel() { ShippingOptions = new List<ShippingOptionViewModel>() };


            string zipCode = postalCode;
            ShippingOptionListViewModel listViewModel = null;
            string currencyCode = PortalAgent.CurrentPortal.CultureCode;
            List<ShoppingCartItemModel> _pickupinstoreItems = cartModel.ShoppingCartItems?.Where(x => x.Custom1 != string.Empty && x.Custom1 != null).ToList<ShoppingCartItemModel>();
            // int? _pickUpItemsCount = _pickupinstoreItems?.Count;

            /*CASE1- All Items in cart are Pick Up In store Items*/
            if (_pickupinstoreItems != null && _pickupinstoreItems.Count == cartModel.ShoppingCartItems.Count)
            {
                //listViewModel=  new ShippingOptionListViewModel() { ShippingOptions = new List<ShippingOptionViewModel>() };
                listViewModel = new ShippingOptionListViewModel { ShippingOptions = _shoppingCartClient.GetShippingEstimates("ALLPICK" + zipCode, cartModel)?.ShippingList?.ToViewModel<ShippingOptionViewModel>()?.ToList() };
                if (listViewModel?.ShippingOptions?.Count > 1)
                {
                    listViewModel?.ShippingOptions?.RemoveAll(x => x.ShippingCode == "FLAT");
                }

                listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).ToList().ForEach(cc => cc.ShippingRate = 0);
                cartModel.ShippingCost = 0;
                cartModel.FreeShipping = true;

                listViewModel?.ShippingOptions?.ToList().ForEach(x => x.FormattedShippingRate = HelperMethods.FormatPriceWithCurrency(x.ShippingRate, currencyCode));
                listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).ToList().ForEach(cc => cc.FormattedShippingRate = cc.FormattedShippingRate + "(Free)");
                return listViewModel?.ShippingOptions?.Count > 0 ? listViewModel : new ShippingOptionListViewModel() { ShippingOptions = new List<ShippingOptionViewModel>() };
                //return listViewModel;
            }


            /*CASE2- If cart have both Pick Up In store and Ship Items Shipping rates should be calculated only on Ship Item subtotal*/
            //if (_pickupinstoreItems.Count > 0)
            //{
            //    ShoppingCartModel modifiedCartModel = new ShoppingCartModel();
            //    modifiedCartModel = CopyModelData(modifiedCartModel, cartModel);
            //    modifiedCartModel.ShoppingCartItems = new List<ShoppingCartItemModel>();
            //    List<ShoppingCartItemModel> shipItemsList = cartModel.ShoppingCartItems?.Where(x => x.Custom1 == string.Empty).ToList<ShoppingCartItemModel>();
            //    modifiedCartModel.ShoppingCartItems.AddRange(shipItemsList);
            //    modifiedCartModel.SubTotal = 0;

            //    foreach (ShoppingCartItemModel scim in shipItemsList)
            //    {
            //        modifiedCartModel.SubTotal += scim.Quantity * scim.UnitPrice;
            //    }

            //    listViewModel = new ShippingOptionListViewModel { ShippingOptions = _shoppingCartClient.GetShippingEstimates("PICKSHIP" + zipCode, modifiedCartModel)?.ShippingList?.ToViewModel<ShippingOptionViewModel>()?.ToList() };

            //    decimal? promotionCartSubTotalValue = Convert.ToDecimal(listViewModel?.ShippingOptions.Where(x => x.ShippingCode == "FEDEX_GROUND")?.FirstOrDefault()?.Custom1);
            //    if (cartModel.SubTotal > promotionCartSubTotalValue)
            //    {
            //        listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).ToList().ForEach(cc => cc.ShippingRate = 0);
            //        listViewModel?.ShippingOptions?.ToList().ForEach(x => x.FormattedShippingRate = HelperMethods.FormatPriceWithCurrency(x.ShippingRate, currencyCode));
            //        listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).ToList().ForEach(cc => cc.FormattedShippingRate = cc.FormattedShippingRate + "(Free)");

            //    }
            //    else
            //    {
            //        listViewModel?.ShippingOptions?.ToList().ForEach(x => x.FormattedShippingRate = HelperMethods.FormatPriceWithCurrency(x.ShippingRate, currencyCode));
            //    }
            //}


            /*CASE3-else If cart have only Ship Items Shipping rates should be calculated  on cart subtotal*/
            else
            {
                listViewModel = new ShippingOptionListViewModel { ShippingOptions = _shoppingCartClient.GetShippingEstimates(zipCode, cartModel)?.ShippingList?.ToViewModel<ShippingOptionViewModel>()?.ToList() };

                listViewModel?.ShippingOptions?.ToList().ForEach(x => x.FormattedShippingRate = HelperMethods.FormatPriceWithCurrency(x.ShippingRate, currencyCode));
                ShippingOptionViewModel sOVM = listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).FirstOrDefault();
                if (sOVM != null)
                {
                    decimal? _fedExGroundRate = Convert.ToDecimal(listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).FirstOrDefault().ShippingRate);
                    ZnodeLogging.LogMessage("RSCHECKOUTAGENT:_fedExGroundRate=" +Convert.ToString(_fedExGroundRate)+ "cartModel.SubTotal="+Convert.ToString(cartModel.SubTotal), "Custom", TraceLevel.Info);
                    if (_fedExGroundRate == 0)
                    {
                        listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).ToList().ForEach(cc => cc.ShippingRate = 0);
                        listViewModel?.ShippingOptions?.ToList().ForEach(x => x.FormattedShippingRate = HelperMethods.FormatPriceWithCurrency(x.ShippingRate, currencyCode));
                        listViewModel?.ShippingOptions.Where(x => x.ShippingCode == FedEx_Ground).ToList().ForEach(cc => cc.FormattedShippingRate = cc.FormattedShippingRate + "(Free)");
                    }
                }
            }
            if (listViewModel?.ShippingOptions?.Count > 1)
            {
                listViewModel?.ShippingOptions?.RemoveAll(x => x.ShippingCode == "FLAT");
            }

            return listViewModel?.ShippingOptions?.Count > 0 ? listViewModel : new ShippingOptionListViewModel() { ShippingOptions = new List<ShippingOptionViewModel>() };


        }

        //private ShoppingCartModel CopyModelData(ShoppingCartModel modifiedCartModel, ShoppingCartModel cartModel)
        //{
        //    modifiedCartModel.ActionMode = cartModel.ActionMode;
        //    modifiedCartModel.AdditionalInstructions = cartModel.AdditionalInstructions;
        //    modifiedCartModel.AdditionalNotes = cartModel.AdditionalNotes;
        //    modifiedCartModel.BillingAddress = cartModel.BillingAddress;
        //    modifiedCartModel.BillingAddressId = cartModel.BillingAddressId;
        //    modifiedCartModel.BillingEmail = cartModel.BillingEmail;
        //    modifiedCartModel.CSRDiscountAmount = cartModel.CSRDiscountAmount;
        //    modifiedCartModel.CSRDiscountApplied = cartModel.CSRDiscountApplied;
        //    modifiedCartModel.CSRDiscountDescription = cartModel.CSRDiscountDescription;
        //    modifiedCartModel.CSRDiscountMessage = cartModel.CSRDiscountMessage;
        //    modifiedCartModel.CardType = cartModel.CardType;
        //    modifiedCartModel.CcCardExpiration = cartModel.CcCardExpiration;
        //    modifiedCartModel.CookieMappingId = cartModel.CookieMappingId;
        //    modifiedCartModel.Coupons = cartModel.Coupons;
        //    modifiedCartModel.CreatedBy = cartModel.CreatedBy;
        //    modifiedCartModel.CreatedDate = cartModel.CreatedDate;
        //    modifiedCartModel.CreditCardExpMonth = cartModel.CreditCardExpMonth;
        //    modifiedCartModel.CreditCardExpYear = cartModel.CreditCardExpYear;
        //    modifiedCartModel.CreditCardNumber = cartModel.CreditCardNumber;
        //    modifiedCartModel.CurrencyCode = cartModel.CurrencyCode;
        //    modifiedCartModel.CurrencySuffix = cartModel.CurrencySuffix;
        //    modifiedCartModel.Custom1 = cartModel.Custom1;
        //    modifiedCartModel.Custom2 = cartModel.Custom2;
        //    modifiedCartModel.Custom3 = cartModel.Custom3;
        //    modifiedCartModel.Custom4 = cartModel.Custom4;
        //    modifiedCartModel.Custom5 = cartModel.Custom5;
        //    modifiedCartModel.CustomShippingCost = cartModel.CustomShippingCost;
        //    modifiedCartModel.CustomTaxCost = cartModel.CustomTaxCost;
        //    modifiedCartModel.CustomerServiceEmail = cartModel.CustomerServiceEmail;
        //    modifiedCartModel.Discount = cartModel.Discount;
        //    modifiedCartModel.EstimateShippingCost = cartModel.EstimateShippingCost;
        //    modifiedCartModel.ExternalId = cartModel.ExternalId;
        //    modifiedCartModel.FeedbackUrl = cartModel.FeedbackUrl;
        //    modifiedCartModel.FreeShipping = cartModel.FreeShipping;
        //    modifiedCartModel.GiftCardAmount = cartModel.GiftCardAmount;
        //    modifiedCartModel.GiftCardApplied = cartModel.GiftCardApplied;
        //    modifiedCartModel.GiftCardBalance = cartModel.GiftCardBalance;
        //    modifiedCartModel.GiftCardMessage = cartModel.GiftCardMessage;
        //    modifiedCartModel.GiftCardNumber = cartModel.GiftCardNumber;
        //    modifiedCartModel.GiftCardValid = cartModel.GiftCardValid;
        //    modifiedCartModel.Gst = cartModel.Gst;
        //    modifiedCartModel.Hst = cartModel.Hst;
        //    modifiedCartModel.IsAllowWithOtherPromotionsAndCoupons = cartModel.IsAllowWithOtherPromotionsAndCoupons;
        //    modifiedCartModel.IsCalCulateTaxAndShipping = cartModel.IsCalCulateTaxAndShipping;
        //    modifiedCartModel.IsCchCalculate = cartModel.IsCchCalculate;
        //    modifiedCartModel.IsEmailSend = cartModel.IsEmailSend;
        //    modifiedCartModel.IsGatewayPreAuthorize = cartModel.IsGatewayPreAuthorize;
        //    modifiedCartModel.IsLineItemReturned = cartModel.IsLineItemReturned;
        //    modifiedCartModel.IsMerged = cartModel.IsMerged;
        //    modifiedCartModel.IsParentAutoAddonRemoved = cartModel.IsParentAutoAddonRemoved;
        //    modifiedCartModel.IsPendingPayment = cartModel.IsPendingPayment;
        //    modifiedCartModel.IsQuoteOrder = cartModel.IsQuoteOrder;
        //    modifiedCartModel.IsSplitCart = cartModel.IsSplitCart;
        //    modifiedCartModel.LocaleId = cartModel.LocaleId;
        //    modifiedCartModel.ModifiedBy = cartModel.ModifiedBy;
        //    modifiedCartModel.ModifiedDate = cartModel.ModifiedDate;
        //    modifiedCartModel.MultipleShipToEnabled = cartModel.MultipleShipToEnabled;
        //    modifiedCartModel.OmsOrderDetailsId = cartModel.OmsOrderDetailsId;
        //    modifiedCartModel.OmsOrderId = cartModel.OmsOrderId;
        //    modifiedCartModel.OmsOrderStatusId = cartModel.OmsOrderStatusId;
        //    modifiedCartModel.OmsQuoteId = cartModel.OmsQuoteId;
        //    modifiedCartModel.OrderAttribute = cartModel.OrderAttribute;
        //    modifiedCartModel.OrderDate = cartModel.OrderDate;
        //    modifiedCartModel.OrderLevelDiscount = cartModel.OrderLevelDiscount;
        //    modifiedCartModel.OrderLevelShipping = cartModel.OrderLevelShipping;
        //    modifiedCartModel.OrderLevelTaxes = cartModel.OrderLevelTaxes;
        //    modifiedCartModel.OrderNumber = cartModel.OrderNumber;
        //    modifiedCartModel.OrderShipment = cartModel.OrderShipment;
        //    modifiedCartModel.OrderStatus = cartModel.OrderStatus;
        //    modifiedCartModel.OverDueAmount = cartModel.OverDueAmount;
        //    modifiedCartModel.PODocumentName = cartModel.PODocumentName;
        //    modifiedCartModel.PayerId = cartModel.PayerId;
        //    modifiedCartModel.Payerid = cartModel.Payerid;
        //    modifiedCartModel.Payment = cartModel.Payment;
        //    modifiedCartModel.PortalId = cartModel.PortalId;
        //    modifiedCartModel.ProfileId = cartModel.ProfileId;
        //    modifiedCartModel.Pst = cartModel.Pst;
        //    modifiedCartModel.PublishedCatalogId = cartModel.PublishedCatalogId;
        //    modifiedCartModel.PurchaseOrderNumber = cartModel.PurchaseOrderNumber;
        //    modifiedCartModel.QuotePaymentSettingId = cartModel.QuotePaymentSettingId;
        //    modifiedCartModel.RemoveAutoAddonSKU = cartModel.RemoveAutoAddonSKU;
        //    modifiedCartModel.ReturnItemList = cartModel.ReturnItemList;
        //    modifiedCartModel.SalesTax = cartModel.SalesTax;
        //    modifiedCartModel.SelectedAccountUserId = cartModel.SelectedAccountUserId;
        //    modifiedCartModel.Shipping = cartModel.Shipping;
        //    modifiedCartModel.ShippingAddress = cartModel.ShippingAddress;
        //    modifiedCartModel.ShippingAddressId = cartModel.ShippingAddressId;
        //    modifiedCartModel.ShippingCost = cartModel.ShippingCost;
        //    modifiedCartModel.ShippingDifference = cartModel.ShippingDifference;
        //    modifiedCartModel.ShippingId = cartModel.ShippingId;
        //    //modifiedCartModel.ShoppingCartItems = cartModel.ShoppingCartItems;
        //    //modifiedCartModel.SubTotal = cartModel.SubTotal;
        //    modifiedCartModel.TaxCost = cartModel.TaxCost;
        //    modifiedCartModel.TaxRate = cartModel.TaxRate;
        //    modifiedCartModel.Token = cartModel.Token;
        //    modifiedCartModel.Total = cartModel.Total;
        //    modifiedCartModel.TotalAdditionalCost = cartModel.TotalAdditionalCost;
        //    modifiedCartModel.TransactionId = cartModel.TransactionId;
        //    modifiedCartModel.UserDetails = cartModel.UserDetails;
        //    modifiedCartModel.UserId = cartModel.UserId;
        //    modifiedCartModel.Vat = cartModel.Vat;


        //    return modifiedCartModel;
        //}
        #endregion

        // Check inventory, min and max quantity.
        private OrdersViewModel CheckInventoryAndMinMaxQuantity(ShoppingCartModel cartModel)
        {
            try
            {
                _orderClient.SetProfileIdExplicitly(RSWebstoreHelper.GetProfileId().GetValueOrDefault());
                OrderModel orderModel = _orderClient.CheckInventoryAndMinMaxQuantity(cartModel);
                return new OrdersViewModel() { IsInventoryAndMinMaxQuantityAvailable = true };
            }
            catch (ZnodeException exception)
            {
                ZnodeLogging.LogMessage(exception, string.Empty, TraceLevel.Warning);
                //Set error message according to ErrorCode.
                switch (exception.ErrorCode)
                {
                    case ErrorCodes.ProcessingFailed:
                        return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), WebStore_Resources.ProcessingFailedError);
                    case ErrorCodes.ErrorSendResetPasswordLink:
                        return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel() { }, WebStore_Resources.ErrorOrderEmailNotSend);
                    case ErrorCodes.OutOfStockException:
                        return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel() { }, WebStore_Resources.OutOfStockException);
                    case ErrorCodes.MinAndMaxSelectedQuantityError:
                        return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), exception.ErrorMessage);
                    default:
                        return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), WebStore_Resources.ErrorFailedToCreate);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), WebStore_Resources.ProcessingFailedError);
            }
        }
        public override string GenerateOrderNumber(int portalId)
        {           
            try
            {
                string OrderNumber = string.Empty;
                OrderNumber = GetLastOrderNumberFromTextFile();
                return OrderNumber;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Error GenerateOrderNumber : {ex}", ZnodeLogging.Components.OMS.ToString());
                return base.GenerateOrderNumber(portalId);
            }
        }
        private string GetLastOrderNumberFromTextFile()
        {
            try
            {
                string lastOrderFilePath = HttpContext.Current.Server.MapPath($"{ConfigurationManager.AppSettings["LastRSOrderFilePath"]}");
                string lastOrderId = string.Empty;
                using (StreamReader sr = new StreamReader(lastOrderFilePath))
                {
                    while (!sr.EndOfStream)
                    {
                        lastOrderId = sr.ReadToEnd().ToString(); //read full file text  
                    }
                }
                int omslastOrderId = Convert.ToInt32(lastOrderId) + 1;
                WriteTextStorage(Convert.ToString(omslastOrderId), lastOrderFilePath, Mode.Write);
                return Convert.ToString(lastOrderId);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Error GetLastOrderNumberFromTextFile : {ex}", ZnodeLogging.Components.OMS.ToString());

            }
            return null;
        }
        private static void WriteTextStorage(string fileData, string filePath, Mode fileMode)
        {
            try
            {


                // Create directory if not exists.
                string logFilePath = filePath;
                FileInfo fileInfo = new FileInfo(logFilePath);

                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                // Check file write mode and write content.
                if (Equals(fileMode, Mode.Append))
                {
                    File.AppendAllText(logFilePath, fileData);
                }
                else
                {
                    File.WriteAllText(logFilePath, fileData);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                throw;
            }
        }

        #region Order Receip issue From Base code
        private static ExpandCollection SetExpandsForReceipt()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString());
            expands.Add(ExpandKeys.Store);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString());
            expands.Add(ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.IsFromOrderReceipt);
            expands.Add(ExpandKeys.PortalTrackingPixel);
            expands.Add(ExpandKeys.IsWebStoreOrderReciept);
            return expands;
        }

        private string GetTrackingUrlByShippingId(int shippingId)
        {
            return _shippingsClient.GetShipping(shippingId)?.TrackingUrl;
        }

        //Set tracking url.
        private string SetTrackingUrl(string trackingNo, string trackingUrl)
             => string.IsNullOrEmpty(trackingUrl) ? trackingNo : "<a target=_blank href=" + trackingUrl + trackingNo + ">" + trackingNo + "</a>";
        // Get userId from session.
        private int GetUserUserIdFromSession()
        {
            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            int userId = IsNull(userViewModel) ? 0 : userViewModel.UserId;
            return userId > 0 ? userId : -1;
        }

        public override OrdersViewModel GetOrderViewModel(int omsOrderId)
        {
            OrderModel orderModel = _orderClient.GetOrderById(omsOrderId, SetExpandsForReceipt(), null);

            if (orderModel?.OmsOrderId > 0)
            {
                List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

                //Create new order line item model.
                CreateSingleOrderLineItem(orderModel, orderLineItemListModel);

                orderModel.OrderLineItems = orderLineItemListModel;
            }
            OrdersViewModel viewModel = orderModel?.ToViewModel<OrdersViewModel>();
            int userId = orderModel.IsQuoteOrder ? orderModel.UserId : GetUserUserIdFromSession();

            if (IsNotNull(viewModel))
            {
                UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey) ?? new UserViewModel();
                if (userId <= 0 && userViewModel?.GuestUserId <= 0)
                {
                    userViewModel.GuestUserId = viewModel.UserId;
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                }

                if ((userViewModel.GuestUserId == viewModel.UserId || viewModel.UserId == userId) && viewModel.OrderLineItems?.Count() > 0)
                {
                    //Order Receipt
                    string trackingUrl = GetTrackingUrlByShippingId(orderModel.ShippingId);
                    viewModel.TrackingNumber = SetTrackingUrl(orderModel.TrackingNumber, trackingUrl);
                    viewModel.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                    viewModel.CultureCode = PortalAgent.CurrentPortal?.CultureCode;
                    viewModel.CouponCode = viewModel.CouponCode?.Replace("<br/>", ", ");
                    viewModel?.OrderLineItems?.ForEach(item =>
                    {
                        item.UOM = orderModel?.ShoppingCartModel?.ShoppingCartItems.FirstOrDefault(x => x.SKU == item.Sku)?.UOM;
                        item.TrackingNumber = SetTrackingUrl(item.TrackingNumber, trackingUrl);
                    });

                    int count = 0;
                    StringBuilder cjURL = new StringBuilder();
                    //Append line item sku, quantity and amount to url.
                    foreach (OrderLineItemViewModel orderDetail in viewModel.OrderLineItems)
                    {
                        count++;
                        cjURL.Append($"&ITEM{count}={orderDetail.Sku}");
                        cjURL.Append($"&AMT{count}={orderDetail.Price}");
                        cjURL.Append($"&QTY{count}={orderDetail.Quantity}");
                    }
                    viewModel.OrderLineItemQueryString = cjURL.ToString();
                    return viewModel;
                }
            }
            return null;
        }
        #endregion
    }
}
