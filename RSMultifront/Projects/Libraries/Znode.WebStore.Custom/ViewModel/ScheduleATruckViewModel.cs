﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;
using Znode.Libraries.Resources;

namespace Znode.WebStore.Custom.ViewModel
{
    public class ScheduleATruckViewModel : BaseViewModel
    {
        public string CompanyName { get; set; }
        public string DatesRequested { get; set; }
        public string TmeRequested { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NoOfEmployee { get; set; }
        public string MaleFemale { get; set; }
        public string Steeltorequired { get; set; }
        public string EmailID { get; set; }
        //[RegularExpression("^[a-zA-Z0-9'' ']+$", ErrorMessageResourceName = "ErrorSpecialCharatersNotAllowed", ErrorMessageResourceType = typeof(WebStore_Resources))]
        public string ContriAmount { get; set; }
        public string Payroll { get; set; }
        public string PosterNeeded { get; set; }
        public string Description { get; set; }

    }
}
