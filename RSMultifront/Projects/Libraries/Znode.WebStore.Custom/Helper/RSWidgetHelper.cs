﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Helper
{
    public static class RSWidgetHelper
    {
        #region Private Variables
        private static readonly IRSWidgetDataAgent _widgetDataAgent;
        #endregion

        #region Constructor
        static RSWidgetHelper()
        {
            RSWebstoreHelper helper = new RSWebstoreHelper();
            _widgetDataAgent = (IRSWidgetDataAgent)helper.RSWidgetDataAgent();
        }

        #endregion

        //Get Cart 
        public static CartViewModel GetCart()
           => _widgetDataAgent.GetCart();
        //Get sub categories.
        public static List<RSCategoryViewModel> GetSubCategories(WidgetParameter widgetparameter)
        => _widgetDataAgent.GetSubCategories(widgetparameter);

        public static string GetDefaultStoreDetailsFromCookie()
       => _widgetDataAgent.GetDefaultStoreDetailsFromCookie();
    }
}
