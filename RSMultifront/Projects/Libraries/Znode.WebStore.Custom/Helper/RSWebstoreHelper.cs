﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;


namespace Znode.WebStore.Custom.Helper
{
    public class RSWebstoreHelper : BaseAgent
    {
        public IWidgetDataAgent RSWidgetDataAgent()
          => new RSWidgetDataAgent(GetClient<WebStoreWidgetClient>(), GetClient<PublishProductClient>(), GetClient<PublishCategoryClient>(), GetClient<BlogNewsClient>(), GetClient<ContentPageClient>(), GetClient<SearchClient>(), GetClient<CMSPageSearchClient>());
                                                                                                                           
        public static int? GetProfileId()
        {
            IWebstoreHelper helper = GetService<IWebstoreHelper>();
            UserViewModel currentUser = helper.GetUserViewModelFromSession();
            return currentUser?.Profiles?.Count > 0 ? currentUser?.ProfileId > 0 ? currentUser.ProfileId : currentUser.Profiles?.Where(x => x.IsDefault.GetValueOrDefault())?.FirstOrDefault()?.ProfileId : HttpContext.Current.User.Identity.IsAuthenticated ? 0 : PortalAgent.CurrentPortal.ProfileId;
        }

    }
}
