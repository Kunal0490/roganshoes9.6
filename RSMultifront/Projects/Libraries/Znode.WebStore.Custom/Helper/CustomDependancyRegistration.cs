﻿using Autofac;
using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.Controllers;

namespace Znode.Engine.WebStore
{
    public class CustomDependancyRegistration : IDependencyRegistration
    {
        public virtual void Register(ContainerBuilder builder)
        {
           // builder.RegisterType<CustomUserController>().As<UserController>().InstancePerDependency();
            builder.RegisterType<RSCartAgent>().As<ICartAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSCartAgent>().As<IRSCartAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSCheckoutAgent>().As<ICheckoutAgent>().InstancePerLifetimeScope();
            // builder.RegisterType<RSHomeController>().As<HomeController>().InstancePerDependency();
            builder.RegisterType<RSStoreLocatorAgent>().As<IStoreLocatorAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSStoreLocatorAgent>().As<IRSStoreLocatorAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSCartController>().As<CartController>().InstancePerDependency();//.PreserveExistingDefaults();  
            builder.RegisterType<RSUserAgent>().As<IUserAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSUserAgent>().As<IRSUserAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSProductController>().As<ProductController>().InstancePerDependency();
            builder.RegisterType<RSProductAgent>().As<IProductAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSCustomizedFormAgent>().As<IRSCustomizedFormAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSCategoryController>().As<CategoryController>().InstancePerDependency();

            //builder.RegisterType<RSCheckoutController>().As<CheckoutController>().InstancePerDependency();
            builder.RegisterType<RSSearchController>().As<SearchController>().InstancePerDependency();
            builder.RegisterType<RSSearchAgent>().As<ISearchAgent>().InstancePerLifetimeScope();

            builder.RegisterType<RSCategoryAgent>().As<ICategoryAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSSalesDetailsAgent>().As<IRSSalesDetailsAgent>().InstancePerLifetimeScope();
            builder.RegisterType<RSBrandController>().As<BrandController>().InstancePerLifetimeScope();
            builder.RegisterType<RSWidgetDataAgent>().As<IWidgetDataAgent>().InstancePerLifetimeScope();
        }
        public int Order
        {
            get { return 1; }
        }
    }
}