﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
//using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.WebStore.Custom.Controllers
{
    public class RSProductController : ProductController
    {
        #region Public Constructor

        private readonly IProductAgent _productAgent;
        private readonly ICartAgent _cartAgent;
        public RSProductController(IProductAgent productAgent, IUserAgent userAgent, ICartAgent cartAgent, IWidgetDataAgent widgetDataAgent, IAttributeAgent attributeAgent, IRecommendationAgent recommendationAgent) :
            base(productAgent, userAgent, cartAgent, widgetDataAgent, attributeAgent, recommendationAgent)
        {
            _productAgent = productAgent;
            _cartAgent = cartAgent;
        }

        [HttpPost]
        //Get configurable data.
        public override ActionResult GetConfigurableProduct(ParameterProductModel model)
        {
            try
            {

                ProductViewModel product = _productAgent.GetConfigurableProduct(model);

                product.IsQuickView = model.IsQuickView;
                product.IsProductEdit = !Equals(ViewBag.IsProductEdit, null) ? ViewBag.IsProductEdit : false;
                product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;

                if (model.IsQuickView)
                {
                    ZnodeLogging.LogMessage("_QuickViewProductView Called-", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    return ActionView("_QuickViewProductView", product);
                }

                return ActionView(product.ProductTemplateName, product);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("RSProductController-GETCONFIGURABLE PRODUCT ERROR." + ex.Message + "--" + ex.StackTrace, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return ActionView("_Big_View", new ProductViewModel());
            }

        }

        //[HttpGet]
        //[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
        //public ActionResult ProductOverview(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        //{
        //    string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
        //    ShortProductViewModel product = _productAgent.GetExtendedProductDetails(id, expandKeys);
        //    if (IsNull(product.ProductImage))
        //    {
        //        return new HttpNotFoundResult("One of the supplied expands is not valid.");
        //    }

        //    return PartialView("_ProductOverviewLite", product);
        //}

        [HttpPost]
        public override ActionResult AddToCartProduct(AddToCartViewModel cartItem, bool IsRedirectToCart = true)
        {
            string _stickyBarImage = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_55/";
            AddToCartViewModel ShoppingCart = _cartAgent.AddToCartProduct(cartItem);

            if (IsRedirectToCart)
            {
                return RedirectToAction<CartController>(x => x.Index());
            }
           
            return Json(new
            {
                status = ShoppingCart.HasError,
                cartCount = ShoppingCart.CartCount,
                Product = ShoppingCart?.ShoppingCartItems?.FirstOrDefault(x => x.SKU == cartItem.SKU),
                ImagePath = $"{_stickyBarImage}/{ShoppingCart?.ShoppingCartItems?.FirstOrDefault(x => x.SKU == cartItem.SKU)?.Product.Attributes.FirstOrDefault(x => x.AttributeCode == "BaseImage")?.AttributeValues}"
            },
            JsonRequestBehavior.AllowGet);

        }
        public override ActionResult ViewComparison()
        {
            ProductCompareViewModel compareProducts = new ProductCompareViewModel();
            compareProducts.ProductList = _productAgent.GetCompareProductsDetails(true);
            if (compareProducts.ProductList.Count <= 0)
            {
                return RedirectToAction("Index", "Home");
            }

            return View("ViewComparison", compareProducts);
        }


        #endregion
    }
}
