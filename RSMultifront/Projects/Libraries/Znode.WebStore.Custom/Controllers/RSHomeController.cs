﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.Helper;

namespace Znode.WebStore.Custom.Controllers
{
    public class RSHomeController : HomeController
    {
        private readonly IRSStoreLocatorAgent _storeLocatorAgent;
        //private readonly IStoreLocatorAgent _storeLocatorAgent;
        private IRSWidgetDataAgent _widgetDataAgent;
       
        public RSHomeController(IPortalAgent portalAgent, IRSStoreLocatorAgent storeLocatorAgent, IUserAgent userAgent, IWidgetDataAgent widgetDataAgent, IBlogNewsAgent blogNewsAgent, ICartAgent cartAgent) :
            base(portalAgent,storeLocatorAgent,  userAgent, widgetDataAgent, blogNewsAgent, cartAgent)
        {
            _storeLocatorAgent = storeLocatorAgent;
            RSWebstoreHelper helper = new RSWebstoreHelper();
            _widgetDataAgent = (IRSWidgetDataAgent)helper.RSWidgetDataAgent();

        }

        [HttpPost]
        public ActionResult SaveInCookie(string storeId, string storeName, string storeAddress)
        {
            try
            {
                StoreLocatorViewModel model = new StoreLocatorViewModel();
                _storeLocatorAgent.SaveInCookie(storeId, storeName, storeAddress);
                return Json(new
                {
                    isSuccess = true,
                    message = "Success",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSHomeController/SaveInCookie", TraceLevel.Error, ex);
                return Json(new
                {
                    isSuccess = true,
                    message = "Success",
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //Get Cart Count for Cache
        [AllowAnonymous]
        public virtual string GetStoreName()
        {
            return _widgetDataAgent.GetDefaultStoreDetailsFromCookie();
        }
    }
}
