﻿using System.Web.Mvc;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Resources;
using Znode.WebStore.Core.Extensions;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Controllers
{

    public class RSCustomizedFormController : BaseController
    {
        private readonly IRSCustomizedFormAgent _RSCustomizedFormAgent;

        public RSCustomizedFormController()
        {
          _RSCustomizedFormAgent = new RSCustomizedFormAgent();
        }
        [HttpGet]
       
        public virtual ActionResult ScheduleATruck()
        {
            return View("../RSCustomizedForms/ScheduleATruck");
        }
        [HttpPost]
        [CaptchaAuthorization]
        public virtual ActionResult ScheduleATruck(ScheduleATruckViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool flag = _RSCustomizedFormAgent.ScheduleATruck(model);

                if (flag == true)
                    return View("../RSCustomizedForms/RSCustomizedSuccessForm");
               
            }
            /* NIVI Code */
            model.ErrorMessage = WebStore_Resources.ErrorRequiredDetails;
            SetNotificationMessage(GetErrorNotificationMessage(model.ErrorMessage));
           
                return View("../RSCustomizedForms/ScheduleATruck");
            /* NIVI Code */

        }
        [HttpGet]
     
        public virtual ActionResult ShoeFinder()
        {
           
            return View("../RSCustomizedForms/ShoeFinder");
        }
        [HttpPost]
        [CaptchaAuthorization]
        public virtual ActionResult ShoeFinder(ShoesFinderViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool flag = _RSCustomizedFormAgent.ShoeFinder(model);
                if (flag == true)
                    //SetNotificationMessage(GetSuccessNotificationMessage(model.SuccessMessage));
                    return View("../RSCustomizedForms/RSCustomizedSuccessForm");               
            }
            /* NIVI Code */
            model.ErrorMessage = WebStore_Resources.ErrorRequiredDetails;
            SetNotificationMessage(GetErrorNotificationMessage(model.ErrorMessage));

            return View("../RSCustomizedForms/ShoeFinder");
            /* NIVI Code */
        }
    }
}