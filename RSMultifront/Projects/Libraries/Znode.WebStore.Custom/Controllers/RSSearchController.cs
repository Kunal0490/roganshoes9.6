﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.WebStore.Custom.Controllers
{
    class RSSearchController: SearchController
    {
        private readonly ISearchAgent _searchAgent;
        private readonly IWidgetDataAgent _widgetDataAgent;
        private readonly ICategoryAgent _categoryAgent;

        public RSSearchController(ISearchAgent searchAgent, IWidgetDataAgent widgetDataAgent, ICategoryAgent categoryAgent):base( searchAgent,widgetDataAgent,categoryAgent)
        {
            _searchAgent = searchAgent;
            _widgetDataAgent = widgetDataAgent;
            _categoryAgent = categoryAgent;
        }

        [HttpGet]
        [RedirectFromLogin]
        public override JsonResult GetSuggestions(string query)
        {
            if (query.Length > 3)
            {            
            List<AutoComplete> result = _searchAgent.GetSuggestions(query, "");
            return Json(result, JsonRequestBehavior.AllowGet);
            }
            List<AutoComplete> resultEmpty = new List<AutoComplete>();
            return Json(resultEmpty, JsonRequestBehavior.AllowGet);
        }

    }
}
