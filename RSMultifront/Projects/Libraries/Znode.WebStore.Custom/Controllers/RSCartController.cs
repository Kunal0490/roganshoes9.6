﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.WebStore.Core.Agents;
using Znode.WebStore.Core.Extensions;
using Znode.WebStore.Custom.Agents.IAgents;
//using Znode.WebStore.Custom.Extensions;

namespace Znode.WebStore.Custom.Controllers
{
    [NoCacheAttribute]
    public class RSCartController:CartController
    {
        private readonly IRSCartAgent _cartAgent;
        private readonly IWSPromotionAgent _promotionAgent;
        #region Constructor
        public RSCartController(IRSCartAgent cartAgent, IPortalAgent portalAgent, IWSPromotionAgent promotionAgent):base(cartAgent,portalAgent,promotionAgent)
        {
            _cartAgent = cartAgent;
            _promotionAgent = promotionAgent;

        }
        #endregion

        [HttpPost]
        public string UpdateDeliveryPreferenceToPickUp(string storeId, string storeName, string storeAddress,int productId = 0)
        {
            _cartAgent.UpdateDeliveryPreferenceToPickUp(storeId,storeName,storeAddress,productId);
            //return RedirectToAction<CartController>(x => x.GetShoppingCart());
            return "{\"msg\":\"success\"}";
        }

        [HttpPost]
        public string UpdateDeliveryPreferenceToShip(int productId)
        {
            _cartAgent.UpdateDeliveryPreferenceToShip(productId);
            //return RedirectToAction<CartController>(x => x.GetShoppingCart());
            return "{\"msg\":\"success\"}";
        }

    }

   

}
