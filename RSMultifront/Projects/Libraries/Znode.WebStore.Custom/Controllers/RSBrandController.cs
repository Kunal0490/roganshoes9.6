﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;

namespace Znode.WebStore.Custom.Controllers
{
    public class RSBrandController : BrandController
    {
        #region Private Variables
        private readonly IBrandAgent _brandAgent;
        private readonly ICategoryAgent _categoryAgent;
        #endregion

        #region Public Constrcutor 
        public RSBrandController(IBrandAgent brandAgent, ICategoryAgent categoryAgent):base(brandAgent, categoryAgent)
        {
            _brandAgent = brandAgent;
            _categoryAgent = categoryAgent;
        }
        #endregion

        #region Public Methods.

        public override ActionResult List() {
            var model = _brandAgent.BrandList();
           return View("List",model);
        }



        #endregion
    }
}

