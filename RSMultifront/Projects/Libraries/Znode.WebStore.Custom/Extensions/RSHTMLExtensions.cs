﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.WebStore.Custom.Helper;

namespace System.Web.Mvc.Html
{
    public static class RSHTMLExtensions
    {
        private static ThemedViewEngine engine = new ThemedViewEngine();
      
        //Sub category list control Widget.
        public static MvcHtmlString SubCategoryControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, RSWidgetHelper.GetSubCategories(widgetparameter));

        //Get Partial view of widgets.
        private static MvcHtmlString GetPartial(HtmlHelper htmlHelper, ViewEngineResult viewResult, ControllerContext controllerContext, object data = null)
        => BuildPartial(htmlHelper, controllerContext, viewResult, data);

        //Get Partial view of widgets.
        private static MvcHtmlString GetPartial(HtmlHelper htmlHelper, string partialViewName, object data = null)
        {
            ControllerContext controllerContext = htmlHelper.ViewContext.Controller.ControllerContext;
            ViewEngineResult viewResult = engine.FindPartialView(controllerContext, partialViewName, true);
            return BuildPartial(htmlHelper, controllerContext, viewResult, data);
        }

        //Find and build partial view for widgets render.
        private static MvcHtmlString BuildPartial(HtmlHelper htmlHelper, ControllerContext controllerContext, ViewEngineResult viewResult, object data = null)
        {
            ViewDataDictionary viewData = new ViewDataDictionary(data);
            TempDataDictionary tempData = htmlHelper.ViewContext.TempData;
            using (StringWriter stringWriter = new StringWriter())
            {
                ViewContext viewContext = new ViewContext(controllerContext, viewResult.View, viewData, tempData, stringWriter);

                viewResult.View.Render(viewContext, stringWriter);

                return MvcHtmlString.Create(stringWriter.GetStringBuilder().ToString());
            }
        }
    }
}
