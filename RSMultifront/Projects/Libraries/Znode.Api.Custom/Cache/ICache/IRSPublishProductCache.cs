﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Cache;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Cache.ICache
{
   public interface IRSPublishProductCache: IPublishProductCache
    {
        RSPublishProductResponse GetStoreLocationDetails(string state, string sku);
    }
}
