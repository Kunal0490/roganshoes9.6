﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.LibrariesAdmin
{
   public class RSOrderHelper: ZnodeOrderHelper
    {
        public RSOrderHelper() : base()
        {


        }
        public override bool ManageOrderInventory(OrderWarehouseModel orderInventory, out List<OrderWarehouseLineItemsModel> productInventoryList)
        {
            string savedCartLineItemXML = HelperUtility.ToXML(orderInventory.LineItems);

            //SP call to Manage Order Inventory.
            IZnodeViewRepository<OrderWarehouseLineItemsModel> objStoredProc = new ZnodeViewRepository<OrderWarehouseLineItemsModel>();
            objStoredProc.SetParameter("SkuXml", savedCartLineItemXML, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodeDomainEnum.PortalId.ToString(), orderInventory.PortalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeUserEnum.UserId.ToString(), orderInventory.UserId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(View_ReturnBooleanEnum.Status.ToString(), null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            productInventoryList = objStoredProc.ExecuteStoredProcedureList("Znode_UpdateInventory @SkuXml, @PortalId, @UserId ,@Status OUT", 3, out status)?.ToList();

            return status == 1;
        }
        #region EmptyCart
        //public override bool SaveAllCartLineItemsInDatabase(int savedCartId, AddToCartModel shoppingCart)
        //{
        //    if (savedCartId > 0 && !Equals(shoppingCart, null))
        //    {
        //        List<SavedCartLineItemModel> savedCartLineItem = new List<SavedCartLineItemModel>();

        //        //Check if Shopping cart and cart items are null. If not then add it ro SavedCartLineItemModel.
        //        if ((HelperUtility.IsNotNull(shoppingCart.ShoppingCartItems)))
        //        {
        //            //Bind AddOns, Bundle product skus if associated rto cart items.
        //            foreach (var item in shoppingCart.ShoppingCartItems)
        //                BindShoppingCartItemModel(item, shoppingCart.PublishedCatalogId, shoppingCart.LocaleId);

        //            int sequence = 0;
        //            foreach (ShoppingCartItemModel cartitem in shoppingCart.ShoppingCartItems)
        //            {
        //                sequence++;
        //                savedCartLineItem.Add(BindSaveCartLineItem(Convert.ToString(cartitem?.OmsOrderLineItemsId), savedCartId, cartitem, shoppingCart.PublishedCatalogId, shoppingCart.LocaleId)); //, sequence));
        //            }
        //        }
        //        string savedCartLineItemXML = HelperUtility.ToXML(savedCartLineItem);

        //        shoppingCart.UserId = HelperUtility.IsNull(shoppingCart.UserId) ? 0 : shoppingCart.UserId;

        //        //SP call to save/update savedCartLineItem
        //        IZnodeViewRepository<SavedCartLineItemModel> objStoredProc = new ZnodeViewRepository<SavedCartLineItemModel>();
        //        objStoredProc.SetParameter("CartLineItemXML", savedCartLineItemXML, ParameterDirection.Input, DbType.String);
        //        objStoredProc.SetParameter(ZnodeUserEnum.UserId.ToString(), shoppingCart.UserId, ParameterDirection.Input, DbType.Int32);
        //        objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
        //        int status = 0;
        //        List<SavedCartLineItemModel> savedCartLineItemList = objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateSaveCartLineItemQuantity @CartLineItemXML, @UserId ,@Status OUT", 2, out status).ToList();

        //        return status == 1;
        //    }
        //    return false;
        //}
        #endregion
    }
}
