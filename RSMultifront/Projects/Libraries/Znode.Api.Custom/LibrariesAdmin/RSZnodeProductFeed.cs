﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Xml;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.LibrariesAdmin
{
    public class RSZnodeProductFeedHelper : ZnodeProductFeedHelper
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
       // private readonly IZnodeRepository<ZnodePublishProductEntity> _publishProductEntity;
        private readonly IZnodeRepository<ZnodePublishCategoryEntity> _publishCategoryEntity;

       // private readonly ZnodeRssWriter rssWriter;
        private const string SEOURLProductType = "Product";
        private const string SEOURLContentPageType = "ContentPage";
        private const string SEOURLCategoryType = "Category";
        private const string LastModifier = "Use the database update date";
        private int fileCount = 0;
        private readonly int recCount = Convert.ToInt32(ZnodeApiSettings.ProductFeedRecordCount);
        #endregion

        public RSZnodeProductFeedHelper() : base()
        {
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _publishCategoryEntity = new ZnodeRepository<ZnodePublishCategoryEntity>(HelperMethods.Context);
        }
        protected override List<PublishedProductEntityModel> GetProductData(string commaSepCatalogIds, int localeId)
        {
            //Get version id 
            string versionIds = GetVersionIds(commaSepCatalogIds, localeId);

            FilterCollection productFilter = new FilterCollection();
            if (!string.IsNullOrEmpty(commaSepCatalogIds))
                productFilter.Add(FilterKeys.ZnodeCatalogId, FilterOperators.In, commaSepCatalogIds);
            productFilter.Add(FilterKeys.PublishedLocaleId, FilterOperators.Equals, Convert.ToString(localeId));
            productFilter.Add(FilterKeys.VersionId, FilterOperators.In, versionIds);
            productFilter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            productFilter.Add(FilterKeys.PublishedIsActive, FilterOperators.Equals, ZnodeConstant.True);
            productFilter.Add(FilterKeys.ZnodeCategoryIds, FilterOperators.NotEquals, "0");
            NameValueCollection sortCollection = new NameValueCollection();

            return GetProductList(productFilter, sortCollection);
        }

        /*nivi code start*/
        public override string GenerateGoogleSiteMapIndexFiles(int fileNameCount, string txtXMLFileName, string priority)
        {
            /*Start: It is use to check sitemap is category, product, contentpage or all*/
            string fileNameDestination = "";
            if (txtXMLFileName.Contains("Category"))
            {
                txtXMLFileName = txtXMLFileName.Replace("Category##_", "");
                fileNameDestination = "sitemapcategory.xml";
            }
            else if (txtXMLFileName.Contains("Product"))
            {
                txtXMLFileName = txtXMLFileName.Replace("Product##_", "");
                fileNameDestination = "sitemapproduct.xml";
            }
            else if (txtXMLFileName.Contains("Content"))
            {
                txtXMLFileName = txtXMLFileName.Replace("Content##_", "");
                fileNameDestination = "sitemapcontent.xml";
            }
            else if (txtXMLFileName.Contains("ALL"))
            {
                txtXMLFileName = txtXMLFileName.Replace("ALL##_", "");
                //fileNameDestination = "sitemap.xml";
                fileNameDestination = "sitemapall.xml";
            }
            /*End: It is use to check sitemap is category, product, contentpage or all*/
            string rootTag = "sitemapindex";
            string roottagxmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            // Construct the XML for the Site Map creation
            XmlDocument requestXMLDoc = new XmlDocument();
            requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
            XmlElement urlsetElement = null;
            urlsetElement = requestXMLDoc.CreateElement(rootTag);
            urlsetElement.SetAttribute("xmlns", roottagxmlns);
            requestXMLDoc.AppendChild(urlsetElement);

            for (int i = 0; i < fileNameCount; i++)
            {
                XmlElement urlElement = requestXMLDoc.CreateElement("sitemap");
                string fileName = ZnodeStorageManager.HttpPath($"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{txtXMLFileName.Trim()}_{i}{".xml"}");
                string filepath = fileName.Replace("~/data/default/content/", "");
                if (fileName.StartsWith("~/"))
                    fileName = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Replace(System.Web.HttpContext.Current.Request.Url.AbsolutePath, string.Empty) + fileName.Substring(1);
                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "loc", fileName));
                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.Now.ToString("yyyy-MM-ddhh-mm-ss")));

                if (!string.IsNullOrEmpty(priority))
                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                urlsetElement.AppendChild(urlElement);
                /*Nivi Code start: Below Lines are written to copy the default sitemap files to webstore root folder */
                try
                {
                    string sitemapsource = ConfigurationManager.AppSettings["SitemapSource"].ToString() + filepath;
                    ZnodeLogging.LogMessage("sitemapsource= " + sitemapsource, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    string sitemapdesti = ConfigurationManager.AppSettings["SitemapDestination"].ToString() + fileNameDestination;
                    ZnodeLogging.LogMessage("sitemapdesti= " + sitemapdesti, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    System.IO.File.Copy(sitemapsource, sitemapdesti, true);
                }
                catch (System.IO.IOException e)
                {
                    ZnodeLogging.LogMessage("Sitemap Error" + e.Message, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                }
                /*Nivi Code end: Below Lines are written to copy the default sitemap files to webstore root folder */
            }
            string strSiteMapIndexFile = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{txtXMLFileName.Trim()}_{DateTime.Now.ToString("yyyy-MM-ddhh-mm-ss")}{".xml"}";
            ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, strSiteMapIndexFile);
            return strSiteMapIndexFile;
        }


        protected override DataSet GetContentPageFeedData(string portalIds, ProductFeedModel model)
        {
            DataSet datasetContentPagesList = null;
            List<ZnodePublishSeoEntity> seoSettings = null;
            try
            {
                datasetContentPagesList = GetResultsFromSP("Znode_GetContentFeedList", portalIds, string.Empty, model.LocaleId);
                seoSettings = GetSeoSettingList(model.LocaleId, portalIds, ZnodeConstant.ContentPage);
                if (HelperUtility.IsNotNull(datasetContentPagesList))
                {
                    AddColumns(ref datasetContentPagesList, ConfigurationManager.AppSettings["changefrequency"].ToString(), model.ProductFeedPriority);

                    foreach (DataRow dr in datasetContentPagesList.Tables[0].Rows)
                    {
                        int portalId = Convert.ToInt16(dr["PortalId"].ToString());
                        if (portalId > 0 && string.IsNullOrEmpty(dr["DomainName"].ToString()))
                            break;
                        int id = Convert.ToInt32(dr["CMSContentPagesId"].ToString());

                        string seoUrl = seoSettings.FirstOrDefault(x => x.SEOId == id)?.SEOUrl;
                        string domainname = $"{HttpContext.Current.Request.Url.Scheme}://{dr["DomainName"].ToString()}";

                        dr["loc"] = string.IsNullOrEmpty(Convert.ToString(dr["loc"])) ? $"{domainname}{"/contentpage/"}{Convert.ToString(dr["CMSContentPagesId"])}"
      : $"{domainname}/{Convert.ToString(dr["loc"])}";
                        if (model.ProductFeedTimeStampName.Equals("None"))
                            dr["lastmod"] = System.DBNull.Value;
                        else if ((!model.ProductFeedTimeStampName.Equals(LastModifier)) && (!model.ProductFeedTimeStampName.Equals("None")))
                            dr["lastmod"] = Convert.ToDateTime(model.Date);
                        dr["changefreq"] = Convert.ToString(dr["changefreq"].ToString());
                    }
                    RemoveUnwantedColumnsFromDS(ref datasetContentPagesList);
                }
                return datasetContentPagesList;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
            finally
            {
                datasetContentPagesList = null;
            }
        }

        private List<ZnodePublishSeoEntity> GetSeoSettingList(int localeId, string portalId, string seoType, List<string> commaSeparatedProductIds = null, string versionIds = null)
        {
            IZnodeRepository<ZnodePublishSeoEntity> _publishSEOEntity = new ZnodeRepository<ZnodePublishSeoEntity>(HelperMethods.Context);
            List<int?> portalIds = portalId.Split(',').Select(n => (int?)Convert.ToInt32(n)).ToList();

            FilterCollection filters = new FilterCollection();
            if (!portalIds.Any(x => x.Value == 0))
                filters.Add("PortalId", FilterOperators.In, portalId);
            if (!string.IsNullOrEmpty(versionIds))
                filters.Add("VersionId", FilterOperators.In, versionIds);
            filters.Add("SEOTypeName", FilterOperators.Is, seoType);
            filters.Add("LocaleId", FilterOperators.Equals, localeId.ToString());

            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollections());
            List<ZnodePublishSeoEntity> publishSEOList = _publishSEOEntity.GetEntityList(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.ToList();

            if (commaSeparatedProductIds?.Count > 0)
                publishSEOList = publishSEOList.Where(x => commaSeparatedProductIds.Contains(x.SEOCode))?.ToList();

            if (publishSEOList?.Count > 0)
                return publishSEOList;

            return new List<ZnodePublishSeoEntity>();
        }


        private XmlElement MakeElement(XmlDocument doc, string tagName, string tagValue)
        {
            XmlElement elem = tagName.Contains("g:") ? doc.CreateElement("g", tagName.Split(':')[1], "http://base.google.com/ns/1.0") : doc.CreateElement(tagName);
            elem.InnerText = tagValue;
            return elem;
        }




        /*Nivi code start: Add category node in sitemap*/
        protected override void AddDataToCategoryDataSet(ref DataSet dsCategory, DataSet categoryDataFromSQL, List<PublishedCategoryEntityModel> publishedCategoryData, string changeFrequency, string priority, List<ZnodePublishSeoEntity> seoEntities)
        {

            //Merge the Product Data from Mongo and SQLin one DataSet
            for (int iCount = 0; iCount < publishedCategoryData.Count; iCount++)
            {
                int catID = publishedCategoryData[iCount].ZnodeCategoryId;
                string seoCode = publishedCategoryData[iCount].Attributes.FirstOrDefault(x => x.AttributeCode == "CategoryCode")?.AttributeValues;
                string seoUrl = seoEntities.FirstOrDefault(x => x.SEOCode == seoCode)?.SEOUrl;

                var result =
        categoryDataFromSQL.Tables[0].AsEnumerable().Where(dr => dr.Field<int>("id") == catID);

                foreach (DataRow row in result)
                {
                    string domainName = $"{HttpContext.Current.Request.Url.Scheme}://{Convert.ToString(row["DomainName"])}";
                    DataRow dr = dsCategory.Tables[0].NewRow();
                    dr["loc"] = string.IsNullOrEmpty(seoUrl) ? $"{domainName}{"/category/"}{Convert.ToString(row["id"])}" : $"{domainName}/{seoUrl}";
                    dr["lastmod"] = DateTime.Now.ToString("yyyy/MM/dd");//row["lastmod"]; 
                    dr["changefreq"] = ConfigurationManager.AppSettings["changefrequency"].ToString();//row["changefreq"];//changeFrequency;
                    dr["priority"] = priority;
                    dsCategory.Tables[0].Rows.Add(dr);
                }
            }
        }
        /*Nivi code End: Add category node in sitemap*/

        /*Nivi code start: Add dataset columns node in sitemap*/
        protected override void CreateDataSetColumns(ref DataSet dsDataSet, string type)
        {
            dsDataSet.Tables.Add(new DataTable());
            switch (type)
            {
                case SEOURLProductType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("lastmod", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("changefreq", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("title", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:condition", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("description", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:id", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:image_link", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("link", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:price", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:identifier_exists", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:availability", typeof(string));
                    break;
                case SEOURLCategoryType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("lastmod", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("changefreq", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("priority", typeof(string));
                    break;
                case SEOURLContentPageType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("changefreq", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("priority", typeof(string));
                    break;
            }
        }
        /*Nivi code end: Add dataset columns node in sitemap*/

        /*Nivi code start: Add product node  in sitemap*/
        public override int GetProductXMLList(string portalId, string rootTagValue, string feedType, string rootTag, ProductFeedModel productFeedModel)
        {
            var productsitemap = CreateProductSiteMap(GetProductFeedDataFromSQL(portalId, productFeedModel.LocaleId, feedType, productFeedModel.ProductFeedTimeStampName, productFeedModel.Date), rootTag, rootTagValue, productFeedModel.FileName, Convert.ToString(productFeedModel.ProductFeedPriority));
            return productsitemap;
        }
        public int CreateProductSiteMap(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName, string priority)
        {
            XmlDocument requestXMLDoc = null;
            try
            {
                if (datasetValues != null)
                {
                    int recordsCount = datasetValues.Tables[0].Rows.Count;
                    int loopCnt = 0;

                    for (; loopCnt < recordsCount;)
                    {
                        string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";
                        string[] rootTagValues = rootTagValue.Split(',');
                        // Construct the XML for the Site Map creation
                        requestXMLDoc = new XmlDocument();
                        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                        XmlElement urlsetElement = null;
                        urlsetElement = requestXMLDoc.CreateElement(rootTag);
                        if (rootTagValues?.Count() > 0)
                        {
                            string[] values = rootTagValues[0].Split('=');
                            urlsetElement.SetAttribute(values[0], values[1]);
                        }
                        requestXMLDoc.AppendChild(urlsetElement);

                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[0].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[0].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) && Equals(dc.ColumnName, "loc"))
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()?.Trim()));
                            }
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.Now.ToString("yyyy/MM/dd")));
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "changefreq", ConfigurationManager.AppSettings["changefrequency"].ToString()));
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                            loopCnt++;
                        }
                        while (loopCnt < recordsCount && (loopCnt + 1) % recCount != 0);
                        ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);
                        // Increment the file count if the file has to be splitted.
                        this.fileCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }
            requestXMLDoc = null;
            return this.fileCount;
        }
        /*Nivi code end: Add product node  in sitemap*/

        /*Nivi code start: Add sitemapall node  in sitemap*/
        public override int GetAllList(ProductFeedModel model, string portalId, string rootTag, string rootTagValue, string feedType)
        {
            var allsitemap = CreateXMLSiteMapForAll(GetAllFeedDataFromSQL(portalId, feedType, model), rootTag, rootTagValue, model.FileName, Convert.ToString(model.ProductFeedPriority));
            return allsitemap;
        }

        public int CreateXMLSiteMapForAll(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName, string priority)
        {
            XmlDocument requestXMLDoc = null;
            try
            {
                int tableCount = datasetValues.Tables.Count;
                string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";
                string[] rootTagValues = rootTagValue.Split(',');
                // Construct the XML for the Site Map creation
                requestXMLDoc = new XmlDocument();
                requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                XmlElement urlsetElement = null;
                urlsetElement = requestXMLDoc.CreateElement(rootTag);
                if (rootTagValues?.Count() > 0)
                {
                    string[] values = rootTagValues[0].Split('=');
                    urlsetElement.SetAttribute(values[0], values[1]);
                }
                requestXMLDoc.AppendChild(urlsetElement);
                for (int i = 0; i < tableCount; i++)
                {
                    int recordsCount = datasetValues.Tables[i].Rows.Count;
                    int loopCnt = 0;
                    if (datasetValues.Tables[i].TableName == "Content" || datasetValues.Tables[i].TableName == "Category")
                    {
                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[i].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[i].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()))
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                            }

                            loopCnt++;
                        }
                        while (loopCnt < recordsCount);
                    }
                    else if (datasetValues.Tables[i].TableName == "Product")
                    {
                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[i].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[i].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) && Equals(dc.ColumnName, "loc"))
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                            }
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.Now.ToString("yyyy/MM/dd")));
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "changefreq", ConfigurationManager.AppSettings["changefrequency"].ToString()));
                            if (!string.IsNullOrEmpty(priority))
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                            loopCnt++;
                        }
                        while (loopCnt < recordsCount);
                    }
                    ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);
                }
                // Increment the file count.
                this.fileCount++;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }
            requestXMLDoc = null;
            return this.fileCount;
        }
        /*Nivi code end: Add sitemapall node  in sitemap*/
        /*nivi code end*/
        protected override List<PublishedCategoryEntityModel> GetCategoryData(string commaSepCatalogIds, int localeId)
        {
            //Get version id 
            // string versionIds = GetVersionIds(commaSepCatalogIds, localeId);
            /*Nivi code start: get current version id*/
            int versionIds = ZnodeDependencyResolver.GetService<IPublishProductHelper>()?.GetCatalogVersionId(Convert.ToInt32(commaSepCatalogIds), localeId) ?? 0;
            /*Nivi code start: get current version id*/
            FilterCollection categoryFilter = new FilterCollection();
            if (!string.IsNullOrEmpty(commaSepCatalogIds))
                categoryFilter.Add(FilterKeys.ZnodeCatalogId, FilterOperators.In, commaSepCatalogIds);
            categoryFilter.Add(FilterKeys.PublishedLocaleId, FilterOperators.Equals, Convert.ToString(localeId));
            categoryFilter.Add(FilterKeys.VersionId, FilterOperators.In, Convert.ToString(versionIds));
            NameValueCollection sortCollection = new NameValueCollection();
            sortCollection.Add(FilterKeys.ZnodeCategoryId, "asc");


            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(categoryFilter.ToFilterDataCollections());
            return _publishCategoryEntity.GetEntityList(whereClauseModel.WhereClause, DynamicClauseHelper.GenerateDynamicOrderByClause(sortCollection))?.ToModel<PublishedCategoryEntityModel>()?.ToList();
        }


    }
}
