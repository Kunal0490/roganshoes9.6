﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.LibrariesAdmin
{
    public class RSZnodeCheckout : ZnodeCheckout
    {

        private readonly IZnodeOrderHelper orderHelper;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly Dictionary<int, string> publishCategory = new Dictionary<int, string>();

        public RSZnodeCheckout()
        {
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }

        public RSZnodeCheckout(UserAddressModel userAccount, ZnodePortalCart shoppingCart)
        {
            this.UserAccount = userAccount;
            this.ShoppingCart = shoppingCart;
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }

        //to set order lineitems 
        public override void SetOrderLineItems(ZnodeOrderFulfillment order, ZnodeMultipleAddressCart addressCart)
        {
            GetDistinctCategoryIdsforCartItem(addressCart);
            List<Libraries.Data.DataModel.ZnodeOmsOrderLineItem> lineItemShippingDateList = orderHelper.GetLineItemShippingDate(order.Order.OmsOrderDetailsId);
            DateTime? ShipDate = DateTime.Now;
            // loop through cart and add line items
            foreach (ZnodeShoppingCartItem shoppingCartItem in addressCart.ShoppingCartItems)
            {
                OrderLineItemModel orderLineItem;

                if (string.IsNullOrEmpty(shoppingCartItem.GroupId))
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }
                else
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.Product.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }


                bool addNewOrderItem = false;
                if (IsNull(orderLineItem))
                {
                    addNewOrderItem = true;
                    orderLineItem = new OrderLineItemModel();
                    orderLineItem.OmsOrderShipmentId = addressCart.OrderShipmentID;
                    if (string.IsNullOrEmpty(shoppingCartItem.Product.ShoppingCartDescription) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.ShoppingCartDescription))
                    {
                        if (string.IsNullOrEmpty(shoppingCartItem.Product.Description) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.Description))
                        {
                            orderLineItem.Description = shoppingCartItem.Description;
                        }
                        else
                        {
                            orderLineItem.Description = shoppingCartItem.Product.Description;
                        }
                    }
                    else
                    {
                        orderLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
                    }

                    orderLineItem.ProductName = shoppingCartItem.Product.Name;
                    orderLineItem.Sku = shoppingCartItem.Product.SKU;
                    orderLineItem.Quantity = ((shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) || (shoppingCartItem?.Product?.ZNodeGroupProductCollection.Count > 0)) ? 0 : shoppingCartItem.Quantity;
                    orderLineItem.Price = (shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) ? 0 : GetParentProductPrice(shoppingCartItem);
                    orderLineItem.DiscountAmount = GetLineItemDiscountAmount(shoppingCartItem.Product.DiscountAmount, shoppingCartItem.Quantity);
                    orderLineItem.ShipSeparately = shoppingCartItem.Product.ShipSeparately;
                    orderLineItem.ParentOmsOrderLineItemsId = null;

                    orderLineItem.DownloadLink = shoppingCartItem.Image;
                    //orderLineItem.DownloadLink = null;

                    orderLineItem.GroupId = shoppingCartItem.GroupId;
                    orderLineItem.IsActive = true;
                    orderLineItem.Vendor = shoppingCartItem.Product.VendorCode;
                    orderLineItem.OrderLineItemStateId = shoppingCartItem.OrderStatusId;
                    orderLineItem.IsItemStateChanged = shoppingCartItem.IsItemStateChanged;
                    if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase) && shoppingCartItem.IsItemStateChanged)
                    {
                        orderLineItem.ShipDate = ShipDate;
                    }
                    else if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        orderLineItem.ShipDate = lineItemShippingDateList.Count > 0 ? lineItemShippingDateList.Find(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemId).ShipDate : null;
                    }

                    if (!string.IsNullOrEmpty(shoppingCartItem.TrackingNumber))
                    {
                        orderLineItem.TrackingNumber = shoppingCartItem.TrackingNumber;
                    }
                    //to apply custom tax/shipping cost
                    orderLineItem.IsLineItemShippingCostEdited = order.IsShippingCostEdited;
                    orderLineItem.IsLineItemTaxCostEdited = order.IsTaxCostEdited;
                    orderLineItem.PartialRefundAmount = shoppingCartItem.PartialRefundAmount;
                    //Assign Auto-add-on SKUs.
                    orderLineItem.AutoAddonSku = string.IsNullOrEmpty(shoppingCartItem.AutoAddonSKUs) ? null : shoppingCartItem.AutoAddonSKUs;

                    //to set order line item attributes
                    orderLineItem.Attributes = SetLineItemAttributes(shoppingCartItem?.Product?.Attributes, shoppingCartItem?.Product.ProductCategoryIds);

                    // then make a shipping cost entry in orderlineItem table.             
                    orderLineItem.ShippingCost = orderLineItem.IsLineItemShippingCostEdited ? 0 : shoppingCartItem.ShippingCost;

                    //Set order tax to order line item.
                    orderLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.HST;
                    orderLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.PST;
                    orderLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.GST;
                    orderLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.VAT;
                    orderLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.SalesTax;
                    orderLineItem.TaxTransactionNumber = shoppingCartItem.TaxTransactionNumber;
                    orderLineItem.TaxRuleId = shoppingCartItem.TaxRuleId;

                    orderLineItem.Custom1 = shoppingCartItem.Custom1;
                    orderLineItem.Custom2 = shoppingCartItem.Custom2;
                    orderLineItem.Custom3 = shoppingCartItem.Custom3;
                    orderLineItem.Custom4 = shoppingCartItem.Custom4;
                    orderLineItem.Custom5 = shoppingCartItem.Custom5;

                    if (shoppingCartItem.Product.RecurringBillingInd)
                    {
                        orderLineItem.RecurringBillingAmount = shoppingCartItem.Product.RecurringBillingInitialAmount;
                        orderLineItem.RecurringBillingCycles = shoppingCartItem.Product.RecurringBillingTotalCycles;
                        orderLineItem.RecurringBillingFrequency = shoppingCartItem.Product.RecurringBillingFrequency;
                        orderLineItem.RecurringBillingPeriod = shoppingCartItem.Product.RecurringBillingPeriod;
                        orderLineItem.IsRecurringBilling = true;
                    }
                    orderLineItem.OrdersDiscount = shoppingCartItem.Product.OrdersDiscount;
                    //To add personalize attribute list
                    orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;
                    orderLineItem.PersonaliseValuesDetail = shoppingCartItem.PersonaliseValuesDetail;
                }
                AddSimpleItemInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add add-on items in order line item
                AddAddOnsItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add bundle items in order line item
                AddBundleItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Configurable item in order line item                
                AddConfigurableItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Group item in order line item
                AddGroupItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //To add personalise attribute list
                orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;
                orderLineItem.OrderLineItemCollection.FirstOrDefault().DownloadLink = shoppingCartItem.Image;
                if (addNewOrderItem)
                {
                    order.OrderLineItems.Add(orderLineItem);
                }

            }
        }

        public override bool ManageOrderInventory(ZnodeOrderFulfillment order, ZnodePortalCart shoppingCart)
        {

            bool isSuccess = false;//base.ManageOrderInventory(order, shoppingCart);
                                   //   if (isSuccess)
                                   //   {
                                   //if (IsNotNull(order) && order.OrderLineItems.Select(x=>x.Custom1!="").Count() > 0)
            if (IsNotNull(order) && order.OrderLineItems.Count() > 0)
            {
                OrderWarehouseModel orderWarehouse = new OrderWarehouseModel();
                orderWarehouse.OrderId = order.Order.OmsOrderDetailsId;
                orderWarehouse.UserId = order.UserID;
                orderWarehouse.PortalId = order.PortalId;

                List<OrderWarehouseLineItemsModel> skusInventory = SetSKUInventorySetting(shoppingCart);
                // List<OrderLineItemModel> orderLineItems = order.OrderLineItems.Where(x => x.Custom1 != "").ToList();
                foreach (OrderLineItemModel item in order.OrderLineItems)
                {
                    bool allowBackOrder;
                    string inventoryTracking = GetInventoryTrackingBySKU(item.Sku, skusInventory, out allowBackOrder);
                    orderWarehouse.LineItems.Add(new OrderWarehouseLineItemsModel { OrderLineItemId = item.OmsOrderLineItemsId, SKU = item.Sku, Quantity = item.Quantity, InventoryTracking = inventoryTracking, WarehouseId = item.Custom1 == "" ? 0 : Convert.ToInt32(item.Custom1) });

                    foreach (OrderLineItemModel childitem in item.OrderLineItemCollection)
                    {
                        if (childitem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Bundles)
                        {
                            inventoryTracking = GetInventoryTrackingBySKU(childitem.Sku, skusInventory,out allowBackOrder);
                            orderWarehouse.LineItems.Add(new OrderWarehouseLineItemsModel { OrderLineItemId = childitem.OmsOrderLineItemsId, SKU = childitem.Sku, Quantity = childitem.Quantity, InventoryTracking = inventoryTracking, WarehouseId = childitem.Custom1 == "" ? 0 : Convert.ToInt32(childitem.Custom1) });

                        }
                    }
                }
                List<OrderWarehouseLineItemsModel> productInventoryList = new List<OrderWarehouseLineItemsModel>();
                 string savedCartLineItemXML = HelperUtility.ToXML(orderWarehouse.LineItems);
                isSuccess = orderHelper.ManageOrderInventory(orderWarehouse,out productInventoryList);
                GetLowInventoryProducts(shoppingCart, productInventoryList);
            }

            // }
            return isSuccess;

        }


    }
}
