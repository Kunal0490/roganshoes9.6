﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Custom.Service.IService
{
    public interface IRSSalesDetailsService
    {
        List<RSSalesDetailsModel> GetSalesDetails();
    }
}
