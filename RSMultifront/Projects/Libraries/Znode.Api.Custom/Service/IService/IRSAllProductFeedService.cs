﻿using Znode.Engine.Api.Models;

namespace Znode.Api.Custom.Service.IService
{
    internal interface IRSAllProductFeedService
    {
        /// <summary>
        /// Create Product feed.
        /// </summary>
        /// <param name="model">Product Feed Model.</param>
        /// <returns>returns Product Feed Model.</returns>
        bool CreateGoogleSiteMapforall();
    }
}
