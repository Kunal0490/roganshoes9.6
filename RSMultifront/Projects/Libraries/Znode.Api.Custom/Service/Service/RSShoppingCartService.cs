﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using System.Web;
//using Znode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Api.Custom.Service.Service
{
    public class RSShoppingCartService : ShoppingCartService
    {
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        private string promotionType = "CART";
        private string promotionName = "ZnodeCartPromotionPercentOffShippingWithCarrier";
        private const string FEDEXGROUND = "FEDEX_GROUND";

        #region TBD EmptyCart Variables
        private readonly IShoppingCartMap _shoppingCartMap;
        private readonly IShoppingCartItemMap _shoppingCartItemMap;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodeAccount> _accountRepository;
        #endregion

        public RSShoppingCartService():base()
        {
            _shoppingCartMap = ZnodeDependencyResolver.GetService<IShoppingCartMap>();
            _shoppingCartItemMap = ZnodeDependencyResolver.GetService<IShoppingCartItemMap>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _accountRepository = new ZnodeRepository<ZnodeAccount>();
        }
        public override AddToCartModel AddToCartProduct(AddToCartModel shoppingCart)
        {
            try
            {
                string selectedStoreID = shoppingCart.Custom1 == null ? "" : shoppingCart.Custom1;
                string selectedStoreName = shoppingCart.Custom2 == null ? "" : shoppingCart.Custom2;
                string selectedStoreAddress = shoppingCart.Custom3 == null ? "" : shoppingCart.Custom3;
                //selectedStoreID = selectedStoreID == "" ? "" : selectedStoreID?.Substring(2, selectedStoreID.Length - 4);
                //if (selectedStoreID == string.Empty)
                //{
                //    selectedStoreName = selectedStoreName == "" ? "" : selectedStoreName.Substring(1, selectedStoreName.Length - 2);
                //}
                //else
                //    selectedStoreName = selectedStoreName == "" ? "" : selectedStoreName.Substring(2, selectedStoreName.Length - 4);
                //selectedStoreAddress = selectedStoreAddress == "" ? "" : selectedStoreAddress.Substring(2, selectedStoreAddress.Length - 4);
                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom1 = selectedStoreID);
                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom2 = selectedStoreName);
                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom3 = selectedStoreAddress);
                //string isProductShipOnly = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom5);
                //isProductShipOnly = isProductShipOnly.Substring(2, isProductShipOnly.Length - 4);

                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom5 = shoppingCart.Custom5);
                base.AddToCartProduct(shoppingCart);

                return shoppingCart;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSShoppingCartService", TraceLevel.Error, ex);
                return base.AddToCartProduct(shoppingCart);
            }
        }

        public override ShippingListModel GetShippingEstimates(string zipCode, ShoppingCartModel model)
        {
            bool _isPickShipCase = false;
            bool _allItemsTobePickedUp = false;
            if (zipCode.Contains("PICKSHIP"))
            {
                zipCode = zipCode.Replace("PICKSHIP", "");
                _isPickShipCase = true;
            }

            if (zipCode.Contains("ALLPICK"))
            {
                zipCode = zipCode.Replace("ALLPICK", "");
                _allItemsTobePickedUp = true;
            }


            //ZnodeLogging.LogMessage("Base - GetShippingEstimates", "Custom", TraceLevel.Error);
            if (HelperUtility.IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, "Shopping Cart Model can not be null.");

            if (string.IsNullOrEmpty(zipCode))
                return null;

            List<ShippingModel> listwithRates = new List<ShippingModel>();
            List<ShippingModel> ShippingList = new List<ShippingModel>();
            try
            {
                List<ShippingModel> list = GetShippingList(model);

                /*NIVI CODE FOR FALLBACK*/

                ShippingModel _customFlatOption = list.Where(x => x.ShippingCode == "FLAT").FirstOrDefault();
                ShippingList.Add(_customFlatOption);

                /*NIVI CODE FOR FALLBACK*/

                if (list?.Count > 0)
                {
                    if (_allItemsTobePickedUp == true)
                    {
                        list?.RemoveAll(r => (r.ShippingCode != "PICKUPFREE") && r.ShippingCode != "FLAT");
                        //return list;
                    }
                    else
                    {
                        list?.RemoveAll(r => (r.ShippingCode == "PICKUPFREE"));
                    }
                    if (Equals(model.ShippingAddress, null))
                    {
                        IZnodeRepository<ZnodeAddress> _addressRepository = new ZnodeRepository<ZnodeAddress>();
                        IZnodeRepository<ZnodeUserAddress> _addressUserRepository = new ZnodeRepository<ZnodeUserAddress>();
                        var shippingAddress = (from p in _addressRepository.Table
                                               join q in _addressUserRepository.Table
                                               on p.AddressId equals q.AddressId
                                               where (q.UserId == model.UserId) && (p.IsDefaultShipping)
                                               select new AddressModel
                                               {
                                                   StateName = p.StateName,
                                                   CountryName = p.CountryName,
                                                   PostalCode = p.PostalCode
                                               }).FirstOrDefault();
                        model.ShippingAddress = (shippingAddress != null) ? shippingAddress : new AddressModel();
                    }
                    string countryCode = model.ShippingAddress.CountryName;
                    string stateCode = GetStateCode(model.ShippingAddress?.StateName);

                    if (!string.IsNullOrEmpty(countryCode) && !string.IsNullOrEmpty(stateCode))
                        list = GetShippingByCountryAndStateCode(countryCode, stateCode, list);

                    if (!string.IsNullOrEmpty(model.ShippingAddress?.PostalCode))
                        list = GetShippingByZipCode(model.ShippingAddress.PostalCode, list);

                    //check shipping type and call that service to get the rates. Add the rates in the list.
                    if (Equals(model.ShippingAddress, null))
                        model.ShippingAddress = new AddressModel();
                    /*NIVI CODE for Hawaii And Alaska State*/
                    if (stateCode == "AK" || stateCode == "HI")
                    {
                        // list.RemoveRange(list.Select(x => x.ShippingName != "USPS").ToList());
                        list?.RemoveAll(r => (r.ShippingTypeName?.ToLower() != ZnodeConstant.USPS.ToLower() && r.ShippingCode != "FLAT"));
                        ZnodeLogging.LogMessage("stateCode=" + stateCode + "ShippingListCount=" + Convert.ToString(list.Count), "RSShoppingCartService", TraceLevel.Info);
                    }

                    model.ShippingAddress.PostalCode = zipCode;
                    model.ShippingAddress.StateCode = stateCode;
                    model.ShippingAddress.CountryName = countryCode;
                    model.ShippingAddress.Address1 = string.IsNullOrEmpty(model.ShippingAddress.Address1) ? string.Empty : model.ShippingAddress.Address1;
                    model.ShippingAddress.CityName = string.IsNullOrEmpty(model.ShippingAddress.CityName) ? string.Empty : model.ShippingAddress.CityName;

                    ShoppingCartMap SCM = new ShoppingCartMap();

                    ZnodeShoppingCart znodeShoppingCart = SCM.ToZnodeShoppingCart(model);

                    /*Nivi Code to display only FedEx Ground option if Cart contains only IsAvailableOnlineOnly Products*/
                    int totalShoppingCartItemsCount = znodeShoppingCart.ShoppingCartItems.Count();
                    
                    int availableOnlineOnlyProductCount = znodeShoppingCart.ShoppingCartItems.SelectMany(x => x.Product.Attributes.Where(y => y.AttributeCode == "IsAvailableOnlineOnly" && y.AttributeValue == "true")).Count();
                    if(totalShoppingCartItemsCount== availableOnlineOnlyProductCount && stateCode != "AK" && stateCode != "HI")
                    {
                        list?.RemoveAll(r => (r.ShippingCode != FEDEXGROUND));
                    }
                    /*Nivi Code end*/

                    List<ShippingModel> upslist = list.Where(w => w.ShippingTypeName.ToLower() == ZnodeConstant.UPS.ToLower() /*|| w.ShippingTypeName.ToLower() == ZnodeConstant.FedEx.ToLower()*/).ToList();
                    list?.RemoveAll(r => (r.ShippingTypeName?.ToLower() == ZnodeConstant.UPS.ToLower() /*|| r.ShippingTypeName?.ToLower() == ZnodeConstant.FedEx.ToLower() */) && !_upsLTLCode.Contains(r.ShippingCode));
                    //Call the respective shipping classes to get the shipping rates rates.
                    foreach (ShippingModel item in list)
                    {
                        if (item.ShippingCode == FEDEXGROUND)
                        {
                            /*NIVI Code for Cache issue for shipping above threshold shipping value*/
                            List<PromotionModel> AllPromotions = promotionHelper.GetAllPromotions();
                            PromotionModel freeshipping = AllPromotions?.Where(x => x.Name == "Threshold Free Shipping Value").FirstOrDefault();
                           // ZnodeLogging.LogMessage("freeshipping values:Name="+freeshipping==null?"NULL":freeshipping.Name, "Custom1", TraceLevel.Info);

                            if (freeshipping == null)
                            {
                                ZnodeLogging.LogMessage("GETSHIPPINGESTIMATES:Threshold Free Shipping Value promotion is null", "Custom1", TraceLevel.Info);
                                HttpRuntime.Cache.Remove("AllPromotionCache");
                                ////calculate PercentOffShipping promotion after shipping is calculated in order to get the calculated shipping cost
                                //ZnodeCartPromotionManager cartPromoManager = new ZnodeCartPromotionManager(znodeShoppingCart, znodeShoppingCart.ProfileId);
                                //cartPromoManager.Calculate();

                            }
                        }
                        model.Shipping.ShippingId = item.ShippingId;
                        model.Shipping.ShippingName = item.ShippingCode;
                        model.Shipping.ShippingCountryCode = string.IsNullOrEmpty(countryCode) ? item.DestinationCountryCode : countryCode;

                        znodeShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
                        ZnodeShippingManager shippingManager = new ZnodeShippingManager(znodeShoppingCart); 
                        shippingManager.Calculate();

                        /*NIVI CODE : To call promotion on Shipping options load in checkout to get promotion applied shipping rates for FEDEX_GROUND*/
                        if (item.ShippingCode == FEDEXGROUND)
                        {

                            ZnodeCartPromotionManager zpm = new ZnodeCartPromotionManager(znodeShoppingCart, znodeShoppingCart.ProfileId);
                            zpm.Calculate();
                            //if (znodeShoppingCart.ShippingCost > 0 && _isPickShipCase)
                            //{
                            //    List<PromotionModel> AllPromotions = promotionHelper.GetAllPromotions();
                            //    List<PromotionModel> ApplicablePromolist = AllPromotions.Where(promo => (DateTime.Today.Date >= promo.StartDate && DateTime.Today.Date <= promo.EndDate)
                            //                                                    && promo.PromotionType.ClassType.Equals(promotionType, StringComparison.OrdinalIgnoreCase)
                            //                                                    && promo.PromotionType.ClassName.Equals(promotionName)
                            //                                                    && promo.PromotionType.IsActive && (promo.PortalId == znodeShoppingCart.PortalId || promo.PortalId == null))
                            //                                   .OrderByDescending(x => typeof(PromotionModel).GetProperty("OrderMinimum").GetValue(x, null)).ToList();
                            //    decimal? ThresholdFreeShippingValue = ApplicablePromolist?.FirstOrDefault()?.OrderMinimum;
                            //    item.Custom1 = ThresholdFreeShippingValue;
                            //}

                        }
                        /*END NIVI CODE*/
                        item.ShippingRate = znodeShoppingCart.ShippingCost;
                        if (item.ShippingCode == "STANDARD_OVERNIGHT" || item.ShippingCode == "FEDEX_2_DAY")
                        {
                            znodeShoppingCart.CustomShippingCost = znodeShoppingCart.OrderLevelShipping;
                            item.ShippingRate = znodeShoppingCart.CustomShippingCost;
                           
                        }
                        /*Nivi code to set ApproximateArrival and EstimateDate*/
                        item.ApproximateArrival = znodeShoppingCart.ApproximateArrival == null ? "" : Convert.ToDateTime(znodeShoppingCart.ApproximateArrival).ToShortDateString();
                        if (item.ShippingCode == FEDEXGROUND)
                          item.EstimateDate = znodeShoppingCart.ApproximateArrival == null ? "" : Convert.ToDateTime(znodeShoppingCart.ApproximateArrival).AddDays(2).ToShortDateString();
                        
                        if (Equals(znodeShoppingCart?.Shipping?.ResponseCode, "0"))
                            listwithRates.Add(item);
                        else
                        {
                            ZnodeLogging.LogMessage("Shipping ERROR :item=" + item.ShippingCode + " Error Code=" + znodeShoppingCart?.Shipping?.ResponseCode + " Error Description=" + znodeShoppingCart?.Shipping?.ResponseMessage, "Custom1", TraceLevel.Error);
                        }
                    }

                    if (upslist.Count > 0)
                    {
                        ZnodeShippingManager manager = null;
                        ZnodeGenericCollection<IZnodeShippingsType> shippingTypes = new ZnodeGenericCollection<IZnodeShippingsType>();
                        List<ZnodeShippingBag> shippingbagList = new List<ZnodeShippingBag>();
                        foreach (ShippingModel item in upslist)
                        {
                            model.Shipping.ShippingId = item.ShippingId;
                            model.Shipping.ShippingName = item.ShippingCode;
                            model.Shipping.ShippingCountryCode = string.IsNullOrEmpty(countryCode) ? item.DestinationCountryCode : countryCode;
                            znodeShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
                            manager = new ZnodeShippingManager(znodeShoppingCart, true, shippingTypes, shippingbagList);
                        }

                        List<ShippingModel> ratelist = manager.GetShippingEstimateRate(znodeShoppingCart, model, countryCode, shippingbagList);

                        upslist = upslist.Join(ratelist, r => r.ShippingCode, p => p.ShippingCode, (ulist, rlist) => ulist).ToList();

                        upslist?.ForEach(f =>
                        {
                            ShippingModel shippingModel = ratelist?.Where(w => w.ShippingCode == f.ShippingCode && !Equals(w.ShippingRate, 0))?.FirstOrDefault();

                            if (HelperUtility.IsNotNull(shippingModel) && shippingModel?.ShippingRate > 0)
                            {
                                f.EstimateDate = shippingModel.EstimateDate;
                                f.ShippingRate = shippingModel.ShippingRate;
                            }

                        });

                        listwithRates.AddRange(upslist);
                    }
                    return new ShippingListModel { ShippingList = listwithRates.OrderBy(o => o.DisplayOrder).ToList() };
                }
                else
                    return new ShippingListModel { ShippingList = new List<ShippingModel>() };
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Failed to get shipping options.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error, ex);
                return new ShippingListModel { ShippingList = listwithRates?.Count() > 0 ? listwithRates.OrderBy(o => o.DisplayOrder).ToList() : GetFallBackOption(model) };
            }
        }
        private List<ShippingModel> GetFallBackOption(ShoppingCartModel model)
        {
            ShoppingCartMap SCM = new ShoppingCartMap();

            ZnodeShoppingCart znodeShoppingCart = SCM.ToZnodeShoppingCart(model);
            string countryCode = model.ShippingAddress.CountryName;
            List<ShippingModel> ShippingList = new List<ShippingModel>();
            List<ShippingModel> list = GetShippingList(model);
            ShippingModel _customFlatOption = list.Where(x => x.ShippingCode == "FLAT").FirstOrDefault();
            if (_customFlatOption != null)
            {
                model.Shipping.ShippingId = _customFlatOption.ShippingId;
                model.Shipping.ShippingName = _customFlatOption.ShippingCode;
                model.Shipping.ShippingCountryCode = string.IsNullOrEmpty(countryCode) ? _customFlatOption.DestinationCountryCode : countryCode;

                znodeShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
                ZnodeShippingManager shippingManager = new ZnodeShippingManager(znodeShoppingCart);

                shippingManager.Calculate();
                _customFlatOption.ShippingRate = znodeShoppingCart.ShippingCost;
                _customFlatOption.ApproximateArrival = znodeShoppingCart.ApproximateArrival;
                ShippingList.Add(_customFlatOption);
            }
            return ShippingList;
        }

        #region EmptyCart
        //public override  ShoppingCartModel GetShoppingCartDetails(CartParameterModel cartParameterModel, ShoppingCartModel cartModel = null)
        //{
           
        //    cartParameterModel.ProfileId = cartModel?.ProfileId ?? GetProfileId();          

        //    SetPublishCatalogId(cartParameterModel);

        //    Libraries.ECommerce.ShoppingCart.ZnodeShoppingCart znodeShoppingCart = ZnodeDependencyResolver.GetService<Libraries.ECommerce.ShoppingCart.IZnodeShoppingCart>().LoadFromDatabase(cartParameterModel);
        //    znodeShoppingCart.IsAllowWithOtherPromotionsAndCoupons = DefaultGlobalConfigSettingHelper.IsAllowWithOtherPromotionsAndCoupons;
        //    //to map cart model to znode shopping cart  
        //    BindCartModel(cartModel, znodeShoppingCart);

        //    IImageHelper imageHelper = ZnodeDependencyResolver.GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", znodeShoppingCart.PortalId.GetValueOrDefault()));

        //    //Map Libraries.ECommerce.ShoppingCart to ShoppingCartModel.
        //    ShoppingCartModel shoppingCartModel = _shoppingCartMap.ToModel(znodeShoppingCart, imageHelper);

        //    //Map null data with requesting cart model
        //    MapNullDataWithRequestingCartModel(cartModel, shoppingCartModel);

            
        //    //Get coupons if already applied.
        //    if (cartModel?.Coupons?.Count > 0)
        //        shoppingCartModel.Coupons = cartModel.Coupons;

            
        //    if (HelperUtility.IsNotNull(cartParameterModel.ShippingId))
        //    {
        //        IZnodeRepository<Libraries.Data.DataModel.ZnodeShipping> _shippingRepository = new ZnodeRepository<Libraries.Data.DataModel.ZnodeShipping>();

        //        //Check if Shipping is null or not,If null then get the shipping 
        //        //on the basis of ShippingId form ShoppingCartModel.
        //        Libraries.Data.DataModel.ZnodeShipping shipping = _shippingRepository.Table.FirstOrDefault(x => x.ShippingId == cartParameterModel.ShippingId);

        //        if (HelperUtility.IsNotNull(shipping))
        //        {
        //            shoppingCartModel.Shipping = new OrderShippingModel
        //            {
        //                ShippingId = shipping.ShippingId,
        //                ShippingDiscountDescription = shipping.Description,
        //                ShippingCountryCode = string.IsNullOrEmpty(cartParameterModel.ShippingCountryCode) ? string.Empty : cartParameterModel.ShippingCountryCode
        //            };
        //        }
        //    }

            
        //    //Check product inventory of the product for all type of product in cart-line item.
        //    if (HelperUtility.IsNotNull(shoppingCartModel?.ShoppingCartItems))
        //    {
        //        if (cartModel?.ShoppingCartItems?.Count > 0)
        //        {
        //            shoppingCartModel?.ShoppingCartItems.ForEach(cartItem =>
        //            {
        //                var lineItem = cartModel.ShoppingCartItems
        //                            .Where(cartLineItem => cartLineItem.SKU == cartItem.SKU && cartLineItem.AddOnProductSKUs == cartItem.AddOnProductSKUs)?.FirstOrDefault();
        //                cartItem.OmsOrderLineItemsId = (lineItem?.OmsOrderLineItemsId).GetValueOrDefault();
        //                cartItem.CartDescription = string.IsNullOrEmpty(cartItem.CartDescription) ? lineItem?.CartDescription : cartItem.CartDescription;

        //            });
        //        }

        //        //Single call instead multiple inventory check.
        //        CheckBaglineItemInventory(shoppingCartModel, cartParameterModel);
        //    }

        //    //Bind cookieMappingId, PortalId, LocaleId, CatalogId, UserId.
        //    BindCartData(shoppingCartModel, cartParameterModel);

        //    if (shoppingCartModel!=null && cartModel?.OmsOrderId > 0)
        //        shoppingCartModel.BillingAddress = cartModel.BillingAddress;

        //    IPIMAttributeService _pimAttributeService = ZnodeDependencyResolver.GetService<IPIMAttributeService>();
        //    //Binding Shoppingcart items' personalize attribute names.
        //    //TODO: _pimAttributeService.GetAttributeLocale will be called for each personalized attr of each shopping cart item.
        //    // So this code may perform slow if cart items are more quantity.
        //    shoppingCartModel.ShoppingCartItems.Where(s => s.PersonaliseValuesDetail != null && s.PersonaliseValuesDetail.Count > 0).ToList()
        //        .ForEach(x => x.PersonaliseValuesDetail
        //            .ForEach(p => p.PersonalizeName = _pimAttributeService.GetAttributeLocale(p.PersonalizeCode, shoppingCartModel.LocaleId)));

        //    ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    return shoppingCartModel;
        //}
        //public override ShoppingCartModel GetShoppingCart(CartParameterModel cartParameterModel)
        //{
        //    if (cartParameterModel.OmsOrderId!=null && cartParameterModel.OmsOrderId > 0)
        //    {
        //        return GetCartByOrderId(cartParameterModel);
        //    }
        //    else if (cartParameterModel.OmsQuoteId > 0)
        //    {
        //        return LoadCartForQuote(cartParameterModel);
        //    }
        //    else
        //    {
        //        return GetShoppingCartDetails(cartParameterModel);
        //    }
        //}

        //private void SetPublishCatalogId(CartParameterModel cartParameterModel)
        //{
        //    if (cartParameterModel.PublishedCatalogId == 0 && cartParameterModel.PortalId > 0)
        //    {
        //        int userAccountId = _userRepository.Table.FirstOrDefault(o => o.UserId == cartParameterModel.UserId).AccountId.GetValueOrDefault();
        //        if (userAccountId != 0)
        //        {
        //            int accountCatalogId = GetAccountCatalogId(userAccountId);
        //            cartParameterModel.PublishedCatalogId = accountCatalogId == 0 ? GetPublishedCatalogId(cartParameterModel.PortalId) : accountCatalogId;
        //        }
        //        else
        //        {
        //            cartParameterModel.PublishedCatalogId = GetPublishedCatalogId(cartParameterModel.PortalId);
        //        }
        //    }
        //}
        //private int GetAccountCatalogId(int userAccountId)
        //{
        //    return _accountRepository.Table.FirstOrDefault(o => o.AccountId == userAccountId).PublishCatalogId.GetValueOrDefault();
        //}
        //private int GetPublishedCatalogId(int portalId)
        //{
        //    return portalId > 0 ? _portalCatalogRepository.Table.FirstOrDefault(o => o.PortalId == portalId).PublishCatalogId : 0;
        //}
        #endregion

    }
}
