﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class RSPublishProductService : PublishProductService, IRSPublishProductService
    {
       
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly IPublishProductHelper publishProductHelper;       
        private readonly ISEOService _seoService;


        #region Constructor
        public RSPublishProductService() : base()
        {
            //_ProductMongoRepository = new MongoRepository<ProductEntity>();
            //_configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();
            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            //_categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _seoService = new SEOService();

        }
        #endregion

        /// <summary>
        /// Store Locations fetching functionality for Store Locator,PDP and Cart
        /// </summary>
        /// <param name="state"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public DataTable GetStoreLocations(string state, string sku)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@State", state, ParameterDirection.Input, SqlDbType.VarChar);
            objStoredProc.GetParameter("@SKU", sku, ParameterDirection.Input, SqlDbType.VarChar);

            DataSet data = objStoredProc.GetSPResultInDataSet("RS_GetStoreAddressDetails");

            DataTable storeLocationTable = data.Tables[0];

            return storeLocationTable;
        }

        /// <summary>
        /// Store Locations fetching functionality for Store Locator,PDP and Cart
        /// </summary>
        /// <param name="state"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public DataTable GetStoreLocationDetails(string state, string sku)
        {
            DataTable storeData = this.GetStoreLocations(state, sku);
            return storeData;
        }

        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                    publishProduct.Attributes.AddRange(parentPersonalizableAttributes);
            }

            return publishProduct;
        }
        /*To be checked if required or not*/
        //public override PublishProductListModel GetPublishProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page, ParameterKeyModel parameters)
        //{
        //    //  ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
        //    PublishProductListModel publishProductListModel = new PublishProductListModel();

        //    switch (parameters.ParameterKey)
        //    {
        //        //case ZnodeConstant.MongoId:
        //        //    int catalogId, portalId, localeId;
        //        //    GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

        //        //    ObjectId[] objectIDs = parameters.Ids.Split(',').Select(objectId => ObjectId.Parse(objectId)).ToArray();

        //        //    //Mongo product query by Object Id.
        //        //    IMongoRepository<ProductEntity> _productMongoRepository = new MongoRepository<ProductEntity>(GetCatalogVersionId());
        //        //    publishProductListModel.PublishProducts = new List<PublishProductModel>();
        //        //    publishProductListModel.PublishProducts = _productMongoRepository.Table.MongoCollection.Find(Query<ProductEntity>.In(pr => pr.Id, objectIDs))?.ToModel<PublishProductModel>()?.ToList();

        //        //    GetExpands(portalId, localeId, expands, publishProductListModel);
        //        //    //get expands associated to Product
        //        //    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProductListModel, localeId, GetLoginUserId(), GetProfileId());
        //        //    if (portalId > 0)
        //        //    {
        //        //        SetProductDetailsForList(portalId, publishProductListModel);
        //        //    }
        //        //    break;
        //        case ZnodeConstant.ZnodeCategoryIds:
        //            filters.Add(new FilterTuple(FilterKeys.ZnodeCategoryIds, FilterOperators.In, parameters.Ids));
        //            publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
        //            break;
        //        case ZnodeConstant.ProductSKU:
        //            filters.Add(new FilterTuple(FilterKeys.SKU, FilterOperators.In, "\"" + parameters.Ids + "\""));
        //            publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
        //            break;
        //        default:
        //            filters.Add(new FilterTuple(FilterKeys.ZnodeProductId, FilterOperators.In, parameters.Ids));
        //            publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
        //            break;
        //    }
        //    // ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
        //    return publishProductListModel;
        //}

       
        /// <summary>
        /// Overridden for complete PDP Swatch Image functionality is developed from here
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="publishProduct"></param>
        /// <param name="associatedProducts"></param>
        /// <param name="ConfigurableAttributeCodes"></param>
        /// <param name="catalogVersionId"></param>
        /// <returns></returns>
        /// protected virtual PublishProductModel GetDefaultConfiurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProduct, List<PublishProductModel> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        protected override PublishProductModel GetDefaultConfigurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProduct, List<PublishProductModel> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {         
            string sku = publishProduct.SKU;
            string parentSEOCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "SKU")?.AttributeValues;
            int categoryIds = publishProduct.ZnodeCategoryIds;
            List<PublishCategoryModel> categoryHierarchyIds = publishProduct.CategoryHierarchy;

            List<PublishAttributeModel> parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
            string parentConfigurableProductName = publishProduct.Name;
            int configurableProductId = publishProduct.PublishProductId;
            publishProduct = associatedProducts.FirstOrDefault();
            publishProduct.ConfigurableProductId = configurableProductId;
            publishProduct.ParentSEOCode = parentSEOCode;
            publishProduct.ConfigurableProductSKU = publishProduct.SKU;
            publishProduct.CategoryHierarchy = categoryHierarchyIds;

            publishProduct.SKU = sku;
            publishProduct.ZnodeCategoryIds = categoryIds;
            publishProduct.IsConfigurableProduct = true;
            publishProduct.ParentConfiguarableProductName = parentConfigurableProductName;

            catalogVersionId = Convert.ToInt32(catalogVersionId) > 0 ? catalogVersionId : GetCatalogVersionId();

            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId, WebstoreVersionId, GetProfileId());

            PublishAttributeModel defaultAttribute = publishProduct.Attributes?.FirstOrDefault(x => x.IsConfigurable);
            bool isChildPersonalizableAttribute = Convert.ToBoolean(publishProduct.Attributes?.Contains(publishProduct.Attributes?.FirstOrDefault(x => x.IsPersonalizable)));

            List<PublishAttributeModel> variants = publishProduct?.Attributes?.Where(x => x.IsConfigurable).ToList();
            Dictionary<string, string> selectedAttribute = GetSelectedAttributes(variants);

            //  List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(newassociatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, newassociatedProducts, ConfigurableAttributeCodes, portalId);
            List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(associatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, associatedProducts, ConfigurableAttributeCodes, portalId);

            foreach (PublishAttributeModel item in attributeList)
            {
                publishProduct.Attributes.RemoveAll(x => x.AttributeCode == item.AttributeCode);
            }

            publishProduct.Attributes.AddRange(attributeList);
            /*NIVI CODE*/
            string _swatchImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_97/";
            // ZnodeLogging.LogMessage("RSPublishProductService -_swatch Image Path line PDP379" + _swatchImagePath, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);

            List<AssociatedProductsModel> _products = (from p in associatedProducts

                                                       select new AssociatedProductsModel
                                                       {
                                                           PublishProductId = p.ConfigurableProductId != 0 ? p.ConfigurableProductId : p.PublishProductId,                                                          
                                                           SKU = p.ConfigurableProductSKU!=null?p.ConfigurableProductSKU:p.SKU,
                                                           OMSColorCode = p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage1")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText1")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage2")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText2")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage3")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText3")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage4")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText4")?.FirstOrDefault()?.AttributeValues + ",",
                                                           OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
                                                           OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           DisplayOrder = Convert.ToInt32(p.DisplayOrder),
                                                           OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
                                                           Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code,
                                                           Custom3 = "|" + p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code + "-" +
                                                                    p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code + "-" +
                                                                    p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
                                                       }).ToList();
            // ZnodeLogging.LogMessage("RSPublishProductService -_swatch Image Path line PDP415" + _products, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            publishProduct.AssociatedProducts = new List<AssociatedProductsModel>();
            publishProduct.AssociatedProducts.AddRange(_products);
            /*NIVI CODE*/
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);

        }
       
        /// <summary>
        /// Overridden to copy parent brand to child for schema.org
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        public override PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Input Parameters publishProductId:", string.Empty, TraceLevel.Info, publishProductId);

            bool isChildPersonalizableAttribute = false;
            List<PublishAttributeModel> parentPersonalizableAttributes = null;
            int portalId, localeId;
            int? catalogVersionId;
            PublishProductModel publishProduct = null;
            List<PublishProductModel> products;
            string parentProductImageName = null;

            //Get publish product 
            products = GetPublishProductFromPublishedData(publishProductId, filters, out portalId, out localeId, out catalogVersionId, publishProduct, out products);

            List<int> associatedCategoryIds = new List<int>();

            if (HelperUtility.IsNotNull(products) && products.Count > 0)
            {
                List<int> categoryIds = products.Select(x => x.ZnodeCategoryIds)?.ToList();

                FilterCollection filter = new FilterCollection();
                if (categoryIds?.Count > 0)
                    filter.Add("ZnodeCategoryId", FilterOperators.In, string.Join(",", categoryIds));
                filter.Add("IsActive", FilterOperators.Equals, ZnodeConstant.TrueValue);
                filter.Add("VersionId", FilterOperators.Equals, catalogVersionId.ToString());

                List<PublishedCategoryEntityModel> categoryEntities = ZnodeDependencyResolver.GetService<IPublishedCategoryDataService>().GetPublishedCategoryList(new PageListModel(filter, null, null))?.ToModel<PublishedCategoryEntityModel>()?.ToList();

                associatedCategoryIds.AddRange(categoryEntities.Select(x => x.ZnodeCategoryId));

                //If no category associated to product then perform else part
                if (associatedCategoryIds?.Count > 0)
                    publishProduct = products.FirstOrDefault(x => associatedCategoryIds.Contains(x.ZnodeCategoryIds));
                else
                    publishProduct = products?.FirstOrDefault();

                parentProductImageName = publishProduct?.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
            }

            if (HelperUtility.IsNotNull(publishProduct))
            {
                PublishedConfigurableProductEntityModel configEntiy = GetConfigurableProductEntity(publishProductId, catalogVersionId);

                if (HelperUtility.IsNotNull(configEntiy))
                {
                    //Selecting child SKU
                    IEnumerable<string> childSKU = publishProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();
                    //Get Associated product by child SKUs
                    List<PublishProductModel> associatedProducts = GetAssociatedConfigurableProducts(localeId, catalogVersionId, childSKU);

                    parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();

                    publishProduct = GetDefaultConfigurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy.ConfigurableAttributeCodes, catalogVersionId.GetValueOrDefault());
                }
                else
                    //Get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault(), WebstoreVersionId, GetProfileId());

                isChildPersonalizableAttribute = publishProduct.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                GetProductImagePath(portalId, publishProduct, true, parentProductImageName);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(portalId, publishProduct);

                publishProduct.ZnodeProductCategoryIds = associatedCategoryIds;
                /*NIVI CODE*/
                publishProduct.Attributes.RemoveAll(x => x.AttributeCode == "Brand");
                PublishAttributeModel publishAttributeModel = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "Brand");

                if (publishAttributeModel != null)
                {
                    publishProduct.Attributes.Add(publishAttributeModel);
                }
                /*NIVI CODE*/
            }
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);
        }
            

        private List<PublishProductModel> GetPublishProductFromPublishedData(int publishProductId, FilterCollection filters, out int portalId, out int localeId, out int? catalogVersionId, PublishProductModel publishProduct, out List<PublishProductModel> products)
        {
            //Get parameter values from filters.
            int catalogId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            //get catalog current version id by catalog id.
            catalogVersionId = GetCatalogVersionId(catalogId);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

            if (catalogVersionId > 0)
                filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");

            publishProduct = null;

            //Get publish product
            products = ZnodeDependencyResolver.GetService<IPublishedProductDataService>().GetPublishProducts(new PageListModel(filters, null, null)).ToModel<PublishProductModel>().ToList();

            return products;
        }
       
    }
}
