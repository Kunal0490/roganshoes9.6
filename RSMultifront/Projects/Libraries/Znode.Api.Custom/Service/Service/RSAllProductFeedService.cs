﻿using System;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service
{
    public class RSAllProductFeedService : IRSAllProductFeedService
    {
        private RSProductFeedService rsProductFeedService = new RSProductFeedService();
        public bool CreateGoogleSiteMapforall()
        {
            
            ProductFeedModel model = new ProductFeedModel();
            model.PortalId = new int[1] { 0 };
            model.LocaleId = 1;
            model.Date = DateTime.Now.ToString();
            model.SuccessXMLGenerationMessage = ZnodeApiSettings.ZnodeApiRootUri;// "http://localhost:44762/";          
            model.ProductFeedPriority = 1;
            model.ProductFeedTimeStampName = "Use the database update date";
            model.ProductFeedTypeCode = "XmlSiteMap";
            model.Stores = "0";

            model.FileName = "RSContent";
            model.ProductFeedSiteMapTypeCode = "Content";
            bool flag = true;
            ProductFeedModel ContentFeed = rsProductFeedService.CreateGoogleSiteMap(model);
            if (ContentFeed == null)
            {
                return false;
            }
            model.FileName = "RSCategory";
            model.ProductFeedSiteMapTypeCode = "Category";
            ProductFeedModel CategoryFeed = rsProductFeedService.CreateGoogleSiteMap(model);
            if (CategoryFeed == null)
            {
                return false;
            }
            model.FileName = "RSProduct";
            model.ProductFeedSiteMapTypeCode = "Product";
            ProductFeedModel productFeed = rsProductFeedService.CreateGoogleSiteMap(model);
            if (productFeed == null)
            {
                return false;
            }
            return flag;
        }
    }
}
