﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Engine.Exceptions;
using System.Collections;
using System.Net;
using System.IO;
using System.Web;
using System.Configuration;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Resources;
using ApplicationUser = Znode.Engine.Services.ApplicationUser;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Data.Helpers;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Script.Serialization;

namespace Znode.Api.Custom.Service.Service
{
    public class RSUserService: UserService
    {
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodeUserProfile> _accountProfileRepository;
        private readonly IZnodeRepository<ZnodeUserPortal> _userPortalRepository;
        private readonly IZnodeRepository<ZnodePortalProfile> _portalProfileRepository;
        private readonly IZnodeRepository<ZnodeProfile> _profileRepository;
        private readonly IZnodeRepository<ZnodeDepartmentUser> _departmentUserRepository;
        private readonly IZnodeRepository<ZnodeAccountProfile> _accountAssociatedProfileRepository;
        private readonly IZnodeRepository<ZnodeAccountPermissionAccess> _accountPermissionAccessRepository;
        private readonly IZnodeRepository<ZnodeAccountPermission> _accountPermissionRepository;

        string ResponseAPIKey= ConfigurationManager.AppSettings["GetResponseAPIKey"];
        string ResAPIUrl = ConfigurationManager.AppSettings["GetResponseAPIURL"];
        string CampaignName = ConfigurationManager.AppSettings["CampaignName"];
        UserLoginHelper LoginHelper = new UserLoginHelper();
        #region Constructor

        public RSUserService() : base()
        {
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _accountProfileRepository = new ZnodeRepository<ZnodeUserProfile>();
            _userPortalRepository = new ZnodeRepository<ZnodeUserPortal>();
            _portalProfileRepository = new ZnodeRepository<ZnodePortalProfile>();
            _profileRepository = new ZnodeRepository<ZnodeProfile>();
            _departmentUserRepository = new ZnodeRepository<ZnodeDepartmentUser>();
            _accountAssociatedProfileRepository = new ZnodeRepository<ZnodeAccountProfile>();
            _accountPermissionAccessRepository = new ZnodeRepository<ZnodeAccountPermissionAccess>();
            _accountPermissionRepository = new ZnodeRepository<ZnodeAccountPermission>();
           
        }

        #endregion Constructor

        public void GetNewsLetterCampaign(string Email)
        {
            if (ResponseAPIKey != null && ResAPIUrl != null)
            {              
                string newsLetterCampaign = CampaignName;
                if (!string.IsNullOrEmpty(newsLetterCampaign))
                {
                    string campaignID = AddContactEmailSubcriber(ResponseAPIKey, ResAPIUrl,
                        newsLetterCampaign, Email.Trim(), Email.Trim());
                }
            }
        }       
        public override bool UpdateUserData(UserModel userModel, bool webStoreUser)
        {
            bool returnParameter = base.UpdateUserData(userModel, webStoreUser);

            if (returnParameter)
            {
                if (userModel.EmailOptIn)   //Add to GetResponse
                {
                    try
                    {
                        string responseContactID = string.Empty;
                      
                        if (ResponseAPIKey != null && ResAPIUrl != null)
                        {

                            //string newsLetterCampaign = GetCompaignNameByPortal();
                            string newsLetterCampaign = CampaignName;
                            if (!string.IsNullOrEmpty(newsLetterCampaign))
                            {
                                string campaignID = AddContactEmailSubcriber(ResponseAPIKey, ResAPIUrl,
                                    newsLetterCampaign, userModel.Email.Trim(), userModel.Email.Trim());

                                //string contactid = emailSubscriber.GetContactEmailSubcriber(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), campaignID, Email.Text.Trim());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, "RSUserService:UpdateUserData ", TraceLevel.Error);
                    }
                }
                else  //Unsubscribe from GetResponse
                {
                    try
                    {
                        if (ResponseAPIKey != null && ResAPIUrl != null)
                        {
                            string contactID = userModel.Custom4;                          
                            if (!String.IsNullOrEmpty(contactID))
                                UnsubscribeContactEmail(ResponseAPIKey, ResAPIUrl, contactID);
                        }
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, "RSUserService:UpdateUserData ", TraceLevel.Error);
                    }

                }
            }
            return returnParameter;
        }

        public override bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            bool Isvalid = base.SignUpForNewsLetter(model);
            if (Isvalid)
            {
                try
                {
                    GetNewsLetterCampaign(model.Email);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, "RSUserService:SignUpForNewsLetter ", TraceLevel.Error);
                }
            }
            return Isvalid;
        }
        // your API key
        // available at http://www.getresponse.com/my_api_key.html
        //String api_key = "8fbc4194dafbba2ea141e4c45e440d98";

        // API 2.x URL
        //String api_url = "http://api2.getresponse.com";

        /// <summary>
        /// Add Contact in GetResponse Email marketing
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaignName"></param>
        /// <param name="contactName"></param>
        /// <param name="subscriberEmail"></param>
        /// <returns></returns>
        public string AddContactEmailSubcriber(string api_key, string api_url, string campaignName, string contactName, string subscriberEmail)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 1;
            // set method name
            _request["method"] = "get_campaigns";
            // set conditions
            Hashtable operator_obj = new Hashtable();
            operator_obj["EQUALS"] = campaignName;
            Hashtable name_obj = new Hashtable();
            name_obj["name"] = operator_obj;
            // set params request object
            object[] params_array = { api_key, name_obj };
            _request["params"] = params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            String response_string = null;

            try
            {
                // call method 'get_messages' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                //Elmah.ElmahErrorManager.Log(e.StackTrace);
                ZnodeLogging.LogMessage(e, "RSUserService:AddContactEmailSubscriber ", TraceLevel.Error);
            }

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;

            string campaign_id = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                campaign_id = key.ToString();
            }

            // add contact to 'sample_marketing' campaign
            // new request object
            _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 2;

            // set method name
            _request["method"] = "add_contact";

            Hashtable contact_params = new Hashtable();
            contact_params["campaign"] = campaign_id;
            contact_params["name"] = contactName;
            contact_params["email"] = subscriberEmail;

      

            // set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;

            try
            {
                // call method 'add_contact' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                //Elmah.ElmahErrorManager.Log(e.StackTrace);
                ZnodeLogging.LogMessage(e, "RSUserService:AddContactEmailSubscriber ", TraceLevel.Error);
            }
            return campaign_id;
           
        }

        /// <summary>
        /// Get Contact detail from GetResponse
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaign_id"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GetContactEmailSubcriber(string api_key, string api_url, string campaign_id, string email)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 3;

            // set method name
            _request["method"] = "get_contacts";

            Hashtable contact_params = new Hashtable();
            contact_params["campaigns"] = campaign_id;

            Hashtable contact_params1 = new Hashtable();
            contact_params1["EQUALS"] = email;

            Hashtable name_obj1 = new Hashtable();
            name_obj1["email"] = contact_params1;

            // set params request object
            object[] add_contact_params_array = { api_key, name_obj1, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            string response_string = null;

            try
            {
                // call method 'get_contacts' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                //Elmah.ElmahErrorManager.Log(e.StackTrace);
                ZnodeLogging.LogMessage(e, "RSUserService:GetContactEmailSubscriber ", TraceLevel.Error);
            }

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
            string contactID = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                contactID = key.ToString();
            }
            return contactID;
        }

        /// <summary>
        /// Delete (Unsubscribe) contact from GetResponse
        /// </summary>
        /// <param name="contactID"></param>
        public void UnsubscribeContactEmail(string api_key, string api_url, string contactID)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String response_string = null;
            // new request object
            Hashtable _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 4;

            // set method name
            _request["method"] = "delete_contact";

            Hashtable contact_params = new Hashtable();
            contact_params["contact"] = contactID;

            // set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;

            try
            {
                // call method 'delete_contact' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                //Elmah.ElmahErrorManager.Log(e.StackTrace);
                ZnodeLogging.LogMessage(e, "RSUserService:UnsubscribeContactEmail ", TraceLevel.Error);
            }
        }




        //public override UserModel CreateCustomer(int portalId, UserModel model)
        //{
        //    ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId, AccountId = model.AccountId });

        //    if (IsNull(model))
        //        throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

        //    //Set the email as an username for external login.
        //    if (model.IsSocialLogin)
        //        SetUserNameForSocialLoginUser(model);

        //    //Create geust user account.
        //    if (model.IsGuestUser)
        //        return CreateGuestUserAccount(model);

        //    model.PortalId = DefaultGlobalConfigSettingHelper.AllowGlobalLevelUserCreation ? (int?)null : model.PortalId;

        //    //Save the user login Details.
        //    ApplicationUser user = CreateOwinUser(model);
        //    if (IsNotNull(user))
        //    {
        //        //Save the user Account, Profile Details 
        //        ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        //        UserModel userdataModel = InsertUserData(model, user, model.User.Password);

        //        //Avoid log password for user created from admin site,For forcing user to reset the password for the first time login.
        //        SetRequestHeaderForPasswordLog(null, userdataModel.UserId);
        //        ZnodeUserAccountBase.LogPassword(Convert.ToString(userdataModel.AspNetUserId), model.User.Password);
        //        //Save recently password change date.
        //        ZnodeUserAccountBase.SetPasswordChangedDate(userdataModel.AspNetUserId);
        //        return userdataModel;

        //    }
        //    else
        //        throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.LoginInalreadyExist);
        //}

        private ApplicationUser CreateOwinUser(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (IsNotNull(model) && IsNotNull(model.User))
            {
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { User = model?.User, AccountId = model?.AccountId });

                //If the user belongs to account get the portal id of the account.
                if (model.AccountId > 0)
                    model.PortalId = GetAccountPortalId(model.AccountId.GetValueOrDefault());

                //Check whether the Registerd default profile present for the portal.
                CheckRegisterProfile(model);

                model.Email = string.IsNullOrEmpty(model.User.Email) ? model.Email : model.User.Email;
                if (model.User.Username?.Any() ?? false)
                {
                    
                    //Check if the user name already exists or not.
                    if (LoginHelper.IsUserExists(model.User.Username, model.PortalId.GetValueOrDefault()))
                        throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.LoginNamesAlreadyExist);

                    //Create the User in Znode Mapper to allow portal level user creation.
                    AspNetZnodeUser znodeUser = LoginHelper.CreateUser(model);
                    if (IsNotNull(znodeUser) && !string.IsNullOrEmpty(znodeUser.AspNetZnodeUserId))
                    {
                        string znodeUserName = znodeUser.AspNetZnodeUserId;
                        ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { znodeUserName = znodeUserName });

                        ApplicationUser user = new ApplicationUser { UserName = znodeUserName, Email = model.User.Email };

                        //Get the Password for the user.
                        string password = GetPassword(model);
                        IdentityResult result = UserManager.Create(user, password);
                        if (result.Succeeded)
                        {
                            user = UserManager.FindByName(znodeUserName);

                            //Save recently password change date.
                            ZnodeUserAccountBase.SetPasswordChangedDate(user.Id);
                            model.User.UserId = user.Id;
                            model.AspNetUserId = user.Id;
                            model.User.Password = password;
                            if (IsNotNull(model.ExternalLoginInfo?.Login))
                            {
                                result = UserManager.AddLogin(user.Id, model.ExternalLoginInfo.Login);
                                if (result.Succeeded)
                                    SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                            }
                            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                            return user;
                        }
                    }
                    throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.LoginNamesAlreadyExist);
                }
            }
            return null;
        }

        private void SetAssignedPortal(UserModel userModel, bool isCustomerEditMode)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Check for userId greater than 0 and sent form customer edit mode.
            if (userModel?.UserId > 0 && isCustomerEditMode)
                SetPortal(userModel);

            if (userModel?.UserId < 1 && !isCustomerEditMode && userModel.PortalIds.Any())
                userModel.PortalId = Convert.ToInt32(userModel.PortalIds.FirstOrDefault());
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private bool SendEmailToStoreAdministrator(string firstName, string newPassword, string email, int portalId, int localeId, bool isCustomerUser = false, bool isWebstoreUser = false, bool isUserCreateFromAdmin = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId, email = email });

            string emailTemplateCode = isWebstoreUser ? "NewUserAccountWebstore" : "NewUserAccount";
            //Method to get Email Template Details by Code.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(emailTemplateCode, portalId, localeId);

            if (IsNotNull(emailTemplateMapperModel))
            {
                string subject = emailTemplateMapperModel?.Subject;
                string senderEmail, messageText;
                //Generate Email Message Content Based on the Email Template.
                GenerateActivationEmail(firstName, newPassword, email, emailTemplateMapperModel.Descriptions, portalId, isCustomerUser, out senderEmail, ref subject, out messageText);
                try
                {
                    //We can set email to bcc user on the basis of emailTemplateMapperModel.IsEnableBcc flag
                    if (!isUserCreateFromAdmin)
                        ZnodeEmail.SendEmail(portalId, email, senderEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, string.Empty), subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, firstName, string.Empty, null, ex.Message, null);
                    return false;
                }
                return true;
            }
            return false;
        }

        private int? GetAccountPortalId(int accountId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { accountId = accountId });

            IZnodeRepository<ZnodePortalAccount> _portalAccountRepository = new ZnodeRepository<ZnodePortalAccount>();
            return _portalAccountRepository.Table.FirstOrDefault(x => x.AccountId == accountId)?.PortalId;
        }

        private void CheckRegisterProfile(UserModel model)
        {
            ZnodeLogging.LogMessage("Profile Id:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProfileId);
            if (model.ProfileId <= 0)
            {
                int portalId = model.PortalId > 0 ? model.PortalId.Value : PortalId;

                //Check the Registered Profile for the Portal.
                int profileId = GetCurrentProfileId(portalId);
                if (profileId <= 0)
                    throw new ZnodeException(ErrorCodes.ProfileNotPresent, Admin_Resources.ErrorUserAccountDeleteConfigureDefaultProfile);
                model.ProfileId = profileId;
            }
        }

        private int GetCurrentProfileId(int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });
            int profileID = 0;
            //Get the Register Profile Details based on the portalId.
            ProfileModel profile = GetDefaultRegisteredProfile(portalId);

            if (HelperUtility.IsNotNull(profile))
                profileID = profile.ProfileId;
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return profileID;
        }

        private ProfileModel GetDefaultRegisteredProfile(int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            //Get the Default Anonymous Profile Id associated to the Portal.
            var portalProfile = _portalProfileRepository.Table.Where(x => x.PortalId == portalId && x.IsDefaultRegistedProfile)?.FirstOrDefault();

            //Get the Profile based on the Profile Id.
            if (IsNotNull(portalProfile))
                return _profileRepository.Table.Where(x => x.ProfileId == portalProfile.ProfileId)?.FirstOrDefault()?.ToModel<ProfileModel>();

            return null;
        }

        private void GenerateActivationEmail(string firstName, string newPassword, string email, string templateContent, int portalId, bool isCustomerUser, out string senderEmail, ref string subject, out string messageText)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            string portalName = ZnodeConfigManager.SiteConfig.StoreName;
            senderEmail = ZnodeConfigManager.SiteConfig.AdminEmail;
            ZnodeLogging.LogMessage("Portal Name:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, portalName);

            string vendorLoginUrl = $"{HttpContext.Current.Request.Url.Scheme}://{ZnodeConfigManager.DomainConfig.DomainName}";
            if (vendorLoginUrl == $"{HttpContext.Current.Request.Url.Scheme}://")
                vendorLoginUrl = ZnodeApiSettings.AdminWebsiteUrl;

            //Get Portal details as well as the Domain Url based on the WEbStore Application Type.
            PortalModel model = GetCustomPortalDetails(portalId);

            //Get Portal Details in case of Customer User
            if (isCustomerUser && IsNotNull(model))
            {
                portalName = model.StoreName;
                senderEmail = model.AdministratorEmail;
                vendorLoginUrl = $"{HttpContext.Current.Request.Url.Scheme}://{model.DomainUrl}";
            }
            subject = $"{portalName} - {subject}";

            //Set Parameters for the Email Templates to be get replaced.
            EmailTemplateHelper.SetParameter("#FirstName#", firstName);
            EmailTemplateHelper.SetParameter("#UserName#", email);
            EmailTemplateHelper.SetParameter("#Url#", vendorLoginUrl);
            EmailTemplateHelper.SetParameter("#Password#", newPassword);
            EmailTemplateHelper.SetParameter("#StoreLogo#", model.StoreLogo);

            //Replace the Email Template Keys, based on the passed email template parameters.
            messageText = EmailTemplateHelper.ReplaceTemplateTokens(templateContent);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

        }

        private void SetPortal(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //If the user belongs to the account then fetch the account's portal Id otherwise user portal id.
            if (userModel.AccountId > 0)
                userModel.PortalId = GetAccountPortalId(Convert.ToInt32(userModel.AccountId));
            else
            {
                userModel.PortalIds = _userPortalRepository.Table.Where(x => x.UserId == userModel.UserId)?.Select(x => x.PortalId.ToString())?.ToArray();
                userModel.PortalId = string.IsNullOrEmpty(userModel.PortalIds?.FirstOrDefault()) ? (int?)null : Convert.ToInt32(userModel.PortalIds?.FirstOrDefault());
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private static string GetPassword(UserModel model)
            => IsNull(model.User) ? GenerateNewPassword() : (string.IsNullOrEmpty(model.User.Password)) ? GenerateNewPassword() : model.User.Password;

        private static string GenerateNewPassword()
          => GetUniqueKey(8);

        private void SetUserNameForSocialLoginUser(UserModel model)
        {
            
            //If the user not exits by email then store email as a username else not.
            if (!string.IsNullOrEmpty(model.User.Email) && !LoginHelper.IsUserExists(model.Email, model.PortalId.GetValueOrDefault()))
                model.User.Username = model.Email;
        }

        private UserModel CreateGuestUserAccount(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = userModel?.AccountId });

            //Save account details.
            ZnodeUser account = _userRepository.Insert(userModel?.ToEntity<ZnodeUser>());
            if (account?.UserId > 0 && IsNotNull(userModel))
            {
                userModel.UserId = account.UserId;
                ZnodeLogging.LogMessage(account.UserId > 0 ? Admin_Resources.SuccessUserDataSave : Admin_Resources.ErrorUserDataSave, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                //Insert portal in UserPortal table.
                _userPortalRepository.Insert(new ZnodeUserPortal { UserId = account.UserId, PortalId = userModel.PortalId });

                //Insert profile in AccountProfile table.
                _accountProfileRepository.Insert(new ZnodeUserProfile { UserId = account.UserId, ProfileId = userModel.ProfileId, IsDefault = true });
                ZnodeLogging.LogMessage("User Id:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, userModel?.UserId);
                return account.ToModel<UserModel>();

            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return new UserModel();
        }

        private UserModel InsertUserData(UserModel model, ApplicationUser user, string password)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            bool isUserCreateFromAdmin = false;
            //Save account details.
            ZnodeUser account = _userRepository.Insert(model.ToEntity<ZnodeUser>());
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { UserId = account?.UserId });

            if (account?.UserId > 0)
            {
                model.UserId = account.UserId;
                ZnodeLogging.LogMessage(account?.UserId > 0 ? Admin_Resources.SuccessUserDataSave : Admin_Resources.ErrorUserDataSave, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                if (!LoginHelper.IsAccountCustomer(model))
                    _accountProfileRepository.Insert(new ZnodeUserProfile { UserId = account.UserId, ProfileId = model.ProfileId, IsDefault = true });

                //Create B2B Customer.
                CreateB2BCustomer(model, account, user);

                //Insert for the user portal.
                InsertIntoUserPortal(model.PortalId, account.UserId, false);

                // Gets the portal ids.
                SetAssignedPortal(model, true);
                //Create link for new register user which is register from admin side.
                //For that we send new admin user 
                if (!model.IsWebStoreUser)
                {
                    isUserCreateFromAdmin = true;
                    ForgotPassword((model.PortalId > 0) ? model.PortalId.Value : PortalId, model, isUserCreateFromAdmin);
                }
                //If firstName is empty,then bind username (at the time of new user creation from webstore).
                model.FirstName = model?.FirstName ?? model?.User?.Username;
                //map account model to account entity.
                if (!SendEmailToStoreAdministrator(model.FirstName, password, account.Email, (model.PortalId > 0) ? model.PortalId.Value : PortalId, model.LocaleId, true, model.IsWebStoreUser, isUserCreateFromAdmin))
                {
                    UserModel accountModel = UserMap.ToModel(account, user.Email, !string.IsNullOrEmpty(model.User?.Username) ? model.User.Username : model.UserName);
                    accountModel.IsEmailSentFailed = true;
                    return accountModel;
                }
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return UserMap.ToModel(account, user.Email, !string.IsNullOrEmpty(model.User.Username) ? model.User?.Username : model.UserName);
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorUserDataSave, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                throw new ZnodeException(ErrorCodes.ExceptionalError, Admin_Resources.ErrorUserCreate);
            }
        }

        private void SetRequestHeaderForPasswordLog(string AspNetUserId = null, int userId = 0)
        {
            if (HelperMethods.GetLoginUserId() <= 0)
            {
                if (!string.IsNullOrEmpty(AspNetUserId))
                {
                    userId = _userRepository.Table.Where(x => x.AspNetUserId == AspNetUserId).Select(x => x.UserId).FirstOrDefault();
                }
                if (userId > 0)
                {
                    HttpContext.Current.Request.Headers.Add("Znode-UserId", Convert.ToString(userId));
                }
            }
        }

        private void CreateB2BCustomer(UserModel model, ZnodeUser account, ApplicationUser user)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Checks if role name is null, then create normal user with role Customer, else create admin user.
            if (LoginHelper.IsAccountCustomer(model))
            {
                InsertIntoUserDepartment(model);

                //Insert all the profiles of the account to which user is associated.
                InsertUserProfile(model, account);

                //Inserts into AspnetUserRole
                UserManager.AddToRole(user.Id, model.RoleName);
            }
            else
                UserManager.AddToRole(user.Id, ZnodeRoleEnum.Customer.ToString());

            if (string.IsNullOrEmpty(model.PermissionCode))
            {
                model.PermissionCode = ZnodePermissionCodeEnum.DNRA.ToString();
                model.AccountPermissionAccessId = GetDNRAAccountPermissionAccessId();
            }

            //Insert the access permission for user.
            InsertAccessPermissionForUser(model, account);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private void InsertIntoUserDepartment(UserModel model, bool isUpdate = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model.AccountId });

            if (isUpdate)
            {
                //If record exists then return.
                if (_departmentUserRepository.Table.Any(x => x.UserId == model.UserId && x.DepartmentId == model.DepartmentId))
                    return;

                //Delete the existing entry.
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeDepartmentUserEnum.UserId.ToString(), ProcedureFilterOperators.Equals, model.UserId.ToString()));

                ZnodeLogging.LogMessage(_departmentUserRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClause(filters.ToFilterDataCollection()))
                    ? string.Format(Admin_Resources.SuccessDeleteUserDepartment, model.DepartmentName) : string.Format(Admin_Resources.ErrorDeleteUserDepartment, model.DepartmentName), ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            }

            if (model.DepartmentId > 0)
                ZnodeLogging.LogMessage(_departmentUserRepository.Insert(GetDepartmentUserEntity(model))?.DepartmentUserId > 0
                    ? string.Format(Admin_Resources.SuccessInsertUserDepartment, model.DepartmentName) : string.Format(Admin_Resources.ErrorInsertUserDepartment, model.DepartmentName), ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private void InsertUserProfile(UserModel model, ZnodeUser account)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (account.AccountId > 0)
            {
                List<ZnodeAccountProfile> associatedProfile = _accountAssociatedProfileRepository.Table.Where(x => x.AccountId == account.AccountId).ToList();

                if (associatedProfile?.Count > 0)
                {
                    //Get all the profiles of user associated account.
                    List<int?> accountProfileIds = associatedProfile.Select(x => x.ProfileId).ToList();
                    ZnodeLogging.LogMessage("accountProfileIds list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, accountProfileIds?.Count());
                    if (accountProfileIds?.Count > 0)
                    {
                        int defaultProfile = associatedProfile.Where(x => x.IsDefault)?.FirstOrDefault()?.ProfileId ?? 0;
                        ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { defaultProfile = defaultProfile });

                        //Create entities to insert
                        List<ZnodeUserProfile> entiesToinsert = new List<ZnodeUserProfile>();
                        foreach (int? item in accountProfileIds)
                            entiesToinsert.Add(new ZnodeUserProfile() { UserId = account.UserId, ProfileId = Convert.ToInt32(item), IsDefault = false });

                        //If account profile contains a profile already present in user then simply set it as IsDefault else add it to the entities to be inserted.
                        if (defaultProfile > 0 && accountProfileIds.Contains(defaultProfile))
                            entiesToinsert.Where(x => x.ProfileId == defaultProfile).ToList().ForEach(x => x.IsDefault = true);
                        else
                            entiesToinsert.Add(new ZnodeUserProfile() { UserId = account.UserId, ProfileId = defaultProfile > 0 ? defaultProfile : model.ProfileId, IsDefault = true });
                        ZnodeLogging.LogMessage("entiesToinsert list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, entiesToinsert?.Count());

                        ZnodeLogging.LogMessage(IsNotNull(_accountProfileRepository.Insert(entiesToinsert)) ? Admin_Resources.SuccessUserProfileInsert : Admin_Resources.ErrorUserProfileInsert, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                    }
                }
            }
            else
            {
                _accountProfileRepository.Insert(new ZnodeUserProfile { UserId = account.UserId, ProfileId = model.ProfileId, IsDefault = true });
            }
        }

        private ZnodeDepartmentUser GetDepartmentUserEntity(UserModel userModel)
           => new ZnodeDepartmentUser { DepartmentId = userModel.DepartmentId, DepartmentUserId = Convert.ToInt32(userModel.DepartmentUserId), UserId = userModel.UserId };
        private int? GetDNRAAccountPermissionAccessId()
            => Convert.ToInt32(_accountPermissionAccessRepository.Table.Join(_accountPermissionRepository.Table.Where(o => !o.AccountId.HasValue && o.AccountPermissionName == ZnodeConstant.DNRAAccountPermissionAccessName),
                                                                                 o => o.AccountPermissionId,
                                                                                 ob => ob.AccountPermissionId,
                                                                                 (o, ob) => o)?.FirstOrDefault()?.AccountPermissionAccessId);
        private void InsertIntoUserPortal(int? portalId, int userId, bool isUpdate)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId, userId = userId });

            //if the global level user creation is set to true then set portal id to null.
            if (DefaultGlobalConfigSettingHelper.AllowGlobalLevelUserCreation)
                portalId = null;

            //If the call is from update then delete the existing mapping.
            if (isUpdate)
            {
                if (_userPortalRepository.Table.Any(x => x.UserId == userId && x.PortalId == portalId))
                    return;

                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeUserPortalEnum.UserId.ToString(), FilterOperators.Equals, userId.ToString()));
                EntityWhereClauseModel whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                //Delete the existing mapping.
                _userPortalRepository.Delete(whereClause.WhereClause, whereClause.FilterValues);
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            }

            //Inserts into user portal.
            ZnodeLogging.LogMessage(_userPortalRepository.Insert(new ZnodeUserPortal { UserId = userId, PortalId = portalId > 0 ? portalId : (int?)null })?.UserPortalId > 0
                ? Admin_Resources.SuccessUserPortalInsert : Admin_Resources.ErrorUserPortalInsert, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }
    }
}
