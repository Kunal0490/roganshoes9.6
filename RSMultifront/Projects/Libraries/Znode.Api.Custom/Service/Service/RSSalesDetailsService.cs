﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Custom.Service.Service
{
    public class RSSalesDetailsService : BaseService, IRSSalesDetailsService
    {
        public virtual List<RSSalesDetailsModel> GetSalesDetails()
        {
            IZnodeViewRepository<RSSalesDetailsModel> objStoredProc = new ZnodeViewRepository<RSSalesDetailsModel>();
            List<RSSalesDetailsModel> salesdetails = objStoredProc.ExecuteStoredProcedureList("RS_GetSalesDetails").ToList();
            return salesdetails;
        }
    }
}
