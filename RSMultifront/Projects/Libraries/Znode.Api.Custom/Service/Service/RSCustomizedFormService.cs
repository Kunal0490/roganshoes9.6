﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Custom.Service.Service
{
   public class RSCustomizedFormService  : BaseService, IRSCustomizedFormService
    {
        public virtual bool ScheduleATruck(ScheduleATruckModel model)
        {

            CreateScheduleATruck(model);
            /*start mail to admin*/
            PortalModel Portalmodel = GetCustomPortalDetails(ZnodeConfigManager.SiteConfig.PortalId);
            string senderEmail = Portalmodel?.AdministratorEmail;
            string receiverEmail = Portalmodel?.AdministratorEmail;
            /*End send mail to admin*/

            //string senderEmail = model.EmailID;
            //string receiverEmail = model.EmailID;
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            //string subject = $"{senderEmail.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";
            string subject = "Schedule A Truck";
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ScheduleATruck", (Portalmodel?.PortalId > 0) ? Convert.ToInt32(Portalmodel?.PortalId) : PortalId, 1);
            if (HelperUtility.IsNull(emailTemplateMapperModel))
                throw (new Exception("Not Implemented"));
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);
                string messageText = GetEmailTemplate(model, receiptHelper);
                return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.EmailID, true);
            }
        }

        public virtual bool ShoeFinder(ShoesFinderModel model)
        {
            CreateShoeFinder(model);
            /*start mail to admin*/
            PortalModel Portalmodel = GetCustomPortalDetails(ZnodeConfigManager.SiteConfig.PortalId);
            string senderEmail = Portalmodel?.AdministratorEmail;
            string receiverEmail = Portalmodel?.AdministratorEmail;
            /*End send mail to admin*/
            //string senderEmail = model.EmailID;
            //string receiverEmail = model.EmailID;
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            //string subject = $"{senderEmail.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";
            string subject = "Shoe Finder";
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ShoeFinder", (Portalmodel?.PortalId > 0) ? Convert.ToInt32(Portalmodel?.PortalId) : PortalId, 1);
            if (HelperUtility.IsNull(emailTemplateMapperModel))
                throw (new Exception("Not Implemented"));
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);

                string messageText = GetEmailTemplate(model, receiptHelper);
               
                return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.EmailID, true);
                
            }
            
        }
        private static bool SendMail(string senderEmail, string receiverEmail, string subject, string messageText, bool isEnableBcc, string EmailAddress, bool isHTMLBody)
        {
            try
            {
                ZnodeEmail.SendEmail(senderEmail, receiverEmail, "", subject, messageText, true, "", EmailAddress);
                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                return false;
            }
        }

        private string GetEmailTemplate(ShoesFinderModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;

            Regex rx1 = new Regex("#Brand#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.SelectBrand == null ? "" : model.SelectBrand.ToString());

            rx1 = new Regex("#ShoeName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ShoeName == null ? "" : model.ShoeName.ToString());

            rx1 = new Regex("#Color#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Color == null ? "" : model.Color.ToString());

            rx1 = new Regex("#Size#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Size == null ? "" : model.Size.ToString());

            rx1 = new Regex("#Gender#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Gender == null ? "" : model.Gender.ToString());

            rx1 = new Regex("#Width#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.width == null ? "" : model.width.ToString());

            rx1 = new Regex("#Location#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.location == null ? "" : model.location.ToString());

            rx1 = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.FirstName);

            rx1 = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.LastName);

            rx1 = new Regex("#Email#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EmailID);

            rx1 = new Regex("#Type#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText,model.Type == null ? "" : model.Type.ToString());

            rx1 = new Regex("#StockNo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.StockNumber == null ? "" : model.StockNumber.ToString());

            rx1 = new Regex("#LikeOurSite#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.LikeOurSite == null ? "" : model.LikeOurSite.ToString());

            rx1 = new Regex("#Comments#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Description == null ? "" : model.Description.ToString());

            string storeLogoPath = GetCustomPortalDetails(PortalId)?.StoreLogo;
            rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            string customerServiceEmail = ZnodeConfigManager.SiteConfig.CustomerServiceEmail;
            rx1 = new Regex("#CustomerServiceEmail#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, customerServiceEmail);

            string customerServicePhoneNumber = ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            rx1 = new Regex("#CustomerServicePhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, customerServicePhoneNumber);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }

        private static bool CreateShoeFinder(ShoesFinderModel model)
        {
            IZnodeViewRepository<ShoesFinderModel> objStoredProc = new ZnodeViewRepository<ShoesFinderModel>();
            objStoredProc.SetParameter("@SelectBrand", model.SelectBrand == null ? "" : model.SelectBrand.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@ShoeName", model.ShoeName == null ? "" : model.ShoeName.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Color", model.Color == null ? "" : model.Color.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Size", model.Size == null ? "" : model.Size.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Gender", model.Gender == null ? "" : model.Gender.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@width", model.width == null ? "" : model.width.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@location", model.location == null ? "" : model.location.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@FirstName", model.FirstName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@LastName", model.LastName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@EmailID", model.EmailID, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Type", model.Type == null ? "" : model.Type.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@StockNumber", model.StockNumber == null ? "" : model.StockNumber.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@LikeOurSite", model.LikeOurSite == null ? "" : model.LikeOurSite.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Description", model.Description == null ? "" : model.Description.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Status", "", ParameterDirection.Output, DbType.Boolean);
            //Znode_InsertShoesFinder,Nivi_ShoesFinder
            var list = objStoredProc.ExecuteStoredProcedureList("Nivi_ShoesFinder @SelectBrand,@ShoeName,@Color,@Size,@Gender,@width,@location,@FirstName,@LastName,@EmailID,@Type,@StockNumber,@LikeOurSite,@Description,@Status output");

            return true;
        }

        private string GetEmailTemplate(ScheduleATruckModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;
            Regex rx1 = new Regex("#CompanyName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.CompanyName == null ? "" : model.CompanyName);

            rx1 = new Regex("#DatesRequested#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.DatesRequested == null ? "" : model.DatesRequested);

            rx1 = new Regex("#TmeRequested#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, Convert.ToString(model.TmeRequested) == null ? "" : Convert.ToString(model.TmeRequested));

            rx1 = new Regex("#Address#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Address == null ? "" : model.Address);

            rx1 = new Regex("#PhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.PhoneNumber == null ? "" : model.PhoneNumber);

            rx1 = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.FirstName == null ? "" : model.FirstName);

            rx1 = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.LastName == null ? "" : model.LastName);

            rx1 = new Regex("#NoOfEmployee#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.NoOfEmployee == null ? "" : model.NoOfEmployee);

            rx1 = new Regex("#MaleFemale#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.MaleFemale == null ? "" : model.MaleFemale);

            rx1 = new Regex("#Steeltorequired#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Steeltorequired == null ? "" : model.Steeltorequired);

            rx1 = new Regex("#EmailID#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EmailID == null ? "" : model.EmailID);

            rx1 = new Regex("#ContriAmount#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, Convert.ToString(model.ContriAmount) == null ? "" : Convert.ToString(model.ContriAmount));

            rx1 = new Regex("#Payroll#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Payroll == null ? "" : model.Payroll);

            rx1 = new Regex("#PosterNeeded#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, Convert.ToString(model.PosterNeeded)==null ? "" : Convert.ToString(model.PosterNeeded));

            rx1 = new Regex("#Description#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Description == null ? "" : model.Description.ToString());

            string storeLogoPath = GetCustomPortalDetails(PortalId)?.StoreLogo;
            rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            string customerServiceEmail = ZnodeConfigManager.SiteConfig.CustomerServiceEmail;
            rx1 = new Regex("#CustomerServiceEmail#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, customerServiceEmail);

            string customerServicePhoneNumber = ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            rx1 = new Regex("#CustomerServicePhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, customerServicePhoneNumber);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }

        private static bool CreateScheduleATruck(ScheduleATruckModel model)
        {
            IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();
            objStoredProc.SetParameter("@CompanyName", model.CompanyName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Address", model.Address, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@PhoneNumber", model.PhoneNumber, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@FirstName", model.FirstName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@NoOfEmployee", model.NoOfEmployee, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Steeltorequired", model.Steeltorequired, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@EmailID", model.EmailID, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@PosterNeeded", model.PosterNeeded, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Description", model.Description == null ? "" : model.Description.ToString(), ParameterDirection.Input, DbType.String);
            var scheduleatruck = objStoredProc.ExecuteStoredProcedureList("Nivi_SPScheduleATruck @CompanyName, @Address, @PhoneNumber, @FirstName, @NoOfEmployee, @Steeltorequired, @EmailID, @PosterNeeded, @Description");
            return true;
        }
        }
}
