﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Framework.Business;
using System.Configuration;

using System.Collections;
using System.Net;
using System.IO;
using Znode.Libraries.ECommerce.Utilities;
using System.Data;
using System.Linq;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace Znode.Api.Custom.Service.Service

{
    public class RSCustomerReviewService: CustomerReviewService
    {
       
        string api_key = ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString();
        string api_url = ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString();
        string CampainName = ConfigurationManager.AppSettings["CampaignName"].ToString();
        private Hashtable ht = new Hashtable();
       

        private readonly IZnodeRepository<ZnodeCMSCustomerReview> _customerReviewRepository;
        #region Constructor
        public RSCustomerReviewService():base()
        {
            _customerReviewRepository = new ZnodeRepository<ZnodeCMSCustomerReview>();
        }
        #endregion

        public override CustomerReviewModel Create(CustomerReviewModel modelReview)
        {
            try
            {
                if (api_key != null && api_url != null)
                {

                    //string newsLetterCampaign = CampainName();
                    string newsLetterCampaign = CampainName;
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        string campaign_id = GetCampaignId(CampainName);
                        string contatcId = GetContactID(modelReview.UserName.Trim(), campaign_id);
                        string campaignID = ProductReviewGetResponse(api_key, api_url,
                            newsLetterCampaign, modelReview.Duration.Trim(), modelReview.UserName.Trim(), contatcId

                            , modelReview.ProductName, modelReview.Headline, modelReview.SEODescription, modelReview.SEOKeywords, modelReview.SEOTitle
                            , modelReview.Comments, modelReview.Status, modelReview.UserLocation,modelReview.SKU );
                    }
                }
                //modelReview.UserName = modelReview.Duration;
                string strUserEmail = modelReview.Duration + " / " + modelReview.UserName;
                modelReview.UserName = strUserEmail;
            }
           catch(Exception ex)
            {
                ZnodeLogging.LogMessage("RSCUSTOMERREVIEWSERVICE api_key=" + api_key + "api_url=" + api_url, "CustomerReview", TraceLevel.Error);
                ZnodeLogging.LogMessage("RSCUSTOMERREVIEWSERVICE Create() Error" + ex.Message + "StackTrace:"+ex.StackTrace, "CustomerReview", TraceLevel.Error);
            }
            return base.Create(modelReview);
        }

        public string GetCampaignId(string campaignName)
        {
            if (ht != null && ht.Count > 0)
            {
                if (ht[campaignName] != null)
                {
                    return ht[campaignName].ToString();
                }
            }
            string campaign_id = string.Empty;
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                // get CAMPAIGN_ID of 'sample_marketing' campaign
                // new request object
                Hashtable _request = new Hashtable();
                _request["jsonrpc"] = "2.0";
                _request["id"] = 1;
                // set method name
                _request["method"] = "get_campaigns";
                // set conditions
                Hashtable operator_obj = new Hashtable();
                operator_obj["EQUALS"] = campaignName;
                Hashtable name_obj = new Hashtable();
                name_obj["name"] = operator_obj;
                // set params request object
                object[] params_array = { api_key, name_obj };
                _request["params"] = params_array;

                // send headers and content in one request
                // (disable 100 Continue behavior)
                System.Net.ServicePointManager.Expect100Continue = false;

                // initialize client
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
                request.Method = "POST";

                byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

                String response_string = null;

                try
                {
                    // call method 'get_messages' and get result
                    Stream request_stream = request.GetRequestStream();
                    request_stream.Write(request_bytes, 0, request_bytes.Length);
                    request_stream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream response_stream = response.GetResponseStream();

                    StreamReader reader = new StreamReader(response_stream);
                    response_string = reader.ReadToEnd();
                    reader.Close();

                    response_stream.Close();
                    response.Close();
                }
                catch (Exception e)
                {
                    //Log Exceptions
                    //LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + e.ToString());
                    Environment.Exit(0);
                }

                // decode response to Json object
                Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;
                                
                    // get result
                Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
               
                // get campaign id
                foreach (object key in result.Keys)
                {
                    campaign_id = key.ToString();
                }
                ht.Add(campaignName, campaign_id);
            }
            catch (Exception ex)
            {
                //DBHelper db = new DBHelper();
               // db.LogError(ex.ToString(), "GetCampaignId() Method", userId);
                //Log Exceptions
               // LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + ex.ToString());

            }
            return campaign_id;
        }
        public string GetContactID(string email, string campaign_id )
        {            
            string contactID = "";
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                // get CAMPAIGN_ID of 'sample_marketing' campaign
                // new request object
                Hashtable _request = new Hashtable();
                _request["jsonrpc"] = "2.0";
                _request["id"] = 3;

                // set method name
                _request["method"] = "get_contacts";

                Hashtable contact_params = new Hashtable();
                contact_params["campaigns"] = campaign_id;

                Hashtable contact_params1 = new Hashtable();
                contact_params1["EQUALS"] = email;

                Hashtable name_obj1 = new Hashtable();
                name_obj1["email"] = contact_params1;

                // set params request object
                object[] add_contact_params_array = { api_key, name_obj1, contact_params };

                _request["params"] = add_contact_params_array;

                // send headers and content in one request
                // (disable 100 Continue behavior)
                System.Net.ServicePointManager.Expect100Continue = false;

                // initialize client
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
                request.Method = "POST";

                byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

                string response_string = null;

                try
                {
                    // call method 'get_contacts' and get result
                    Stream request_stream = request.GetRequestStream();
                    request_stream.Write(request_bytes, 0, request_bytes.Length);
                    request_stream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream response_stream = response.GetResponseStream();

                    StreamReader reader = new StreamReader(response_stream);
                    response_string = reader.ReadToEnd();
                    reader.Close();

                    response_stream.Close();
                    response.Close();
                }
                catch (Exception e)
                {
                    //Log Exceptions
                   // LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + e.ToString());
                    Environment.Exit(0);
                }

                // decode response to Json object
                Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

                // get result
                Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;


                // get campaign id
                foreach (object key in result.Keys)
                {
                    contactID = key.ToString();
                }
            }
            catch (Exception ex)
            {
                //DBHelper db = new DBHelper();
                //db.LogError(ex.ToString(), "GetContactID() Method", userId);
                //Log Exceptions
               // LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + ex.ToString());
            }
            return contactID;
        }     
        public string ProductReviewGetResponse(string api_key, string api_url, string campaignName, string contactName, string subscriberEmail, string contatcId
            ,string ProductName,string Review ,string pros ,string cons ,string bestuses ,string comment ,string gender ,string country,string SKU
            )
        {
            ZnodeLogging.LogMessage("ProductReviewGetResponse begin.." + campaignName + "," + contactName + "-" + subscriberEmail + "-" + Review + "-" + pros + "-" + cons + "-" + bestuses + "-" + comment + "-" + gender + "-" + country + "-" + SKU, "RSCustomerReviewService", TraceLevel.Info);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 1;
            // set method name
            _request["method"] = "get_campaigns";
            // set conditions
            Hashtable operator_obj = new Hashtable();
            operator_obj["EQUALS"] = campaignName;
            Hashtable name_obj = new Hashtable();
            name_obj["name"] = operator_obj;
            // set params request object
            object[] params_array = { api_key, name_obj };
            _request["params"] = params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
           // ZnodeLogging.LogMessage("ProductReviewGetResponse Before..HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);", "RSCustomerReviewService", TraceLevel.Info);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";
          //  ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 1", "RSCustomerReviewService", TraceLevel.Info);
            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));
           // ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 2", "RSCustomerReviewService", TraceLevel.Info);
            String response_string = null;

            try
            {
                //ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 3", "RSCustomerReviewService", TraceLevel.Info);
                // call method 'get_messages' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                ZnodeLogging.LogMessage("Exception1-" + e.Message + "StackTrace-" + e.StackTrace , "RSCustomerReviewService", TraceLevel.Error);
                //check for communication and response errors
                //implement handling if needed
                //Elmah.ElmahErrorManager.Log(e.StackTrace);

            }

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;
            //ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.1 " + jsonContent["error"], "RSCustomerReviewService", TraceLevel.Info);
            //ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.2 " + jsonContent["jsonrpc"], "RSCustomerReviewService", TraceLevel.Info);
            //ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.3 " + jsonContent["id"], "RSCustomerReviewService", TraceLevel.Info);
            //foreach (object key in jsonContent.Keys)
            //{
            //    ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.1 Key=" + key+",Vlaue="+ jsonContent[key], "RSCustomerReviewService", TraceLevel.Info);
            //}

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
           // ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 5"+ jsonContent, "RSCustomerReviewService", TraceLevel.Info);
            string campaign_id = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                campaign_id = key.ToString();
            }

            // add contact to 'sample_marketing' campaign
            // new request object
            _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 2;

            // set method name
            //update contacts
            Hashtable contact_params = new Hashtable();
            if (string.IsNullOrEmpty(contatcId))
            {
                
                _request["method"] = "add_contact";
                contact_params["campaign"] = campaign_id;
                contact_params["name"] = contactName;
                contact_params["email"] = subscriberEmail;
            }
            else
            {
                _request["method"] = "set_contact_customs";
                contact_params["contact"] = contatcId;
            }

            string[] GenderReview = gender.Split('#');

            string splitGender = ""; string splitRecommend = ""; string splitgeturl = "";
            //try
            //{
            //    splitGender = GenderReview[0].ToString();
            //}
            //catch 
            //{ splitGender = ""; }
            //try
            //{
            //    splitRecommend = GenderReview[1].ToString();
            //}
            //catch
            //{ splitRecommend = ""; }
            //try
            //{
            //    splitgeturl = GenderReview[2].ToString();
            //}
            //catch
            //{ }
            
            splitGender = GenderReview.Length > 0 ? Convert.ToString(GenderReview[0]) : "";
            splitRecommend = GenderReview.Length > 1 ? Convert.ToString(GenderReview[1]) : "";
            splitgeturl = GenderReview.Length > 2 ? Convert.ToString(GenderReview[2]) : "";
           

            Hashtable customProductName = new Hashtable();
            customProductName["name"] = "ProductName";
            customProductName["content"] = ProductName;

            Hashtable customReview = new Hashtable();
            customReview["name"] = "Review";
            customReview["content"] = Review;


            Hashtable customComment = new Hashtable();
            customComment["name"] = "comment";
            customComment["content"] = comment;

            Hashtable customCountry = new Hashtable();
            customCountry["name"] = "country";
            customCountry["content"] = country;

            Hashtable customgeturl = new Hashtable();
            customgeturl["name"] = "ProductURL";
            customgeturl["content"] = Convert.ToString(splitgeturl);

            /*multiple selections*/
            // object[] customs_array = { customGender, customRecommend, customProductName, customReview, customComment, customCountry };
            if(string.IsNullOrEmpty(pros))
            {
                pros="";
            }
            if (string.IsNullOrEmpty(cons))
            {
                cons = "";
            }
            if (string.IsNullOrEmpty(bestuses))
            {
                bestuses = "";
            }
            string[] Splitpros = pros.Split(',');
            string[] SplitCons = cons.Split(',');
            string[] SplitBestUses = bestuses.Split(',');

            int SplitprosLenghth = 0;
            int SplitConsLength = 0;
            int SplitBestUsesLength = 0;

            if (pros !=""){SplitprosLenghth = Splitpros.Length; }
            if (cons !="") { SplitConsLength = SplitCons.Length; }
            if (bestuses !="") { SplitBestUsesLength = SplitBestUses.Length; }


            Hashtable customGender = new Hashtable();

            int splitGenderlength = 0;
            if (!string.IsNullOrWhiteSpace(splitGender)) { splitGenderlength = 1;

               
                customGender["name"] = "gender";
                customGender["content"] = Convert.ToString(splitGender);                
            }


            Hashtable customRecommend = new Hashtable();
            int splitRecommendlength = 0;
            if (!string.IsNullOrWhiteSpace(splitRecommend)) {
                splitRecommendlength = 1;
                
                customRecommend["name"] = "ref";
                customRecommend["content"] = Convert.ToString(splitRecommend);                
            }
        
            int i = SplitprosLenghth + SplitConsLength + SplitBestUsesLength + splitGenderlength + splitRecommendlength + 5;

            object[] customs_array = new object[i];
            int j = 0;
           
            foreach (string str in Splitpros)
            {
                if (str == "") { continue; }
                string str2 = "hashtable"+ j.ToString();
                Hashtable customPros = new Hashtable();
                customPros["name"] = "pros";
                customPros["content"] = str ;
                customs_array.SetValue(customPros, j);
                j++;
             }
                   
            foreach (string str in SplitCons)
            {
                if (str == "") { continue; }
                Hashtable customCons = new Hashtable();
                customCons["name"] = "cons";
                customCons["content"] = str;
                customs_array.SetValue(customCons, j);
                j++;

            }
           
            foreach (string str in SplitBestUses)
            {
                if (str == "") { continue; }
                Hashtable customBestUses = new Hashtable();
                customBestUses["name"] = "bestuses";
                customBestUses["content"] = str;
                customs_array.SetValue(customBestUses, j);
                j++;

            }
          
            customs_array.SetValue(customProductName, j);
            customs_array.SetValue(customReview, j+1);
            customs_array.SetValue(customComment, j+2);
            customs_array.SetValue(customCountry, j+3);
            customs_array.SetValue(customgeturl, j+4);

            if (!string.IsNullOrWhiteSpace(splitGender))
            {
                customs_array.SetValue(customGender, j+5);
            }

            if (!string.IsNullOrWhiteSpace(splitRecommend))
            {
                if (string.IsNullOrWhiteSpace(splitGender))
                {
                    customs_array.SetValue(customRecommend, j+5);
                }
                else
                {
                    customs_array.SetValue(customRecommend, j+6);
                }                
            }

            contact_params["customs"] = customs_array;

            //// set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;

            try
            {
                // call method 'add_contact' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                //Elmah.ElmahErrorManager.Log(e.StackTrace);
                //ZnodeLogging.LogMessage(e, "RSUserService:AddContactEmailSubscriber ", TraceLevel.Error);
            }
            return campaign_id;
             
             
        }

        public override CustomerReviewListModel GetCustomerReviewList(string localeId, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter localeId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, localeId);

            //Check if filter contains status as Active, Inactive or New and create filters for them.
            CheckAndCreateStatusFilter(filters, localeId);
            int portalId;
            Int32.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out portalId);
            filters.RemoveAll(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase));

            int localeID;
            Int32.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.LocaleId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeID);
            filters.RemoveAll(x => x.FilterName.Equals(FilterKeys.LocaleId, StringComparison.InvariantCultureIgnoreCase));

            ZnodeLogging.LogMessage("portalId and localeID generated by filters: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, localeID });

            CustomerReviewListModel listModel = new CustomerReviewListModel();
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            /*Nivi code start: for increase pagelength of reviews*/
            if (pageListModel.PagingLength == 16) {
                pageListModel.PagingLength = 40;
            }
            /*Nivi code end */
            ZnodeLogging.LogMessage("pageListModel generated to get customer review list: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            IZnodeViewRepository<CustomerReviewModel> objStoredProc = new ZnodeViewRepository<CustomerReviewModel>();

            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeID, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);

            listModel.CustomerReviewList = objStoredProc.ExecuteStoredProcedureList("Znode_GetCMSCustomerReviewInformation  @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@LocaleId,@PortalId", 4, out pageListModel.TotalRowCount).ToList();
            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("customerReviewList count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, listModel.CustomerReviewList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return listModel;
        }
        private static void CheckAndCreateStatusFilter(FilterCollection filters, string localeId)
        {
            if (Equals(filters?.Where(x => x.FilterName == ZnodeCMSCustomerReviewEnum.Status.ToString().ToLower())?.FirstOrDefault()?.FilterValue, "inactive") || Equals(filters?.Where(x => x.FilterName == ZnodeCMSCustomerReviewEnum.Status.ToString().ToLower())?.FirstOrDefault()?.FilterValue, "active") || Equals(filters?.Where(x => x.FilterName == ZnodeCMSCustomerReviewEnum.Status.ToString().ToLower())?.FirstOrDefault()?.FilterValue, "new"))
            {
                string status = filters?.Where(x => x.FilterName == ZnodeCMSCustomerReviewEnum.Status.ToString().ToLower())?.FirstOrDefault()?.FilterValue.Substring(0, 1);
                if (filters.Any(x => x.FilterName == ZnodeCMSCustomerReviewEnum.Status.ToString().ToLower()))
                {
                    filters?.Remove(filters.FirstOrDefault(x => x.FilterName == ZnodeCMSCustomerReviewEnum.Status.ToString().ToLower()));
                    filters?.Add(ZnodeCMSCustomerReviewEnum.Status.ToString(), ProcedureFilterOperators.Is, status?.ToString());
                }
            }
            filters?.Add(ZnodeLocaleEnum.LocaleId.ToString(), ProcedureFilterOperators.Is, localeId);
        }

    }
}
