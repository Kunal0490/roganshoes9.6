﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes.Helper;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Service.Service
{
    public class RSOrderService : OrderService
    {
        private decimal thresholdShippingValue = Convert.ToDecimal(ConfigurationManager.AppSettings["ThresholdShippingValue"]);
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderShipment> _orderOrderShipmentRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderOrderDetailRepository;
        private readonly IShoppingCartMap _shoppingCartMap;
        private readonly IZnodeRepository<ZnodeOmsOrderState> _omsOrderStateRepository;
        #region Constructor

        public RSOrderService() : base()
        {
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _orderOrderShipmentRepository = new ZnodeRepository<ZnodeOmsOrderShipment>();
            _orderOrderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _shoppingCartMap = GetService<IShoppingCartMap>();
            _omsOrderStateRepository = new ZnodeRepository<ZnodeOmsOrderState>();
        }

        #endregion Constructor
        public override IZnodeCheckout SetShoppingCartDataToCheckout(IZnodeCheckout checkout, ShoppingCartModel model)
        {
            IZnodeCheckout zCheckout = base.SetShoppingCartDataToCheckout(checkout, model);
            zCheckout.ShoppingCart.CustomShippingCost = model.ShippingCost;
            if(zCheckout.ShoppingCart?.AddressCarts?.Count>0)
            zCheckout.PoDocument = zCheckout.ShoppingCart.AddressCarts[0].Description;
            zCheckout.ShoppingCart.CreditCardNumber = null;
            zCheckout.ShoppingCart.CardType = null;
            zCheckout.ShoppingCart.CreditCardExpMonth = null;
            zCheckout.ShoppingCart.CreditCardExpYear = null;

            return zCheckout;
        }

        //Update order line items.
        //public override OrderModel SaveOrder(ShoppingCartModel model, SubmitOrderModel updateordermodel = null)
        //{
        //    //ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    ZnodeLogging log = new ZnodeLogging();

        //    bool isUpdateAction = model.OmsOrderId > 0;
        //    bool isEnableBcc = false;
        //    model.OrderAttribute = DefaultGlobalConfigSettingHelper.DefaultOrderAttribute;
        //    if (model?.PublishStateId < 1)
        //        model.PublishStateId = PublishStateId;

        //    SetStateCode(model);

        //    UserAddressModel userDetails = SetUserDetails(model);
        //    //Get refunded line item for CCH tax.
        //    GetRefundedLineItemForCCH(model, updateordermodel);
        //    IZnodeCheckout checkout = SetCheckoutData(userDetails, model, log);
        //    // Perform validation and start the timer
        //    ValidateCheckout(checkout);
        //    log.LogActivityTimerStart();

        //    // Instantiate the order fullfillment
        //    ZnodeOrderFulfillment order;
        //    string isInventoryInStockMessage = string.Empty;
        //    Dictionary<int, string> minMaxSelectableQuantity = new Dictionary<int, string>();
        //    try
        //    {
        //        // Do pre-submit processing
        //        bool preSubmitOrderSuccess = checkout.ShoppingCart.PreSubmitOrderProcess(out isInventoryInStockMessage, out minMaxSelectableQuantity);
        //        if (preSubmitOrderSuccess)
        //        {
        //            checkout.ShoppingCart.ReturnOrderLineItem(model);

        //            order = checkout.SubmitOrder(updateordermodel, model);

        //            //Save in Quote if it is quote to order.
        //            SaveInQuote(model, order);
        //        }
        //        else
        //        {
        //            log.LogActivityTimerEnd((int)ZnodeLogging.ErrorNum.OrderSubmissionFailed, null);
        //            throw new ZnodeException(ErrorCodes.ProcessingFailed, Admin_Resources.ErrorPresubmitOrderProcessing);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
        //        if (!string.IsNullOrEmpty(isInventoryInStockMessage))
        //            throw new ZnodeException(ErrorCodes.OutOfStockException, Admin_Resources.ErrorPlaceOrder);

        //        if (IsNotNull(minMaxSelectableQuantity) && minMaxSelectableQuantity.Count > 0 && minMaxSelectableQuantity.ContainsKey(ErrorCodes.MinAndMaxSelectedQuantityError))
        //            throw new ZnodeException(ErrorCodes.MinAndMaxSelectedQuantityError, minMaxSelectableQuantity[ErrorCodes.MinAndMaxSelectedQuantityError]);

        //        throw;
        //    }

        //    // If checkout successful then do post-submit processing
        //    if (checkout.IsSuccess)
        //    {
        //        // Do post submit processing
        //        PostSubmitOrder(order, checkout, model.FeedbackUrl);
        //        log.LogActivityTimerEnd((int)ZnodeLogging.ErrorNum.OrderSubmissionSuccess, order.OrderID.ToString());
        //    }
        //    else
        //    {
        //        log.LogActivityTimerEnd((int)ZnodeLogging.ErrorNum.OrderSubmissionFailed, null, null, null, null, checkout.PaymentResponseText);
        //        throw new Exception(checkout.PaymentResponseText);
        //    }
        //    OrderModel orderModel = BindOrderData(order, model);
        //    ZnodeLogging.LogMessage("ShippingName and ShippingID to get ShippingName from repository: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ShippingName = checkout?.ShoppingCart?.Shipping?.ShippingName, ShippingID = checkout?.ShoppingCart?.Shipping?.ShippingID });
        //    checkout.ShoppingCart.Shipping.ShippingName = GetShippingName(checkout.ShoppingCart.Shipping.ShippingName, checkout.ShoppingCart.Shipping.ShippingID);
        //    if (!isUpdateAction)
        //    {
        //        // And finally attach the receipt HTML to the order and return.

        //        int accountId = IsNotNull(model.UserDetails) ? model.UserDetails.AccountId.GetValueOrDefault() : 0;
        //        orderModel.ReceiptHtml = GetOrderReceipt(order, checkout, model.FeedbackUrl, model.LocaleId, isUpdateAction, out isEnableBcc, accountId);

        //        if (!string.IsNullOrEmpty(orderModel.ReceiptHtml) && model.QuoteTypeCode != ZnodeConstant.Quote)
        //            orderModel.IsEmailSend = SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, $"{Admin_Resources.TitleOrderReceipt} - {orderModel.OrderNumber}", ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeConfigManager.SiteConfig.AdminEmail, orderModel.ReceiptHtml, isEnableBcc);

        //        if (checkout.ShoppingCart.LowInventoryProducts?.Count > 0)
        //            SendEmailNotificationForLowInventory(checkout, order, orderModel, model.LocaleId);
        //    }
        //    //Send purchased product details Email To Vendor.
        //    SendEmailToVendor(order, checkout, model.FeedbackUrl, model.LocaleId, isEnableBcc);

        //    //Send order alert email for store notification
        //    SendEmailNotification(model, isUpdateAction, checkout, order, orderModel, isEnableBcc);

        //    //to send line item state change receipt to user.
        //    SendLineItemStateChangeEmail(order, isEnableBcc);

        //    InitializeERPConnectorForCreateUpdateOrder(orderModel);

        //    if (orderModel?.OrderLineItems.Count > 0)
        //    {
        //        List<OrderLineItemModel> downLoadablelineItems = new List<OrderLineItemModel>();
        //        foreach (OrderLineItemModel item in orderModel.OrderLineItems)
        //        {
        //            var status = (item.OrderLineItemCollection?.Where(x => x.IsDownloadableSKU).ToList());
        //            if (status?.Count() > 0)
        //                downLoadablelineItems.AddRange(status);
        //        }
        //        if (downLoadablelineItems.Any())
        //            //Save Downloadable product key to database
        //            SaveDownloadableProductKey(model, isUpdateAction, isEnableBcc, checkout, order, orderModel, downLoadablelineItems);

        //    }
        //    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    return orderModel;
        //}
        //private bool SendEmailNotificationForLowInventory(IZnodeCheckout checkout, ZnodeOrderFulfillment order, OrderModel orderModel, int localeId)
        //{
        //    IZnodeOrderReceipt receipt = GetOrderReceiptInstance(order, checkout.ShoppingCart);

        //    string receiptHtml = string.Empty;

        //    //Method to get Email Template.
        //    EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(ZnodeConstant.LowInventoryOrderNotification, (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
        //    if (HelperUtility.IsNotNull(emailTemplateMapperModel))
        //    {
        //        string receiptContent = emailTemplateMapperModel.Descriptions;
        //        ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //        receiptHtml = EmailTemplateHelper.ReplaceTemplateTokens(receipt.CreateLowInventoryNotification(receiptContent));
        //    }

        //    if (!string.IsNullOrEmpty(receiptHtml) && !string.IsNullOrEmpty(ZnodeConfigManager.SiteConfig.AdminEmail) && !string.IsNullOrEmpty(ZnodeConfigManager.SiteConfig.CustomerServiceEmail))
        //        orderModel.IsEmailSend = SendOrderReceipt(orderModel.PortalId, ZnodeConfigManager.SiteConfig.CustomerServiceEmail, $"{Admin_Resources.TitleLowInventoryOrderNotification} - {orderModel.OrderNumber}", ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeConfigManager.SiteConfig.AdminEmail, receiptHtml);
        //    return true;
        //}
        #region Order Receipt Changes
        public override string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate, out bool isEnableBcc,int accountId)
        {
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                {
                    item.PersonaliseValueList.Remove("AllocatedLineItems");
                }

                if (item.PersonaliseValuesDetail != null)
                {
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
                }
            }

            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, order.Custom1);
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                string text = "";
                try
                {
                    text = EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));
                }
                catch(Exception ex)
                {

                }
                return text;
            }
            isEnableBcc = false;
            return string.Empty;
        }

        public override bool SendOrderReceipt(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, bool isEnableBcc = false)
        {
            bool isSuccess = false;
            if (subject.Contains(Admin_Resources.TitleOrderReceipt))
                subject = Admin_Resources.SubjectOrderReceipt;
            ZnodeEmail.SendEmail(portalId, userEmailId, senderEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, bccEmailId), subject, receiptHtml, true);
            isSuccess = true;
            return isSuccess;
        }

        private string ShowOrderAdditionalDetails(string receiptContent, string customData)
        {
            if (!string.IsNullOrEmpty(customData))
            {
                return receiptContent.Replace("#FeedBack#", GenerateOrderAdditionalInfoTemplate(customData));
            }
            else
            {
                return receiptContent;
            }
        }
        //to generate order additional information template
        private string GenerateOrderAdditionalInfoTemplate(string customData)
        {
            string template = string.Empty;
            try
            {
                Dictionary<string, string> CustomDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(customData);
                if (HelperUtility.IsNotNull(CustomDict))
                {
                    template = ("<b>Additional Information</b>");

                    if (CustomDict.ContainsKey("ProductName"))
                    {
                        template += $" <br />Product will be used by  { CustomDict["ProductName"]}";
                    }

                    if (CustomDict.ContainsKey("RecipientName"))
                    {
                        template += $" <br />Recipient of the product {CustomDict["RecipientName"]}";
                    }

                    if (CustomDict.ContainsKey("ApproverManager"))
                    {
                        template += $" <br />Approving Manager {CustomDict["ApproverManager"]}";
                    }

                    if (CustomDict.ContainsKey("ProjectName"))
                    {
                        template += $" <br />Project Name {CustomDict["ProjectName"]}";
                    }

                    if (CustomDict.ContainsKey("EventDate"))
                    {
                        template += $" <br />Event Date {CustomDict["EventDate"]}";
                    }

                    if (CustomDict.ContainsKey("InHandsDate"))
                    {
                        template += $" <br />In Hands Date {CustomDict["InHandsDate"]}";
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return template;
        }

        #endregion

        #region Order EMail
        /// <summary>
        /// Override to remove extra space
        /// </summary>
        /// <param name="orderShipment"></param>
        /// <returns></returns>     
        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                string ShippingcompanyName = _addressRepository.Table.Where(x => x.AddressId == orderShipment.AddressId)?.FirstOrDefault()?.CompanyName;

                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}" : ShippingcompanyName;
                //return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
                if (orderShipment.ShipToCompanyName != null)
                {
                    //orderShipment.ShipToCompanyName =   orderShipment.ShipToCompanyName;
                    orderShipment.ShipToCompanyName = orderShipment?.ShipToCompanyName;
                }
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
            }
            return string.Empty;
        }
        /// <summary>
        /// Override to remove extra space-REmove company name as it not get in list
        /// </summary>
        /// <param name="orderBilling"></param>
        /// <returns></returns>
        public override string GetOrderBillingAddress(OrderModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street1 = string.IsNullOrEmpty(orderBilling.BillingAddress.Address2) ? string.Empty : "<br />" + orderBilling.BillingAddress.Address2;
                string CompanyName = "";
                if (orderBilling?.BillingAddress.CompanyName != null)
                {
                    CompanyName = string.IsNullOrEmpty(orderBilling.BillingAddress.CompanyName) ? string.Empty : "<br />" + orderBilling.BillingAddress.CompanyName;
                }
                //return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{"<br />"}{orderBilling?.BillingAddress.CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"<br />"}{orderBilling.BillingAddress.PostalCode}{"<br />"}{orderBilling.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.BillingAddress.PhoneNumber}";
                return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{orderBilling.BillingAddress.CompanyName}{CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"<br />"}{orderBilling.BillingAddress.PostalCode}{"<br />"}{orderBilling.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.BillingAddress.PhoneNumber}";
            }
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderModel"></param>
        public override void SendOrderStatusEmail(OrderModel orderModel)
        {
            ImageHelper imageHelper = new ImageHelper(orderModel.PortalId);
            foreach (OrderLineItemModel item in orderModel.OrderLineItems)
            {
                item.OrderLineItemCollection.AddRange(orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList());
                item.ProductImagePath = "";
                if (item.DownloadLink != "")
                {
                    item.ProductImagePath = item.DownloadLink;
                    string images = imageHelper.GetImageHttpPathSmallThumbnail(Convert.ToString(item.ProductImagePath));
                    item.ProductImagePath = "<img src=\"" + images + "\" data-src=\"" + images + "\"/>";
                }

            }
            orderModel.OrderLineItems.RemoveAll(x => x.ParentOmsOrderLineItemsId == null);

            string subject = string.Empty;
            bool isEnableBcc = false;
            if (orderModel.OrderState == ZnodeOrderStatusEnum.CANCELLED.ToString())
            {
                //subject = $"{Admin_Resources.CancelledOrderStatusSubject} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectCancelReceipt;
                orderModel.ReceiptHtml = GetCancelledOrderReceiptForEmail(orderModel, out isEnableBcc);
            }
            else if (orderModel.OrderState == ZnodeOrderStatusEnum.SUBMITTED.ToString())
            {
                //subject = $"{Admin_Resources.OrderStatusUpdated} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectSubmittedReceipt;
                orderModel.ReceiptHtml = GetSubmittedReceiptForEmail(orderModel, out isEnableBcc);
            }
            else if (orderModel.OrderState == ZnodeOrderStatusEnum.SHIPPED.ToString())
            {
                // And finally attach the receipt HTML to the order and return
                // subject = $"{Admin_Resources.ShippedOrderStatusSubject} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectShippingReceipt;
                orderModel.ReceiptHtml = GetShippingReceiptForEmail(orderModel, out isEnableBcc);
            }

            SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, subject, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
        }

        public override string GetCancelledOrderReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("CancelledOrderReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        //to generate shipping status receipt
        //Get Html Resend Receipt For Email.
        public override string GetShippingReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ShippingReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        public string GetSubmittedReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderSubmitted", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        #endregion      

       
        //Get order list.
        public override OrdersListModel GetOrderList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            //Bind the Filter conditions for the authorized portal access.
            BindUserPortalFilter(ref filters);

            //Replace sort key name.
            if (HelperUtility.IsNotNull(sorts))
            {
                ReplaceSortKeys(ref sorts);
            }

            int userId = 0;
            GetUserIdFromFilters(filters, ref userId);

            int fromAdmin = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase));
            ReplaceFilterKeys(ref filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IList<OrderModel> list = GetOrderList(pageListModel, userId, fromAdmin);
            foreach (OrderModel order in list)
            {
                List<ZnodeOmsOrderLineItem> orderLineItemDetails = GetOrderLineItemListByOrderId(order.OmsOrderDetailsId);
                List<OrderLineItemModel> orderLineItemModel = orderLineItemDetails?.ToModel<OrderLineItemModel>().ToList();                
                order.OrderLineItems = orderLineItemModel;
                int OrderShipmentId = Convert.ToInt32(orderLineItemDetails.FirstOrDefault()?.OmsOrderShipmentId);
                order.ShippingAddress=SetShippingAddress(OrderShipmentId);
                ZnodeOmsOrderDetail omsOrderDetail=_orderOrderDetailRepository.Table.Where(w => w.OmsOrderDetailsId == order.OmsOrderDetailsId).FirstOrDefault();
                order.BillingAddress = SetBillingAddress(omsOrderDetail);
               
            }
            OrdersListModel orderListModel = new OrdersListModel() { Orders = list?.ToList() };

            GetCustomerName(userId, orderListModel);

            orderListModel.BindPageListModel(pageListModel);
            return orderListModel;
        }

        private AddressModel SetBillingAddress(ZnodeOmsOrderDetail omsOrderDetail)
        {
            AddressModel addressModel = new AddressModel();
            addressModel.FirstName = omsOrderDetail.BillingFirstName;
            addressModel.LastName = omsOrderDetail.BillingLastName;
            addressModel.EmailAddress = omsOrderDetail.BillingEmailId;

            addressModel.CompanyName = omsOrderDetail.BillingCompanyName;

            addressModel.CityName = omsOrderDetail.BillingCity;

            addressModel.StateCode = omsOrderDetail.BillingStateCode;

            addressModel.CountryCode = omsOrderDetail.BillingCountry;       

            addressModel.PostalCode = omsOrderDetail.BillingPostalCode;

            addressModel.Address1 = omsOrderDetail.BillingStreet1;

            addressModel.Address2 = omsOrderDetail.BillingStreet2;
            addressModel.PhoneNumber = omsOrderDetail.BillingPhoneNumber;
            return addressModel;
        }
        private AddressModel SetShippingAddress(int OrderShipmentId)
        {
            ZnodeOmsOrderShipment OrderShipment = _orderOrderShipmentRepository.Table.Where(x => x.OmsOrderShipmentId == OrderShipmentId).FirstOrDefault();
            
            AddressModel addressModel = new AddressModel();
            addressModel.FirstName = OrderShipment.ShipToFirstName;
            addressModel.LastName = OrderShipment.ShipToLastName;
            addressModel.EmailAddress = OrderShipment.ShipToEmailId;

            addressModel.CompanyName = OrderShipment.ShipToCompanyName;

            addressModel.CityName = OrderShipment.ShipToCity;
          
            addressModel.StateCode = OrderShipment.ShipToStateCode;

            addressModel.CountryCode = OrderShipment.ShipToCountry;

            addressModel.PostalCode = OrderShipment.ShipToPostalCode;

            addressModel.Address1 = OrderShipment.ShipToStreet1;

            addressModel.Address2 = OrderShipment.ShipToStreet2;
            addressModel.PhoneNumber = OrderShipment.ShipToPhoneNumber;

            return addressModel;
        }
        public override OrderModel CheckInventoryAndMinMaxQuantity(ShoppingCartModel shoppingCartModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(shoppingCartModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorShoppingCartModelNull);
            if (shoppingCartModel.UserDetails == null)
                shoppingCartModel.UserDetails = new UserModel();
            UserAddressModel userDetails = SetUserDetails(shoppingCartModel);

            var znodeShoppingCart = _shoppingCartMap.ToZnodeShoppingCart(shoppingCartModel, userDetails);

            // Create the checkout object
            IZnodeCheckout checkout = CheckoutMap.ToZnodeCheckout(userDetails, znodeShoppingCart);
            string isInventoryInStockMessage = string.Empty;
            Dictionary<int, string> minMaxSelectableQuantity;
            //Nivi chnages to pass call to custom RsShoppingcrt class
            //checkout.ShoppingCart.CheckInventoryAndMinMaxQuantity(out isInventoryInStockMessage, out minMaxSelectableQuantity);
            znodeShoppingCart.CheckInventoryAndMinMaxQuantity(out isInventoryInStockMessage, out minMaxSelectableQuantity);

            if (!string.IsNullOrEmpty(isInventoryInStockMessage))
                throw new ZnodeException(ErrorCodes.OutOfStockException, Admin_Resources.OutOfStockException);

            if (IsNotNull(minMaxSelectableQuantity) && minMaxSelectableQuantity.Count > 0 && minMaxSelectableQuantity.ContainsKey(ErrorCodes.MinAndMaxSelectedQuantityError))
                throw new ZnodeException(ErrorCodes.MinAndMaxSelectedQuantityError, minMaxSelectableQuantity[ErrorCodes.MinAndMaxSelectedQuantityError]);

            ZnodeLogging.LogMessage("Is available inventory message and MinMaxQuantity flag:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { isInventoryInStockMessage = isInventoryInStockMessage, minMaxSelectableQuantity = minMaxSelectableQuantity });

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return new OrderModel() { IsAvailabelInventoryAndMinMaxQuantity = true };
        }

        public virtual List<ZnodeOmsOrderLineItem> GetOrderLineItemListByOrderId(int OmsOrderDetailsId)
        {
            IZnodeViewRepository<ZnodeOmsOrderLineItem> objStoredProc = new ZnodeViewRepository<ZnodeOmsOrderLineItem>();
            objStoredProc.SetParameter("@OmsOrderDetailsId", OmsOrderDetailsId, ParameterDirection.Input, DbType.Int32);
            return objStoredProc.ExecuteStoredProcedureList("RS_GetOmsOrderLineDetails @OmsOrderDetailsId").ToList();
        }

        #region Order Status Update
        //to update order status.
        private ZnodeOmsOrderDetail IsValidOrder(OrderStateParameterModel model)
        {
            if (IsNull(model))
                throw new Exception("Order model cannot be null.");

            if (model.OmsOrderId < 1)
                throw new Exception("Order ID cannot be less than 1.");

            if (IsNull(model.OmsOrderStateId) || model.OmsOrderStateId < 1)
                throw new Exception("Invalid order status.");

            ZnodeOmsOrderDetail order = _orderDetailsRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(CreatFiltersForOrder(model).ToFilterDataCollection())?.WhereClause, GetExpandsForOrderLineItem(GetOrderLineItemExpands()));
            return order;
        }
        //Cancel tax transaction.
        private static void CancelTaxTransaction(bool updated, ZnodeOmsOrderDetail order, List<ZnodeOmsOrderState> orderStateList)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            bool isCancelledOrder = order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.CANCELLED.ToString())?.OmsOrderStateId);
            if (updated && isCancelledOrder)
            {
                ZnodeTaxHelper taxHelper = new ZnodeTaxHelper();
                int? omsLineItemId = order?.ZnodeOmsOrderLineItems?.FirstOrDefault()?.OmsOrderLineItemsId;
                if (IsNotNull(omsLineItemId))
                    taxHelper.CancelTaxTransaction(omsLineItemId.Value, order?.PortalId);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }
        public override bool UpdateOrderStatus(OrderStateParameterModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("OrderStateParameterModel with Id: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderId = model?.OmsOrderId });
            bool updated = false;

            ZnodeOmsOrderDetail order = IsValidOrder(model);

            if (IsNotNull(order))
            {
                int previousOrderStateId = order.OmsOrderStateId;
                order.OmsOrderStateId = model.OmsOrderStateId;
                order.TrackingNumber = model.TrackingNumber;
                order.ModifiedDate = GetDateTime();
                order.Custom1 = Convert.ToString(model.Custom1);
                order.Custom2 = Convert.ToString(model.Custom2);
                List<ZnodeOmsOrderState> orderStateList = _omsOrderStateRepository.GetEntityList(string.Empty)?.ToList();
                ZnodeLogging.LogMessage("orderStateList Count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderStateListCount = orderStateList?.Count });

                if (orderStateList?.Count > 0)
                {
                    if (order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.SHIPPED.ToString())?.OmsOrderStateId))
                        order.ShipDate = DateTime.Now;
                    else if (order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.RETURNED.ToString())?.OmsOrderStateId))
                        order.ReturnDate = GetDateTime().Date + GetDateTime().TimeOfDay;
                }

                updated = _orderDetailsRepository.Update(order);
                if (updated)
                    UpdateLineItemState(model.OmsOrderId, previousOrderStateId, model.OmsOrderStateId, order.ShipDate);
                //Cancel tax transaction.
                CancelTaxTransaction(updated, order, orderStateList);

                if (updated && orderHelper.IsSendEmail(order.OmsOrderStateId))
                {
                    OrderModel orderModel = GetOrderById(order.OmsOrderId, null, GetOrderExpands());
                    SendOrderStatusEmail(orderModel);
                    return true;
                }
            }
            return updated;
        }
        #endregion
    }
}
