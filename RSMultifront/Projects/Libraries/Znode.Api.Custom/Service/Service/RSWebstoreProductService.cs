﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Service.Service
{
    public class RSWebstoreProductService : ProductService, IProductService
    {
        private IZnodeRepository<ZnodePublishWebstoreEntity> _publishWebstoreEntity = new ZnodeRepository<ZnodePublishWebstoreEntity>(HelperMethods.Context);

        public override bool SendComparedProductMail(ProductCompareModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("PortalId, WebstoreDomainName and WebstoreDomainScheme properties of input parameter ProductCompareModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { PortalId = model?.PortalId, WebstoreDomainName = model?.WebstoreDomainName, WebstoreDomainScheme = model?.WebstoreDomainScheme });

            string senderEmail = model.SenderEmailAddress;
            string subject = $"{senderEmail.Split('@').FirstOrDefault()}  wants you to see this item";

            if (!model.IsProductDetails)
            {
                //Method to get Email Template.
                EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ProductCompare", (model.PortalId > 0) ? model.PortalId : PortalId, model.LocaleId);

                if (HelperUtility.IsNotNull(emailTemplateMapperModel))
                {
                    List<PublishProductModel> products = GetCompareProductList(model);

                    DataTable productDetailItems = BindDataToTabelRow(products, model.WebstoreDomainName, model.WebstoreDomainScheme);

                    ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel.Descriptions);

                    string messageText = GetMessageText(model, productDetailItems, receiptHelper, model.WebstoreDomainName, model.WebstoreDomainScheme);

                    ZnodeLogging.LogMessage("Input parameters senderEmail, RecieverEmailAddress, subject, messageText, IsEnableBcc, PortalId of method SendMail: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { senderEmail, model?.RecieverEmailAddress, subject, messageText, emailTemplateMapperModel?.IsEnableBcc, model?.PortalId });

                    return SendMail(senderEmail, model.RecieverEmailAddress, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.PortalId);
                }
            }
            return false;
        }



        public override List<PublishProductModel> GetCompareProductList(ProductCompareModel model)
        {
            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodeConstant.Pricing, ZnodeConstant.Pricing);
            expands.Add(ZnodeConstant.SEO, ZnodeConstant.SEO);
            expands.Add(ZnodeConstant.ConfigurableAttribute, ZnodeConstant.ConfigurableAttribute);

            FilterCollection filters = new FilterCollection();
            filters.Add(FilterKeys.LocaleId, FilterOperators.Equals, model.LocaleId.ToString());
            filters.Add(FilterKeys.PortalId, FilterOperators.Equals, model.PortalId.ToString());
            filters.Add(FilterKeys.ZnodeProductId, FilterOperators.In, model.ProductIds);
            filters.Add(FilterKeys.VersionId, FilterOperators.Equals, GetCatalogVersionId().ToString());
            IPublishProductService publishProductService = GetService<IPublishProductService>();
            List<PublishProductModel> products = publishProductService.GetPublishProductList(expands, filters, new NameValueCollection(), new NameValueCollection())?.PublishProducts;

            ZnodeLogging.LogMessage("PublishProductModel list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, products?.Count);
            return products;
        }

        private DataTable BindDataToTabelRow(List<PublishProductModel> products, string webstoreDomainName, string webstoreDomainScheme)
        {
            //AttributesSelectValuesModel attributesSelectValuesModel = new AttributesSelectValuesModel();

            DataTable productDetailItems = new DataTable();
            productDetailItems.Columns.Add("Image");
            productDetailItems.Columns.Add("ProductName");
            productDetailItems.Columns.Add("Price");
            productDetailItems.Columns.Add("Variants");
            productDetailItems.Columns.Add("SEOUrl");

            foreach (PublishProductModel product in products)
            {
                string BaseImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString();
                string BaseImage = "";
                if (Convert.ToString(product.Attributes?.Value("BaseImage")) != null)
                {
                    BaseImage = BaseImagePath + @"t_170/" + Convert.ToString(product.Attributes?.Value("BaseImage"));
                }
                //product.Attributes.RemoveAll(x => x.AttributeCode == "Image" || x.AttributeCode == "Price" || !x.IsComparable && !x.IsConfigurable);
                product.Attributes.RemoveAll(x => x.AttributeCode == "BaseImage" || x.AttributeCode == "Price" || !x.IsComparable && !x.IsConfigurable);
                if (HelperUtility.IsNotNull(productDetailItems))
                {

                    DataRow productImageRow = productDetailItems.NewRow();
                    //string ImageUrl = !string.IsNullOrEmpty(product.ImageThumbNailPath) ? product.ImageThumbNailPath.Replace(" ", "%20") : "";
                    string ImageUrl = !string.IsNullOrEmpty(BaseImage) ? BaseImage.Replace(" ", "%20") : "";
                    productImageRow["Image"] = "<img src=" + ImageUrl + ">";
                    productImageRow["ProductName"] = (HelperUtility.IsNotNull(product.Name) || !Equals(product.Name, string.Empty)) ? $"<a href='{webstoreDomainScheme}://{webstoreDomainName}/{(string.IsNullOrEmpty(product?.SEOUrl) ? "product/" + product.PublishProductId : product.SEOUrl)}'>{product.Name}</a>" : "-";
                    productImageRow["Price"] = !string.IsNullOrEmpty(product.GroupProductPriceMessage) ? product.GroupProductPriceMessage : (HelperUtility.IsNull(product.SalesPrice) ? ServiceHelper.FormatPriceWithCurrency(product.RetailPrice, string.IsNullOrEmpty(product.CultureCode) ? GetDefaultCulture() : product.CultureCode) : ServiceHelper.FormatPriceWithCurrency(product.SalesPrice, string.IsNullOrEmpty(product.CultureCode) ? GetDefaultCulture() : product.CultureCode));
                    productImageRow["Variants"] = !string.IsNullOrEmpty(GetProductVariants(product)) ? GetProductVariants(product) : "NA";
                    productImageRow["SEOUrl"] = $"{webstoreDomainName}/{(string.IsNullOrEmpty(product?.SEOUrl) ? "product/" + product.PublishProductId : product.SEOUrl)}";
                    foreach (PublishAttributeModel attribute in product.Attributes)
                    {
                        DataColumnCollection columns = productDetailItems.Columns;
                        if (!columns.Contains(attribute.AttributeName))
                            productDetailItems.Columns.Add(attribute.AttributeName);
                        //if (attribute.SelectValues.Count > 0)

                        //    productImageRow[attribute.AttributeName] = attribute.SelectValues.FirstOrDefault()?.Value;
                        //else
                        //    productImageRow[attribute.AttributeName] = attribute.AttributeValues;
                    }
                    productDetailItems.Rows.Add(productImageRow);
                }
            }
            return productDetailItems;
        }

        private string GetMessageText(ProductCompareModel model, DataTable productDetailItems, ZnodeReceiptHelper receiptHelper, string webstoreDomainName, string webstoreDomainScheme)
        {

            receiptHelper.Parse("ComparedProducts", productDetailItems.CreateDataReader());
            string messageText = receiptHelper.Output;

            Regex rx1 = new Regex("#CustomerServiceEmail#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, ZnodeConfigManager.SiteConfig.CustomerServiceEmail.Replace(",", ", "));

            rx1 = new Regex("#CustomerServicePhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            string storeLogoPath = GetStoreLogoPath(model.PortalId);
            ZnodeLogging.LogMessage("storeLogoPath returned from method GetStoreLogoPath: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, storeLogoPath);

            rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, storeLogoPath);

            rx1 = new Regex("#EmailAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.SenderEmailAddress);

            //rx1 = new Regex("#storeurl#", RegexOptions.IgnoreCase);
            //messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, $"{webstoreDomainName}'>{webstoreDomainScheme}://{webstoreDomainName}");

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }

        private static bool SendMail(string senderEmail, string receiverEmail, string subject, string messageText, bool isEnableBcc, int portalId)
        {
            try
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
                ZnodeEmail.SendEmail(receiverEmail, senderEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, string.Empty), subject, messageText, true, null);
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                return false;
            }
        }



        //Get product variants for configurable product.
        private string GetProductVariants(PublishProductModel product)
        {
            List<PublishAttributeModel> variants = product?.Attributes?.Where(x => x.IsConfigurable).ToList();
            ZnodeLogging.LogMessage("Product variants list count for configurable product: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, variants?.Count);

            string productType = product?.Attributes?.SelectAttributeList(ZnodeConstant.ProductType)?.FirstOrDefault()?.Code;
            string variantData = string.Empty;
            string variantHTML = string.Empty;
            if (Equals(productType, ZnodeConstant.ConfigurableProduct) && variants?.Count > 0)
            {
                //Bind variant html for the product.
                variantHTML = "<ul style='padding:0;margin:0 10px 0 0;list-style:none;border:1px solid #c3c3c3;border-bottom:0;'>";
                foreach (var item in variants)
                    variantData += $"<li style='padding:3px 5px;margin:0;border-bottom:1px solid #c3c3c3;'><span><strong> {item.AttributeName}</strong></span>:&nbsp;</span><span> {string.Join(", ", item.ConfigurableAttribute.Select(x => x.AttributeValue))}</span></li>";

                return $"{variantHTML}{variantData}</ul>";
            }
            return null;
        }
        private string GetStoreLogoPath(int portalId)
        {
            ZnodeLogging.LogMessage("Input parameter portalId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, portalId);
            //IMongoQuery query = Query<WebStoreEntity>.EQ(pr => pr.PortalId, portalId);
            //string storeLogo = new MongoRepository<WebStoreEntity>(WebstoreVersionId).GetEntity(query)?.WebsiteLogo;
             
           ZnodePublishWebstoreEntity webStoreEntity = _publishWebstoreEntity.Table.FirstOrDefault(x => x.PortalId == portalId);
            MediaConfigurationModel configurationModel = GetService<IMediaConfigurationService>().GetDefaultMediaConfiguration();
            string serverPath = GetMediaServerUrl(configurationModel);
            string tumbnailPath = $"{serverPath}{configurationModel.ThumbnailFolderName}";
            return $"{tumbnailPath}/{webStoreEntity.WebsiteLogo}";
        }

        public override bool SendMailToFriend(EmailAFriendListModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);

            string senderEmail = model.YourMailId;
            string subject = $"{senderEmail.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("EmailAFriend", (model.PortalId > 0) ? model.PortalId : PortalId, model.LocaleId);
            if (HelperUtility.IsNull(emailTemplateMapperModel))
                throw (new Exception("Not Implemented"));
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel.Descriptions);

                string messageText = GetEmailTemplate(model, receiptHelper);

                ZnodeLogging.LogMessage("Input parameters senderEmail, FriendMailId, subject, messageText, IsEnableBcc, PortalId of method SendMail: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { senderEmail, model?.FriendMailId, subject, messageText, emailTemplateMapperModel?.IsEnableBcc, model?.PortalId });

                return SendMail(senderEmail, model.FriendMailId, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.PortalId);
            }
        }

        private string GetEmailTemplate(EmailAFriendListModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;

            Regex rx1 = new Regex("#ProductLink#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, "<a href=" + model.ProductUrl + ">" + model.ProductName + "</a>");

            rx1 = new Regex("#CatalogName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, ZnodeConfigManager.SiteConfig.CompanyName);

            string storeLogoPath = GetCustomPortalDetails(model.PortalId)?.StoreLogo;

            rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            rx1 = new Regex("#CustomerServiceEmail#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, ZnodeConfigManager.SiteConfig.CustomerServiceEmail.Replace(",", ", "));

            rx1 = new Regex("#CustomerServicePhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }

    }
}
