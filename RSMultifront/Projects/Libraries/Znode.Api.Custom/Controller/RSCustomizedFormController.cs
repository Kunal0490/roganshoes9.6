﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using Znode.Api.Custom.Service.Service;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Custom.Controller
{
    public class RSCustomizedFormController : BaseController
    {
        private readonly IRSCustomizedFormService _RSCustomizedFormService;

        public RSCustomizedFormController(IRSCustomizedFormService RSCustomizedFormService)
        {
            _RSCustomizedFormService = RSCustomizedFormService;

        }
        
        public virtual HttpResponseMessage ScheduleATruck([FromBody] ScheduleATruckModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool schedule = _RSCustomizedFormService.ScheduleATruck(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = schedule });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        public virtual HttpResponseMessage ShoeFinder([FromBody] ShoesFinderModel shoefinderModel)
        {
            HttpResponseMessage response;

            try
            {

                bool result = _RSCustomizedFormService.ShoeFinder(shoefinderModel);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = result });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.MediaManager.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
