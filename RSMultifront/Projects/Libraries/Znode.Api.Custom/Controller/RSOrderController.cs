﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache.Cache;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Request;

namespace Znode.Api.Custom.Controller
{
    public class RSOrderController : BaseController
    {
        #region Private Variables
        private readonly IRSOrderCache _orderCache;
        private readonly IOrderService _orderService;
        private readonly IRSOrderAPIService _APIorderService;
        #endregion
        public RSOrderController(IOrderService orderService, IRSOrderAPIService apiorderService)
        {
            _orderService = orderService;
            _APIorderService = apiorderService;
            _orderCache = new RSOrderCache(_orderService);
        }
        /// <summary>
        /// Get the list of all Orders.
        /// </summary>
        /// <returns>Returns list of all orders.</returns>
        [ResponseType(typeof(OrderListResponse))]
        [HttpPut]
        public HttpResponseMessage GetOrderList(RSOrderRequestModel OrderListRequest)
        {
            HttpResponseMessage response;
            try
            {
                string data = _orderCache.GetOrderList(OrderListRequest, RouteUri, RouteTemplate);
                response = string.IsNullOrEmpty(data) ? CreateNoContentResponse() : CreateOKResponse<OrderListResponse>(data);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new OrderListResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new OrderListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Update order status.
        /// </summary>
        /// <param name="model">OrderStateParameterModel</param>
        /// <returns>Returns true if updated sucessfully else return false.</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public HttpResponseMessage UpdateOrderStatus([FromBody] OrderStateParameterModel model)
        {
            HttpResponseMessage response;
            try
            {
                /*
                  1. Send List of status Id for Order status to eric
                  2. in last project they send ship date , but out of box it set ship date as current date.
                  3. What about shipping info.
                */
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _orderService.UpdateOrderStatus(model) });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return response;
        }

        /// <summary>
        /// Update order status many.
        /// </summary>
        /// <param name="model">OrderStateParameterModel</param>
        /// <returns>Returns true if updated sucessfully else return false.</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public HttpResponseMessage UpdateOrderStatusMany([FromBody] OrderLineItemDataListModel orderDetailListModel)
        {
            HttpResponseMessage response;
            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _APIorderService.UpdateOrderStatusMany(orderDetailListModel) });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return response;
        }
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public HttpResponseMessage ExecutePackage()
        {
            HttpResponseMessage response;
            try
            {
                string data = _APIorderService.ExecutePackage();
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse(new StringResponse { Response = data }) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return response;
        }
    }
}
