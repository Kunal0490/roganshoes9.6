﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Controllers;
using Znode.Sample.Api.Model.Responses;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Custom.Controller
{
    public class RSSalesDetailsController : BaseController
    {
        private readonly IRSSalesDetailsService _RSSalesDetailsService;
        public RSSalesDetailsController(IRSSalesDetailsService RSSalesDetailsService)
        {
            _RSSalesDetailsService = RSSalesDetailsService;
        }

        [HttpGet]
        public virtual RSSalesDetailsResponse GetSalesDetails()
        {
            List<RSSalesDetailsModel> SalesDetails = _RSSalesDetailsService.GetSalesDetails();
            RSSalesDetailsResponse response = new RSSalesDetailsResponse { SalesDetails = SalesDetails };
            return response;
        }

    }
}
