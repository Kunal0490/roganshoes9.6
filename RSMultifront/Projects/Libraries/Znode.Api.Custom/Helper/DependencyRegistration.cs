﻿using Autofac;
using Znode.Api.Custom.LibrariesAdmin;
using Znode.Api.Custom.Maps;
using Znode.Api.Custom.Service;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Custom.Service.Service;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper
{
    public class DependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<CustomPortalService>().As<ICustomPortalService>().InstancePerRequest();
            builder.RegisterType<RSPublishProductService>().As<IPublishProductService>().InstancePerRequest();
            builder.RegisterType<RSPublishProductService>().As<IRSPublishProductService>().InstancePerRequest();
            builder.RegisterType<RSShoppingCartService>().As<IShoppingCartService>().InstancePerRequest();
            builder.RegisterType<RSOrderService>().As<IOrderService>().InstancePerRequest();
            builder.RegisterType<RSOrderAPIService>().As<IRSOrderAPIService>().InstancePerRequest();
            builder.RegisterType<RSCustomerReviewService>().As<ICustomerReviewService>().InstancePerRequest();
            builder.RegisterType<RSZnodeOrderReceipt>().As<ZnodeOrderReceipt>().InstancePerRequest();
            builder.RegisterType<RSCustomizedFormService>().As<IRSCustomizedFormService>().InstancePerRequest();
            builder.RegisterType<RSUserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<RSPublishProductHelper>().As<IPublishProductHelper>().InstancePerDependency();
            builder.RegisterType<RSOrderHelper>().As<IZnodeOrderHelper>().InstancePerDependency();
            builder.RegisterType<RSProductFeedService>().As<IProductFeedService>().InstancePerRequest();
            builder.RegisterType<RSZnodeCheckout>().As<IZnodeCheckout>().InstancePerDependency();

            builder.RegisterType<RSZnodeShoppingCart>().As<IZnodeShoppingCart>().InstancePerRequest();
            builder.RegisterType<RSShoppingCartItemMap>().As<IShoppingCartItemMap>().InstancePerRequest();
            builder.RegisterType<RSWebstoreProductService>().As<IProductService>().InstancePerRequest();
            builder.RegisterType<RSSearchService>().As<ISearchService>().InstancePerRequest();
            builder.RegisterType<RSSalesDetailsService>().As<IRSSalesDetailsService>().InstancePerRequest();
            //Here override znode base code method by injecting dependancy mention as below.
            //"In CustomPortalService.cs we have override 'DeletePortal()' of znode base code".
            //builder.RegisterType<CustomPortalService>().As<IPortalService>().InstancePerRequest();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
