﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using System.Data;
using Znode.Libraries.Data;
namespace Znode.Api.Custom.ShoppingCart
{
    public class RSZnodeShoppingCart : ZnodeShoppingCart
    {
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IZnodeOrderHelper orderHelper;
        public RSZnodeShoppingCart()
        {
            publishProductHelper = GetService<IPublishProductHelper>();
            orderHelper = GetService<IZnodeOrderHelper>();
        }
        protected override void BindProductDetails(ZnodeShoppingCartItem znodeCartItem, PublishProductModel productModel, string parentSKu = null, int userId = 0, int omsOrderId = 0, decimal? unitPrice = null, string parentSKUProductName = null, int profileId = 0)
        {
            int catalogVersionId = GetCatalogVersionId(productModel.PublishedCatalogId, productModel.LocaleId);
            // ProductEntity product = publishProductHelper.GetPublishProductBySKU(productModel.SKU, productModel.PublishedCatalogId, productModel.LocaleId, catalogVersionId, omsOrderId);
            PublishProductModel product = GetPublishProductBySKU(productModel.SKU, productModel.PublishedCatalogId, productModel.LocaleId, catalogVersionId, omsOrderId)?.ToModel<PublishProductModel>();
            if (IsNotNull(product) && IsNotNull(znodeCartItem))
            {
                bool isGroupProduct = productModel.GroupProductSKUs.Count > 0;
                string countryCode = znodeCartItem.ShippingAddress?.CountryName;
                PublishProductModel publishProduct = product;
                publishProduct.GroupProductSKUs = productModel.GroupProductSKUs;
                publishProduct.ConfigurableProductId = productModel.ParentPublishProductId;
                ZnodeProduct baseProduct = GetProductDetails(publishProduct, this.PortalId.GetValueOrDefault(), productModel.LocaleId, catalogVersionId, znodeCartItem.ShippingAddress?.CountryName, isGroupProduct, parentSKu, userId, omsOrderId, parentSKUProductName, profileId);
                znodeCartItem.ProductCode = product.Attributes.Where(x => x.AttributeCode == ZnodeConstant.ProductCode)?.FirstOrDefault()?.AttributeValues;
                znodeCartItem.ProductType = product.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues?.FirstOrDefault()?.Code;
                znodeCartItem.Product = new ZnodeProductBase(baseProduct, znodeCartItem.ShippingAddress, unitPrice);
                znodeCartItem.Product.ZNodeAddonsProductCollection = GetZnodeProductAddons(productModel, productModel.PublishedCatalogId, productModel.LocaleId, baseProduct.AddOns, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeBundleProductCollection = GetZnodeProductBundles(productModel, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeConfigurableProductCollection = GetZnodeProductConfigurables(productModel.ConfigurableProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, productModel.ParentPublishProductId, userId, omsOrderId, profileId, productModel.Quantity.GetValueOrDefault(), catalogVersionId);
                znodeCartItem.Product.ZNodeGroupProductCollection = GetZnodeProductGroup(productModel.GroupProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Quantity = GetProductQuantity(znodeCartItem, productModel.Quantity.GetValueOrDefault());
                znodeCartItem.ParentProductId = productModel.ParentPublishProductId;
                znodeCartItem.UOM = baseProduct.UOM;
                znodeCartItem.ParentProductSKU = znodeCartItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group)
                                       ? znodeCartItem.ParentProductSKU : product.SKU;
                znodeCartItem.Product.SKU = !string.IsNullOrEmpty(parentSKu) && (!string.IsNullOrEmpty(productModel.ConfigurableProductSKUs) || isGroupProduct) ? parentSKu : product.SKU;
                //Nivi Image Changes start
                znodeCartItem.Image = znodeCartItem.Product.ZNodeGroupProductCollection?.Count > 0 ? znodeCartItem.Product.ZNodeGroupProductCollection[0].Attributes?.Where(x => x.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValue : product.Attributes?.Where(x => x.AttributeCode == "BaseImage")?.FirstOrDefault()?.AttributeValues;
                ZnodeLogging.LogMessage("RSZnodeShoppingCart - shopping cart" + znodeCartItem.Image, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                //Nivi Image Changes End
                znodeCartItem.Product.Container = GetAttributeValueByCode(znodeCartItem, product, ZnodeConstant.ShippingContainer);
                znodeCartItem.Product.Size = GetAttributeValueByCode(znodeCartItem, product, ZnodeConstant.ShippingSize);
                znodeCartItem.Product.PackagingType = product.Attributes.Where(x => x.AttributeCode == ZnodeConstant.PackagingType)?.FirstOrDefault()?.SelectValues[0]?.Value;
                znodeCartItem.Product.DownloadableProductKey = GetProductKey(znodeCartItem.Product.SKU, znodeCartItem.Quantity, znodeCartItem.OmsOrderLineItemId);
                znodeCartItem.AssociatedAddOnProducts = productModel.AssociatedAddOnProducts;
                SetInventoryData(znodeCartItem.Product);
            }
        }

        protected override void BindProductDetailsV2(ZnodeShoppingCartItem znodeCartItem, PublishProductModel productModel, PublishProductModel publishProduct,
            string parentSKu = null, int userId = 0, int omsOrderId = 0, decimal? unitPrice = null, string parentSKUProductName = null,
            int profileId = 0, int catalogVersionId = 0, List<PublishProductModel> publishProducts = null, List<TaxClassRuleModel> lstTaxClassSKUs = null,
            List<ZnodePimDownloadableProduct> lstDownloadableProducts = null, List<PublishedConfigurableProductEntityModel> configEntities = null)
        {
            if (IsNotNull(publishProduct) && IsNotNull(znodeCartItem))
            {
                bool isGroupProduct = productModel.GroupProductSKUs.Count > 0;
                string countryCode = znodeCartItem.ShippingAddress?.CountryName;
                publishProduct.GroupProductSKUs = productModel.GroupProductSKUs;
                publishProduct.ConfigurableProductId = productModel.ParentPublishProductId;
                publishProduct.ParentPublishProductId = productModel.ParentPublishProductId;
                productModel.PublishProductId = publishProduct.PublishProductId;
                publishProduct.SEOUrl = productModel.ParentPublishProductId > 0 ? publishProducts.FirstOrDefault(x => x.PublishProductId == productModel.ParentPublishProductId)?.SEOUrl : publishProduct.SEOUrl;
                ZnodeProduct baseProduct = GetProductDetailsV2(publishProduct, this.PortalId.GetValueOrDefault(), productModel.LocaleId, znodeCartItem.ShippingAddress?.CountryName, isGroupProduct, parentSKu, userId, omsOrderId, parentSKUProductName, profileId, lstTaxClassSKUs, configEntities);
                znodeCartItem.ProductCode = publishProduct.Attributes.Where(x => x.AttributeCode == ZnodeConstant.ProductCode)?.FirstOrDefault()?.AttributeValues;
                znodeCartItem.ProductType = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues?.FirstOrDefault()?.Code;
                znodeCartItem.Product = new ZnodeProductBase(baseProduct, znodeCartItem.ShippingAddress, unitPrice);
                znodeCartItem.Product.ZNodeAddonsProductCollection = GetZnodeProductAddons(productModel, productModel.PublishedCatalogId, productModel.LocaleId, baseProduct.AddOns, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeBundleProductCollection = GetZnodeProductBundles(productModel, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeConfigurableProductCollection = GetZnodeProductConfigurables(productModel.ConfigurableProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, productModel.ParentPublishProductId, userId, omsOrderId, profileId, productModel.Quantity.GetValueOrDefault(), catalogVersionId, publishProducts, lstTaxClassSKUs, configEntities);
                znodeCartItem.Product.ZNodeGroupProductCollection = GetZnodeProductGroup(productModel.GroupProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Quantity = GetProductQuantity(znodeCartItem, productModel.Quantity.GetValueOrDefault());
                znodeCartItem.ParentProductId = productModel.ParentPublishProductId;
                znodeCartItem.UOM = baseProduct.UOM;
                znodeCartItem.ParentProductSKU = znodeCartItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group)
                                       ? znodeCartItem.ParentProductSKU : publishProduct.SKU;
                znodeCartItem.Product.SKU = !string.IsNullOrEmpty(parentSKu) && (!string.IsNullOrEmpty(productModel.ConfigurableProductSKUs) || isGroupProduct) ? parentSKu : publishProduct.SKU;
                //Nivi Image Changes start               
                znodeCartItem.Image = znodeCartItem.Product.ZNodeGroupProductCollection?.Count > 0 ? znodeCartItem.Product.ZNodeGroupProductCollection[0].Attributes?.Where(x => x.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValue : publishProduct.Attributes?.Where(x => x.AttributeCode == "BaseImage")?.FirstOrDefault()?.AttributeValues;
                //Nivi Image Changes End

                znodeCartItem.Product.Container = GetAttributeValueByCode(znodeCartItem, publishProduct, ZnodeConstant.ShippingContainer);
                znodeCartItem.Product.Size = GetAttributeValueByCode(znodeCartItem, publishProduct, ZnodeConstant.ShippingSize);
                znodeCartItem.Product.PackagingType = publishProduct.Attributes.Where(x => x.AttributeCode == ZnodeConstant.PackagingType)?.FirstOrDefault()?.SelectValues[0]?.Value;
                znodeCartItem.Product.DownloadableProductKey = GetProductKey(znodeCartItem.Product.SKU, znodeCartItem.Quantity, znodeCartItem.OmsOrderLineItemId, lstDownloadableProducts);
                znodeCartItem.AssociatedAddOnProducts = productModel.AssociatedAddOnProducts;
                SetInventoryData(znodeCartItem.Product);
            }
        }

        public override bool IsInventoryInStock()
        {
            //Initialize SKU quantity per line items
            PreOrderSubmitProcessInItSKUQuantity();
            //Check quantity with in-stock inventory
            bool isInventoryInStock = CheckWithInStockInventory();
            return isInventoryInStock;
        }

        public bool CheckWithInStockInventory()
        {
            bool isInventoryInStock = true;
            List<OrderWarehouseLineItemsModel> OrderWarehouseLineItemsModel = new List<OrderWarehouseLineItemsModel>();
            foreach (ZnodeShoppingCartItem item in ShoppingCartItems)
            {
                OrderWarehouseLineItemsModel lineItem = new OrderWarehouseLineItemsModel();
                lineItem.SKU = item.SKU;
                lineItem.Quantity = item.Quantity;
                lineItem.WarehouseId = item.Custom1 == "" ? 0 : Convert.ToInt32(item.Custom1);
                OrderWarehouseLineItemsModel.Add(lineItem);
            }
            int portalId = this.PortalId ?? ZnodeConfigManager.SiteConfig.PortalId;
            isInventoryInStock = RSCheckInventoryBySKUs(OrderWarehouseLineItemsModel, portalId);

            return isInventoryInStock;
        }
        //Get Inventory Associated to SKU.
        public bool RSCheckInventoryBySKUs(List<OrderWarehouseLineItemsModel> OrderWarehouseLineItemsModel, int portalId)
        {
            string savedCartLineItemXML = HelperUtility.ToXML(OrderWarehouseLineItemsModel);

            //SP call to Manage Order Inventory.
            IZnodeViewRepository<OrderWarehouseLineItemsModel> objStoredProc = new ZnodeViewRepository<OrderWarehouseLineItemsModel>();
            objStoredProc.SetParameter("SkuXml", savedCartLineItemXML, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodeDomainEnum.PortalId.ToString(), PortalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(View_ReturnBooleanEnum.Status.ToString(), null, ParameterDirection.Output, DbType.Int32);
            int Status = 0;
            objStoredProc.ExecuteStoredProcedureList("RSZnode_GetInventoryBySkus @SkuXml, @PortalId,@Status OUT", 2, out Status);

            return Status == 1;
        }

        public override void CheckInventoryAndMinMaxQuantity(out string isInventoryInStockMessage, out Dictionary<int, string> minMaxSelectableQuantity)
        {
            isInventoryInStockMessage = string.Empty;
            isInventoryInStockMessage = !IsInventoryInStock() ? "Unable to place the order as product is not available." : "";

            minMaxSelectableQuantity = IsValidMinAndMaxSelectedQuantity();
        }

        #region EmptyCart
        //public override AddToCartModel SaveAddToCartData(AddToCartModel cartModel, string groupIdProductAttribute = "", GlobalSettingValues groupIdPersonalizeAttribute = null)
        //{
        //    if (IsNotNull(cartModel))
        //    {
        //        //Get CookieMappingId
        //        int cookieMappingId = (!string.IsNullOrEmpty(cartModel.CookieMappingId) ? Convert.ToInt32(new ZnodeEncryption().DecryptData(cartModel.CookieMappingId)) : 0) == 0 ? orderHelper.GetCookieMappingId(cartModel.UserId, cartModel.PortalId) : !string.IsNullOrEmpty(cartModel.CookieMappingId) ? Convert.ToInt32(new ZnodeEncryption().DecryptData(cartModel.CookieMappingId)) : 0;

        //        //Get SavedCartId
        //        int savedCartId = orderHelper.GetSavedCartId(ref cookieMappingId, cartModel.PortalId, cartModel.UserId);

        //        foreach (ShoppingCartItemModel cartItem in cartModel.ShoppingCartItems)
        //        {
        //            BindCartProductDetails(cartItem, cartModel.PublishedCatalogId, cartModel.LocaleId, groupIdProductAttribute, groupIdPersonalizeAttribute);
        //        }

        //        //Save all shopping cart line items.
        //        bool status = orderHelper.SaveAllCartLineItemsInDatabase(savedCartId, cartModel);

        //        cartModel.CookieMappingId = new ZnodeEncryption().EncryptData(cookieMappingId.ToString());

        //        List<ZnodeOmsSavedCartLineItem> lineItems = orderHelper.GetSavedCartLineItem(savedCartId);

        //        lineItems?.RemoveAll(x => x.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns) || x.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles));

        //        cartModel.CartCount = lineItems?.Sum(x => x.Quantity) ?? 0.00M;

        //        cartModel.HasError = !status;

        //        return cartModel;
        //    }
        //    return null;
        //}

        //public override ZnodeShoppingCart LoadFromDatabase(CartParameterModel cartParameterModel)
        //{
        //    // return base.LoadFromDatabase(cartParameterModel);
        //    cartParameterModel.CookieId = !string.IsNullOrEmpty(cartParameterModel.CookieMappingId) ? Convert.ToInt32(new ZnodeEncryption().DecryptData(cartParameterModel.CookieMappingId)) : 0;

        //    //AccountQuoteLineItemModel contains properties of Account Quotes and Saved cart line items.
        //    List<AccountQuoteLineItemModel> cartLineItems;

        //    if (cartParameterModel.OmsQuoteId > 0)
        //    {
        //        cartLineItems = GetAccountQuoteLineItems(cartParameterModel);
        //    }
        //    else
        //    {
        //        //Check if cookieMappingId is null or 0.
        //        if (string.IsNullOrEmpty(cartParameterModel.CookieMappingId) || cartParameterModel.CookieId == 0)
        //        {
        //            List<ZnodeOmsCookieMapping> cookieMappings = orderHelper.GetCookieMappingList(cartParameterModel);
        //            cartParameterModel.CookieId = Convert.ToInt32(cookieMappings?.FirstOrDefault()?.OmsCookieMappingId);
        //            cartParameterModel.CookieMappingId = new ZnodeEncryption().EncryptData(cartParameterModel.CookieId.ToString());
        //        }

        //        //Get saved cart line items.
        //        cartLineItems = GetSavedCartLineItems(cartParameterModel);
        //    }

        //    List<PublishedConfigurableProductEntityModel> configEntities;
        //    List<string> skus = cartLineItems.Select(x => x.SKU.ToLower())?.Distinct().ToList();

        //    List<string> navigationProperties = new List<string> { ZnodeConstant.Promotions, ZnodeConstant.Pricing, ZnodeConstant.Inventory, ZnodeConstant.AddOns, ZnodeConstant.SEO };

        //    int catalogVersionId = publishProductHelper.GetCatalogVersionId(cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId);

        //    List<PublishProductModel> cartLineItemsProductData = publishProductHelper.GetDataForCartLineItems(skus, cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId, navigationProperties, cartParameterModel.UserId.GetValueOrDefault(), cartParameterModel.PortalId, catalogVersionId, out configEntities);

        //    List<TaxClassRuleModel> lstTaxClassSKUs = GetTaxRules(skus);

        //    List<ZnodePimDownloadableProduct> lstDownloadableProducts = new ZnodeRepository<ZnodePimDownloadableProduct>().Table.Where(x => skus.Contains(x.SKU)).ToList();


        //    //Set Portal Id in Context Header, to avoid loop based calls.
        //    //SetPortalIdInRequestHeader();

        //    //Get the Saved Cart Line Item ids, to avoid loop based calls.
        //    // List<int?> lstCartLineIds = GetSavedCartLineItemIds(cartLineItems);

        //    //List<PersonaliseValueModel> lstPersonlizedValues = GetService<IZnodeOrderHelper>()?.GetPersonalisedValueCartLineItem(lstCartLineIds);
        //    //clear existing items in shopping cart           
        //    foreach (AccountQuoteLineItemModel cartLineItem in cartLineItems)
        //    {
        //        bool isConfigurableExists = false;
        //        if (cartParameterModel.OmsQuoteId > 0)
        //        {
        //            isConfigurableExists = ((cartLineItem.ParentOmsQuoteLineItemId < 1 || cartLineItem.ParentOmsQuoteLineItemId == null) && cartLineItems.Any(lineItem => lineItem?.ParentOmsQuoteLineItemId == cartLineItem.OmsQuoteLineItemId && (lineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))))
        //               ? true
        //               : cartLineItems.Any(lineItem => lineItem?.OmsQuoteLineItemId == cartLineItem.ParentOmsQuoteLineItemId && ((cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Configurable)) || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group)) || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Simple))));
        //        }
        //        else
        //        {
        //            isConfigurableExists = (cartLineItem.ParentOmsSavedCartLineItemId < 1 && cartLineItems.Any(lineItem => lineItem?.ParentOmsSavedCartLineItemId == cartLineItem.OmsSavedCartLineItemId && ((lineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)))))
        //           ? true
        //           : cartLineItems.Any(lineItem => lineItem?.OmsSavedCartLineItemId == cartLineItem.ParentOmsSavedCartLineItemId
        //           && ((cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Configurable))
        //           || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Simple))
        //           || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group))
        //           ));
        //        }

        //        //add new items from savedcartlineitem in shopping cart                              
        //        if (isConfigurableExists)
        //            this.AddToShoppingCartV2(cartLineItem, cartLineItems, cartParameterModel, cartLineItemsProductData, catalogVersionId, lstTaxClassSKUs, lstDownloadableProducts, configEntities, new List<PersonaliseValueModel>());
        //    }
        //    return this;
        //}
        //protected new void AddToShoppingCartV2(AccountQuoteLineItemModel cartLineItemModel, List<AccountQuoteLineItemModel> cartLineItems, CartParameterModel cartParameterModel, List<PublishProductModel> publishProductModel, int catalogVersionId, List<TaxClassRuleModel> lstTaxClassSKUs = null, List<ZnodePimDownloadableProduct> lstDownloadableProducts = null, List<PublishedConfigurableProductEntityModel> configEntities = null, List<PersonaliseValueModel> lstPersonlizedValues = null)
        //{
        //    if (string.IsNullOrEmpty(cartLineItemModel.SKU))
        //        return;

        //    string parentSKUProductName = string.Empty;

        //    var configurableLineItem = new List<AccountQuoteLineItemModel>();
        //    configurableLineItem.Add(cartLineItemModel);

        //    List<AccountQuoteLineItemModel> shoppingCartLineItems = cartLineItemModel?.OmsQuoteId > 0 ? (
        //           cartLineItemModel.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group) ?
        //           cartLineItems.Where(x => x.OmsQuoteLineItemId == cartLineItemModel.OmsQuoteLineItemId)?.ToList() :
        //           cartLineItems.Where(x => x.ParentOmsQuoteLineItemId == cartLineItemModel.OmsQuoteLineItemId)?.ToList()) :
        //           cartLineItemModel.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group) ?
        //           cartLineItems.Where(x => x.OmsSavedCartLineItemId == cartLineItemModel.OmsSavedCartLineItemId)?.ToList() :
        //           cartLineItems.Where(x => x.ParentOmsSavedCartLineItemId == cartLineItemModel.OmsSavedCartLineItemId)?.ToList();


        //    this.PortalId = cartParameterModel.PortalId > 0 ? cartParameterModel.PortalId : GetHeaderPortalId();

        //    List<AccountQuoteLineItemModel> bundleLineItems = BindProductType(shoppingCartLineItems, ZnodeCartItemRelationshipTypeEnum.Bundles);
        //    List<AccountQuoteLineItemModel> configurableLineItems = BindProductType(configurableLineItem, ZnodeCartItemRelationshipTypeEnum.Configurable);
        //    List<AccountQuoteLineItemModel> groupLineItems = BindProductType(shoppingCartLineItems, ZnodeCartItemRelationshipTypeEnum.Group);
        //    List<AccountQuoteLineItemModel> addonLineItems = new List<AccountQuoteLineItemModel>();

        //    if (groupLineItems?.Count > 0)
        //    {
        //        foreach (AccountQuoteLineItemModel item in cartLineItems)
        //        {
        //            if (item.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns) && (item.OmsQuoteId > 0 ? groupLineItems.Any(y => y.OmsQuoteLineItemId == item.ParentOmsQuoteLineItemId) : groupLineItems.Any(y => y.OmsSavedCartLineItemId == item.ParentOmsSavedCartLineItemId)))
        //                addonLineItems.Add(item);
        //        }

        //        parentSKUProductName = groupLineItems?.Count > 0 ? cartLineItemModel?.ProductName : string.Empty;
        //    }
        //    else
        //        addonLineItems = BindProductType(shoppingCartLineItems, ZnodeCartItemRelationshipTypeEnum.AddOns);

        //    List<AssociatedProductModel> addOnProducts = new List<AssociatedProductModel>();

        //    if (addonLineItems?.Count > 0)
        //        addonLineItems.ForEach(doc => addOnProducts.Add(new AssociatedProductModel { Sku = doc.SKU, Quantity = doc.Quantity }));

        //    ZnodeShoppingCartItem cartLineItem = new ZnodeShoppingCartItem(null);
        //    cartLineItem.OmsOrderId = cartLineItemModel.OmsOrderId;
        //    cartLineItem.OmsSavedCartLineItemId = cartLineItemModel.OmsSavedCartLineItemId;
        //    cartLineItem.ParentOmsSavedCartLineItemId = cartLineItemModel.ParentOmsSavedCartLineItemId;
        //    cartLineItem.CustomText = cartLineItemModel.CustomText;
        //    cartLineItem.CreatedBy = cartLineItemModel.CreatedBy;
        //    cartLineItem.CreatedDate = cartLineItemModel.CreatedDate;
        //    cartLineItem.ModifiedBy = cartLineItemModel.ModifiedBy;
        //    cartLineItem.ModifiedDate = cartLineItemModel.ModifiedDate;
        //    string parentSKu = cartParameterModel.OmsQuoteId > 0 ? GetQuoteParentSKU(cartLineItemModel, cartLineItems) : GetParentSKU(cartLineItemModel, cartLineItems);
        //    if (cartParameterModel.OmsQuoteId > 0)
        //    {
        //        //Map AccountQuoteLineItemModel to AccountQuoteLineItemModel.
        //        ToZNodeShoppingCartItem(cartLineItemModel, cartLineItem, parentSKu);
        //    }

        //    List<AssociatedProductModel> groupProduct = new List<AssociatedProductModel>();

        //    if (groupLineItems?.Count > 0)
        //        groupLineItems.ForEach(doc => groupProduct.Add(new AssociatedProductModel { Sku = doc.SKU, Quantity = doc.Quantity, OmsSavedCartLineItemId = doc.OmsSavedCartLineItemId, ProductName = doc.ProductName }));

        //    //Get cartitem having configurable product sku.
        //    AccountQuoteLineItemModel cartItem = cartLineItem.OmsQuoteId > 0 ? cartLineItems.FirstOrDefault(x => x.ParentOmsQuoteLineItemId == cartLineItemModel.OmsQuoteLineItemId && x.OrderLineItemRelationshipTypeId == 3)
        //                         : cartLineItems.FirstOrDefault(x => x.ParentOmsSavedCartLineItemId == cartLineItemModel.OmsSavedCartLineItemId && x.OrderLineItemRelationshipTypeId == 3);

        //    if (IsNotNull(cartItem))
        //        cartLineItemModel = cartItem;

        //    DateTime dtBindProductStartTime, dtBindProductEndTime;
        //    TimeSpan BindProducttotalTime;
        //    dtBindProductStartTime = DateTime.Now;
        //    ZnodeLogging.LogMessage("Cart - NewBindProductDetails - Start Time=" + dtBindProductStartTime.ToString(), "Cart", TraceLevel.Info);

        //    BindProductDetailsV2(cartLineItem, new PublishProductModel
        //    {
        //        SKU = cartLineItemModel.SKU,
        //        ParentPublishProductId = publishProductModel.FirstOrDefault(x => x.SKU.Equals(parentSKu, StringComparison.InvariantCultureIgnoreCase))?.PublishProductId ?? 0,
        //        Quantity = cartLineItemModel.Quantity,
        //        LocaleId = cartParameterModel.LocaleId,
        //        PublishedCatalogId = cartParameterModel.PublishedCatalogId,
        //        AddonProductSKUs = string.Join(",", addonLineItems.Select(b => b.SKU)),
        //        AssociatedAddOnProducts = addOnProducts,
        //        BundleProductSKUs = string.Join(",", bundleLineItems.Select(b => b.SKU)),
        //        ConfigurableProductSKUs = string.Join(",", configurableLineItems.Select(b => b.SKU)),
        //        GroupProductSKUs = groupProduct,
        //    }, publishProductModel.FirstOrDefault(x => x.SKU == cartLineItemModel.SKU), parentSKu, cartParameterModel.UserId.GetValueOrDefault(), 0, null, parentSKUProductName, cartParameterModel.ProfileId, catalogVersionId, publishProductModel, lstTaxClassSKUs, lstDownloadableProducts, configEntities);


        //    dtBindProductEndTime = DateTime.Now;
        //    BindProducttotalTime = dtBindProductEndTime.Subtract(dtBindProductStartTime);
        //    ZnodeLogging.LogMessage("Cart - NewBindProductDetails - Total Time=" + BindProducttotalTime.ToString(), "Cart", TraceLevel.Info);

        //    if (cartLineItemModel.OmsSavedCartLineItemId.Equals(0))
        //    {
        //        cartLineItem.PersonaliseValuesDetail = GetService<IZnodeOrderHelper>()?.GetPersonalisedQuoteValueCartLineItem(cartLineItemModel.OmsQuoteLineItemId);

        //        cartLineItem.GroupId = (cartLineItemModel.ParentOmsQuoteLineItemId.GetValueOrDefault() > 0)
        //          ? cartLineItems.Where(x => x.OmsQuoteLineItemId == cartLineItemModel.ParentOmsQuoteLineItemId).Select(x => x.GroupId).FirstOrDefault()
        //          : cartLineItems.Where(x => x.OmsQuoteLineItemId == cartLineItemModel.OmsQuoteLineItemId).Select(x => x.GroupId).FirstOrDefault();
        //    }
        //    else
        //    {
        //        //This will have only child cart line item id. For simple product it will have the parent ID itself.
        //        int cartLineItemId = cartLineItemModel.OmsSavedCartLineItemId;

        //        if (IsNotNull(bundleLineItems) && bundleLineItems.Count > 0)
        //        {
        //            int? cartItemId = bundleLineItems.FirstOrDefault(x => x.ParentOmsSavedCartLineItemId == cartLineItemId)?.OmsSavedCartLineItemId;
        //            cartLineItemId = cartItemId > 0 ? cartItemId.GetValueOrDefault() : cartLineItemId;
        //        }

        //        List<PersonaliseValueModel> childLinePersonlizedValues = lstPersonlizedValues.Where(x => x.OmsSavedCartLineItemId == cartLineItemId).ToList();

        //        cartLineItem.PersonaliseValuesDetail = childLinePersonlizedValues?.Count > 0 ? childLinePersonlizedValues : lstPersonlizedValues.Where(x => x.OmsSavedCartLineItemId == cartLineItemModel.ParentOmsSavedCartLineItemId).ToList();

        //        cartLineItem.GroupId = (cartLineItemModel.ParentOmsSavedCartLineItemId > 0)
        //            ? cartLineItems.Where(x => x.OmsSavedCartLineItemId == cartLineItemModel.ParentOmsSavedCartLineItemId).Select(x => x.GroupId).FirstOrDefault()
        //            : cartLineItems.Where(x => x.OmsSavedCartLineItemId == cartLineItemModel.OmsSavedCartLineItemId).Select(x => x.GroupId).FirstOrDefault();
        //    }

        //    cartLineItem.OrderLineItemRelationshipTypeId = cartLineItemModel.OrderLineItemRelationshipTypeId;

        //    if (!string.IsNullOrEmpty(cartLineItemModel.AutoAddon))
        //        cartLineItem.AutoAddonSKUs = cartLineItemModel.AutoAddon;

        //    BindCustomData(cartLineItemModel, cartLineItem);

        //    if (IsNotNull(cartLineItem.Product))
        //        this.ShoppingCartItems.Add(cartLineItem);

        //}
       
        #endregion
    }
}