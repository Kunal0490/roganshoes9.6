﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Znode.Engine.Services;
using System.Configuration;

namespace Znode.Api.Custom.ShoppingCart
{
    public class RSZnodeOrderReceipt : ZnodeOrderReceipt
    {
        private readonly IZnodeOrderHelper orderHelper;
        private string _cultureCode = string.Empty;
        private string _currencyCode = string.Empty;

        public RSZnodeOrderReceipt(ZnodeOrderFulfillment order, ZnodeShoppingCart shoppingCart) : base(order, shoppingCart)
        {

        }
        public RSZnodeOrderReceipt(ZnodeOrderFulfillment order) : base(order)
        {
            Order = order;
        }

        public RSZnodeOrderReceipt(OrderModel order) : base(order)
        {
            OrderModel = order;
            orderHelper = GetService<IZnodeOrderHelper>();
        }


        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}" : Order.ShippingAddress.CompanyName;
                if (orderShipment.ShipToCompanyName != null)
                {
                    orderShipment.ShipToCompanyName = "<br />" + orderShipment.ShipToCompanyName;
                }
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}";
            }
            return string.Empty;
        }
        //to get shipping address
        public override string GetOrderBillingAddress(AddressModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street2 = string.IsNullOrEmpty(orderBilling.Address2) ? string.Empty : "<br />" + orderBilling.Address2;
                if (orderBilling?.CompanyName != null)
                {
                    orderBilling.CompanyName = "<br />" + orderBilling?.CompanyName;
                }
                return $"{orderBilling?.FirstName}{" "}{orderBilling?.LastName}{orderBilling?.CompanyName}{"<br />"}{orderBilling.Address1}{street2}{"<br />"}{ orderBilling.CityName}{"<br />"}{orderBilling.StateName}{"<br />"}{orderBilling.PostalCode}{"<br />"}{orderBilling.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.PhoneNumber}";
            }
            return string.Empty;
        }

        public override string GetOrderShippingAddress(OrderModel orderShipping)
        {
            if (IsNotNull(orderShipping))
            {
                string street1 = string.IsNullOrEmpty(orderShipping.ShippingAddress.Address2) ? string.Empty : "<br />" + orderShipping.ShippingAddress.Address2;
                if (orderShipping?.ShippingAddress.CompanyName != null)
                {
                    orderShipping.ShippingAddress.CompanyName = "<br />" + orderShipping?.ShippingAddress.CompanyName;
                }
                return $"{orderShipping?.ShippingAddress.FirstName}{" "}{orderShipping?.ShippingAddress.LastName}{orderShipping.ShippingAddress.CompanyName}{"<br />"}{orderShipping.ShippingAddress.Address1}{street1}{"<br />"}{ orderShipping.ShippingAddress.CityName}{"<br />"}{orderShipping.ShippingAddress.StateName}{"<br />"}{orderShipping.ShippingAddress.PostalCode}{"<br />"}{orderShipping.ShippingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipping.ShippingAddress.PhoneNumber}";
            }
            return string.Empty;
        }

        public override DataTable CreateOrderLineItemTable()
        {
            DataTable orderlineItemTable = new DataTable();
            orderlineItemTable.Columns.Add("Image");
            orderlineItemTable.Columns.Add("Name");
            orderlineItemTable.Columns.Add("SKU");
            orderlineItemTable.Columns.Add("Quantity");
            orderlineItemTable.Columns.Add("Description");
            orderlineItemTable.Columns.Add("UOMDescription");
            orderlineItemTable.Columns.Add("Price");
            orderlineItemTable.Columns.Add("ExtendedPrice");
            orderlineItemTable.Columns.Add("OmsOrderShipmentID");
            orderlineItemTable.Columns.Add("ShortDescription");
            orderlineItemTable.Columns.Add("ShippingId");
            orderlineItemTable.Columns.Add("OrderLineItemState");
            orderlineItemTable.Columns.Add("TrackingNumber");
            orderlineItemTable.Columns.Add("Custom1");
            orderlineItemTable.Columns.Add("Custom2");
            orderlineItemTable.Columns.Add("Custom3");
            orderlineItemTable.Columns.Add("Custom4");
            orderlineItemTable.Columns.Add("Custom5");
            orderlineItemTable.Columns.Add("GroupId");
            orderlineItemTable.Columns.Add("GroupingRowspan");
            orderlineItemTable.Columns.Add("GroupingDisplay");
            return orderlineItemTable;
        }
        //to set order details
        // Builds the order line item table.
        public override void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable)
        {
            List<OrderLineItemModel> OrderLineItemList = Order?.OrderLineItems.GroupBy(p => new { p.OmsOrderShipmentId }).Select(g => g.First()).ToList();
            IEnumerable<OrderShipmentModel> orderShipments = OrderLineItemList.Select(s => s.ZnodeOmsOrderShipment);

            int shipmentCounter = 1;

            foreach (OrderShipmentModel orderShipment in orderShipments)
            {
                DataRow addressRow = multipleAddressTable.NewRow();

                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = $"Shipment #{shipmentCounter++}{orderShipment.ShipName}";
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OmsOrderShipmentID"] = orderShipment.OmsOrderShipmentId;
                int counter = 0;

                foreach (OrderLineItemModel lineitem in Order.OrderLineItems.Where(x => x.OmsOrderShipmentId == orderShipment.OmsOrderShipmentId))
                {
                    IEnumerable<ZnodeShoppingCartItem> shoppingCartItems = ((ZnodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OmsOrderShipmentId).SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>());
                    ImageHelper imageHelper = new ImageHelper(Order.PortalId);
                    lineitem.OrderLineItemCollection.RemoveAll(x => x.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns));
                    if (lineitem.OrderLineItemCollection?.Any(x => x.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)) ?? false)
                    {
                        foreach (OrderLineItemModel childLineItem in lineitem.OrderLineItemCollection)
                        {
                            ZnodeShoppingCartItem shoppingCartItem = null;
                            if (Order.Order.IsQuoteOrder)
                            {
                                shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && (!string.IsNullOrEmpty(s.SKU) ? childLineItem.Sku.Contains(s.SKU) : false) && s.OrderLineItemRelationshipTypeId.HasValue);
                                if (IsNull(shoppingCartItem))
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && childLineItem.Sku == s.ParentProductSKU && s.OrderLineItemRelationshipTypeId.HasValue);
                                }
                            }
                            else
                            {
                                shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && s.SKU == childLineItem.Sku && s.OrderLineItemRelationshipTypeId.HasValue);
                            }

                            //Get ShoppingCartItem when GroupId is Null
                            if (IsNull(shoppingCartItem))
                            {
                                shoppingCartItem = shoppingCartItems.FirstOrDefault(m => m.ParentProductSKU.Equals(childLineItem.Sku));
                            }
                            setGroupProductDetails(lineitem, childLineItem);

                            StringBuilder sb = new StringBuilder();
                            sb.Append(lineitem.ProductName + "<br />");

                            //For binding personalise attribute to Name
                            if (IsNotNull(lineitem.PersonaliseValueList))
                            {
                                sb.Append(GetPersonaliseAttributes(lineitem.PersonaliseValueList));
                            }
                            else
                            {
                                sb.Append(GetPersonaliseAttributesDetail(lineitem.PersonaliseValuesDetail));
                            }

                            if (!String.IsNullOrEmpty(shoppingCartItem?.Product?.DownloadLink?.Trim()))
                            {
                                sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                            }

                            if (orderLineItemTable != null)
                            {
                                //Nivi Image change code start
                                string OrderReceiptImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_55/";
                                string images = OrderReceiptImagePath + Convert.ToString(shoppingCartItem.Product.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValue);

                                //Nivi Image change code end
                                string ProductImage = "<img src=\"" + images + "\" data-src=\"" + images + "\"/>";

                                DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                                orderlineItemDbRow["Image"] = ProductImage;
                                orderlineItemDbRow["Name"] = sb.ToString();
                                orderlineItemDbRow["SKU"] = childLineItem.Sku;
                                orderlineItemDbRow["Description"] = lineitem.Description;
                                orderlineItemDbRow["UOMDescription"] = string.Empty;
                                orderlineItemDbRow["Quantity"] = childLineItem.Quantity;
                                if (childLineItem.Custom4 == "PICKUP")
                                {
                                    orderlineItemDbRow["Custom2"] = "PickUp From-" + childLineItem.Custom2;
                                }
                                else
                                {
                                    orderlineItemDbRow["Custom2"] = "To be Shipped";
                                }
                                if (shoppingCartItem != null)
                                {
                                    orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                                    orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                                    orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                                }

                                orderlineItemDbRow["OmsOrderShipmentID"] = childLineItem.OmsOrderShipmentId;
                                orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;

                                orderLineItemTable.Rows.Add(orderlineItemDbRow);
                            }
                            counter++;
                        }
                    }
                    else
                    {
                        ZnodeShoppingCartItem shoppingCartItem = shoppingCartItems.ElementAt(counter++);

                        foreach (OrderLineItemModel orderLineItem in lineitem.OrderLineItemCollection)
                        {
                            setGroupProductDetails(lineitem, orderLineItem);
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append(lineitem.ProductName + "<br />");

                        if (!String.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
                        {
                            sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                        }

                        if (orderLineItemTable != null)
                        {
                            DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                            ZnodeLogging.LogMessage("ProductImage-" + lineitem.ProductImagePath, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                            orderlineItemDbRow["Image"] = lineitem.ProductImagePath;
                            orderlineItemDbRow["Name"] = sb.ToString();
                            orderlineItemDbRow["SKU"] = lineitem.Sku;
                            orderlineItemDbRow["Description"] = lineitem.Description;
                            orderlineItemDbRow["UOMDescription"] = string.Empty;
                            orderlineItemDbRow["Quantity"] = lineitem.OrderLineItemRelationshipTypeId.Equals((int)ZnodeCartItemRelationshipTypeEnum.Group) ? lineitem.GroupProductQuantity : Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
                            orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                            orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                            orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
                            orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                            orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;
                            orderLineItemTable.Rows.Add(orderlineItemDbRow);
                        }
                    }
                }

                ZnodeMultipleAddressCart addressCart = ((ZnodePortalCart)ShoppingCart).AddressCarts.FirstOrDefault(y => y.OrderShipmentID == orderShipment.OmsOrderShipmentId);

                if (addressCart != null && orderShipments.Count() > 1)
                {
                    object globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
                    if (globalResourceObject != null)
                    {
                        BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), addressCart.SubTotal, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
                    }

                    BuildOrderShipmentTotalLineItem($"Shipping Cost({addressCart.Shipping.ShippingName})", addressCart.ShippingCost, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax").ToString(), addressCart.SalesTax, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), addressCart.VAT, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), addressCart.GST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), addressCart.HST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), addressCart.PST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
                }
                multipleAddressTable.Rows.Add(addressRow);
            }
        }
        //Set Order Line Item Table.
        public override DataRow SetOrderLineItemTable(DataTable orderLineItemTable, OrderLineItemModel lineitem, decimal extendedPrice)
        {
            StringBuilder sb = new StringBuilder();
            ImageHelper imageHelper = new ImageHelper(1);
            //Nivi Image change code start
            string OrderReceiptCancel = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_55/";
            string images = OrderReceiptCancel + Convert.ToString(lineitem.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValue) + lineitem.DownloadLink;

            //Nivi Image change code end
            sb.Append(lineitem.ProductName + "<br />");

            //For binding personalise attribute to Name
            sb.Append(GetPersonaliseAttributesDetail(lineitem.PersonaliseValuesDetail));
            DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
            orderlineItemDbRow["Image"] = images;//lineitem.ProductImagePath;
            orderlineItemDbRow["Name"] = sb.ToString();
            orderlineItemDbRow["SKU"] = string.IsNullOrEmpty(lineitem?.OrderLineItemCollection?.FirstOrDefault()?.Sku) ? lineitem.Sku : lineitem?.OrderLineItemCollection.FirstOrDefault().Sku;
            orderlineItemDbRow["Description"] = lineitem.Description;
            orderlineItemDbRow["UOMDescription"] = string.Empty;
            double quantity;
            double.TryParse(Convert.ToString(lineitem.Quantity > 0 ? lineitem.Quantity : lineitem?.OrderLineItemCollection?.FirstOrDefault(x => x.Quantity > 0)?.Quantity), out quantity);
            orderlineItemDbRow["Quantity"] = Convert.ToString(quantity);
            orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(lineitem.Price);
            orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(extendedPrice);
            orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
            orderlineItemDbRow["ShortDescription"] = string.Empty;
            orderlineItemDbRow["OrderLineItemState"] = lineitem.OrderLineItemState;
            orderlineItemDbRow["TrackingNumber"] = lineitem.TrackingNumber;
            orderlineItemDbRow["Custom1"] = string.Empty;
            orderlineItemDbRow["Custom2"] = string.Empty;
            orderlineItemDbRow["Custom3"] = string.Empty;
            orderlineItemDbRow["Custom4"] = string.Empty;
            orderlineItemDbRow["Custom5"] = string.Empty;
            orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;
            return orderlineItemDbRow;
        }

        public override DataRow SetReturnedOrderLineItemTable(DataTable returnedOrderLineItemTable, OrderLineItemModel lineitem, decimal extendedPrice)
        {
            //Nivi Image change code start
            string OrderReceiptCancel = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_55/";
            string ProductImagePath = OrderReceiptCancel + Convert.ToString(lineitem.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValue) + lineitem.DownloadLink;

            //Nivi Image change code end
            DataRow orderlineItemDbRow = returnedOrderLineItemTable.NewRow();
            if (IsNotNull(lineitem))
            {
                orderlineItemDbRow["ReturnedProductImage"] = ProductImagePath;//lineitem.ProductImagePath;
                orderlineItemDbRow["ReturnedName"] = lineitem.ProductName;
                orderlineItemDbRow["ReturnedSKU"] = lineitem.Sku;
                orderlineItemDbRow["ReturnedDescription"] = lineitem.Description;
                orderlineItemDbRow["ReturnedQuantity"] = Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
                orderlineItemDbRow["ReturnedPrice"] = GetFormatPriceWithCurrency(lineitem.Price);
                orderlineItemDbRow["ReturnedOmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
            }
            orderlineItemDbRow["ReturnedExtendedPrice"] = GetFormatPriceWithCurrency(extendedPrice);
            orderlineItemDbRow["ReturnedShortDescription"] = string.Empty;
            orderlineItemDbRow["ReturnedUOMDescription"] = string.Empty;
            return orderlineItemDbRow;
        }
        public override string SetTrackingURL(OrderModel ordermodel)
        {
            string TrackingNumbers = "";
            try
            {
                //Get shipping type name based on provided shipping id
                string SPEEDY = Convert.ToString(ConfigurationManager.AppSettings["SPEEDYTrackingUrl"]);

                string[] shipping = ordermodel.Custom1?.Split(',');
                string[] TrackNumbers = ordermodel.TrackingNumber?.Split(',');
                //string shippingType = GetShippingType(ordermodel.ShippingId);
                int icount = 0;
                if (TrackNumbers?.Count() > 0)
                {
                    foreach (string shippingname in shipping)
                    {
                        if (ZnodeConstant.UPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + "UPS :" + $"<a target=_blank href={ ZnodeApiSettings.UPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.FedEx == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers  + "FedEx :" + $"<a target=_blank href={ ZnodeApiSettings.FedExTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.USPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers  +"USPS :" + $"<a target=_blank href={ ZnodeApiSettings.USPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if ("SPEEDEE" == shippingname)
                            TrackingNumbers = TrackingNumbers + "SPEEDEE :" + $" <a target=_blank href={ SPEEDY + TrackNumbers[icount]}>{TrackNumbers[icount]} </a >" + ",";
                        icount = icount + 1;
                    }
                    if (TrackingNumbers != "")
                    {
                        TrackingNumbers = TrackingNumbers.Remove(TrackingNumbers.Length - 1, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Shipping=" + Convert.ToString(ordermodel.Custom1) + " TrackingNo=" + ordermodel.TrackingNumber + "error =" + ex.Message + " stacktrace =" + ex.StackTrace, string.Empty, TraceLevel.Error);
            }
            return TrackingNumbers;
        }

        //public override string CreateOrderReceipt(string template)
        //{
        //    if (string.IsNullOrEmpty(template))
        //        return template;

        //    //order to bind order details in data tabel
        //    System.Data.DataTable orderTable = SetOrderData();

        //    //create order line Item
        //    DataTable orderlineItemTable = CreateOrderLineItemTable();

        //    //order to bind order amount details in data tabel
        //    DataTable orderAmountTable = SetOrderAmountData();

        //    //create multiple Address
        //    DataTable multipleAddressTable = CreateOrderAddressTable();

        //    //create multiple tax address
        //    DataTable multipleTaxAddressTable = CreateOrderTaxAddressTable();

        //    //bind line item data
        //    BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable);

        //    ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(template);

        //    // Parse order table
        //    receiptHelper.Parse(orderTable.CreateDataReader());

        //    // Parse order line items table
        //    receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
        //    foreach (DataRow address in multipleAddressTable.Rows)
        //    {
        //        // Parse OrderLineItem
        //        var filterData = orderlineItemTable.DefaultView;

        //        List<DataTable> group = filterData.ToTable().AsEnumerable()
        //        .GroupBy(r => new { Col1 = r["GroupId"] })
        //        .Select(g => g.CopyToDataTable()).ToList();

        //        filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
        //        receiptHelper.ParseWithGroup("LineItems" + address["OmsOrderShipmentID"], group);

        //        //Parse Tax based on order shipment
        //        var amountFilterData = multipleTaxAddressTable.DefaultView;
        //        amountFilterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
        //        receiptHelper.Parse($"AmountLineItems{address["OmsOrderShipmentID"]}", amountFilterData.ToTable().CreateDataReader());
        //    }
        //    // Parse order amount table
        //    receiptHelper.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());
        //    //Replace the Email Template Keys, based on the passed email template parameters.

        //    // Return the HTML output
        //    return receiptHelper.Output;
        //}
    }
}
