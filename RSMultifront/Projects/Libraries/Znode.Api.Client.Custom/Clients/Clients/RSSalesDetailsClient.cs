﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Znode.Sample.Api.Model.Responses;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Client.Custom.Clients.Clients
{
    public class RSSalesDetailsClient : BaseClient, IRSSalesDetailsClient
    {
        public virtual List<RSSalesDetailsModel> GetSalesDetails()
        {
            string endpoint = RSSalesDetailsEndpoint.GetSalesDetails();
            ApiStatus status = new ApiStatus();
            RSSalesDetailsResponse response = GetResourceFromEndpoint<RSSalesDetailsResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return response?.SalesDetails;
        }
    }
}
