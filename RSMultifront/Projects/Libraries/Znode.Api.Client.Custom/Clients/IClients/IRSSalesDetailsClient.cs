﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Client.Custom.Clients.IClients
{
    public interface IRSSalesDetailsClient
    {
        List<RSSalesDetailsModel> GetSalesDetails();
    }
}
