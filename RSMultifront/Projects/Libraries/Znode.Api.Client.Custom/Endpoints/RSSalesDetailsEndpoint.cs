﻿using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class RSSalesDetailsEndpoint : BaseEndpoint
    {
        public static string GetSalesDetails() => $"{ApiRoot}/rssalesdetails/getsalesdetails";
    }
}
