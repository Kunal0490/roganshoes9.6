﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Api.Client;
using Znode.Libraries.Framework.Business;
using System.Configuration;
using System.Web;
using System.IO;
using System.Diagnostics;

namespace Znode.Admin.Custom.Agents.Agents
{
    public class RSOrderAgent: OrderAgent
    {
        public RSOrderAgent(IShippingClient shippingClient, IShippingTypeClient shippingTypeClient, IStateClient stateClient,
             ICityClient cityClient, IProductsClient productClient, IBrandClient brandClient,
             IUserClient userClient, IPortalClient portalClient, IAccountClient accountClient, IRoleClient roleClient,
             IDomainClient domainClient, IOrderClient orderClient, IEcommerceCatalogClient ecomCatalogClient,
             ICustomerClient customerClient, IPublishProductClient publishProductClient, IMediaConfigurationClient mediaConfigClient,
             IPaymentClient paymentClient, IShoppingCartClient shoppingCartClient, IAccountQuoteClient accountQuoteClient,
             IOrderStateClient orderStateClient, IPIMAttributeClient pimAttributeClient, ICountryClient countryClient, IAddressClient addressClient) :base(shippingClient, shippingTypeClient, stateClient,cityClient,  productClient,  brandClient,
             userClient,  portalClient,  accountClient,  roleClient, domainClient,  orderClient,  ecomCatalogClient,customerClient,  publishProductClient,  mediaConfigClient,
              paymentClient,  shoppingCartClient,  accountQuoteClient,orderStateClient,  pimAttributeClient,  countryClient,  addressClient)
        {

        }

        public override string GenerateOrderNumber(int portalId)
        {
            try
            {
                string OrderNumber = string.Empty;
                OrderNumber = GetLastOrderNumberFromTextFile();
                return OrderNumber;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Error GenerateOrderNumber : {ex}", ZnodeLogging.Components.OMS.ToString());
                return base.GenerateOrderNumber(portalId);
            }
        }

        private string GetLastOrderNumberFromTextFile()
        {
            try
            {
                //string lastOrderFilePath = HttpContext.Current.Server.MapPath($"{ConfigurationManager.AppSettings["LastRSOrderFilePath"]}");
                string lastOrderFilePath = ConfigurationManager.AppSettings["LastRSOrderFilePath"];
                string lastOrderId = string.Empty;
                using (StreamReader sr = new StreamReader(lastOrderFilePath))
                {
                    while (!sr.EndOfStream)
                    {
                        lastOrderId = sr.ReadToEnd().ToString(); //read full file text  
                    }
                }
                int omslastOrderId = Convert.ToInt32(lastOrderId) + 1;
                WriteTextStorage(Convert.ToString(omslastOrderId), lastOrderFilePath, Mode.Write);
                return Convert.ToString(lastOrderId);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Error GetLastOrderNumberFromTextFile : {ex}", ZnodeLogging.Components.OMS.ToString());

            }
            return null;
        }

        private static void WriteTextStorage(string fileData, string filePath, Mode fileMode)
        {
            try
            {


                // Create directory if not exists.
                string logFilePath = filePath;
                FileInfo fileInfo = new FileInfo(logFilePath);

                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                // Check file write mode and write content.
                if (Equals(fileMode, Mode.Append))
                {
                    File.AppendAllText(logFilePath, fileData);
                }
                else
                {
                    File.WriteAllText(logFilePath, fileData);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                throw;
            }
        }

    }
}
