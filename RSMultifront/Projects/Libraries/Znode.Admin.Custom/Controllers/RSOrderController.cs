﻿using MvcSiteMapProvider;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers.Extensions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

using Newtonsoft.Json;
using Znode.Engine.Admin.Controllers;

namespace Znode.Admin.Custom.Controllers
{
    public class RSOrderController : OrderController
    {

        private readonly IOrderAgent _orderAgent;
        private readonly IUserAgent _userAgent;

        private const string storeListView = "_StoreListAsidePanel";
        private const string orderLineItemView = "_OrderLineItemList";
        private const string manageCustomerAddressView = "_ManageCustomerAddress";
        private const string manageCustomerView = "_ManageCustomerInformation";
        private const string manageTotalTableView = "_ManageTotalTable";
        private const string manageShoppingCartView = "ManageShoppingCart";
        private const string shippingOptionsView = "ShippingOptions";

        public RSOrderController(IOrderAgent orderAgent, IUserAgent userAgent, IShippingAgent shippingAgent, ICartAgent cartAgent, IAccountQuoteAgent quoteAgent, IStoreAgent storeAgent, IRMARequestAgent rmaRequestAgent, IWebSiteAgent websiteAgent):base(orderAgent,  userAgent,  shippingAgent,  cartAgent,  quoteAgent,  storeAgent,  rmaRequestAgent,  websiteAgent)
        {
            _orderAgent = orderAgent;
            _userAgent = userAgent;           
        }
        [HttpGet]
        public override ActionResult AddNewCustomer(int portalId)
       => ActionView("CreateCustomer", new CustomerViewModel { StoreName = _userAgent.GetStoreName(portalId), Accounts = new List<SelectListItem>() });


        //Create new customer.
        [HttpPost]
        public override ActionResult AddNewCustomer(CustomerViewModel userAddressDataViewModel)
        {
            if (IsNotNull(userAddressDataViewModel.AccountId) && !string.IsNullOrEmpty(userAddressDataViewModel.RoleName) && string.Equals(userAddressDataViewModel.RoleName, ZnodeRoleEnum.User.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                userAddressDataViewModel.AccountPermissionList = _userAgent.GetPermissionList(userAddressDataViewModel.AccountId.GetValueOrDefault(), userAddressDataViewModel.AccountPermissionAccessId.GetValueOrDefault());
            }

            if (ModelState.IsValid)
            {
                CustomerViewModel model = _userAgent.CreateCustomerAccount(userAddressDataViewModel);

                if (IsNotNull(model) && !model.HasError)
                {
                   // _userAgent.SetOMSCustomerViewModel(userAddressDataViewModel);
                    userAddressDataViewModel.UserId = model.UserId;

                    return ActionView(AdminConstants.CreateCustomerView, userAddressDataViewModel);
                }
            }
           // _userAgent.SetOMSCustomerViewModel(userAddressDataViewModel);
            userAddressDataViewModel.Portals = _userAgent.GetPortals();
            if (userAddressDataViewModel?.RoleName?.ToLower() == AdminConstants.UserRoleName)
                _userAgent.GetPermissionList(userAddressDataViewModel.AccountId.GetValueOrDefault(), userAddressDataViewModel.AccountPermissionAccessId.GetValueOrDefault());

            return ActionView(AdminConstants.CreateCustomerView, userAddressDataViewModel);
        }
    }
}
