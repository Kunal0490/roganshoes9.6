var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var User = /** @class */ (function (_super) {
    __extends(User, _super);
    function User() {
        return _super.call(this) || this;
    }
    User.prototype.Init = function () {
        User.prototype.RemoveIconWishlist();
        User.prototype.LoadQuote();
        User.prototype.RestrictEnterButton();
        User.prototype.BindStates(null);
    };
    User.prototype.RestrictEnterButton = function () {
        $('#frmUpdateQuoteQuantity').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    };
    User.prototype.RemoveIconWishlist = function () {
        $("#layout-account-wishlist .wishlist-item-remove a").on("click", function (ev) {
            ev.preventDefault();
            User.prototype.RemoveWishlistItem(this);
        });
    };
    User.prototype.RemoveWishlistItem = function (el) {
        var clicked = $(el);
        var wishlistId = clicked.data("id");
        var wishListCount = parseInt($("#wishlistcount").text());
        Endpoint.prototype.RemoveProductFromWishList(wishlistId, function (res) {
            if (res.success) {
                clicked.closest(".wishlist-item").remove();
                $("#wishlistcount").html(res.data.total);
                if (res.data.total == 0) {
                    $('#subTextWishList').text('');
                    $('#subTextWishList').text(ZnodeBase.prototype.getResourceByKeyName("MessageNoProductsInWishlist"));
                }
            }
            else {
            }
        });
    };
    User.prototype.UpdateQuoteStatus = function (control, statusId) {
        var quoteIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (quoteIds.length > 0 && statusId > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.UpdateQuoteStatus(quoteIds, statusId, function (res) {
                DynamicGrid.prototype.RefreshGrid(control, res);
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.GenerateInvoice = function () {
        var arrIds = [];
        var collection = CheckBoxCollection;
        if (CheckBoxCollection.length > 0) {
            for (var _i = 0, CheckBoxCollection_1 = CheckBoxCollection; _i < CheckBoxCollection_1.length; _i++) {
                var entry = CheckBoxCollection_1[_i];
                arrIds.push(entry.replace("rowcheck_", ""));
            }
        }
        if (arrIds != undefined && arrIds.length > 0) {
            $("#orderIds").val(arrIds);
            setTimeout(function () { ZnodeBase.prototype.HideLoader(); }, 1000);
            return true;
        }
        else {
            $("#SuccessMessage").html("");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneOrder"), "error", false, 0);
            return false;
        }
    };
    User.prototype.LoadQuote = function () {
        $("#btnBottomReview").on('click', function () {
            $("#OrderStatus").val('IN REVIEW');
        });
        $("#btnBottomApprove").on('click', function () {
            $("#OrderStatus").val('APPROVED');
        });
        $("#btnBottomReject").on('click', function () {
            $("#OrderStatus").val('REJECTED');
        });
        $("#btnTopReview").on('click', function () {
            $("#OrderStatus").val('IN REVIEW');
        });
        $("#btnTopApprove").on('click', function () {
            $("#OrderStatus").val('APPROVED');
        });
        $("#btnTopReject").on('click', function () {
            $("#OrderStatus").val('REJECTED');
        });
    };
    User.prototype.UpdateQuoteLineItemQuantity = function (control) {
        var sku = $(control).attr("data-cart-sku");
        var minQuantity = parseInt($(control).attr("min-Qty"));
        var maxQuantity = parseInt($(control).attr("max-Qty"));
        $("#quantity_error_msg_" + sku).text('');
        var inventoryRoundOff = parseInt($(control).attr("data-inventoryRoundOff"));
        var selectedQty = $(control).val();
        var decimalPoint = 0;
        var decimalValue = 0;
        if (selectedQty.split(".")[1] != null) {
            decimalPoint = selectedQty.split(".")[1].length;
            decimalValue = parseInt(selectedQty.split(".")[1]);
        }
        if (this.CheckDecimalValue(decimalPoint, decimalValue, inventoryRoundOff, sku)) {
            if (this.CheckIsNumeric(selectedQty, sku)) {
                if (this.CheckMinMaxQuantity(parseInt(selectedQty), minQuantity, maxQuantity, sku)) {
                    $(control).closest("form").submit();
                }
            }
        }
        return false;
    };
    User.prototype.CheckDecimalValue = function (decimalPoint, decimalValue, inventoryRoundOff, sku) {
        if (decimalValue != 0 && decimalPoint > inventoryRoundOff) {
            $("#quantity_error_msg_" + sku).text(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityHaving") + inventoryRoundOff + ZnodeBase.prototype.getResourceByKeyName("XNumbersAfterDecimalPoint"));
            return false;
        }
        return true;
    };
    User.prototype.CheckIsNumeric = function (selectedQty, sku) {
        var matches = selectedQty.match(/^-?[\d.]+(?:e-?\d+)?$/);
        if (matches == null) {
            $("#quantity_error_msg_" + sku).text(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"));
            return false;
        }
        return true;
    };
    User.prototype.CheckMinMaxQuantity = function (selectedQty, minQuantity, maxQuantity, sku) {
        if (selectedQty < minQuantity || selectedQty > maxQuantity) {
            $("#quantity_error_msg_" + sku).text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + minQuantity + ZnodeBase.prototype.getResourceByKeyName("To") + maxQuantity + ZnodeBase.prototype.getResourceByKeyName("FullStop"));
            return false;
        }
        return true;
    };
    User.prototype.DeleteCurrentAddress = function () {
        var url = $("#deleteCurrentAddress").attr('data-url');
        $("#frmEditAddress_billing").attr('action', url);
        $("#frmEditAddress_billing").submit();
    };
    User.prototype.DeleteTemplate = function (control) {
        var templateIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (templateIds.length > 0) {
            Endpoint.prototype.DeleteTemplate(templateIds, function (res) {
                DynamicGrid.prototype.RefreshGrid(control, res);
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.ProcessContinueOnClick = function () {
        if (parseInt($("#InventoryOutOfStockCount").val()) == parseInt($("#ShoppingCartItemsCount").val())) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("QuoteItemsOutOfStockErrorMsg"), "error", isFadeOut, 0);
            return false;
        }
        var cartItemcount = $("#CartItemCount").val();
        if (parseInt(cartItemcount) > 0) {
            var omsQuoteLineItemId = $("#omsQuoteLineItemId").val();
            $("#QuoteConfirmPopup").modal('show');
        }
        else {
            User.prototype.ProcessQuote();
        }
    };
    User.prototype.ProcessQuote = function () {
        $("#FormQuoteView").attr('action', "/User/ProcessQuote").submit();
    };
    User.prototype.DeleteQuoteLineItem = function () {
        var omsQuoteLineItemId = $("#OmsQuoteLineItemId").val();
        var omsQuoteId = $("#OmsQuoteId").val();
        var orderStatus = $("#OrderStatus").val();
        var roleName = $("#RoleName").val();
        var token = $('input[name="__RequestVerificationToken"]', $('#FormQuoteView')).val();
        Endpoint.prototype.DeleteQuoteLineItem(omsQuoteLineItemId, omsQuoteId, 1, orderStatus, roleName, token, function (res) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
            window.location.href = window.location.protocol + "//" + window.location.host + "/User/QuoteHistory";
        });
    };
    User.prototype.DeleteDraft = function () {
        $("#DraftConfirmPopup").modal('show');
    };
    User.prototype.ValidateCreateEditTemplate = function () {
        var templateName = $("#TemplateName").val();
        var isValid = true;
        if (!templateName) {
            $("#validTemplateName").html(ZnodeBase.prototype.getResourceByKeyName("RequiredTemplateName"));
            $("#validTemplateName").addClass("error-msg");
            $("#validTemplateName").show();
            isValid = false;
        }
        Endpoint.prototype.IsTemplateNameExist(templateName, $("#OmsTemplateId").val(), function (response) {
            if (!response) {
                $("#validTemplateName").html(ZnodeBase.prototype.getResourceByKeyName("TemplateNameAlreadyExist"));
                $("#validTemplateName").addClass("error-msg");
                $("#validTemplateName").show();
                isValid = false;
            }
        });
        if (isValid)
            $("#frmCreateEditTemplate").submit();
        else
            return false;
    };
    User.prototype.SetManageQuoteUrl = function () {
        $("#grid tbody tr td").find(".zf-view").each(function () {
            var orderStatus = $(this).attr("data-parameter").split('&')[1].split('=')[1];
            var newhref = $(this).attr("href");
            if (newhref.length > 0) {
                if (orderStatus.toLowerCase() == "ordered") {
                    var omsQuoteId = $(this).attr("data-parameter").split('&')[0].split('=')[1];
                    newhref = window.location.protocol + "//" + window.location.host + "/User/OrderReceipt?OmsOrderId=" + omsQuoteId;
                }
                else {
                    newhref = window.location.protocol + "//" + window.location.host + newhref;
                }
            }
            $(this).attr('href', newhref);
        });
    };
    User.prototype.SetQuoteIdLinkURL = function () {
        $("#grid tbody tr .linkQuoteId").each(function () {
            var orderStatus = $(this).children().attr("href").split('&')[1].split('=')[1];
            var newhref = $(this).children().attr("href");
            if (newhref.length > 0) {
                if (orderStatus.toLowerCase() == "ordered") {
                    var omsQuoteId = $(this).children().attr("href").split('&')[0].split('=')[1];
                    newhref = window.location.protocol + "//" + window.location.host + "/User/OrderReceipt?OmsOrderId=" + omsQuoteId;
                }
                else {
                    newhref = window.location.protocol + "//" + window.location.host + newhref;
                }
            }
            $(this).children().attr('href', newhref);
        });
    };
    User.prototype.HideAddressChangeLink = function () {
        $("#FormQuoteView").find('.address-change').hide();
        $("#FormQuoteView").find('.change-address').hide();
    };
    //Saved Credit Card Region
    User.prototype.ShowCardPaymentOptions = function (customerGUID) {
        Endpoint.prototype.GetSaveCreditCardCount(customerGUID, function (count) {
            $("#creditCardCount").html($("#creditCardCount").html().replace("0", count.toString()));
        });
    };
    User.prototype.ShowPaymentOptions = function (data, CustomerPaymentGUID, isAccount) {
        Endpoint.prototype.GetPaymentDetails(data, true, function (response) {
            if (!response.HasError) {
                Endpoint.prototype.GetSaveCreditCardCount(CustomerPaymentGUID, function (count) {
                    $("#creditCardCount").html($("#creditCardCount").html().replace("0", count.toString()));
                });
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorContactPaymentApp"), "error", false, 0);
            }
        });
    };
    User.prototype.HideGridColumnForPODocument = function () {
        //Hide PODocument path column
        $('#grid tbody tr').each(function () {
            var podoc = $(this).find('td').last();
            if (podoc.hasClass('z-podocument')) {
                //Get the PoDocument path if exist and create a hyper link to download PODocument.
                var filePath = podoc.text();
                if (filePath != "" && typeof filePath != "undefined") {
                    $(this).find('td').each(function () {
                        if ($(this).hasClass("z-paymenttype")) {
                            if ($(this).text().toLocaleLowerCase() == "purchase_order") {
                                $(this).text("");
                                $(this).append($('<div>').html("<a href='" + podoc.text() + "' target='_blank'>Purchase Order</a>"));
                            }
                        }
                    });
                }
            }
        });
    };
    User.prototype.PrintOrderDetails = function (e) {
        var printContents = $("#userorderdetails").html();
        var originalContents = document.body.innerHTML;
        var orderNumber = $("#OrderNumber").val();
        var emailAddress = $("#EmailAddress").val();
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        $("#OrderNumber").val(orderNumber);
        $("#EmailAddress").val(emailAddress);
        $.validator.unobtrusive.parse($("#frmOrderDetails"));
    };
    User.prototype.LoginMethod = function () {
        var actualurl = window.location.href;
        //If actual url does not contain return url then only append return url.
        if (actualurl.indexOf("returnUrl") == -1) {
            actualurl = decodeURIComponent(actualurl);
            var returnUrl = decodeURIComponent(actualurl.replace(document.location.origin, ''));
            returnUrl = encodeURIComponent(returnUrl);
            if (returnUrl != "/User/Login")
                window.location.href = window.location.protocol + "//" + window.location.host + '/User/Login?returnUrl=' + returnUrl;
        }
        else
            window.location.href = window.location.protocol + "//" + window.location.host + '/User/Login';
    };
    User.prototype.AppendLoaderOnSubmit = function () {
        if ($("#login_password").val() != "" && $("#login_username").val() != "") {
            //Nivi Code start
            ZnodeBase.prototype.ShowLoader();
            //Nivi Code end
            if ($(".field-validation-error").eq(0).html() == "") {
                ZnodeBase.prototype.ShowLoader();
            }
        }
    };
    User.prototype.BindAddressModel = function (addressType) {
        var stateName = $("#frmEditAddress_" + addressType).find('#txtStateCode[disabled]').length > 0 ? $("#frmEditAddress_" + addressType).find("#SelectStateName option:selected").val() : $("#frmEditAddress_" + addressType).find("#txtStateCode").val();
        var _addressModel = {
            Address1: $("#frmEditAddress_" + addressType).find("input[name=Address1]").val(),
            Address2: $("#frmEditAddress_" + addressType).find("input[name=Address2]").val(),
            AddressId: parseInt($("#frmEditAddress_" + addressType).find("#AddressId").val()),
            CityName: $("#frmEditAddress_" + addressType).find("input[name=CityName]").val(),
            FirstName: $("#frmEditAddress_" + addressType).find("input[name=FirstName]").val(),
            LastName: $("#frmEditAddress_" + addressType).find("input[name=LastName]").val(),
            PostalCode: $("#frmEditAddress_" + addressType).find("input[name=PostalCode]").val(),
            StateName: stateName,
            CountryName: $("#frmEditAddress_" + addressType).find('select[name="CountryName"]').val(),
            AddressType: addressType,
        };
        return _addressModel;
    };
    User.prototype.ValidateAddressForm = function (addressType) {
        var _addressType = $("#frmEditAddress_" + addressType);
        var isValid = false;
        if (User.prototype.IsValidZipCode(_addressType.find('#address_postalcode').val(), _addressType)) {
            _addressType.find('#valid-postalcode').hide();
            isValid = true;
        }
        else {
            _addressType.find('#valid-postalcode').show();
            isValid = false;
        }
        return isValid;
    };
    User.prototype.IsValidZipCode = function (zipCode, _addressType) {
        var countryCode = _addressType.find('#ShippingAddressModel_CountryCode').val();
        //Currently few country regex available.If want to validate for other country add regex in 'ZipCodeRegex'
        var zipCodeRegexp = ZipCodeRegex[countryCode];
        if (zipCodeRegexp) {
            var regexp = new RegExp(zipCodeRegexp);
            return regexp.test(zipCode);
        }
        return true;
    };
    User.prototype.SaveChanges = function (event, id, addressType) {
        event ? event.preventDefault() : "";
        if (id != "" && typeof id != "undefined" && id != null) {
            $("#frmEditAddress_" + addressType).find("input[name=Address1]").val($("#recommended-address1-" + id + "").text());
            $("#frmEditAddress_" + addressType).find("input[name=Address2]").val($("#recommended-address2-" + id + "").text());
            $("#frmEditAddress_" + addressType).find("input[name=CityName]").val($("#recommended-address-city-" + id + "").text());
            $("#frmEditAddress_" + addressType).find("input[name=PostalCode]").val($("#recommended-address-postalcode-" + id + "").text());
            $("#frmEditAddress_" + addressType).find('#txtStateCode[disabled]').length > 0 ? $("#frmEditAddress_" + addressType).find("select[name=StateName]").val($("#recommended-address-state-" + id + "").text()) : $("#frmEditAddress_" + addressType).find("input[name=StateName]").val($("#recommended-address-state-" + id + "").text());
            $("#formChange").val("true");
        }
        $('#custom-modal').modal('hide');
        //Start Nivi Code Hide Show Shipping/Payment Options
        $(".ShowShippingPanel").show();
        $(".ShowPaymentPanel").show();
        //End Nivi Code Hide Show Shipping/Payment Options
        $("#frmEditAddress_" + addressType).find("#btnSaveAddress").closest("form").submit();
        Checkout.prototype.RefreshAddressOptions(addressType);
        return true;
    };
    User.prototype.RecommendedAddress = function (addressType) {
        /*Nivi code Start*/
        $('[id="CompanyName"]').each(function () {
            try {
                //alert('CompanyName');
                $(this).rules('add', {
                    required: false // overwrite an existing rule
                });
            }
            catch (err) {
                console.log(err.message);
            }
        });
        $('[id="address_street2"]').each(function () {
            $(this).rules('add', {
                required: false // overwrite an existing rule
            });
        });
        /*Nivi code End*/
        if (!$("#frmEditAddress_" + addressType).valid())
            return false;
        if (!User.prototype.ValidateAddressForm(addressType))
            return false;
        ZnodeBase.prototype.ShowLoader();
        var addressModel = User.prototype.BindAddressModel(addressType);
        var isSuggestedAddress = false;
        Endpoint.prototype.GetRecommendedAddress(addressModel, function (response) {
            var htmlString = response.html;
            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#custom-modal').find('#custom-content').empty();
                $('#custom-modal').find('#custom-content').append(htmlString);
                $("#user-entered-address").empty();
                var enteredaddress = "<div class='address-street'><div id='enteredAddress1'>" + addressModel.Address1 + "</div>";
                if (addressModel.Address2 != "" && typeof addressModel.Address2 != "undefined" && addressModel.Address2 != null) {
                    enteredaddress += "<div id='enteredAddress2'>" + addressModel.Address2 + "</div> ";
                }
                enteredaddress += "<div class='address-citystate'><span id='enteredCity'>" + addressModel.CityName + "</span> <span id='enteredState'>" + addressModel.StateName + "</span> <span id='enteredPostalCode'>" + addressModel.PostalCode + "</span> <div id='enteredCountry'>" + addressModel.CountryName + "</div></div>";
                $("#user-entered-address").append(enteredaddress);
                User.prototype.ShowHideRecommendedPopUp(addressType);
                ZnodeBase.prototype.HideLoader();
                isSuggestedAddress = false;
                $(".address-popup").modal("hide");
            }
            else {
                isSuggestedAddress = true;
            }
        });
        return isSuggestedAddress;
    };
    //If the recommended address matches completely then it will hide popup and save the address.
    User.prototype.ShowHideRecommendedPopUp = function (addressType) {
        var isShowRecommendedAddress = true;
        isShowRecommendedAddress = User.prototype.MatchAddress();
        if (isShowRecommendedAddress) {
            return User.prototype.SaveChanges(null, null, addressType);
        }
        $('#custom-modal').modal('show');
    };
    //Match entered address with recommended address.
    User.prototype.MatchAddress = function () {
        var isMatchedAddress = true;
        for (var i = 1; i < $("#custom-modal .address-details").length; i++) {
            isMatchedAddress = User.prototype.ValidateRecommendedAddress("#enteredAddress1", "#recommended-address1-" + i, isMatchedAddress);
            isMatchedAddress = User.prototype.ValidateRecommendedAddress("#enteredAddress2", "#recommended-address2-" + i, isMatchedAddress);
            isMatchedAddress = User.prototype.ValidateRecommendedAddress("#enteredCity", "#recommended-address-city-" + i, isMatchedAddress);
            isMatchedAddress = User.prototype.ValidateRecommendedAddress("#enteredState", "#recommended-address-state-" + i, isMatchedAddress);
            isMatchedAddress = User.prototype.ValidateRecommendedAddress("#enteredCountry", "#recommended-address-country-" + i, isMatchedAddress);
            isMatchedAddress = User.prototype.ValidateRecommendedAddress("#enteredPostalCode", "#recommended-address-postalcode-" + i, isMatchedAddress);
        }
        return isMatchedAddress;
    };
    User.prototype.ValidateRecommendedAddress = function (selector, recommendedAddressSelector, isMatchedAddress) {
        if (!($(selector).text().trim().toLowerCase() == $(recommendedAddressSelector).text().trim().toLowerCase())) {
            $(recommendedAddressSelector).addClass("address-error");
            isMatchedAddress = false;
        }
        return isMatchedAddress;
    };
    User.prototype.HideShowAddressPopUP = function () {
        $("#AddressError").html("");
        $("#custom-modal").modal("hide");
    };
    User.prototype.OnUserTypeSelection = function () {
        var selectedRole = $("#ddlUserType option:selected").text();
        if (selectedRole == null && selectedRole == "") {
            $('#ddlRole').children('option:not(:first)').remove();
            $('#divRole').hide();
            return false;
        }
        if (selectedRole == "User") {
            $('#divRole').show();
            Endpoint.prototype.GetPermissionList($("#AccountId").val(), $("#AccountPermissionAccessId").val(), function (response) {
                $('#permission_options').html("");
                $('#permission_options').html(response);
                $("#ddlPermission").attr("onchange", "User.prototype.OnPermissionSelection();");
            });
            $("#ddlPermission").change();
        }
        else {
            $('#divRole').hide();
            $('#approvalNamesDiv').hide();
            $('#maxBudgetDiv').hide();
            $("#BudgetAmount").val("");
        }
    };
    User.prototype.OnPermissionSelection = function () {
        var permission = $("#ddlPermission option:selected").attr('data-permissioncode');
        var $sel = $("#divRole");
        var value = $sel.val();
        var text = $("option:selected", $sel).text();
        $('#PermissionCode').val(permission);
        $('#PermissionsName').val(text);
        if (permission != undefined && permission == 'ARA') {
            User.prototype.ShowApprovalList();
            $('#maxBudgetDiv').hide();
        }
        else if (permission != undefined && permission == 'SRA') {
            User.prototype.ShowApprovalList();
            $('#maxBudgetDiv').show();
        }
        else {
            $('#approvalNamesDiv').hide();
            $('#maxBudgetDiv').hide();
            $("#BudgetAmount").val("");
        }
    };
    User.prototype.OnUserProfileSelection = function () {
        var profileId = $("#ddlUserProfile option:selected").val();
        if (profileId != undefined && profileId > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.ChangeUserProfile(profileId, function (response) {
                if (response.status) {
                    window.location.reload();
                }
            });
        }
    };
    User.prototype.ShowApprovalList = function () {
        var accountId = $("#AccountId").val();
        var userId = parseInt($("#UserId").val(), 10);
        Endpoint.prototype.GetApproverList(accountId, userId, function (response) {
            var approvalUserId = $("#ApprovalUserId").val();
            $("#ddlApproverList").html("");
            $('#ddlApproverList').find('option').remove().end();
            $('#ddlApproverList').children('option:not(:first)').remove();
            for (var i = 0; i < response.length; i++) {
                if (response[i].Value == approvalUserId)
                    var opt = new Option(response[i].Text, response[i].Value, false, true);
                else
                    var opt = new Option(response[i].Text, response[i].Value);
                $('#ddlApproverList').append(opt);
            }
            $('#approvalNamesDiv').show();
        });
    };
    User.prototype.ValidateUserNameExists = function () {
        if ($("#divAddCustomerAsidePanel #UserName").val() != '') {
            Endpoint.prototype.IsUserNameExist($("#divAddCustomerAsidePanel #UserName").val(), $("#PortalId").val(), function (response) {
                if (!response) {
                    $("#UserName").addClass("input-validation-error");
                    $("#errorUserName").addClass("error-msg");
                    $("#errorUserName").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistUserName"));
                    $("#errorUserName").show();
                    $("#loading-div-background").hide();
                    return false;
                }
            });
        }
        return User.prototype.ValidateBudgetAmount();
    };
    User.prototype.ValidateBudgetAmount = function () {
        if ($("#BudgetAmount").is(':visible')) {
            if ($("#BudgetAmount").val() == null || $("#BudgetAmount").val() == "") {
                $("#errorRequiredAccountPermissionAccessId").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorBudgetAmount")).addClass("field-validation-error").show();
                $("#BudgetAmount").addClass('input-validation-error');
                return false;
            }
        }
        return true;
    };
    User.prototype.SubmitCustomerCreateEditForm = function () {
        return User.prototype.ValidationForUser();
    };
    User.prototype.ValidateAccountsCustomer = function () {
        $("#frmCreateEditCustomerAccount").submit(function () {
            return User.prototype.ValidationForUser();
        });
    };
    User.prototype.ValidationForUser = function () {
        var flag = true;
        var _AllowGlobalLevelUserCreation = $("#AllowGlobalLevelUserCreation").val();
        if (_AllowGlobalLevelUserCreation == "False" && $("#AccountName").val() == "" && $("#hdnPortalId").val() == "") {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("SelectPortal")).addClass("field-validation-error").show();
            $("#txtPortalName").addClass('input-validation-error');
            flag = false;
        }
        if ($("#hdnRoleName").val() == "User") {
            if ($("#BudgetAmount").is(':visible')) {
                if ($("#BudgetAmount").val() == null || $("#BudgetAmount").val() == "") {
                    $("#errorRequiredAccountPermissionAccessId").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorBudgetAmount")).addClass("field-validation-error").show();
                    $("#BudgetAmount").addClass('input-validation-error');
                    flag = false;
                }
            }
            if ($("#ddlApproverList").is(':visible')) {
                if ($("#ddlApproverList").val() == null || $("#ddlApproverList").val() == "") {
                    $("#errorRequiredApprovalUserId").html("<span>" + ZnodeBase.prototype.getResourceByKeyName("SelectApprovalUserId") + "</span>");
                    $("#ddlApproverList").addClass('input-validation-error');
                    flag = false;
                }
            }
        }
        if (!$("#BudgetAmount").is(':visible')) {
            $("#BudgetAmount").val("");
        }
        if ($("#Email").is(':visible') && $("#Email").val() == '') {
            $("#errorRequiredEmail").text('').text(ZnodeBase.prototype.getResourceByKeyName("EmailAddressIsRequired")).removeClass('field-validation-valid').addClass("field-validation-error").show();
            $("#Email").removeClass('valid').addClass('input-validation-error');
            flag = false;
        }
        return flag;
    };
    User.prototype.CancelUpload = function (targetDiv) {
        if ($(".add-to-cart-popover").html() != null && $(".add-to-cart-popover").html() != undefined && $(".add-to-cart-popover").html() != "")
            $(".add-to-cart-popover").remove();
        $("#" + targetDiv).hide(700);
        $("#" + targetDiv).html("");
        $("body").css('overflow', 'auto');
        User.prototype.RemovePopupOverlay();
    };
    User.prototype.RemovePopupOverlay = function () {
        //Below code is used to close the overlay of popup, as it was not closed in server because container is updated by Ajax call
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        $("body").css('overflow', 'auto');
    };
    User.prototype.DeleteMultipleAccountCustomer = function (control) {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.DeleteAccountCustomers(accountIds, function (res) {
                ZnodeBase.prototype.HideLoader();
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountUser").find("#refreshGrid"), res);
            });
        }
    };
    User.prototype.EnableCustomerAccount = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerEnableDisableAccount($("#AccountId").val(), accountIds, true, function (res) {
                ZnodeBase.prototype.HideLoader();
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountUser").find("#refreshGrid"), res);
                if (res.status == true)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("EnableMessage"), 'success', isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, 'error', isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.DisableCustomerAccount = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerEnableDisableAccount($("#AccountId").val(), accountIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountUser").find("#refreshGrid"), res);
                if (res.status == true)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DisableMessage"), 'success', isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, 'error', isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.CustomerResetPassword = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerAccountResetPassword($("#AccountId").val(), accountIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountUser").find("#refreshGrid"), res);
                ZnodeBase.prototype.HideLoader();
                if (res.status == true)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessResetPassword"), 'success', isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName(res.message), 'error', isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.GetUserPermissionList = function () {
        if ($("#hdnRoleName").val() == "User") {
            Endpoint.prototype.GetPermissionList($("#AccountId").val(), $("#AccountPermissionAccessId").val(), function (response) {
                $('#permission_options').html("");
                $('#permission_options').html(response);
                $("#ddlPermission").attr("onchange", "User.prototype.OnPermissionSelection();");
            });
        }
    };
    User.prototype.ShowHidePermissionDiv = function () {
        if ($("#hdnRoleName").val() != "User") {
            $("#permissionsToHide").hide();
        }
        else {
            $("#permissionsToHide").show();
        }
    };
    User.prototype.ResetPasswordCustomer = function () {
        var accountId = $("#AccountId").val();
        window.location.href = window.location.protocol + "//" + window.location.host + "/user/singleresetpassword?accountId=" + accountId;
    };
    User.prototype.ResetPasswordUsers = function () {
        var userId = $("#divAddCustomerAsidePanel #UserId").val();
        if (userId == undefined)
            userId = $("#UserId").val();
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.SingleResetPassword(userId, function (res) {
            ZnodeBase.prototype.HideLoader();
            var errorType = 'error';
            if (res.status) {
                errorType = 'success';
            }
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, errorType, isFadeOut, fadeOutTime);
        });
    };
    User.prototype.BindStates = function (addresstype) {
        if (addresstype == null || addresstype == '') {
            $(".addressType").each(function () {
                addresstype = $(this).val();
                if (addresstype.toLowerCase() == "shipping")
                    User.prototype.BindStatestoShippingAddress();
                else if (addresstype.toLowerCase() == "billing")
                    User.prototype.BindStatestoBillingAddress();
                else {
                    var countryCode = ($('select[name="CountryName"]').val() != undefined) ? $('select[name="CountryName"]').val() : $('select[name="Address.CountryName"]').val();
                    if (countryCode.toLowerCase() == 'us' || countryCode.toLowerCase() == 'ca') {
                        Endpoint.prototype.GetStates(countryCode, function (response) {
                            var stateName = $('#SelectStateName');
                            stateName.empty();
                            $("#txtStateCode").attr("disabled", "disabled");
                            $("#dev-statecode-textbox").hide();
                            $("#dev-statecode-select").show();
                            $("#txtStateCode").val('');
                            $.each(response.states, function (key, value) {
                                stateName.append('<option value="' + value.Value + '">' + value.Text + '</option>');
                            });
                            var code = $("#hdn_StateCode").val();
                            $("#SelectStateName option").filter(function () {
                                return ($(this).val() == code);
                            }).prop('selected', true);
                        });
                    }
                    else {
                        $("#txtStateCode").prop("disabled", false);
                        $("#dev-statecode-textbox").show();
                        $("#dev-statecode-select").hide();
                    }
                }
            });
        }
        else if (addresstype.toLowerCase() == "shipping")
            User.prototype.BindStatestoShippingAddress();
        else if (addresstype.toLowerCase() == "billing")
            User.prototype.BindStatestoBillingAddress();
    };
    User.prototype.BindStatestoShippingAddress = function () {
        var countryCode = ($("#frmEditAddress_shipping").find('select[name="CountryName"]').val() != undefined) ? $("#frmEditAddress_shipping").find('select[name="CountryName"]').val() : $("#frmEditAddress_shipping").find('select[name="Address.CountryName"]').val();
        if (countryCode.toLowerCase() == 'us' || countryCode.toLowerCase() == 'ca') {
            Endpoint.prototype.GetStates(countryCode, function (response) {
                var stateName = $("#frmEditAddress_shipping").find('#SelectStateName');
                stateName.empty();
                $("#frmEditAddress_shipping").find("#txtStateCode").attr("disabled", "disabled");
                $("#frmEditAddress_shipping").find("#dev-statecode-textbox").hide();
                $("#frmEditAddress_shipping").find("#dev-statecode-select").show();
                $("#frmEditAddress_shipping").find("#txtStateCode").val('');
                $.each(response.states, function (key, value) {
                    stateName.append('<option value="' + value.Value + '">' + value.Text + '</option>');
                });
                var code = $("#frmEditAddress_shipping").find("#hdn_StateCode").val();
                $("#frmEditAddress_shipping").find("#SelectStateName option").filter(function () {
                    return ($(this).val() == code);
                }).prop('selected', true);
            });
        }
        else {
            $("#frmEditAddress_shipping").find("#txtStateCode").prop("disabled", false);
            $("#frmEditAddress_shipping").find("#dev-statecode-textbox").show();
            $("#frmEditAddress_shipping").find("#dev-statecode-select").hide();
        }
    };
    User.prototype.BindStatestoBillingAddress = function () {
        var countryCode = ($("#frmEditAddress_billing").find('select[name="CountryName"]').val() != undefined) ? $("#frmEditAddress_billing").find('select[name="CountryName"]').val() : $("#frmEditAddress_billing").find('select[name="Address.CountryName"]').val();
        if (countryCode.toLowerCase() == 'us' || countryCode.toLowerCase() == 'ca') {
            Endpoint.prototype.GetStates(countryCode, function (response) {
                var stateName = $("#frmEditAddress_billing").find('#SelectStateName');
                stateName.empty();
                $("#frmEditAddress_billing").find("#txtStateCode").attr("disabled", "disabled");
                $("#frmEditAddress_billing").find("#dev-statecode-textbox").hide();
                $("#frmEditAddress_billing").find("#dev-statecode-select").show();
                $("#frmEditAddress_billing").find("#txtStateCode").val('');
                $.each(response.states, function (key, value) {
                    stateName.append('<option value="' + value.Value + '">' + value.Text + '</option>');
                });
                var code = $("#frmEditAddress_billing").find("#hdn_StateCode").val();
                $("#frmEditAddress_billing").find("#SelectStateName option").filter(function () {
                    return ($(this).val() == code);
                }).prop('selected', true);
            });
        }
        else {
            $("#frmEditAddress_billing").find("#txtStateCode").prop("disabled", false);
            $("#frmEditAddress_billing").find("#dev-statecode-textbox").show();
            $("#frmEditAddress_billing").find("#dev-statecode-select").hide();
        }
    };
    User.prototype.SetPrimaryAddress = function (event, type) {
        var selectedAddressId = event.value;
        User.prototype.ShowLoader();
        Endpoint.prototype.SetPrimaryAddress(selectedAddressId, type, function (response) {
            if (type == "shipping") {
                $("#defaultShippingAddressDiv").html(response.html);
            }
            if (type == "billing") {
                $("#defaultBillingAddressDiv").html(response.html);
            }
            User.prototype.HideLoader();
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, response.status ? "success" : "error", isFadeOut, fadeOutTime);
        });
    };
    User.prototype.GetUserApproverList = function (omsQuoteId) {
        Endpoint.prototype.GetUserApproverList(omsQuoteId, function (response) {
            $("#user-approver-popup-content").html(response);
        });
    };
    User.prototype.LoginInPopup = function () {
        var actualurl = window.location.href;
        var returnUrl;
        //If actual url does not contain return url then only append return url.
        if (actualurl.indexOf("returnUrl") == -1) {
            actualurl = decodeURIComponent(actualurl);
            returnUrl = decodeURIComponent(actualurl.replace(document.location.origin, ''));
            if (returnUrl == "/User/Login")
                returnUrl = "";
        }
        Endpoint.prototype.Login(returnUrl, function (response) {
            $("#sign-in-nav").html(response);
        });
    };
    User.prototype.GetAccountMenus = function () {
        Endpoint.prototype.GetAccountMenus(function (response) {
            ZnodeBase.prototype.HideLoader();
            $("#sign-in-nav").html(response);
        });
    };
    User.prototype.ForgotPassword = function () {
        Endpoint.prototype.ForgotPassword(function (response) {
            $("#sign-in-nav").html(response);
        });
    };
    User.prototype.GetResult = function (data) {
        if (data.status == false) {
            $("#error-content").html(data.error);
            $("#login_password").val("");
            ZnodeBase.prototype.HideLoader();
        }
        else if (data.status == true) {
            if (data.link != null) {
                if (data.link == "/User/Wishlist") {
                    localStorage.setItem("Status", data.status);
                    window.location.reload();
                }
                if (data.link !== null && data.link !== '') {
                    if (data.link.indexOf(window.location.origin) >= 0)
                        window.location.href = data.link;
                    else
                        window.location.href = window.location.origin + '/' + data.link;
                }
                else
                    window.location.href = window.location.pathname;
            }
            else if (window.location.href.indexOf('/User/signup') >= 0) {
                window.location.href = '/';
            }
            else {
                window.location.reload();
            }
        }
        else {
            if (window.location.href.indexOf('/User/signup') >= 0) {
                window.location.href = '/';
            }
            else {
                window.location.reload();
            }
        }
    };
    User.prototype.LogOff = function () {
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.Logoff(function (reponse) {
            window.location.href = window.location.protocol + "//" + window.location.host + "/Home/Index";
            ZnodeBase.prototype.HideLoader();
        });
    };
    User.prototype.RedirectToLogin = function (data) {
        $("#sign-in-nav").html(data);
        ZnodeBase.prototype.HideLoader();
    };
    User.prototype.RemoveValidationMessage = function (addressType) {
        var _addressType = $("#frmEditAddress_" + addressType);
        _addressType.find('#valid-postalcode').hide();
    };
    User.prototype.LoginOnPasswordReset = function () {
        ZnodeBase.prototype.HideLoader();
        window.location.reload();
    };
    User.prototype.AddToCartOnCreateTemplate = function () {
        var flag = true;
        var cartItemCount = $("#hdnTemplateCartItemCount").val();
        var templateId = $("#OmsTemplateId").val();
        if (cartItemCount > 0) {
            if (templateId > 0) {
                ZnodeBase.prototype.ShowLoader();
                Endpoint.prototype.IsTemplateItemsModified(templateId, function (response) {
                    ZnodeBase.prototype.HideLoader();
                    if (response.status == true) {
                        flag = true;
                        window.location.href = "/User/AddTemplateToCart?omsTemplateId=" + templateId;
                    }
                    else {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorSaveOrderTemplate"), "error", false, 0);
                        flag = false;
                    }
                });
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorSaveOrderTemplate"), "error", false, 0);
                flag = false;
            }
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorAtLeastOneProductTemplate"), "error", false, 0);
            flag = false;
        }
        return flag;
    };
    return User;
}(ZnodeBase));
$("#btnSaveAddress").on("click", function () {
    var addressId = $("#addressid").val();
    var addressType = $("#addressType").val();
    if ($('#asdefault_shipping:checked').length > 0 && (addressId == "" || typeof addressId == 'undefined')) {
        return User.prototype.RecommendedAddress(addressType);
    }
});
$('#custom-modal').on('hidden.bs.modal', function () {
    if ($("#custom-modal .close, .popup").length > 1) {
        $('body').addClass('modal-open');
    }
});
$('.address-popup').on('hidden.bs.modal', function () {
    $('body').addClass('modal-open');
});
/*Nivi code start for show city name and state*/
$("#shipping-content").find(":submit").click(function () {
    if ($("#shipmethod").val() == 'SHIP' && $("#guestuser").val() == 0) {
        /*Nivi code start for guest user on save changes*/
        if ($(".shipname").val() == "") {
            if ($(".address-citystate").attr("data-address-cityname") == "" && $(".address-citystate").attr("data-address-statecode") == "" || $("#address_city").val() == "") {
                $(".shipname").text("");
            }
            else {
                var shipstatecity = " - (" + $("#address_city").val() + ", " + $("#SelectStateName").val() + ")";
                $(".shipname").text(shipstatecity);
            }
        }
        else {
            var shipstatecity = " - (" + $(".address-citystate").attr("data-address-cityname") + ", " + $(".address-citystate").attr("data-address-statecode") + ")";
            $(".shipname").text(shipstatecity);
        }
        /*Nivi code end for guest user*/
    }
    else if ($("#shipmethod").val() == 'SHIP' && $("#guestuser").val() > 0) {
        /*Nivi code start for login user on save changes*/
        if ($("#shipaddresscity").val() != "" && $("#shipaddressstate").val() != "") {
            /*Nivi code start for existing user*/
            var shipstatecity = " - (" + $("#shipaddresscity").val() + ", " + $("#shipaddressstate").val() + ")";
            $(".shipname").text(shipstatecity);
        }
        else if ($(".shipname").val() == "") {
            /*Nivi code start for create new user*/
            if ($("#address_city").val() == "" && $("#SelectStateName").val() == "" || $("#address_city").val() == "") {
                $(".shipname").text("");
            }
            else {
                var shipstatecity = " - (" + $("#address_city").val() + ", " + $("#SelectStateName").val() + ")";
                $(".shipname").text(shipstatecity);
            }
        }
        else {
            $(".shipname").text("");
        }
        /*Nivi code start for login user*/
    }
});
/*Nivi code end for show city name and state*/
//# sourceMappingURL=User.js.map