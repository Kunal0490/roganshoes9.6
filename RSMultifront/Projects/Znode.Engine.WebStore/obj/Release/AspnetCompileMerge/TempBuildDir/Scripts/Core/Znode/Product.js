var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var isFromCategoryPage;
var isAddToCartGroupProduct = true;
var Product = /** @class */ (function (_super) {
    __extends(Product, _super);
    function Product() {
        return _super.call(this) || this;
    }
    Product.prototype.Init = function () {
        isFromCategoryPage = localStorage.getItem("isFromCategoryPage");
        Product.prototype.GetProductBreadCrumb(parseInt(window.sessionStorage.getItem("lastCategoryId"), 10), false);
        Product.prototype.ActiveReadReviews();
    };
    Product.prototype.GetProductDetails = function (control) {
        $('#quick-view-content').html("<span style='position:absolute;top:0;bottom:0;left:0;right:0;text-align:center;transform:translate(0px, 45%);font-weight:600;'>Loading...</span>");
        var productId = control.dataset.value;
        var isQuickView = control.dataset.isquickview;
        var publishState = control.dataset.publishState;
        Endpoint.prototype.GetProductDetails(productId, isQuickView, publishState, function (res) {
            if (res != null && res != "") {
                $("#quick-view-content").html(res);
                isFromCategoryPage = localStorage.getItem("isFromCategoryPage");
                Product.prototype.GetProductBreadCrumb(parseInt(window.sessionStorage.getItem("lastCategoryId"), 10), true);
            }
        });
    };
    Product.prototype.GetProductOutOfStockDetails = function (control, e) {
        e.preventDefault();
        var productId = $(control).parent().find("#dynamic-productid").val();
        Endpoint.prototype.GetProductOutOfStockDetails(productId, function (response) {
            if (response.status) {
                Product.prototype.ShowHideWishlistErrorMsg(control, false, response);
                $(control).closest("form").submit();
            }
            else {
                Product.prototype.ShowHideWishlistErrorMsg(control, true, response);
                return false;
            }
        });
    };
    //if true then show wishlist error message otherwise hide error message
    Product.prototype.ShowHideWishlistErrorMsg = function (control, isShow, response) {
        var errorMessageField = $(control).parent().parent().find("#wishlist-error-msg");
        errorMessageField.text("");
        isShow ? errorMessageField.text(response.errorMessage) : "";
        isShow ? errorMessageField.addClass("error-msg") : errorMessageField.removeClass("error-msg");
        isShow ? $(control).prop("disabled", true) : $(control).prop("disabled", false);
    };
    Product.prototype.AddToWishList = function (control) {
        var sku = $(control).attr("data-sku");
        var addOnSKUs = Product.prototype.GetSelectedAddons();
        Endpoint.prototype.AddToWishList(sku, addOnSKUs.join(), function (res) {
            if (res.status) {
                $("#accountWishList").attr('href', res.link);
                $("#accountWishList").attr('class', res.style);
                $("#accountWishList").html(res.message);
                $("#accountWishList_" + control.dataset.id).attr('href', res.link);
                $("#accountWishList_" + control.dataset.id).attr('class', res.style);
                $("#accountWishList_" + control.dataset.id).text(res.message);
            }
            else {
                if (res.isRedirectToLogin) {
                    // document.location = res.link;
                    document.location.href = res.link;
                }
            }
        }, true);
    };
    /*ORIGINAL METHOD
    OnQuantityChange(): boolean {
        var flag = true;
        var cartCount = 0;
        $("#quantity-error-msg").text('');
        var productId: number = parseInt($("#scrollReview form").children("#dynamic-productid").val());
        var _productDetail = Product.prototype.BindProductModelData();
        //To Do:This call is taking time if session does not contain value for cart.
        ////Getting cart count data for the product to perform validation while adding more product quantity in cart.
        //Endpoint.prototype.GetCartCountByProductId(productId, function (response) {
        //    cartCount = parseInt(response);
        //});

        cartCount = parseInt(_productDetail.Quantity);
        if (this.CheckIsNumeric(_productDetail.Quantity, productId, _productDetail.QuantityError)) {
            if (this.CheckDecimalValue(_productDetail.DecimalPoint, _productDetail.DecimalValue, _productDetail.InventoryRoundOff, productId, _productDetail.QuantityError)) {
                if (this.CheckQuantityGreaterThanZero(_productDetail.MaxQuantity, _productDetail.MinQuantity, cartCount, productId, _productDetail.QuantityError)) {
                    flag = false;
                    $("#button-addtocart_" + productId).prop("disabled", false);

                    Product.prototype.UpdateProductVariations(false, _productDetail.SKU, _productDetail.MainProductSKU, _productDetail.Quantity, _productDetail.MainProductId, function (response) {
                        var salesPrice = response.data.price;
                        flag = Product.prototype.UpdateProductValues(response, _productDetail.Quantity);
                        if (flag == true) {
                            flag = Product.prototype.InventoryStatus(response);
                        }
                    });
                }
                else {
                    flag = false;
                }
            }
            else {
                flag = false;
            }
        }
        else {
            flag = false;
        }
        return flag;
    }
    */
    Product.prototype.OnQuantityChange = function () {
        var flag = true;
        var cartCount = 0;
        $("#quantity-error-msg").text('');
        var productId = parseInt($("#scrollReview form").children("#dynamic-productid").val());
        var stockforselectedstore = $("#SelectedWarehouseStock").val();
        var _productDetail = Product.prototype.BindProductModelData();
        /*NIVI Code Start*/
        cartCount = parseInt(_productDetail.Quantity);
        if (this.CheckIsNumeric(_productDetail.Quantity, productId, _productDetail.QuantityError)) {
            if (this.CheckDecimalValue(_productDetail.DecimalPoint, _productDetail.DecimalValue, _productDetail.InventoryRoundOff, productId, _productDetail.QuantityError)) {
                if (this.CheckQuantityGreaterThanZero(_productDetail.MaxQuantity, _productDetail.MinQuantity, cartCount, productId, _productDetail.QuantityError)) {
                    flag = true;
                    $("#button-addtocart_" + productId).prop("disabled", false);
                    if (stockforselectedstore > 0) {
                        var ProductQty = parseInt(_productDetail.Quantity);
                        if (ProductQty > stockforselectedstore) {
                            $("#quantity-error-msg").show().html("OUT OF STOCK");
                            //alert("if");
                            flag = false;
                        }
                        else {
                            $("#button-addtocart_" + productId).prop("disabled", false);
                            $("#product-details-quantity input[name='Quantity']").attr('data-change', 'false');
                            // alert("else");
                            flag = true;
                        }
                    }
                    else {
                        Product.prototype.UpdateProductVariations(false, _productDetail.SKU, _productDetail.MainProductSKU, _productDetail.Quantity, _productDetail.MainProductId, function (response) {
                            var salesPrice = response.data.price;
                            flag = Product.prototype.UpdateProductValues(response, _productDetail.Quantity);
                            if (flag == true) {
                                flag = Product.prototype.InventoryStatus(response);
                            }
                        });
                    }
                }
                else {
                    flag = false;
                }
            }
            else {
                flag = false;
            }
        }
        else {
            flag = false;
        }
        return flag;
    };
    Product.prototype.OnAssociatedProductQuantityChange = function () {
        $("#QuickViewQuantiyErrorMessage").text('');
        var isAddToCartArray = [];
        $("#dynamic-product-variations .quantity").each(function () {
            var productId = parseInt($("#scrollReview form").children("#dynamic-productid").val());
            var _productDetail = Product.prototype.BindProductModel(this, true);
            var _showAddToCart = $("#ShowAddToCart").val();
            if (_productDetail.Quantity != null && _productDetail.Quantity != "") {
                if (Product.prototype.CheckIsNumeric(_productDetail.Quantity, productId, _productDetail.QuantityError)) {
                    if (Product.prototype.CheckDecimalValue(_productDetail.DecimalPoint, _productDetail.DecimalValue, _productDetail.InventoryRoundOff, productId, _productDetail.QuantityError)) {
                        if (Product.prototype.CheckQuantityGreaterThanZero(_productDetail.MaxQuantity, _productDetail.MinQuantity, parseInt(_productDetail.Quantity), productId, _productDetail.QuantityError)) {
                            if (_showAddToCart != "False") {
                                $("#button-addtocart_" + productId).prop("disabled", false);
                            }
                            $(_productDetail.QuantityError).text("");
                            $(_productDetail.QuantityError).removeClass("error-msg");
                            isAddToCartArray.push(true);
                        }
                        else {
                            isAddToCartArray.push(false);
                        }
                    }
                    else {
                        isAddToCartArray.push(false);
                    }
                }
                else {
                    isAddToCartArray.push(false);
                }
            }
            else {
                if (_showAddToCart != "False") {
                    $("#button-addtocart_" + productId).prop("disabled", false);
                }
                $(_productDetail.QuantityError).text("");
                $(_productDetail.QuantityError).removeClass("error-msg");
                isAddToCartArray.push(true);
            }
        });
        isAddToCartGroupProduct = !($.inArray(false, isAddToCartArray) > -1);
        return isAddToCartGroupProduct;
    };
    Product.prototype.BindProductModel = function (control, isGroup) {
        var _productDetail = {
            MainProductId: parseInt($(control).attr("data-parentProductId")),
            InventoryRoundOff: parseInt($(control).attr("data-inventoryroundoff")),
            ProductId: parseInt($(control).attr('data-productId')),
            Quantity: $(control).val(),
            MaxQuantity: parseInt($(control).attr("data-maxquantity")),
            MinQuantity: parseInt($(control).attr("data-minquantity")),
            SKU: $(control).attr("data-sku"),
            MainProductSKU: $(control).attr("data-parentsku"),
            DecimalPoint: $(control).val().split(".")[1] != null ? $(control).val().split(".")[1].length : 0,
            DecimalValue: $(control).val().split(".")[1] != null ? $(control).val().split(".")[1] : 0,
            QuantityError: isGroup ? "#quantity-error-msg_" + $(control).attr('data-productId') : "#quantity-error-msg",
            /*Nivi Code*/
            Custom1: "",
            Custom2: "",
            Custom3: "",
            Custom5: "",
        };
        return _productDetail;
    };
    Product.prototype.BindProductModelData = function () {
        var isGroup = false;
        var _productDetail = {
            MainProductId: parseInt($("#Quantity").attr("data-parentProductId")),
            InventoryRoundOff: parseInt($("#Quantity").attr("data-inventoryroundoff")),
            ProductId: parseInt($("#Quantity").attr('data-productId')),
            Quantity: $("#Quantity").val(),
            MaxQuantity: parseInt($("#Quantity").attr("data-maxquantity")),
            MinQuantity: parseInt($("#Quantity").attr("data-minquantity")),
            SKU: $("#Quantity").attr("data-sku"),
            MainProductSKU: $("#Quantity").attr("data-parentsku"),
            DecimalPoint: $("#Quantity").val().split(".")[1] != null ? $("#Quantity").val().split(".")[1].length : 0,
            DecimalValue: $("#Quantity").val().split(".")[1] != null ? $("#Quantity").val().split(".")[1] : 0,
            QuantityError: isGroup ? "#quantity-error-msg_" + $("#Quantity").attr('data-productId') : "#quantity-error-msg",
            /*Nivi Code*/
            Custom1: "",
            Custom2: "",
            Custom3: "",
            Custom5: "",
        };
        return _productDetail;
    };
    Product.prototype.BindGroupProductModelData = function () {
        var _productDetail = {
            SKU: $("#dynamic-sku").val(),
            ParentSKU: $("#dynamic-sku").val(),
            Quantity: "0",
            ParentProductId: $("#dynamic-parentproductid").val()
        };
        return _productDetail;
    };
    Product.prototype.CheckDecimalValue = function (decimalPoint, decimalValue, inventoryRoundOff, productId, quantityError) {
        if (decimalValue != 0 && decimalPoint > inventoryRoundOff) {
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityHaving") + inventoryRoundOff + ZnodeBase.prototype.getResourceByKeyName("XNumbersAfterDecimalPoint"));
            $(quantityError).addClass("error-msg");
            return false;
        }
        return true;
    };
    Product.prototype.CheckIsNumeric = function (selectedQty, productId, quantityError) {
        var matches = selectedQty.match(/^-?[\d.]+(?:e-?\d+)?$/);
        if (matches == null) {
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"));
            $(quantityError).addClass("error-msg");
            return false;
        }
        return true;
    };
    Product.prototype.CheckQuantityGreaterThanZero = function (maxQuantity, minQuantity, selectedQty, productId, quantityError) {
        if (selectedQty == 0) {
            $("#dynamic-inventory").text("");
            $(quantityError).addClass("error-msg");
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("ErrorProductQuantity"));
            return false;
        }
        if (maxQuantity < selectedQty || minQuantity > selectedQty) {
            $("#dynamic-inventory").text("");
            $(quantityError).addClass("error-msg");
            /*NIVI*/
            // $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + minQuantity + " to" + maxQuantity);
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("ErrorProductQuantity"));
            /*NIVI*/
            return false;
        }
        return true;
    };
    /*ORIGINAL METHOD
   InventoryStatus(response): boolean {
       // In stock
       var invetoryMessge: string;
       if (response.Quantity && response.Quantity > 0) {
           invetoryMessge = response.message + '<span class="product-count padding-left-5">(' + response.Quantity + ')</span>';
       } else {
           invetoryMessge = response.message;
       }
       if (response.success) {
           $("#dynamic-inventory").removeClass("error-msg");
           $("#dynamic-inventory").addClass("success-msg");
           // In stock
           $("#dynamic-inventory").show().html(invetoryMessge);
           $("#button-addtocart_" + response.data.productId).prop("disabled", false);
           $("#product-details-quantity input[name='Quantity']").attr('data-change', 'false');
           return true;
       } else {
           $("#dynamic-inventory").removeClass("success-msg");
           $("#dynamic-inventory").addClass("error-msg");
           // Out of stock
           $("#dynamic-inventory").show().html(invetoryMessge);
           return false;
       }
   }
    */
    /*NIVI*/
    Product.prototype.InventoryStatus = function (response) {
        // In stock
        var invetoryMessge;
        invetoryMessge = response.message;
        if (response.success) {
            $("#button-addtocart_" + response.data.productId).prop("disabled", false);
            $("#product-details-quantity input[name='Quantity']").attr('data-change', 'false');
            return true;
        }
        else {
            // Out of stock            
            $("#quantity-error-msg").show().html(invetoryMessge);
            return false;
        }
    };
    Product.prototype.RefreshPrice = function (amount) {
        $("#product_Detail_Price_Div").show();
        $("#layout-product .dynamic-product-price").html(amount);
    };
    Product.prototype.OnAddonSelect = function (control) {
        var _productSKU;
        _productSKU = Product.prototype.GetGroupProductSKUQuantity(control);
        if (_productSKU != null && _productSKU.SKU != null && _productSKU.Quantity != null) {
            Product.prototype.UpdateProductVariations(false, _productSKU.SKU, _productSKU.ParentSKU, _productSKU.Quantity, _productSKU.ParentProductId, function (response) {
                var salesPrice = response.data.price;
                var flag = Product.prototype.UpdateProductValues(response, _productSKU.Quantity);
                Product.prototype.RefreshPrice(salesPrice);
                if (flag) {
                    Product.prototype.InventoryStatus(response);
                }
                Product.prototype.RemoveAddonRequired(control);
            });
        }
        else {
            if ($("#quick-view-popup-ipad").is(":visible")) {
                $("#QuickViewQuantiyErrorMessage").html(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityError"));
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityError"), "error", true, fadeOutTime);
            }
            $("#dynamic-inventory").show().text('');
            $(".AddOn").val('0');
            if ($(control).attr('type') == "checkbox") {
                $(control).prop("checked", false);
            }
        }
    };
    Product.prototype.CheckGroupProductAddonQuantity = function () {
        var flag = true;
        if (Product.prototype.getAddOnIds("").length > 0) {
            var _productSKU = Product.prototype.BindGroupProductModelData();
            if (_productSKU != null && _productSKU.SKU != null && _productSKU.Quantity != null) {
                Product.prototype.UpdateProductVariations(false, _productSKU.SKU, _productSKU.ParentSKU, _productSKU.Quantity, _productSKU.ParentProductId, function (response) {
                    flag = Product.prototype.UpdateProductValues(response, _productSKU.Quantity);
                    if (flag == true) {
                        flag = Product.prototype.InventoryStatus(response);
                    }
                });
            }
            return flag;
        }
        else {
            return flag;
        }
    };
    Product.prototype.RemoveAddonRequired = function (control) {
        var addOnId = $(control).data("addongroupid");
        var errormsgdivid = $(control).data("errormsgdivid");
        $("#" + errormsgdivid + addOnId).css("display", "none");
    };
    Product.prototype.GetGroupProductSKUQuantity = function (control) {
        var _productSKU;
        $("input[type=text].quantity").each(function () {
            if ($(this).attr("name") != "Quantity") {
                var groupProductQuantity = $(this).val();
                if (groupProductQuantity != null && groupProductQuantity != "") {
                    _productSKU = {
                        Quantity: groupProductQuantity,
                        SKU: $(this).attr("data-sku"),
                        ParentSKU: $(control).attr("data-sku"),
                        ParentProductId: parseInt($("#dynamic-parentproductid").val()),
                    };
                    return false;
                }
            }
        });
        if (_productSKU == null) {
            _productSKU = {
                Quantity: $("#Quantity").val(),
                SKU: $("#Quantity").attr("data-sku"),
                ParentSKU: $("#Quantity").attr("data-parentsku"),
                ParentProductId: parseInt($("#Quantity").attr("data-parentProductId")),
            };
        }
        return _productSKU;
    };
    Product.prototype.getAddOnIds = function (parentSelector) {
        var selectedAddOnIds = [];
        if (typeof parentSelector == "undefined") {
            parentSelector = "";
        }
        $(parentSelector + " select.AddOn").each(function () {
            if ($(this).val() != "0") {
                selectedAddOnIds.push($(this).val());
            }
        });
        $(parentSelector + " input.AddOn:checked").each(function () {
            if ($(this).val() != "0") {
                selectedAddOnIds.push($(this).val());
            }
        });
        return (selectedAddOnIds.join());
    };
    Product.prototype.UpdateProductVariations = function (htmlContainer, sku, parentProductSKU, quantity, parentProductId, callbackMethod) {
        var selectedAddOnIds = Product.prototype.getAddOnIds("");
        Endpoint.prototype.GetProductPrice(sku, parentProductSKU, quantity, selectedAddOnIds, parentProductId, function (res) {
            if (callbackMethod) {
                callbackMethod(res);
            }
        });
    };
    Product.prototype.UpdateProductValues = function (response, quantity) {
        var selectedAddOnIds = Product.prototype.getAddOnIds("");
        // Set form values for submit
        $("#dynamic-sku").val(response.data.sku);
        $("#Quantity").val(quantity);
        $("#dynamic-addons").val(selectedAddOnIds);
        $("input[name='AddOnValueIds']").val(selectedAddOnIds);
        $("#dynamic-productName").val(response.data.ProductName);
        if (response.data.addOnMessage != undefined) {
            $("#dynamic-addOninventory").show();
            $("#dynamic-addOninventory").html(response.data.addOnMessage);
            return false;
        }
        else {
            $("#dynamic-addOninventory").hide();
            $("#dynamic-addOninventory").html("");
            return true;
        }
    };
    /*NIVI CODE START*/
    Product.prototype.OnColorChangeDDL = function (control) {
        var IsProductWithWidth = $("#HaveWidth").val();
        var IsProductWithSize = $("#HaveSize").val();
        $(control).attr('checked', 'checked');
        try {
            var color = $(control).val();
            $(" input[name=Color]").each(function () {
                $('label[for="' + $(this).attr('id') + '"]').removeClass("active");
            });
            $('label[for="' + $(control).attr('id') + '"]').addClass("active");
            var sizes = $(control).attr('data-sizes').split(',');
            $("#Size").html('');
            $.each(sizes, function (i, size) {
                if (size != "") {
                    if (i == 0) {
                        $("#Size").append($("<option></option>").val("Select").html("Select"));
                    }
                    else {
                        $("#Size").append($("<option></option>").val(size).html(size));
                    }
                }
            });
        }
        catch (err) {
            console.log(err.message);
        }
        /*Image Chnages*/
        $("#colorname").html(color);
        var galleryimageandalttextsstring = $(control).attr('data-gallery');
        var newstring = Product.prototype.Replacestring(galleryimageandalttextsstring);
        var galImageWithAltText = newstring.split(',');
        var isMobile = $("#IsMobileCase").val();
        // alert("isMobile=" + isMobile);
        var galImagePath = "";
        if (isMobile == "True") {
            galImagePath = "https://images.rogansshoes.com/t_1000/";
        }
        else {
            galImagePath = "https://images.rogansshoes.com/t_170/";
        }
        var galZoomImagePath = "https://images.rogansshoes.com/t_1000/";
        var i = 0;
        var numItems = $("#slider-thumbs .item").length;
        $("#slider-thumbs .item").each(function () {
            try {
                var galImageAltTextString = galImageWithAltText[i].split("||");
                var galImage = galImageAltTextString[0];
                var galImageAltText = galImageAltTextString[1];
                $(this).html('');
                $(this).append("<a  href = '#' ><img src=" + galImagePath + galImage + " data-src=" + galImagePath + galImage + " class='img-responsive b-lazy thumbnail fixedbottomimg' alt='" + galImageAltText + "' title='" + galImageAltText + "'></a>");
                $(this).on('click', 'a', function (e) {
                    $("#product-image").attr('src', galZoomImagePath + galImage);
                    $("#product-lens-image").attr('href', galImagePath + galImage);
                    $(".zoomImg:first").attr('src', galZoomImagePath + galImage);
                });
            }
            catch (err) {
                $(this).html('');
            }
            i = i + 1;
        });
        var j = 0;
        var k = 0;
        $("#myModal .item").each(function () {
            try {
                if (typeof (galImageWithAltText[j]) == 'undefined') { //alert("in my modal loop if");
                    $(this).html('<center><h1>No Image Available</h1></center>');
                }
                else {
                    var galImageAltTextString = galImageWithAltText[j].split("||");
                    var galImage = galImageAltTextString[0];
                    var galImageAltText = galImageAltTextString[1];
                    $(this).html('');
                    $(this).append("<a href = '#' id='ex" + k + "' class='zoom'><img src=" + galZoomImagePath + galImage + " data-src=" + galZoomImagePath + galImage + " class='img-responsive b-lazy thumbnail fixedbottomimg' alt='" + galImageAltText + "' title='" + galImageAltText + "'></a>");
                }
            }
            catch (err) {
            }
            j = j + 1;
            k = k + 1;
        });
        $('#ex2').zoom({ on: 'grab' });
        $('#ex3').zoom({ on: 'grab' });
        $('#ex4').zoom({ on: 'grab' });
        $('#ex5').zoom({ on: 'grab' });
        var prdimage = $(control).attr('data-image');
        $("#product-image").attr('src', galZoomImagePath + prdimage);
        $("#product-lens-image").attr('href', galImagePath + prdimage);
        $(".easyzoom-img").attr('src', galZoomImagePath + prdimage);
        $(".easyzoom-img").attr('href', galZoomImagePath + prdimage);
        $(".zoomImg:first").attr('src', galZoomImagePath + prdimage);
        var radioValue = $("input[id='Pickup']:checked").val();
        if (radioValue == 2) {
            $('#btnGetStock').click();
        }
    };
    Product.prototype.OnSizeChangeDDL = function (control) {
        //alert("Original=" + $("#ColorSizeWidthList").val());
        var ColorSizeWidthListP = JSON.parse($("#ColorSizeWidthList").val());
        // alert("ColorSizeWidthListP=" + ColorSizeWidthListP);
        var ColorSizeWidthList = JSON.stringify(ColorSizeWidthListP);
        // alert("ColorSizeWidthList=" + ColorSizeWidthList);
        var ShoeWidthValuesP = JSON.parse($("#ShoeWidthValues").val());
        var ShoeWidthValues = JSON.stringify(ShoeWidthValuesP);
        var ShoeWidthValues1 = ShoeWidthValues.split(',');
        alert("ShoeWidthValues1=" + ShoeWidthValues1);
        var color = $("input[name='Color']:checked").val();
        var size = $('#Size :selected').text();
        $("#ShoeWidth").html('');
        $.each(ShoeWidthValues1, function (i, width) {
            alert("Width=" + width);
            if (i == 0) {
                $("#ShoeWidth").append($("<option></option>").val("Select").html("Select"));
            }
            else {
                var s = "";
                if (width.indexOf('"') != -1 && width.length > 1) {
                    // width = width[0]; 
                    width = width.substring(0, width.length - 1);
                    alert("Width[0]=" + width);
                }
                s = "|" + color + "-" + size + "-" + width + ',';
                alert(s);
                if (ColorSizeWidthList.indexOf(s) != -1) {
                    $("#ShoeWidth").append($("<option></option>").val(width).html(width));
                }
            }
        });
    };
    Product.prototype.OnWidthChangeDDL = function (control) {
        $("#WidthError").hide();
        var radioValue = $("input[id='Pickup']:checked").val();
        if (radioValue == 2) {
            $('#btnGetStock').click();
        }
    };
    Product.prototype.Replacestring = function (str) {
        if (str.indexOf(',||,') != -1) {
            str = str.replace(',||,', ",");
            return Product.prototype.Replacestring(str);
        }
        var lastChar = str[str.length - 1];
        if (lastChar == ',') {
            str = str.slice(0, -1);
        }
        return str;
    };
    Product.prototype.OnColorChange = function (control) {
        try {
            var color = $(control).val();
            $("#colorname").html(color);
            $(" input[name=Color]").each(function () {
                $('label[for="' + $(this).attr('id') + '"]').removeClass("active");
            });
            $('label[for="' + $(control).attr('id') + '"]').addClass("active");
            $("#SelectedColor").val(color);
            var sizes = $(control).attr('data-sizes');
            $(" input[name=Size]").each(function () {
                var s = "," + $(this).val() + ",";
                if (sizes.indexOf(s) == -1) {
                    // alert("inside size=" + s);
                    $('label[for="' + $(this).attr('id') + '"]').addClass("scratchattribute");
                }
                else {
                    $('label[for="' + $(this).attr('id') + '"]').removeClass("scratchattribute");
                }
            });
            /*if size is already selected which is not scratched*/
            var size = $("#SelectedSize").val();
            if (size != "") {
                var ColorSizeWidthListP = JSON.parse($("#ColorSizeWidthList").val());
                var ColorSizeWidthList = JSON.stringify(ColorSizeWidthListP);
                $(" input[name=ShoeWidth]").each(function () {
                    var s = "|" + color + "-" + size + "-" + $(this).val() + ',';
                    //alert(s)
                    if (ColorSizeWidthList.indexOf(s) == -1) {
                        // alert("inside s=" + s);
                        $('label[for="' + $(this).attr('id') + '"]').addClass("scratchattribute");
                    }
                    else {
                        $('label[for="' + $(this).attr('id') + '"]').removeClass("scratchattribute");
                    }
                });
            }
        }
        catch (err) {
            //alert(err.message);
        }
        /*Image Chnages*/
        var galleryimageandalttextsstring = $(control).attr('data-gallery');
        //alert("galleryimageandalttextsstring=" + galleryimageandalttextsstring);
        if (typeof (galleryimageandalttextsstring) != 'undefined') {
            galleryimageandalttextsstring = galleryimageandalttextsstring.split(",||,").join(',');
        }
        var galImageWithAltText = galleryimageandalttextsstring.split(',');
        // alert("galImageWithAltText=" + galImageWithAltText);
        var isMobile = $("#IsMobileCase").val();
        var galImagePath = "";
        if (isMobile == "True") {
            galImagePath = "https://images.rogansshoes.com/t_1000/";
        }
        else {
            galImagePath = "https://images.rogansshoes.com/t_170/";
        }
        var galZoomImagePath = "https://images.rogansshoes.com/t_1000/";
        var i = 0;
        $("#slider-thumbs .item").each(function () {
            try {
                var galImageAltTextString = galImageWithAltText[i].split("||");
                var galImage = galImageAltTextString[0];
                var galImageAltText = galImageAltTextString[1];
                $(this).html('');
                if (galImage != "") {
                    $(this).append("<a onclick=ChangeImage('" + galZoomImagePath + galImage + "') href = '#'><img src = '" + galZoomImagePath + galImage + "' data-src=" + galImagePath + galImage + " class='img-responsive b-lazy thumbnail fixedbottomimg' alt = " + galImageAltText + " title = " + galImageAltText + "></a>");
                }
                else {
                    // $(this).append("<a onclick=ChangeImage('https://images.rogansshoes.com/t_1000/sample.jpg') href = '#'><img src = '" + galZoomImagePath + galImage + "' data-src=" + galImagePath + galImage + " class='img-responsive b-lazy thumbnail fixedbottomimg' alt = " + galImageAltText + " title = " + galImageAltText + "></a>");
                    $(this).append("<a onclick='ChangeImage('https://images.rogansshoes.com/t_1000/sample.jpg')' href = '#' >");
                    if (isMobile == "True") {
                        $(this).append("<img src='https://images.rogansshoes.com/t_1000/sample.jpg' data-src='https://images.rogansshoes.com/t_1000/sample.jpg' class='img-responsive b-lazy thumbnail fixedbottomimg' alt='No Image' title='NO Image'>");
                    }
                    else {
                        $(this).append("<img src='https://images.rogansshoes.com/t_170/sample.jpg' data-src='https://images.rogansshoes.com/t_170/sample.jpg' class='img-responsive b-lazy thumbnail fixedbottomimg' alt='No Image' title='NO Image'>");
                    }
                    $(this).append("</a>");
                }
                i = i + 1;
                if (i == 4) {
                    i = 0;
                }
            }
            catch (err) {
                $(this).html('');
                //$(this).append("<h1>Hello</h1>");
                // ChangeImage('http://images.rogansshoes.com/t_1000/FS_17088_BGE1.JPG')
                $(this).append("<a onclick='ChangeImage('https://images.rogansshoes.com/t_1000/sample.jpg')' href = '#' >");
                if (isMobile == "True") {
                    $(this).append("<img src='https://images.rogansshoes.com/t_1000/sample.jpg' data-src='https://images.rogansshoes.com/t_1000/sample.jpg' class='img-responsive b-lazy thumbnail fixedbottomimg' alt='No Image' title='NO Image'>");
                }
                else {
                    $(this).append("<img src='https://images.rogansshoes.com/t_170/sample.jpg' data-src='https://images.rogansshoes.com/t_170/sample.jpg' class='img-responsive b-lazy thumbnail fixedbottomimg' alt='No Image' title='NO Image'>");
                }
                $(this).append("</a>");
                i = i + 1;
                if (i == 3) {
                    i = 0;
                }
            }
        });
        var j = 0;
        var k = 2;
        $("#myModal .item").each(function () {
            try {
                if (typeof (galImageWithAltText[j]) == 'undefined') { //alert("in my modal loop if");
                    $(this).html('<div class="item"><a><img src="https://images.rogansshoes.com/t_1000/sample.jpg" class="img-responsive thumbnail fixedbottomimg" alt="No Image" title="No Image"></a></div>');
                }
                else {
                    //alert(galImageWithAltText[j]);
                    var galImageAltTextString = galImageWithAltText[j].split("||");
                    var galImage = galImageAltTextString[0];
                    var galImageAltText = galImageAltTextString[1];
                    // alert(galZoomImagePath +","+ galImage);
                    $(this).html('');
                    //$(this).append("<img src=" + galZoomImagePath + galImage + " data-src=" + galZoomImagePath + galImage + " class='img-responsive b-lazy thumbnail fixedbottomimg' alt='" + galImageAltText + "' title='" + galImageAltText + "'>");
                    $(this).append("<a href = '#' id='ex" + k + "' class='zoom'><img src=" + galZoomImagePath + galImage + " data-src=" + galZoomImagePath + galImage + " class='img-responsive b-lazy thumbnail fixedbottomimg' alt='" + galImageAltText + "' title='" + galImageAltText + "'></a>");
                }
            }
            catch (err) {
                //$(this).html('');
                //alert("err="+err.message);
            }
            j = j + 1;
            k = k + 1;
        });
        $('#ex2').zoom({ on: 'grab' });
        $('#ex3').zoom({ on: 'grab' });
        $('#ex4').zoom({ on: 'grab' });
        $('#ex5').zoom({ on: 'grab' });
        var prdimage = $(control).attr('data-image');
        // $("#product-image").attr('src', galZoomImagePath + prdimage);
        //  $(".easyzoom-img").attr('src', galZoomImagePath + prdimage);
        $("#product-image").attr('src', galZoomImagePath + prdimage);
        $("#product-lens-image").attr('href', galImagePath + prdimage);
        $(".easyzoom-img").attr('src', galZoomImagePath + prdimage);
        $(".easyzoom-img").attr('href', galZoomImagePath + prdimage);
        $(".zoomImg:first").attr('src', galZoomImagePath + prdimage);
        var radioValue = $("input[id='Pickup']:checked").val();
        if (radioValue == 2) {
            $('#btnGetStock').click();
        }
    };
    Product.prototype.OnSizeChange = function (control) {
        $("#SizeError").hide();
        var IsProductWithWidth = $("#HaveWidth").val();
        var IsProductWithSize = $("#HaveSize").val();
        var size = $(control).val();
        var color = $("#SelectedColor").val();
        //new
        if (color == "") {
            color = $("input[name='Color']:checked").val();
            $("#SelectedColor").val(color);
        }
        //new
        var availableWidths = [];
        $("#SelectedSize").val(size);
        $(control).attr('checked', 'checked');
        $(" input[name=Size]").each(function () {
            $('label[for="' + $(this).attr('id') + '"]').removeClass("active");
        });
        $('label[for="' + $(control).attr('id') + '"]').addClass("active");
        var ColorSizeWidthListP = JSON.parse($("#ColorSizeWidthList").val());
        var ColorSizeWidthList = JSON.stringify(ColorSizeWidthListP);
        //alert("Stringified ColorSizeWidthList=" + ColorSizeWidthList);
        $(" input[name=ShoeWidth]").each(function () {
            var s = "|" + color + "-" + size + "-" + $(this).val() + ',';
            //  alert(s)
            if (ColorSizeWidthList.indexOf(s) == -1) {
                //  alert("inside s=" + s);
                $('label[for="' + $(this).attr('id') + '"]').addClass("scratchattribute");
            }
            else {
                $('label[for="' + $(this).attr('id') + '"]').removeClass("scratchattribute");
            }
        });
        var radioValue = $("input[id='Pickup']:checked").val();
        if (radioValue == 2) {
            $('#btnGetStock').click();
        }
    };
    Product.prototype.OnWidthChange = function (control) {
        var IsSizeSelected = $("input[name='Size']:checked").val();
        //  alert(IsSizeSelected);
        //var size = $("#SelectedSize").val();
        //  alert("size=" + size);
        if (!IsSizeSelected) {
            $("#SizeError").show();
        }
        else {
            $("#SizeError").hide();
            $("#WidthError").hide();
            $(control).attr('checked', 'checked');
            $("#SelectedWidth").val($(control).val());
            $(" input[name=ShoeWidth]").each(function () {
                $('label[for="' + $(this).attr('id') + '"]').removeClass("active");
            });
            $('label[for="' + $(control).attr('id') + '"]').addClass("active");
            // Product.prototype.OnAttributeSelect(control);
        }
        var radioValue = $("input[id='Pickup']:checked").val();
        if (radioValue == 2) {
            $('#btnGetStock').click();
        }
    };
    Product.prototype.Validate = function (control, event) {
        // alert("Validate pickup");
        var isSizeSelected = true;
        var isWidthSelected = true;
        var selectedSize = $('#Size :selected').text();
        var selectedWidth = $('#ShoeWidth :selected').text();
        // alert("selectedSize=" + selectedSize);
        // alert("selectedWidth=" + selectedWidth);
        if (selectedSize != "Select") {
            $("#SizeError").hide();
        }
        else {
            $("#SizeError").show();
            // $("#Size").css("border", "2px red solid", "color", "red");
            isSizeSelected = false;
            return false;
        }
        if (selectedWidth != "Select") {
            $("#WidthError").hide();
        }
        else {
            $("#WidthError").show();
            //  $("#ShoeWidth").css("border", "2px red solid", "color", "red");
            isWidthSelected = false;
            return false;
        }
        if (isSizeSelected == true && isWidthSelected == true) {
            if (Product.prototype.BindAddOnProductSKU(control, event)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    Product.prototype.ValidateSwatch = function (control, event) {
        // alert("ValidateSwatch called");
        var Codes = [];
        var isSizeSelected = true;
        var isWidthSelected = true;
        var IsProductWithWidth = $("#HaveWidth").val();
        var IsProductWithSize = $("#HaveSize").val();
        $(" input.ConfigurableAttribute:checked").each(function () {
            var selectedclass = $('label[for="' + $(this).attr('id') + '"]').attr('class'); //scratch-text active
            if (selectedclass == 'scratch-text active') {
                Codes.push($(this).attr('code'));
            }
        });
        console.log("1");
        if (IsProductWithSize == "True" && Codes.indexOf("Size") == -1) {
            $("#SizeError").show();
            // $("#Size").css("border", "2px red solid", "color", "red");
            isSizeSelected = false;
            console.log("isSizeSelected = false;");
            return false;
        }
        else {
            $("#SizeError").hide();
        }
        if (IsProductWithWidth == "True" && Codes.indexOf("ShoeWidth") == -1) {
            $("#WidthError").show();
            // $("#ShoeWidth").css("border", "2px red solid", "color", "red");
            isWidthSelected = false;
            console.log("isWidthSelected = false;");
            return false;
        }
        else {
            $("#WidthError").hide();
        }
        if (isSizeSelected == true && isWidthSelected == true) {
            if (Product.prototype.BindAddOnProductSKU(control, event)) {
                return true;
            }
            else {
                console.log("Product.prototype.BindAddOnProductSKU(control, event) = false;");
                return false;
            }
        }
        else {
            return false;
        }
    };
    /*NIVI*/
    Product.prototype.OnAttributeSelect = function (control) {
        var productId = $("#scrollReview form").children("#dynamic-parentproductid").val();
        var Codes = [];
        var Values = [];
        var sku = $("#dynamic-configurableproductskus").val();
        var ParentProductSKU = $("#dynamic-sku").val();
        var selectedCode = $(control).attr('code');
        var selectedValue = $(control).val();
        var existingBreadCrumbHtml = "";
        $("select.ConfigurableAttribute").each(function () {
            Values.push($(this).val());
            Codes.push($(this).attr('id'));
        });
        $(" input.ConfigurableAttribute:checked").each(function () {
            Values.push($(this).val());
            Codes.push($(this).attr('code'));
        });
        var catgoryIds = $("#categoryIds").val();
        var parameters = {
            "SelectedCode": selectedCode,
            "SelectedValue": selectedValue,
            "SKU": sku,
            "Codes": Codes.join(),
            "Values": Values.join(),
            "ParentProductId": productId,
            "ParentProductSKU": ParentProductSKU,
            "IsQuickView": $("#isQuickView").val(),
            "IsProductEdit": $('#isProductEdit').val(),
            "ParentOmsSavedCartLineItemId": $('#ParentOmsSavedCartLineItemId').val()
        };
        if ($("#breadCrumb") != undefined && $("#breadCrumb").length > 0 && $("#breadCrumb").html().length > 0) {
            existingBreadCrumbHtml = $("#breadCrumb").html();
        }
        Endpoint.prototype.GetProduct(parameters, function (res) {
            $("#layout-product").replaceWith(res);
            if (existingBreadCrumbHtml.length > 0) {
                $("#breadCrumb").html(existingBreadCrumbHtml);
            }
            else {
                $("#categoryIds").val(catgoryIds);
                isFromCategoryPage = localStorage.getItem("isFromCategoryPage");
                Product.prototype.GetProductBreadCrumb(parseInt(window.sessionStorage.getItem("lastCategoryId"), 10), $("#isQuickView").val());
            }
            $("#breadcrumb-productname").html($(".product-name").html());
        });
    };
    //TO DO:
    Product.prototype.GetSuggestionResult = function (item) {
        $.each(item.properties, function (brand) {
            window.location.href = "/Search/Index?SearchTerm=" + encodeURIComponent(item.displaytext) + "&CategoryId=" + item.properties[brand];
        });
    };
    Product.prototype.ShowErrorAddonError = function (isError, id, addOnId) {
        if (!isError) {
            $("#" + id + addOnId).css("display", "none");
            return true;
        }
        else {
            $("#" + id + addOnId).removeAttr("style");
            return false;
        }
    };
    //Bind add on values of Product.
    Product.prototype.BindAddOnProductSKU = function (control, event) {
        ZnodeBase.prototype.ShowLoader();
        $("#CombinationErrorMessage").html('');
        /*NIVI Code Start*/
        var LayoutType = $('#LayoutType').val();
        //alert(LayoutType);
        /*Custom Code to set Child SKU and its Id to hidden field and Quantity input box--Start*/
        var IsProductWithWidth = $("#HaveWidth").val();
        var IsProductWithSize = $("#HaveSize").val();
        var json = $('#AssociatedProducts').val();
        //alert("json=" + json);
        /*SWATCH/DDL check*/
        var Color = $("input[name='Color']:checked").val();
        var Size = "";
        var Width = "";
        if (LayoutType == 'Swatch') {
            Size = $("input[name='Size']:checked").val();
            Width = $("input[name='ShoeWidth']:checked").val();
        }
        else {
            // alert("DDL View");
            Size = $('#Size :selected').text();
            Width = $('#ShoeWidth :selected').text();
        }
        var Sku = "", ProductId = "";
        var iflag = 0, IsSet = 0;
        var columns = ['PublishProductId', 'SKU', 'OMSColorValue', 'Custom1', 'Custom2'];
        var result = JSON.parse(json).map(function (obj) {
            return columns.map(function (key) {
                // alert("Key=" + key + " obj[key]=" + obj[key]);
                return obj[key];
            });
        });
        result.unshift(columns);
        //alert("result=" + result);
        for (var j = 0; j < result.length; j++) {
            if (IsSet == 0) {
                if (IsProductWithSize == "False" && IsProductWithWidth == "False") {
                    if (result[j][2] == Color) {
                        iflag = 1;
                        Sku = result[j][1];
                        ProductId = result[j][0];
                        IsSet = 1;
                    }
                }
                else if (IsProductWithWidth == "False") {
                    if (result[j][2] == Color && result[j][3] == Size) {
                        iflag = 1;
                        Sku = result[j][1];
                        ProductId = result[j][0];
                        IsSet = 1;
                    }
                }
                else if (result[j][2] == Color && result[j][3] == Size && result[j][4] == Width) {
                    iflag = 1;
                    Sku = result[j][1];
                    // alert("j=" + j);
                    ProductId = result[j][0];
                    IsSet = 1;
                }
            }
        }
        if (iflag == 1) {
            $("#Quantity").attr('data-sku', Sku);
            $("#dynamic-productid").val(ProductId);
            $("#dynamic-configurableproductskus").val(Sku);
            alert("configurableproductskus=" + Sku);
            $("#CombinationErrorMessage").html('');
        }
        else {
            $("#CombinationErrorMessage").html('This product is not available.');
            ZnodeBase.prototype.HideLoader();
            return false;
        }
        /*Custom Code to set Child SKU and its Id to hidden field and Quantity input box--End*/
        /*NIVI Code End*/
        var productType = $(control).closest('form').children("#dynamic-producttype").val();
        if (productType == "GroupedProduct") {
            if (isAddToCartGroupProduct == false) {
                ZnodeBase.prototype.HideLoader();
                return false;
            }
            if (!Product.prototype.CheckGroupProductAddonQuantity()) {
                ZnodeBase.prototype.HideLoader();
                return false;
            }
        }
        else if (Product.prototype.OnQuantityChange() == false) {
            ZnodeBase.prototype.HideLoader();
            return false;
        }
        var personalisedForm = $("#frmPersonalised");
        if (personalisedForm.length > 0 && !personalisedForm.valid()) {
            ZnodeBase.prototype.HideLoader();
            return false;
        }
        var addOnValues = [];
        var bundleProducts = [];
        var groupDontTrackInventory = "";
        var groupProducts = "";
        var groupProductsQuantity = "";
        var personalisedCodes = [];
        var personalisedValues = [];
        var quantity = "";
        var flag = true;
        var finalFlag = true;
        //for checkbox   
        $(".chk-product-addons").each(function () {
            var optional = $(this).data("isoptional");
            var displayType = $(this).data("displaytype");
            var id = $(this).attr("id");
            var addOnId = $(this).data("addongroupid");
            if (optional == "False") {
                flag = true;
            }
            else {
                var isError = true;
                if (displayType != "") {
                    var addOnDivId = "";
                    displayType = displayType.toLowerCase();
                    if (displayType == "checkbox") {
                        if ($('#' + id + ' input[type=checkbox]:checked').length > 0) {
                            isError = false;
                        }
                        addOnDivId = "paraCommentCheckBox-";
                    }
                    if (displayType == "radiobutton") {
                        if ($('#' + id + ' input[type=radio]:checked').length > 0) {
                            isError = false;
                        }
                        addOnDivId = "paraCommentRadioButton-";
                    }
                    if (displayType == "dropdown") {
                        var isSelected = $('#' + id).find('option:selected').val() == "0" || $('#' + id).find('option:selected').val() == undefined ? false : true;
                        if (isSelected) {
                            isError = false;
                        }
                        addOnDivId = "paraCommentDropDown-";
                    }
                    if (!isError) {
                        $("#" + addOnDivId + addOnId).css("display", "none");
                        flag = true;
                    }
                    else {
                        $("#" + addOnDivId + addOnId).removeAttr("style");
                        flag = false;
                    }
                    if (flag == false) {
                        finalFlag = false;
                        ZnodeBase.prototype.HideLoader();
                    }
                }
            }
        });
        //Get selected add on product skus.
        addOnValues = Product.prototype.GetSelectedAddons();
        //Get bundle product skus.
        bundleProducts = Product.prototype.GetSelectedBundelProducts();
        //Get group product skus and their quantity.
        $("input[type=text].quantity").each(function () {
            if ($(this).attr("name") != "Quantity") {
                var quantity = $(this).val();
                if (quantity != null && quantity != "") {
                    groupProducts = groupProducts + $(this).attr("data-sku") + ",";
                    groupProductsQuantity + $(this).val() + "_";
                    groupProductsQuantity = groupProductsQuantity + $(this).val() + "_";
                }
                if (parseInt($(this).attr("data-maxquantity")) < parseInt(quantity))
                    return Product.prototype.CheckQuickViewAndShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("RequiredProductQuantity"));
                if (quantity != null && quantity != "" && $(this).attr("data-inventory") == "False") {
                    groupDontTrackInventory = groupDontTrackInventory + $(this).attr("data-sku") + ",";
                }
            }
        });
        groupProductsQuantity = groupProductsQuantity.substr(0, groupProductsQuantity.length - 1);
        groupProducts = groupProducts.substr(0, groupProducts.length - 1);
        if (productType == "GroupedProduct") {
            if (groupProductsQuantity == null || groupProductsQuantity == "") {
                ZnodeBase.prototype.HideLoader();
                return Product.prototype.CheckQuickViewAndShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("RequiredProductQuantity"));
            }
            else if (!Product.prototype.OnAssociatedProductQuantityChange()) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorProductQuantity"), "error", true, 10000);
                ZnodeBase.prototype.HideLoader();
                return false;
            }
            else {
                var mainProductId = parseInt($("#dynamic-parentproductid").val());
                if (!Product.prototype.CheckGroupProductQuantity(mainProductId, groupProducts, groupProductsQuantity, groupDontTrackInventory)) {
                    ZnodeBase.prototype.HideLoader();
                    return false;
                }
            }
        }
        else {
            quantity = $("#Quantity").val();
        }
        $("input[IsPersonalizable = True]").each(function () {
            var $label = $("label[for='" + this.id + "']");
            personalisedValues.push($(this).val());
            personalisedCodes.push($label.text());
            $(this).val("");
        });
        //Map Cart item model values for add to cart.
        Product.prototype.SetCartItemModelValues(addOnValues, bundleProducts, groupProducts, groupProductsQuantity, quantity, personalisedCodes.join(), personalisedValues.join(), control);
        Product.prototype.DeSelectAddonsOnAddToCart();
        return finalFlag;
    };
    Product.prototype.CheckGroupProductQuantity = function (productId, groupProductSkus, groupProductQuantities, groupDontTrackInventory) {
        var isSuccess = true;
        if (groupDontTrackInventory.trim() != "") {
            Endpoint.prototype.CheckGroupProductInventory(productId, groupProductSkus, groupProductQuantities, function (response) {
                if (!response.ShowAddToCart) {
                    Product.prototype.CheckQuickViewAndShowErrorMessage(response.InventoryMessage);
                }
                isSuccess = response.ShowAddToCart;
            });
        }
        return isSuccess;
    };
    Product.prototype.CheckQuickViewAndShowErrorMessage = function (errorMessage) {
        if ($("#quick-view-popup-ipad").is(":visible")) {
            $("#QuickViewQuantiyErrorMessage").html(errorMessage);
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(errorMessage, "error", true, fadeOutTime);
        }
        return false;
    };
    Product.prototype.GetSelectedAddons = function () {
        var addOnValues = [];
        $(".AddOn").each(function () {
            var values = "";
            if ($(this).is(":checked")) {
                values = $(this).val();
            }
            else {
                values = $(this).children(":selected").attr("data-addonsku");
            }
            if (values != null && values != "") {
                addOnValues.push(values);
            }
        });
        return addOnValues;
    };
    Product.prototype.GetSelectedBundelProducts = function () {
        var bundleProducts = [];
        $(".bundle").each(function () {
            var values = $(this).attr("data-bundlesku");
            bundleProducts.push(values);
        });
        return bundleProducts;
    };
    Product.prototype.SetCartItemModelValues = function (addOnValues, bundleProducts, groupProducts, groupProductsQuantity, quantity, personalisedcodes, personalisedvalues, control) {
        $(control).closest('form').children("#dynamic-addonproductskus").val(addOnValues);
        $(control).closest('form').children("#dynamic-bundleproductskus").val(bundleProducts);
        if (quantity != null || quantity != "") {
            $(control).closest('form').children("#dynamic-quantity").val(quantity);
        }
        $(control).closest('form').children("#dynamic-personalisedcodes").val(personalisedcodes);
        $(control).closest('form').children("#dynamic-personalisedvalues").val(personalisedvalues);
        $(control).closest('form').children("#dynamic-groupproductskus").val(groupProducts);
        $(control).closest('form').children("#dynamic-groupproductsquantity").val(groupProductsQuantity);
        $(control).closest('form').children("#dynamic-groupproductsquantity").val(groupProductsQuantity);
        /*Nivi*/
        var defaultStore = $("#SelectedWarehouseId").val();
        $(control).closest('form').children("#dynamic-custom1").val(defaultStore);
        $(control).closest('form').children("#dynamic-custom2").val($("#SelectedWarehouseName").val());
        $(control).closest('form').children("#dynamic-custom3").val($("#SelectedWarehouseAddress").val());
        $(control).closest('form').children("#dynamic-custom5").val($("#IsProductShipOnly").val());
        /*Nivi*/
        $(control).closest('form').append("<input type='hidden' id='dynamic-isproductedit' name='IsProductEdit' value='" + $('#isProductEdit').val() + "' />");
    };
    Product.prototype.ActiveReadReviews = function () {
        var url = document.URL.toString();
        var name = '';
        if (!(url.indexOf('#') === -1)) {
            var urlParts = url.split("#");
            name = urlParts[1];
        }
        if (name == "scrollReview") {
            $("#tab-reviews").click();
        }
    };
    Product.prototype.SendMailPopUp = function () {
        Endpoint.prototype.SendMail(function (res) {
            $("#btnSendMailPopup").click();
            $("#popUp_sendMail").html(res);
            $("#divSendMail").html(res);
        });
    };
    Product.prototype.SendMailResult = function (data) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.Message, data.Type, isFadeOut, fadeOutTime);
        $("#divSendMail").hide();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        $("#divSendMail").find("button[class=close]").click();
    };
    Product.prototype.OnClickSendMail = function () {
        $("#divSendMail").hide();
        ZnodeBase.prototype.ShowLoader();
    };
    Product.prototype.EmailToFriendSuccess = function () {
        jQuery('#modelEmailToFriend').trigger('click');
        $("#YourMailId").val("");
        $("#FriendMailId").val("");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessMailSending"), "success", true, 10000);
        ZnodeBase.prototype.HideLoader();
    };
    Product.prototype.EmailToFriendBegin = function () {
        $("#ProductName").val($(".product-name").html());
        ZnodeBase.prototype.ShowLoader();
        jQuery('#modelEmailToFriend').trigger('click');
    };
    Product.prototype.EmailToFriendFailure = function () {
        jQuery('#modelEmailToFriend').trigger('click');
        $("#YourMailId").val("");
        $("#FriendMailId").val("");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorMailSending"), "error", true, 10000);
        ZnodeBase.prototype.HideLoader();
    };
    Product.prototype.GetProductBreadCrumb = function (categoryId, isQuickView) {
        //var categoryIds: Array<string> = $("#categoryIds").val().split(",");
        /*Nivi :Code to Remove Search Category from breadcrumb.
          took Value of hideenfield categoryIds in variable and used this
          */
        var TempcategoryIds = $("#categoryIds").val();
        TempcategoryIds = TempcategoryIds.replace("6040,", "");
        TempcategoryIds = TempcategoryIds.replace(",6040", "");
        var categoryIds = TempcategoryIds.split(",");
        $("#categoryIds").val(TempcategoryIds);
        //Nivi: Code ends
        if (isFromCategoryPage == "true" && categoryId > 0) {
            if ($.inArray(categoryId.toString(), categoryIds) > -1) {
                Endpoint.prototype.GetBreadCrumb(categoryId, $("#categoryIds").val(), false, function (response) {
                    if (!isQuickView)
                        $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>");
                    $("#seeMore").html(response.seeMore);
                });
            }
            else {
                if (!$("#categoryIds").val()) {
                    if (!isQuickView)
                        $("#breadCrumb").html("<a href='/'>" + ZnodeBase.prototype.getResourceByKeyName("TextHome") + "</a>" + " / " + $(".product-name").html());
                    $("#seeMore").html("");
                }
                else {
                    Endpoint.prototype.GetBreadCrumb(parseInt(categoryIds[0], 10), $("#categoryIds").val(), false, function (response) {
                        if (!isQuickView)
                            $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>");
                        $("#seeMore").html(response.seeMore);
                    });
                }
            }
        }
        //new tab
        else if (isFromCategoryPage == "true" && (isNaN(categoryId))) {
            Endpoint.prototype.GetBreadCrumb(0, $("#categoryIds").val(), true, function (response) {
                if (!isQuickView)
                    $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>");
                $("#seeMore").html(response.seeMore);
            });
        }
        else if (isFromCategoryPage != "true") {
            if (!$("#categoryIds").val()) {
                if (!isQuickView)
                    $("#breadCrumb").html("<a href='/'>" + ZnodeBase.prototype.getResourceByKeyName("TextHome") + "</a>" + " / " + $(".product-name").html());
                $("#seeMore").html("");
            }
            else {
                Endpoint.prototype.GetBreadCrumb(parseInt(categoryIds[0], 10), $("#categoryIds").val(), false, function (response) {
                    if (!isQuickView) {
                        /*Original*/
                        //    $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                        /*NIVI Code Start*/
                        try {
                            var a = response.breadCrumb;
                            if (a.indexOf('Shop by Brand') != -1) {
                                var res = a.replace('shop-by-brand', 'Brand/List');
                                $("#breadCrumb").html(res + " / " + "<span id='breadcrumb-productname' class='mobile-header'>" + $(".product-name").html() + "</span>");
                            }
                            else {
                                $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname' class='mobile-header'>" + $(".product-name").html() + "</span>");
                            }
                        }
                        catch (err) {
                            $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname' class='mobile-header'>" + $(".product-name").html() + "</span>");
                        }
                    }
                    /*NIVI Code End*/
                    $("#seeMore").html(response.seeMore);
                });
            }
        }
    };
    Product.prototype.GetPriceAsync = function () {
        var products = new Array();
        $(".product-details .price-span").each(function () {
            var _control = $(this);
            var _product = { sku: _control.data("sku"), type: _control.data("type") };
            products.push(_product);
        });
        if (products.length > 0)
            Product.prototype.CallPriceApi(products);
    };
    Product.prototype.GetPrice = function (obsoleteClass, minQuantity) {
        if (minQuantity === void 0) { minQuantity = "1"; }
        var products = new Array();
        $(".cloudflareSpan").each(function () {
            var _control = $(this);
            var _product = { sku: _control.data("sku"), type: _control.data("type"), PublishProductId: _control.data("id"), MinQuantity: minQuantity, PriceView: true, ObsoleteClass: obsoleteClass };
            products.push(_product);
        });
        $("#CloudflareTierPriceSpan").each(function () {
            var _control = $(this);
            var _product = { sku: _control.data("sku"), type: _control.data("type"), PublishProductId: _control.data("id"), MinQuantity: minQuantity, PriceView: true, ObsoleteClass: obsoleteClass };
            products.push(_product);
        });
        Endpoint.prototype.CallInventoryPriceApi(products, function (response) {
            $.each(response.data, function (index, value) {
                $(".cloudflareSpan[data-sku='" + value.SKU + "']").html(value.HtmlText);
                if (value.TierPriceText)
                    $("#CloudflareTierPriceSpan[data-sku='" + value.SKU + "']").html(value.TierPriceText);
            });
        });
    };
    Product.prototype.GetInventory = function (obsoleteClass) {
        var products = new Array();
        $(".cloudflareInventorySpan").each(function () {
            var _control = $(this);
            var _product = { sku: _control.data("sku"), type: _control.data("type"), PublishProductId: _control.data("id"), MinQuantity: "1", PriceView: false, ObsoleteClass: obsoleteClass };
            products.push(_product);
        });
        Endpoint.prototype.CallInventoryPriceApi(products, function (response) {
            $.each(response.data, function (index, value) {
                $(".cloudflareInventorySpan[data-sku='" + value.SKU + "']").html(value.HtmlText);
            });
        });
    };
    /*ORIGINAL CODE
   //Redirect to product details page and show add to cart result.
   public DisplayAddToCartResult(data): void {
       ZnodeBase.prototype.HideLoader();
       if (!data.status) {
           // var productData = Product.prototype.GetProductData(this);
           Product.prototype.DisplayStickBar(data);
           $(".cartcount").html(data.cartCount);
           //  ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ProductAddedToCart"), 'success', isFadeOut, fadeOutTime);
       }
       else {
           $('[data-id="stickyBar"]').hide();
           ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AddedToCartErrorMessage"), 'error', isFadeOut, fadeOutTime);
       }
       ZnodeBase.prototype.HideLoader();
   }

   DisplayStickBar(data): void {
       var productData = data.Product;
       var stickyBarDiv = $('[data-id="stickyBar"]');
       stickyBarDiv.find('img').attr('src', data.ImagePath);
       stickyBarDiv.find('.addtocart-label').text("Added to Cart: " + productData.Quantity + " Qty");
       //stickyBarDiv.find('.stickProductName').text(productData.productName);
       stickyBarDiv.find('.stickProductSKU').text("SKU: " + productData.SKU);
       if ($('.bx-align').length > 0) {
           var height = $('.bx-align').height();
           var top = 110;
           var totalTop = top + height;
           $('.static-bar').css({ top: totalTop + 'px' })
       }
       !stickyBarDiv.is(':visible') ? stickyBarDiv.show() : "";

       //Hide Other Stickybar
       $('[data-stick="product-sticky"]').hide();
   }

   DisplayAddToCartMessage(data): void {
       ZnodeBase.prototype.HideLoader();
       $(".quick-view-popup").modal('hide')
       if (!data.status) {
           $(".cartcount").html(data.cartCount);
           if (data.hasOwnProperty("CartNotification") && data.CartNotification.hasOwnProperty("IsEnabled") && data.CartNotification.IsEnabled) {
               Product.prototype.DisplayAddToCartNotification(data.CartNotification);
           }
           else {
               ZnodeNotification.prototype.DisplayNotificationMessagesHelper("Added to cart" + " <a href='/cart'>Click here</a> to view your shopping cart and checkout.", 'success', isFadeOut, fadeOutTime);
           }
       }
       else {
           ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AddedToCartErrorMessage"), 'error', isFadeOut, fadeOutTime);
       }
   }

   DisplayAddToCartNotification(product) {
       if (product != null && product != undefined) {
           Endpoint.prototype.DisplayAddToCartNotification(JSON.stringify(product), function (response) {
               var element: any = $("#addToCartNotification");
               $("#addToCartNotification").removeAttr("style");
               $(window).scrollTop(0);
               $(document).scrollTop(0);
               if (element.length) {
                   if (response !== "" && response != null) {
                       element.html(response);
                       setTimeout(function () {
                           element.fadeOut().empty();
                       }, fadeOutTime);
                   }
               }
           });
       }
   }
    */
    /*NIVI CODE START*/
    //Redirect to product details page and show add to cart result.
    Product.prototype.DisplayAddToCartResult = function (data) {
        ZnodeBase.prototype.HideLoader();
        if (!data.status) {
            // var productData = Product.prototype.GetProductData(this);
            Product.prototype.DisplayStickBar(data);
            $(".cartcount").html(data.cartCount);
            //  ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ProductAddedToCart"), 'success', isFadeOut, fadeOutTime);
        }
        else {
            $('[data-id="stickyBar"]').hide();
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AddedToCartErrorMessage"), 'error', isFadeOut, fadeOutTime);
        }
        ZnodeBase.prototype.HideLoader();
    };
    Product.prototype.DisplayStickBar = function (data) {
        var productData = data.Product;
        //alert("productData" + JSON.stringify(productData));
        var stickyBarDiv = $('[data-id="stickyBar"]');
        //alert("stickyBarDiv" + JSON.stringify(stickyBarDiv));
        stickyBarDiv.find('img').attr('data-src', data.ImagePath);
        stickyBarDiv.find('img').attr('src', data.ImagePath);
        //alert("image path" + JSON.stringify(stickyBarDiv.find('img').attr('src', data.ImagePath)));
        stickyBarDiv.find('.addtocart-label').text("Added to Cart: " + productData.Quantity + " Qty");
        //stickyBarDiv.find('.stickProductName').text(productData.productName);
        stickyBarDiv.find('.stickProductSKU').text("SKU: " + productData.SKU);
        if ($('.bx-align').length > 0) {
            var height = $('.bx-align').height();
            var top = 110;
            var totalTop = top + height;
            $('.wcn-static-bar').css({ top: totalTop + 'px' });
        }
        !stickyBarDiv.is(':visible') ? stickyBarDiv.show() : "";
        //Hide Other Stickybar
        $('[data-stick="product-sticky"]').hide();
    };
    Product.prototype.DisplayAddToCartMessage = function (data) {
        ZnodeBase.prototype.HideLoader();
        $(".quick-view-popup").modal('hide');
        if (!data.status) {
            //var price: number = parseFloat($("#totalPrice").text());
            //var totalPrice = price + parseFloat(data.total);
            try {
                Product.prototype.DisplayStickBar(data);
                $(".cartcount").html(data.cartCount);
                //$(".totallign").html("$" + totalPrice);
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ProductAddedToCart"), 'success', isFadeOut, fadeOutTime);
                //ZnodeNotification.prototype.DisplayNotificationMessagesHelper("Added to cart" + " <a href='/Cart'>Click here</a> to view your shopping cart and checkout.", 'success', isFadeOut, fadeOutTime);
            }
            catch (err) {
                alert(err.message);
            }
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AddedToCartErrorMessage"), 'error', isFadeOut, fadeOutTime);
        }
    };
    /*NIVI Code END*/
    Product.prototype.CallPriceApi = function (products) {
        Endpoint.prototype.CallPriceApi(JSON.stringify(products), function (responce) {
            Product.prototype.AssignPricetoProduct(responce.data);
        });
    };
    Product.prototype.AssignPricetoProduct = function (data) {
        $.each(data, function (index, value) {
            if (value.DisplaySalesPrice != null && value.DisplaySalesPrice != "") {
                $(".product-details .price-span[data-sku='" + value.sku + "']").html(value.DisplaySalesPrice);
                if (value.DisplayRetailPrice != null && value.DisplayRetailPrice != "") {
                    $(".product-details .price-span[data-sku='" + value.sku + "']").append("<span class='cut-price'>" + value.DisplayRetailPrice + "</span>");
                }
            }
            else if (value.DisplayRetailPrice != null && value.DisplayRetailPrice != "") {
                $(".product-details .price-span[data-sku='" + value.sku + "']").html(value.DisplayRetailPrice);
            }
        });
    };
    //#region B2B
    Product.prototype.AddToFavourites = function (control) {
        var sku = $(control).attr("data-sku");
        var addOnSKUs = Product.prototype.GetSelectedAddons();
        Endpoint.prototype.AddToWishList(sku, addOnSKUs.join(), function (res) {
            if (res.status) {
                $(".btn-wishlist").addClass('added-to-wishlist');
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, "success", true, fadeOutTime);
                $(control).attr("onclick", "Product.prototype.RemoveFromFavourites(" + res.wishListId + ")");
            }
            else {
                if (res.isRedirectToLogin) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, "info", true, fadeOutTime);
                    $('#loginForm').attr('action', '/User/Login?returnUrl=/Product/AddToWishList?productSKU=' + sku);
                    $('.account-signup-link').attr('href', '/User/Signup?returnUrl=/Product/AddToWishList?productSKU=' + sku);
                }
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, "error", true, fadeOutTime);
            }
        });
    };
    Product.prototype.RemoveFromFavourites = function (wishListId) {
        if (wishListId > 0) {
            Endpoint.prototype.RemoveFromWishList(wishListId, function (res) {
                if (res.success) {
                    $(".btn-wishlist").removeClass('added-to-wishlist');
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, "success", true, fadeOutTime);
                    $('#btnAddToWishList').attr("onclick", "Product.prototype.AddToFavourites(this)");
                }
                else {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, "error", true, fadeOutTime);
                }
            });
        }
        else
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorProductRemoveFromWishList"), "error", true, fadeOutTime);
    };
    Product.prototype.RegisterDomEvents = function () {
        // Click for write review form stars
        $(document).on("click", "#layout-writereview .setrating label", function () {
            $("#layout-writereview .setrating label").removeClass("full").addClass("empty"); // Reset all to empty            
            var stars = $(this).data("stars");
            $("#Rating").val(stars);
            for (var a = 1; a <= stars; a += 1) {
                $(".star" + a).removeClass("empty").addClass("full");
            }
        });
        $(document).on("keypress", "#product-details-quantity input[name='Quantity']", function (e) {
            $(this).attr("data-change", "true");
            var key = e.keyCode || e.which;
            if ((47 < key) && (key < 58) || key === 8) {
                return true;
            }
            return false;
        });
        $(document).on("cut copy paste", "#product-details-quantity input[name='Quantity'],.product-details-quantity input[class='quantity']", function (e) {
            e.preventDefault();
        });
        //for group product quantity check.
        $(document).on("keypress", ".product-details-quantity input[class='quantity']", function (e) {
            var key = e.keyCode || e.which;
            if ((47 < key) && (key < 58) || key === 8) {
                return true;
            }
            return false;
        });
    };
    Product.prototype.DeSelectAddonsOnAddToCart = function () {
        $(".AddOn").each(function () {
            if ($(this).attr('type') == "checkbox" || $(this).attr('type') == "radio") {
                $(this).prop("checked", false);
            }
            else {
                $(this).val("0");
            }
        });
    };
    Product.prototype.DisplayAllLocationInveory = function () {
        if ($("#PublishProductId").length > 0) {
            var productId = $("#PublishProductId").val();
            $('#inventory-popup-content').html("<span style='position:absolute;top:0;bottom:0;left:0;right:0;text-align:center;transform:translate(0px, 45%);font-weight:600;'>Loading...</span>");
            $(".inventory-popup").first().modal('show');
            $(".inventory-popup .modal-content").css('min-height', '300px');
            $(".inventory-popup .modal-content").css('max-height', '800px');
            $(".inventory-popup .modal-content").css('margin', '0 auto');
            Endpoint.prototype.ShowProductAllLocationInventory(productId, function (res) {
                if (res != null && res != "") {
                    $("#inventory-popup-content").html(res);
                }
                else {
                    $("#inventory-popup-content").html(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessingRequest"));
                }
            });
        }
    };
    Product.prototype.GetAllLocationInventory = function () {
        var productId;
        if ($("#PublishProductId").length > 0) {
            productId = $("#PublishProductId").val();
            Endpoint.prototype.GetAllLocationInventory(productId, function (response) {
                var defaultInventoryName;
                var defaultInventoryCount;
                var totalInventoryCount = 0;
                $.each(response.data.Inventory, function (index, value) {
                    if (value.IsDefaultWarehouse) {
                        defaultInventoryName = value.WarehouseName;
                        defaultInventoryCount = value.Quantity;
                    }
                    totalInventoryCount = totalInventoryCount + parseFloat(value.Quantity);
                });
                if ($("#lblDefaultInventoryCount").length > 0) {
                    $("#lblDefaultInventoryCount").html(defaultInventoryCount);
                }
                if ($("#lblDefaultLocationName").length > 0) {
                    $("#lblDefaultLocationName").html(defaultInventoryName);
                }
                if ($("#lblAllInventoryCount").length > 0) {
                    $("#lblAllInventoryCount").html(totalInventoryCount + "");
                }
            });
        }
    };
    return Product;
}(ZnodeBase));
// Click for write review form stars
$(document).on("click", "#layout-writereview .setrating label", function () {
    $("#layout-writereview .setrating label").removeClass("full").addClass("empty"); // Reset all to empty            
    var stars = $(this).data("stars");
    $("#Rating").val(stars);
    for (var a = 1; a <= stars; a += 1) {
        $(".star" + a).removeClass("empty").addClass("full");
    }
});
$(document).on("keypress", "#product-details-quantity input[name='Quantity']", function (e) {
    $(this).attr("data-change", "true");
    var key = e.keyCode || e.which;
    if ((47 < key) && (key < 58) || key === 8) {
        return true;
    }
    return false;
});
$(document).on("cut copy paste", "#product-details-quantity input[name='Quantity'],.product-details-quantity input[class='quantity']", function (e) {
    e.preventDefault();
});
//for group product quantity check.
$(document).on("keypress", ".product-details-quantity input[class='quantity']", function (e) {
    var key = e.keyCode || e.which;
    if ((47 < key) && (key < 58) || key === 8) {
        return true;
    }
    return false;
});
//# sourceMappingURL=Product.js.map