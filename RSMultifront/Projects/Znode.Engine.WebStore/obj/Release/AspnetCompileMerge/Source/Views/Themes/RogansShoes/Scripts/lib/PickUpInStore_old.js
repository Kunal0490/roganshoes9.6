﻿
var xhr = new XMLHttpRequest();
var calledfrom = '';
var storeId = '';
var productId = '';
/*Called from PDP,CART and STORELocator Use My LOCation Button CLick*/
function GetLocation(c) {
    try {        
        var ipaddress = $("#hdnIPAddress").val();
        console.log("IPaddress: " + ipaddress);
        calledfrom = c;
        xhr.onreadystatechange = processRequest;
        //xhr.open('GET', "//ipinfo.io/json", true);
        xhr.open('GET', ipaddress, true);
        xhr.send();

    }
    catch (err) {
        console.log(err.message);
    }
}

/*Called from GetLocation()*/
function processRequest(e) {
    if (xhr.readyState == 4 && xhr.status == 200) {
        $("#responseerror").text("");
        var response = JSON.parse(xhr.responseText);
        var sku = $("#sku").val();
        $("#storedetails").html('');
        /*StoreLocator Case*/
        if (typeof sku === 'undefined') {
            sku = '0';
            $("#storedetails").append("<br/>");
        }
        else/*PDP Case*/ {
            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding margin-top'><h1>" + "Select a Store (" + response.region + ")</h1><br/></div>");
        }
        $("#storedetails").show();
        var sku = $("#sku").val();

        var array = response.loc.split(",");
        var _userCord = new google.maps.LatLng(array[0], array[1]);
        var response = JSON.parse(xhr.responseText);
        if (calledfrom == 'pdp') {
            GetStores(response.region, sku, _userCord);
        }
        else if (calledfrom == 'cart') {
            sku = $("#UseMyCurrentLocation").attr("data-sku");
            GetStoresForCart(response.region, sku, _userCord);
        }
        else if (calledfrom == 'storelocator') {
            GetStoresForStoreLocator(response.region, sku, _userCord);
        }


    }
    else {


        // var sku = $("#sku").val();       
        // var _userCord = new google.maps.LatLng("21.1500", "79.1000");
        // //var _userCord = new google.maps.LatLng(array[0], array[1]);

        //// GetStores("Maharashtra", sku, _userCord);

        // if (calledfrom == 'pdp') {
        //     GetStores("Maharashtra", sku, _userCord);
        // }
        // else if (calledfrom == 'cart') {
        //     sku = $("#UseMyCurrentLocation").attr("data-sku");    

        //     GetStoresForCart("Maharashtra", sku, _userCord);
        // }
        // else if (calledfrom == 'storelocator') {
        //     GetStoresForStoreLocator("Maharashtra", sku, _userCord);
        // }
        $("#responseerror").text("It seems currently this service is down, but no problem You can always search for nearest stores by entering your address.")
        return false;

    }

}

/*******************PDP/STORE LOCATOR METHODS START****************/

/*Called from processRequest(e) for both PDP and Store Locator*/
function GetStores(state, sku, _userCord) {
    if (sku != '0') {
        sku = $("#dynamic-configurableproductskus").val();
    }

    try {
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#loaderId").html("");
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {

                            //if (json[j].Quantity > 0) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;
                            // }

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            //if (json[j].Quantity > 0) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];

                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + address['StoreName'] + ">");
                            if (sku == '0') {
                                /*STORE LOCATOR CASE*/                              
                                $("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><button id=" + address['WarehouseId'] + " onclick=' return ShowPopUpSL(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store'>Make this My Store</button></div>" + "</div>");
                            }
                            else {
                                /*PDP CASE*/
                                //var status = "AVAILABLE(" + address['Quantity']+")";
                                var status = "<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + address['Quantity'] + " left</span>";
                                if (address['Quantity'] == 0) {
                                    status = "Out of stock";
                                    //$("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4><label style='color:darkred;font-weight:bold;'>" + status + "</label></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><label id=" + address['WarehouseId'] + " data-qty=" + address['Quantity'] + " class='btn btn-danger' data-dismiss='modal' aria-hidden='true'>Not Available</label></div>" + "</div>");
                                    $("#storedetails").append("<div class='col-lg-6 col-xs-9 col-md-9 nopadding'>" + "<div><h4>" + address['StoreName'] + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + " <span class= 'error-msg'> " + status + "</span>" + "</div>");
                                }
                                else {
                                    $("#storedetails").append("<div class='col-lg-6 col-xs-9 col-md-9 nopadding'>" + "<div><h4>" + address['StoreName'] + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + "<span class='form-group'>" + status + "</span>" + "<div><button id=" + address['WarehouseId'] + " data-qty=" + address['Quantity'] + " onclick='Click(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\",\"" + address['Quantity'] + "\");' class='btn btn-primary popup-btnpickup' data-dismiss='modal' aria-hidden='true' style='text-transform: none;'>Pick Up here</button></div>" + "</div>");
                                }
                            }
                            $("#storedetails").append("<div class='col-lg-6 col-xs-3 col-md-3 nopadding text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> Miles Away" + "</div>");
                            $("#storedetails").append("</div><hr>");
                            $("#storedetails").show();
                            // }

                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
				 $("#loaderId").html("");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

/*******************PDP/STORE LOCATOR METHODS End******************/

/*******************PDP METHODS Start*****************************/

/*Called from PDP on PickUp Here Click.
Custom values are taken from $("#SelectedWarehouseId"),$("#SelectedWarehouseName"),$("#SelectedWarehouseAddress") and assigned in SetCartItemModelValuesmethod of Product.ts
to be passed on to cart*/
function Click(obj, storename, storeaddress, stock) {
    try {

        var selectedstoreid = $(obj).attr('id');

        $("#SelectedWarehouseId").val(selectedstoreid);
        $("#SelectedWarehouseName").val(storename);
        $("#SelectedWarehouseAddress").val(storeaddress);
        $("#SelectedWarehouseStock").val(stock);

        $("#defaultstorename").html('');
        $("#defaultstorename").append("<b class='storename'>" + storename +"</b><p style='padding-left:20px;'><span id='status' style='display:none;' class='error-msg pdp-error-msg'>(NOT AVAILABLE)</span></p> <span class='select-store Tablet-view-changestore'><a style='text-decoration: underline;' onclick='ChangeStore();' href='JavaScript:Void(0);' title='Change Store'>Change Store</a>&nbsp;<span class='text-danger Tablet-view-changestore-redtxt' style='display:none;' id='showmsgpickup'>or select the ship option</span></span>");
        $('.select-store').show();

        $("#Pickup").prop('checked', true);
        $("#status").html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + stock + " left</span>");
        $("#status").show();
        
        $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
        $('[data-test-selector="btnAddToCart"]').prop("disabled", false);
		SaveinCookie(selectedstoreid, storename, storeaddress);
    }
    catch (err) {
        console.log(err.message);
    }
}

/*Called from PDP on clicking Pick up in store Radio Button*/
function AssignStoreDetails(selectedstoreid, storename, storeaddress) {
$("#InstoreWarning").show();
    var IsSKUSlected = VerifySKU();
    if (IsSKUSlected == true) {

        $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
        GetStockForSelectedStore('pdp');
        $("#SelectedWarehouseId").val(selectedstoreid);
        $("#SelectedWarehouseName").val(storename);
        $("#SelectedWarehouseAddress").val(storeaddress);
    }

}

/*Called on  $('#btnGetStock').click(); in Product.ts whenever Color,Size or width is changed in PDP */
function ResetSKUAndStock() {
    var IsSKUSlected = VerifySKU();
    if (IsSKUSlected == true) {

        $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
        GetStockForSelectedStore('pdp');
    }

}

/*Called from PDP on clicking Ship Radio Button*/
function RemoveStoreDetails() {
    $("#SelectedWarehouseId").val('');
    $("#SelectedWarehouseAddress").val('');
    $("#SelectedWarehouseStock").val('');
    $('[data-test-selector="btnAddToCart"]').prop("disabled", false);
    $('#status').hide();
	$("#InstoreWarning").show();
}

function FitGuide(fitguideurl) {
    window.open(fitguideurl, "_blank", "toolbar=yes,width=1000,height=700");
}

/*Called from Add To cart button for mobile view */
function RedirectToCart() {
    var webstoreURL = $("#WebstoreURL").val();

    window.location.href = webstoreURL + "/cart";
}

/*Called internally from PickUpinStore.js to check SKU (color-size-width combination) is selected before starting Pick Up. */
function VerifySKU() {

    var IsAllAttributesSelected = "True";
    var IsProductWithWidth = $("#HaveWidth").val();
    var IsProductWithSize = $("#HaveSize").val();
    var json = $('#AssociatedProducts').val();

    ///*SWATCH/DDL check*/
    var Color = $("input[name='Color']:checked").val();

    var Size = "";
    var Width = "";
    var LayoutType = $('#LayoutType').val();

    if (LayoutType == 'Swatch') {
        Size = $("input[name='Size']:checked").val();
        Width = $("input[name='ShoeWidth']:checked").val();
    }
    else {
        Size = $('#Size :selected').text();
        Width = $('#ShoeWidth :selected').text();
    }
    if (IsProductWithWidth == "True") {
        if ((Size == "Select" || typeof (Size) == "undefined")) {
            $("#SizeError").show();
            IsAllAttributesSelected = "False";
        }
        else {
            $("#SizeError").hide();
        }

        if (Width == "Select" || typeof (Width) == "undefined") {
            $("#WidthError").show();
            IsAllAttributesSelected = "False";
        }
        else {
            $("#WidthError").hide();
        }
    }
    else if (IsProductWithSize == "True") {
        if ((Size == "Select" || typeof (Size) == "undefined")) {
            $("#SizeError").show();
            IsAllAttributesSelected = "False";
        }
        else {
            $("#SizeError").hide();
        }
    }
    if (IsAllAttributesSelected == "True") {
        $("#SizeError").hide();
        $("#WidthError").hide();
        var Sku = "";
        var iflag = 0;
        var columns = ['PublishProductId', 'SKU', 'OMSColorValue', 'Custom1', 'Custom2'];
        var result = JSON.parse(json).map(function (obj) {
            return columns.map(function (key) {
                return obj[key];
            });
        });
        result.unshift(columns);
        for (var j = 0; j < result.length; j++) {

            if (IsProductWithSize == "False" && IsProductWithSize == "False") {
                if (result[j][2] == Color) {
                    iflag = 1;
                    Sku = result[j][1];
                }
            }
            else if (IsProductWithSize == "False") {
                if (result[j][2] == Color) {
                    iflag = 1;
                    Sku = result[j][1];
                }
            }
            else if (IsProductWithWidth == "False") {
                if (result[j][2] == Color && result[j][3] == Size) {
                    iflag = 1;
                    Sku = result[j][1];
                }
            }
            else if (result[j][2] == Color && result[j][3] == Size && result[j][4] == Width) {
                iflag = 1;
                Sku = result[j][1];
            }

        }
        if (iflag == 1) {
            $("#dynamic-configurableproductskus").val(Sku);
            return true;
        }
        else {
            $("#CombinationErrorMessage").html('This product is not available.');
            return false;
        }
    }
    else {
        $("#Ship").prop('checked', true);
        return false;
    }

}

/*Called on clicking Pick Up if default store is not selected. Opens Store Location pop up.*/
function ShowPopUp() {
$("#InstoreWarning").show();
    var IsSKUSlected = VerifySKU();
    var defaultstorenamehtml = $(".storename").val();
    if (IsSKUSlected == true) {
        if (typeof ($(".storename").val()) == "undefined") {
            $("#Ship").prop('checked', true);
            $("#storedetails").html('');
            $('#btnTrigger').click();
        }
        else {
            var IsSKUSlected = VerifySKU();
            if (IsSKUSlected == true) {

                $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
                GetStockForSelectedStore('pdp');
            }
        }

    }
}

/*Called on Change Store Link Click. Opens Change Store Confirmation Pop Up  */
function ChangeStore() {
    //trigger Pop Up Code
    $('#btnConfirmStorePopUp').click();
}

/*Called on Change Store Confirmation Pop Up's Proceed button Click. Opens Store Location pop up. */
function ShowStatus() {
    var IsSKUSlected = VerifySKU();
    if (IsSKUSlected == true) {
        GetStockForSelectedStore('pdpChangeStore');
        $("#storedetails").html('');
        $('#btnTrigger').click();
    }
}

/*Called internally from PickUpinStore.js to get stock of SKU in selected stores. */
function GetStockForSelectedStore(calledfrom) {

    var sku = $("#dynamic-configurableproductskus").val();

    var state = "ALL";
    var selectedstore = "";
    if (calledfrom == 'pdp') {
        selectedstore = $(".storename").text();
    }
//alert("1");
    try {
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));

                        $.each(json, function (j, val) {
                            if (json[j].StoreName == selectedstore) {

                                $("#SelectedWarehouseStock").val(json[j].Quantity);
                                if (calledfrom == 'pdp') {
                                    if (json[j].Quantity == 0) {
                                        $('[data-test-selector="btnAddToCart"]').prop("disabled", true);
                                        $("#status").html('(NOT AVAILABLE)');
                                        $("#status").show();
                                        $("#showmsgpickup").show();
                                    }
                                    else {
                                        $('[data-test-selector="btnAddToCart"]').prop("disabled", false);
                                        $("#status").html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                        $("#status").show();
                                        $("#showmsgpickup").hide();
                                    }
                                    
                                }
                            }
                        });
                    }
                });
				$("#loaderId").html("");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

/*******************PDP METHODS END*****************************/

/*******************CART METHODS START*****************************/

/*CART- called from processRequest(e) and GetLatLang()*/
function GetStoresForCart(state, sku, _userCord) {
    try {
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $("#storedetails").html('');
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];
                            }
                            catch (err) {
                                console.log(err.message);
                            }                     
                            
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + address['StoreName'] + ">");
                          
                            var status = "<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + address['Quantity'] + " left</span>";
                            if (address['Quantity'] == 0) {
                                status = "Out of stock";                                
                                $("#storedetails").append("<div class='col-lg-8 col-xs-9 col-md-4 nopadding'>" + "<div><h4>" + address['StoreName'] + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + " <span class= 'error-msg'> " + status + "</span>" + "</div>");
                            }
                            else {                                
                                $("#storedetails").append("<div class='col-lg-8 col-xs-9 col-md-4 nopadding'>" + "<div><h4>" + address['StoreName'] + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + "<span class='form-group'>" + status + "</span>" + "<div><button id='" + address['WarehouseId'] + "' data-qty=" + address['Quantity'] + " onclick='ClickCart(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\",\"" + sku + "\",\""+ address['Quantity'] + "\");' class='btn btn-primary popup-btnpickup' data-dismiss='modal' aria-hidden='true' style='text-transform: none;'>Pick Up here</button></div>" + "</div>");
                            }

                            $("#storedetails").append("<div class='col-lg-4 col-xs-3 col-md-4 text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</div>");
                            $("#storedetails").append("</div><hr/>");
                            $("#storedetails").show();                   

                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

/*CART- Called on PickUp Here Click of Cart*/
function ClickCart(obj, storename, storeaddress, sku,stock) {
    try {    
        var selectedstoreid = $(obj).attr('id');       
        var clickedradiobutton = "#Pickup-" + productId;
        $(clickedradiobutton).prop('checked', true);

        $("#SelectedWarehouseId").val(selectedstoreid);
        $("#SelectedWarehouseName").val(storename);
        $("#SelectedWarehouseAddress").val(storeaddress);        
        $(".storename").each(function () {
            $(this).html('');            
            $(this).append(storename);               
        });
        var availQtyErrorMsg = ("#avl_quantity_error_msg_" + productId).text;
        var pickupstockerror = $("#PickUpStockError").text();

        if (typeof (availQtyErrorMsg) == "undefined" || availQtyErrorMsg == "" || pickupstockerror != "") {

            $(".checkout").each(function () {
                $(".checkout ").removeClass('disable-anchor');
                $("#PickUpStockError").text('');
                $("#status_" + productId).text('');               
            });
        }
        
        //GetStockForSelectedStoreCart(sku, productId);
        GetStockForSelectedStoreClickCart(sku, productId);
        $("#status_" + productId).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + stock + " left</span>");
        $("#status_" + productId).show();      

        $.ajax({
            type: "POST",
            url: "/rscart/UpdateDeliveryPreferenceToPickUp/",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ storeId: selectedstoreid, storeName: storename, storeAddress: storeaddress, productId: productId }),
            processData: false,
            success: function (response) {
				//alert("success");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
			//	alert("error");
                $("#" + selectedstoreid).removeClass('btn btn-primary-1');
                $("#" + selectedstoreid).addClass('btn btn-primary');
            }
        });
        $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
        $("#hdnSelectedStoreID").val(storeId);
        $("." + storeId + ":first").prop('checked', true);


    }
    catch (err) {
        console.log(err.message);
    }


}

/*CART- Called from Cart on clicking Pick up in store Radio Button*/
function AssignStoreDetailsForCart(selectedstoreid, storename, storeaddress, productId, sku) {
    $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
	var pickupstockerror = $("#PickUpStockError").text();
    if (pickupstockerror == 'Entire order must be SHIP or PICK UP in store') {
        $(".checkout").each(function () {
            $(".checkout ").removeClass('disable-anchor');
            $("#PickUpStockError").text('');
        });
    }
    GetStockForSelectedStoreCart(sku, productId);
    $("#avl_quantity_error_msg_" + productId).html('');
    $.ajax({
        type: "POST",
        url: "/rscart/UpdateDeliveryPreferenceToPickUp/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ storeId: selectedstoreid, storeName: storename, storeAddress: storeaddress, productId: productId }),
        processData: false,
        success: function (response) {
			//alert("success");
        },
        failure: function (data) {
            console.log("Failure-" + data.responseText);
        },
        error: function (data) {
		//	alert("error");
            //console.log(data.responseText);
            //  $("#" + selectedstoreid).removeClass('btn btn-primary-1');
            //  $("#" + selectedstoreid).addClass('btn btn-primary');
        }
    });

}

/*CART- Called from Cart on clicking Ship Radio Button*/
function RemoveStoreDetailsForCartOnShipClick(productId) {
    var status = "status_" + productId;
    $("#" + status).hide();
    var msg = "showmsgpickup_" + productId;
    $("#" + msg).hide();
    $("#quantity_error_msg_" + productId).text('');
    $(".qty").each(function () {

        var qtyproductid = $(this).attr('data-cart-productid');
        if (productId == qtyproductid) {

            $(this).attr('data-IsPickUp', 'false');
            $(this).attr('data-inventory', '');
        }
    });
    var availQtyErrorMsg = ("#avl_quantity_error_msg_" + productId).text;
    var pickupstockerror = $("#PickUpStockError").text();
  //  alert("availQtyErrorMsg=" + availQtyErrorMsg + " ,pickupstockerror=" + pickupstockerror);
   // if ((typeof (availQtyErrorMsg) == "undefined" || availQtyErrorMsg == "" )&& pickupstockerror != "") {
    if (typeof (availQtyErrorMsg) == "undefined" || availQtyErrorMsg == "" || pickupstockerror != "") {
        $(".checkout").each(function () {
            $(".checkout ").removeClass('disable-anchor');
            $("#PickUpStockError").text('');
            $("#status_" + productId).text('');
            //$(".checkout ").addAttr('href');
        });
    }
    $.ajax({
        type: "POST",
        url: "/rscart/UpdateDeliveryPreferenceToShip/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ productId: productId }),
        processData: false,
        success: function (response) {

        },
        failure: function (data) {
            console.log("Failure-" + data.responseText);
        },
        //error: function (data) {
        //    console.log(data.);
        //   // $("#" + selectedstoreid).removeClass('btn btn-primary-1');
        //   // $("#" + selectedstoreid).addClass('btn btn-primary');
        //}
        error: function (data) {
            console.log(data.responseText);
        }
    });

}

/*CART -(Previously GetPickUpRadio) Select Store Case, directly opens Store details Pop up*/
//function OnCartSelectStoreClick(obj) {
//    storeId = $(obj).attr("data-pickupTarget");
//    productId = $(obj).attr("data-product");
//    var sku = $(obj).attr("data-sku");
//    $("#UseMyCurrentLocation").attr('data-sku', sku);
//    $("#btnSubmit").attr('data-sku', sku);

//    GetStockForSelectedStoreCart(sku, productId);
//}

/*CART- Change Store Case, On click opens Change Store confirmation Pop up*/
function OnCartChangeStoreClick(obj) {
    storeId = $(obj).attr("data-pickupTarget");
    productId = $(obj).attr("data-product");
    var sku = $(obj).attr("data-sku");
    $("#UseMyCurrentLocation").attr('data-sku', sku);
    $("#btnSubmit").attr('data-sku', sku);
    GetStockForSelectedStoreCart(sku, productId);
    //trigger Pop Up Code
    $('#btnConfirmStorePopUp').click();
}

/*CART- Confirmation Pop Up,Proceed button Click, Opens Store details Pop up*/
function OnConfirmationProceedClick() {
    // GetStockForSelectedStoreCart(sku, productId);   
    $("#storedetails").html('');
    $('#btnTrigger').click();
}

/*CART- Called internally from PickUpinStore.js to get stock of SKU in selected stores.*/
function GetStockForSelectedStoreCart(sku, productid) {

    //var sku = $("#dynamic-configurableproductskus").val();

    var state = "ALL";
    var selectedstore = $(".storename:first").text();
    //alert(selectedstore);
    try {
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));
                        //console.log(json);
                        $.each(json, function (j, val) {
                            if (json[j].StoreName == selectedstore) {

                                $("#SelectedWarehouseStock").val(json[j].Quantity);
                                var status = "status_" + productid;
                                var showmsgpickup = "showmsgpickup_" + productid;
                                if (json[j].Quantity == 0) {
                                    //$("#Ship_" + productid).prop('checked', true);
                                    $(".qty").each(function () {
                                       // alert("inside .qty");
                                        var qtysku = $(this).attr('data-sku');
                                        var radioValue = $("input[id='Pickup-" + productid + "']:checked").val();
                                        if (sku == qtysku && radioValue == 2) {
                                            $(this).attr('data-IsPickUp', 'true');
                                            $("#" + status).html('Not Available');
                                            $("#" + status).show();
                                            $("#" + showmsgpickup).show();
                                        }
                                    });
                                }
                                else {
                                    $("#" + status).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                    $("#" + status).show();
                                    $("#" + showmsgpickup).hide();
                                    try {
                                        $(".qty").each(function () {

                                            var qtysku = $(this).attr('data-sku');
                                            var radioValue = $("input[id='Pickup-" + productid + "']:checked").val();
                                            if (sku == qtysku && radioValue == 2) {

                                                $(this).attr('data-IsPickUp', 'true');
                                                $(this).attr('data-inventory', json[j].Quantity);
                                            }
                                        });
                                    }
                                    catch (err) {
                                        console.log(err.message);
                                    }
                                }
								//alert("loader empty");
                               

                            }
                        });
                    }
                });
				 $("#loaderId").html("");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

/*CART- Called from Pick Up Here Store locations Pop Up*/
function GetStockForSelectedStoreClickCart(sku, productid) {

    //var sku = $("#dynamic-configurableproductskus").val();
    var selectedStoreId = $("#SelectedWarehouseId").val();
    //alert("selectedStoreId=" + selectedStoreId);
    var state = "stock" + selectedStoreId;
    var selectedstore = $(".storename:first").text();
    sku = "";
    $("div.cart-products table tbody tr").each(function () {
        var qtyTextbox = $(this).find('input[name="Quantity"]');
        sku = sku + "," + $(qtyTextbox).attr("data-sku");
    });

    try {
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));

                        $.each(json, function (j, val) {
                            $("div.cart-products table tbody tr").each(function () {
                                var qtyTextbox = $(this).find('input[name="Quantity"]');
                                var productId = parseInt($(qtyTextbox).attr("data-cart-productId"));
                                var sku = $(qtyTextbox).attr("data-sku");
                                var radioValue = $("input[id='Pickup-" + productId + "']:checked").val();
                                var status = "status_" + productId;
                                var showmsgpickup = "showmsgpickup_" + productId;
                                if (radioValue == 2) {
                                    if (json[j].SKU == sku) {
                                        if (json[j].Quantity == 0) {
                                            $("#" + status).html('Not Available');
                                            $("#" + status).show();
                                            $("#" + showmsgpickup).show();
                                            $(qtyTextbox).attr('data-IsPickUp', 'true');
                                        }
                                        else {
                                            $("#" + status).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                            $("#" + status).show();
                                            $("#" + showmsgpickup).hide();
                                            $(qtyTextbox).attr('data-IsPickUp', 'true');
                                            $(qtyTextbox).attr('data-inventory', json[j].Quantity);
                                        }
                                    }
                                }
                            });
                           
                        });
                    }
                });
				 $("#loaderId").html("");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

/*CART- Called from Pick Up click when first time customer is setting Default Store*/
function ShowPopUpCart(sku, productid) {

    $("#UseMyCurrentLocation").attr('data-sku', sku);
    $("#btnSubmit").attr('data-sku', sku);

    $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
    GetStockForSelectedStoreCart(sku, productid);
    $('#btnTrigger').click();
}

/*CART-Called from Cart.ts EnableDisableCheckoutButton() where btnResetStock click is called in _Quantity.cshtml which calls this method*/
function ResetCartOnSubmit() {
    $(".qty").each(function () {
        var sku = $(this).attr('data-sku');
        var productId = $(this).attr('data-cart-productId');
        GetStockForSelectedStoreCart(sku, productId);
    });
}

/*******************CART METHODS END******************************/

/**************STORE LOCATOR METHODS START************************/

/*Called On Store Locator Make This My Store Click*/
function SaveinCookie(selectedstoreid, storename, storeaddress) {
    try {
      //  var selectedstoreid = $(obj).attr('id');
       // alert("SaveinCookie called");
        $(".btn-primary-1").each(function () {
            $(this).removeClass('btn btn-primary-1');
            $(this).addClass('btn btn-primary');
            $(this).text('Make This My Store');
        });

        $("#" + selectedstoreid).removeClass('btn btn-primary');
        $("#" + selectedstoreid).addClass('btn btn-primary-1');
        $("#" + selectedstoreid).text('My Store');
       // alert("1");
        $.ajax({
            type: "POST",
            url: "/rshome/saveincookie/",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ storeId: selectedstoreid, storeName: storename, storeAddress: storeaddress }),
            processData: false,
            success: function (response) {
                $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
               // alert(data.responseText);
               // $("#" + selectedstoreid).removeClass('btn btn-primary');
               // $("#" + selectedstoreid).addClass('btn btn-primary');
                
            }
        });

        return false;

    }
    catch (err) {
        console.log(err.message);
        $("#" + selectedstoreid).removeClass('btn btn-primary');
        $("#" + selectedstoreid).addClass('btn btn-primary');
    }
    return false;
}

/*Called From Store Locator,PDP and Cart store location pop up Submit button click*/
function GetLatLng(calledfrom) {
    try {

        $("#responseerror").text("");
        var addr = $("#txtzipcitystate").val();
        if (addr == '') {
            $("#error").text("*This is a required field.")
            return false;
        }
        else {
            $("#error").text("");
        }
        var lat = '';
        var lng = '';
        var address = addr;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();

                var _userCord = new google.maps.LatLng(lat, lng);
                var sku = $("#dynamic-configurableproductskus").val();
                if (calledfrom == 'storelocator') {
                    GetStoresForStoreLocator('ALL', '0', _userCord);
                }
                else if (calledfrom == 'pdp') {
                    GetStores('ALL', sku, _userCord);
                }
                else {
                    sku = $("#btnSubmit").attr('data-sku');
                    GetStoresForCart('ALL', sku, _userCord);
                }
                // initMap();
            }
            else {
                console.log("Geocode was not successful for the following reason: " + status);
            }
        });

    }
    catch (err) {
        console.log(err.message);
    }
}

/*Called from processRequest(e) and GetLatLang()*/
function GetStoresForStoreLocator(state, sku, _userCord) {
    try {
        var imagePath = $("#ImagePathURL").val();
        var webstoreUrl = $("#WebStoreUrl").val();
        var currentStore = $(".StoreLocatorLink span").html();
        //alert(currentStore);
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {

                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            
                            if (address['seourl'] == null) {
                                storedetailshref = "#";
                            }
                            else {
                                storedetailshref = webstoreUrl + '/' + address['seourl'];
                            }
                            
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];
                                var arr = address['StoreTiming'].split(',');
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            
                            var HtmlTags = $("#storedetails").html();
                            HtmlTags += "<div class='col-lg-12 col-md-12 nopadding storeLocationCoordinate' style='margin-bottom:20px !important' id=" + address['StoreName'] + " data-distance='0'  data-lng=" + address['Longitude'] + "  data-lat=" + address['Latitude'] + "  data-address=" + addr.replace(/^,/, '') + "  data-title=" + address['StoreName'] + ">"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding-mobile'><img alt=" + address['StoreName'] + " title=" + address['StoreName'] + " src='" + imagePath + address['ImageName'] + "'/></div>"
                            HtmlTags += "<div class='col-lg-8 col-md-8 col-sm-8 col-xs-12 nopadding'>" + "<div class='col-lg-5 col-md-5 col-sm-5 col-xs-12 nopadding-mobile'>" + "<div class='form-group'><h4 style='margin: 0px;'>" + address['StoreName'] + "</h4></div>"
                           
                            HtmlTags += "<div style='margin-bottom: 15px !important;'><h4 style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>"
                            $.each(arr, function (j, val) {
                                HtmlTags += "<div class='form-group' style='line-height: 10px;'><p class='store-hours'>" + arr[j] + "</p></div>"
                            });
                            HtmlTags += "</div>"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 nopadding col-xs-12'><div class='form-group'>" + addr.replace(/^,/, '') + "</div>"
                            HtmlTags += "<div class='form-group'><p class='storeLocationCoordinate recent-order-reorder'><a href=" + address['MapLocationURL'] + " target='_blank' style='text-decoration: underline;' title='Map and Directions'>Map and Directions</a></p></div></div>"
                            HtmlTags += "<div class='col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center margin-bottom'><h1><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</h1></div>"
                            HtmlTags += "<div class='col-lg-12 col-xs-12 col-md-12 margin-top'><div class='form-inlineclass'><div>"
                            
                            HtmlTags += "<a style='color: #fff !important;font-weight: normal;' class='btn red' href=" + storedetailshref + " target='_blank' title='Store Details'>Store Details</a></div>"
                            if (currentStore == address['StoreName']) {
                                HtmlTags += "<div><button id=" + address['WarehouseId'] + " onclick=' return ShowPopUpSL(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\");' class='btn btn-primary-1' data-dismiss='modal' aria-hidden='true' value='Make this My Store' title='Make this My Store' style='margin-left: 15px;'>My Store</button></div></div></div>" + "</div>";
                            }
                            else {
                                HtmlTags += "<div><button id=" + address['WarehouseId'] + " onclick=' return ShowPopUpSL(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store' title='Make this My Store' style='margin-left: 15px;'>Make this My Store</button></div></div></div>" + "</div>";
                            }
                            HtmlTags += "</div></div><br/>";

                            $("#storedetails").html(HtmlTags);
                            $("#storedetails").show();
                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

/*Not in use*/
function GetMapURL(storename, ctrl) {
    var state = "ALL";
    var sku = "0";
    var locationMapUrl = "";
    $.ajax({
        type: "GET",
        url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {            
            $.each(data, function (i, item) {
                if (i == "StoreLocations") {
                    var json = JSON.parse(JSON.stringify(data[i]));
                    $.each(json, function (j, val) {
                        var address1 = JSON.stringify(json[j]);
                        var address = JSON.parse(address1);
                        if (address['StoreName'] == storename) {
                            locationMapUrl = address['MapLocationURL'];
                           
                            window.location.href = locationMapUrl;
                            
                        }

                    });
                }
            });
        },
        failure: function (data) {
            console.log("Failure-" + data.responseText);
        },
        error: function (data) {
            // console.log(data.responseText);
        }

    });


}

function ShowPopUpSL(obj, storename, storeaddress) {
    try {
        
      //  alert("ShowPopUpSL called storename=" + storename + ",storeaddress=" + storeaddress);
        $('#btnProceed').attr('data-storeid', $(obj).attr('id'));
        $('#btnProceed').attr('data-storename', storename);
        $('#btnProceed').attr('data-storeaddress', storeaddress);
       // alert("btnProceed=" + $(obj).attr('id') + "," + $('#btnProceed').attr('data-storename') + "," + $('#btnProceed').attr('data-storeaddress'));
   
        $('#btnConfirmStorePopUp').click();
        return false;
    }
    catch (err) {
        console.log(err.message);
    }
}

function ShowStores(obj) {
    try {
       // alert("ShowStores called");
        var selectedstoreid = $(obj).attr('data-storeid');
        var storename = $(obj).attr('data-storename');
        var storeaddress = $(obj).attr('data-storeaddress');
       // alert(selectedstoreid + "," + storename + "," + storeaddress);
        SaveinCookie(selectedstoreid, storename, storeaddress)
    }
    catch (err) {
        console.log("ShowStores msg="+err.message);
    }

}
/***************STORE LOCATOR METHODS END***************************/


/**************************CheckOut Methods**********************************/
function GetStockForCheckout(sku, productid) {
    var state = "ALL";
    var selectedstore = $(".storename:first").text();
   // alert(selectedstore);
    try {
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
              
                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));
                        //console.log(json);
                        $.each(json, function (j, val) {
                            if (json[j].StoreName == selectedstore) {

                                $("#SelectedWarehouseStock").val(json[j].Quantity);
                                var status = "status_" + productid;
                                var showmsgpickup = "showmsgpickup_" + productid;
                                var qty = "#qty_" + productid;
                               // alert("qty=" + $(qty).html());
                                if (json[j].Quantity == 0) {
                                    //$("#Ship_" + productid).prop('checked', true);
                                    $(".qty").each(function () {
                                       // alert("inside .qty");
                                        var qtysku = $(this).attr('data-sku');
                                       // var radioValue = $("input[id='Pickup-" + productid + "']:checked").val();
                                        if (sku == qtysku) {
                                            //$(this).attr('data-IsPickUp', 'true');
                                            $("#" + status).html('Not Available <br/> <span class="select-store"><a style="text-decoration: underline;padding: 0px;font-weight: 400" class="text-danger" href="/cart" title="Select the ship option">Select the ship option</a></span>');
                                            $("#" + status).show();
                                          //  $("#btnCompleteCheckout").addClass('disable-anchor');
                                           // $("#paypal-express-checkout").addClass('disable-anchor');
                                        }
                                    });
                                }                                 
                                else {
                                    if (json[j].Quantity < $(qty).html()) {
                                        $("#" + status).html("<b style='color:#000'>Out of stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left <br/><a href='/cart'>Update</a></span>");
                                        //$("#btnCompleteCheckout").addClass('disable-anchor');
                                        //$("#paypal-express-checkout").addClass('disable-anchor');
                                    }
                                    else {
                                        $("#" + status).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                        //$("#btnCompleteCheckout").removeClass('disable-anchor');
                                       // $("#paypal-express-checkout").removeClass('disable-anchor');
                                    }
                                    $("#" + status).show();
                                  //  $("#" + showmsgpickup).hide();                                    
                                }
                                

                            }
                        });
                    }
                });
				
					 $(".pickupsku").each(function () {
            var value = $(this).text();
            // alert("value=" + value);
            var arr = value.split('-');
            var productid = arr[1];
            var status = "status_" + productid;
            if ($("#" + status).html().indexOf("Not Available") != -1) {
				//alert("inside not available");
				 $("#btnCompleteCheckout").addClass('disable-anchor');
                $("#paypal-express-checkout").addClass('disable-anchor');

            }
        });
		$("#loaderId").html("");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}