var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Quote = /** @class */ (function (_super) {
    __extends(Quote, _super);
    function Quote() {
        return _super.call(this) || this;
    }
    Quote.prototype.Init = function () {
    };
    Quote.prototype.SubmitQuote = function () {
        Checkout.prototype.ShowLoader();
        //Set recipient name if recipient textbox is available
        Checkout.prototype.SaveRecipientNameAddressData('shipping', function (response) {
            if (!Quote.prototype.IsCheckoutDataValid()) {
                ZnodeBase.prototype.HideLoader();
            }
            else {
                if (!Quote.prototype.ShippingErrorMessage()) {
                    Checkout.prototype.HideLoader();
                    return false;
                }
                if ($("#dynamic-allowesterritories").length > 0) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AllowedTerritories"), "error", false, 0);
                    Checkout.prototype.HideLoader();
                    return false;
                }
                else {
                    var userType = ZnodeBase.prototype.GetParameterValues("mode");
                    if (userType == undefined) {
                        userType = "";
                    }
                    userType = (userType != "") ? userType.replace("#", "") : userType;
                    Quote.prototype.SubmitPlaceQuoteForm();
                }
            }
        });
    };
    Quote.prototype.SubmitPlaceQuoteForm = function () {
        var data = {};
        //Get all the selected values required to submit order.
        Quote.prototype.SetQuoteFormData(data);
        //Create form to submit order.
        var form = Quote.prototype.CreateSubmitQuoteForm(data);
        // submit form
        form.submit();
        form.remove();
    };
    //Get all the selected values required to submit order.
    Quote.prototype.SetQuoteFormData = function (data) {
        data["ShippingAddressId"] = $("#shipping-content").find("#AddressId").val();
        data["BillingAddressId"] = $("#billing-content").find("#AddressId").val();
        data["ShippingId"] = $("input[name='ShippingOptions']:checked").val(); //shipping type id
        data["ShippingCode"] = $("input[name='ShippingOptions']:checked").attr("data-shippingcode");
        data["AdditionalInstruction"] = $("#AdditionalInstruction").val();
        data["FreeShipping"] = $("#cartFreeShipping").val();
        data["InHandDate"] = $("#InHandDate").val();
        data["PortalId"] = $("#hdnPortalId").val();
        data["UserId"] = $("#UserId").val();
        data["ShippingConstraintCode"] = $("input[name='ShippingConstraintCode']:checked").val();
        data["JobName"] = $("#JobName").val();
    };
    //Create form to submit order.
    Quote.prototype.CreateSubmitQuoteForm = function (data) {
        var form = $('<form/></form>');
        form.attr("action", "/Quote/SubmitQuote");
        form.attr("method", "POST");
        form.attr("style", "display:none;");
        form.attr("enctype", "multipart/form-data");
        Quote.prototype.AddFormFields(form, data);
        $("body").append(form);
        return form;
    };
    Quote.prototype.AddFormFields = function (form, data) {
        if (data != null) {
            $.each(data, function (name, value) {
                if (value != null) {
                    var input = $("<input></input>").attr("type", "hidden").attr("name", name).val(value);
                    form.append(input);
                }
            });
        }
    };
    Quote.prototype.IsCheckoutDataValid = function () {
        var Total = $("#Total").val();
        Total = Total.replace(',', '.');
        if (Total != "" && Total != null && Total != 'undefined') {
            Total = Total.replace(',', '');
        }
        var isValid = true;
        var shippingOptionValue = $("input[name='ShippingOptions']:checked").val();
        var isBillingAddresOptional = $("#IsBillingAddressOptional").val();
        $("#errorShippingMethod").hide();
        $("#expeditedShippingWarningDiv").removeClass("error");
        if ($("#shipping-content .address-recipient").length == 0 && ($("#shipping-content .address-name").text().trim() == "")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredShippingAddress"), "error", false, 0);
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        else if (($("#shipping-content .address-recipient").length > 0) && ($("#shipping-content .address-recipient").val().trim() == "")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredRecipientName"), "error", false, 0);
            $("#shipping-content .address-recipient").focus();
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        if ($("#billing-content .address-citystate").length < 1 && isBillingAddresOptional != 'true') {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredBillingAddress"), "error", false, 0);
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        if ($("#billing-content .address-citystate").length > 1 && ($("#billing-content .address-citystate").attr("data-address-postalcode").trim() == "") && isBillingAddresOptional != 'true') {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredBillingAddress"), "error", false, 0);
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        else if ((shippingOptionValue == null || shippingOptionValue == "") && ($("#cartFreeShipping").val() != "True" || $("#hdnIsFreeShipping").val() != "True")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectShippingOption"), "error", isFadeOut, fadeOutTime);
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        else if ($("#hndShippingclassName").val() != undefined && $("#hndShippingclassName").val() == Constant.ZnodeCustomerShipping && ($("#AccountNumber").val() == undefined || $("#AccountNumber").val() == "")) {
            $("#errorAccountNumber").show();
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        else if ($("#hndShippingclassName").val() != undefined && $("#hndShippingclassName").val() == Constant.ZnodeCustomerShipping && ($("#ShippingMethod").val() == undefined || $("#ShippingMethod").val() == "")) {
            $("#errorShippingMethod").show();
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        else if ($("#expeditedShippingWarningDiv").is(':visible') && $("#expeditedCheckbox").is(':checked') === false) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ConfirmShippingMethod"), "error", false, 0);
            $("#expeditedShippingWarningDiv").addClass("error");
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        else if ($("#EnableUserOrderAnnualLimit").val() && $("#EnableUserOrderAnnualLimit").val().toLowerCase() == "true" && parseInt($("#AnnualOrderLimit").val()) > 0 && (parseInt($("#AnnualBalanceOrderAmount").val()) - parseInt(Total) <= 0)) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AnnualOrderLimitFailed") + $("#AnnualOrderLimitWithCurrency").val(), "error", false, 0);
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        else if ($("#EnablePerOrderlimit").val() && $("#EnablePerOrderlimit").val().toLowerCase() == "true" && parseInt($("#PerOrderLimit").val()) > 0 && parseInt($("#PerOrderLimit").val()) <= parseInt(Total)) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PerOrderLimitFailed") + $("#PerOrderLimitWithCurrency").val(), "error", false, 0);
            isValid = false;
            Checkout.prototype.HideLoader();
        }
        return isValid;
    };
    Quote.prototype.ShippingErrorMessage = function () {
        var shippingErrorMessage = $("#ShippingErrorMessage").val();
        var shippingHasError = $("#ValidShippingSetting").val();
        if (shippingHasError != null && shippingHasError != "" && shippingHasError != 'undefined' && shippingHasError.toLowerCase() == "false" && shippingErrorMessage != null && shippingErrorMessage != "" && shippingErrorMessage != 'undefined') {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(shippingErrorMessage, "error", false, 0);
            return false;
        }
        else if (shippingErrorMessage != null && shippingErrorMessage != "" && shippingErrorMessage != 'undefined') {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(shippingErrorMessage, "error", false, 0);
            return true;
        }
        Checkout.prototype.DisablePaymentOnZeroOrderTotal();
        Checkout.prototype.ToggleFreeShipping();
        return true;
    };
    Quote.prototype.GetPaymentOptions = function () {
        $("#errorPayment").html("");
        $('#payment-view-content').html("<span style='position:absolute;top:0;bottom:0;left:0;right:0;text-align:center;transform:translate(0px, 45%);font-weight:600;'>Loading...</span>");
        Endpoint.prototype.PaymentOptions(true, true, function (response) {
            $("#payment-view-content").html(response);
        });
    };
    Quote.prototype.ClosePopup = function () {
        $("#errorPayment").html("");
        $("#payment-view-popup-ipad").find(".close").click();
        ZnodeBase.prototype.HideLoader();
    };
    Quote.prototype.ConvertQuoteToOrder = function () {
        Checkout.prototype.ShowLoader();
        if (!Quote.prototype.IsQuoteDataValid()) {
            Checkout.prototype.isPayMentInProcess = false;
        }
        else {
            var paymentOptionId = $("input[name='PaymentOptions']:checked").attr("id");
            var paymentType = Checkout.prototype.GetPaymentType(paymentOptionId);
            var userType = ZnodeBase.prototype.GetParameterValues("mode");
            if (userType == undefined) {
                userType = "";
            }
            userType = (userType != "") ? userType.replace("#", "") : userType;
            switch (paymentType.toLowerCase()) {
                case "cod":
                    Quote.prototype.ClosePopup();
                    Quote.prototype.SubmitQuoteForm();
                    break;
                case "credit_card":
                    Quote.prototype.SubmitPayment();
                    break;
                default:
                    // global data
                    if (Checkout.prototype.CheckValidPODocument()) {
                        Quote.prototype.SubmitQuoteForm();
                    }
                    else {
                        Checkout.prototype.HideLoader();
                        return false;
                    }
                    break;
            }
        }
    };
    Quote.prototype.IsQuoteDataValid = function () {
        var isValid = true;
        var paymentOptionValue = $("input[name='PaymentOptions']:checked").val();
        if (paymentOptionValue == null || paymentOptionValue == "") {
            isValid = false;
            $("#errorPayment").html(ZnodeBase.prototype.getResourceByKeyName("SelectPaymentOption"));
            Checkout.prototype.HidePaymentLoader();
        }
        return isValid;
    };
    Quote.prototype.SubmitQuoteForm = function () {
        Checkout.prototype.ShowLoader();
        var data = {};
        //Get all the selected values required to submit order.
        Quote.prototype.SetPaymentData(data);
        //Create form to submit order.
        var form = Quote.prototype.CreateForm(data);
        // submit form
        form.submit();
        form.remove();
    };
    //Get all the selected values required to submit order.
    Quote.prototype.SetPaymentData = function (data) {
        var paymentOptionId = $("input[name='PaymentOptions']:checked").attr("id");
        var paymentType = Checkout.prototype.GetPaymentType(paymentOptionId);
        data["OmsQuoteId"] = $("#QuoteId").val();
        data["UserId"] = $("#hdnUserId").val();
        data["PaymentDetails.PaymentSettingId"] = $("input[name='PaymentOptions']:checked").val();
        data["PaymentDetails.paymentType"] = paymentType;
        data["PaymentDetails.PurchaseOrderNumber"] = $("#txtPurchaseOrderNumber").val();
        data["PaymentDetails.PODocumentName"] = $("#po-document-path").val();
    };
    //Create form to submit order.
    Quote.prototype.CreateForm = function (data) {
        var form = $('<form/></form>');
        form.attr("action", "/Quote/ConvertQuoteToOrder");
        form.attr("method", "POST");
        form.attr("style", "display:none;");
        form.attr("enctype", "multipart/form-data");
        Checkout.prototype.AddFormFields(form, data);
        $("body").append(form);
        return form;
    };
    //Submit Payment
    Quote.prototype.SubmitPayment = function () {
        var Total = $("#Total").val();
        Total = Total.replace(',', '.');
        if (Checkout.prototype.IsOrderTotalGreaterThanZero(Total)) {
            if (Quote.prototype.IsValidCreditCardDetails()) {
                var shippingId = $("#shipping-content").find("#AddressId").val();
                var billingId = $("#billing-content").find("#AddressId").val();
                if ($("#IsBillingAddressOptional").val() == 'true' && (parseInt(billingId) == 0)) {
                    billingId = $("#shipping-content").find("#AddressId").val();
                    $("#billing-content").find("#AddressId").val(billingId);
                }
                var currentStatus = Checkout.prototype.isPayMentInProcess;
                Quote.prototype.ClosePopup();
                Checkout.prototype.ShowPaymentProcessDialog();
                Endpoint.prototype.GetshippingBillingAddress(parseInt($("#hdnPortalId").val()), parseInt(shippingId), parseInt(billingId), function (response) {
                    Checkout.prototype.isPayMentInProcess = currentStatus;
                    if (!response.Billing.HasError) {
                        if ($("#ajaxProcessPaymentError").html() == undefined) {
                        }
                        else {
                            $("#ajaxProcessPaymentError").html(ZnodeBase.prototype.getResourceByKeyName("ProcessingPayment"));
                        }
                        var _a = Quote.prototype.GetOrderDetails(response), BillingCity = _a.BillingCity, BillingCountryCode = _a.BillingCountryCode, BillingFirstName = _a.BillingFirstName, BillingLastName = _a.BillingLastName, BillingPhoneNumber = _a.BillingPhoneNumber, BillingPostalCode = _a.BillingPostalCode, BillingStateCode = _a.BillingStateCode, BillingStreetAddress1 = _a.BillingStreetAddress1, BillingStreetAddress2 = _a.BillingStreetAddress2, BillingEmailId = _a.BillingEmailId, ShippingCity = _a.ShippingCity, ShippingCountryCode = _a.ShippingCountryCode, ShippingFirstName = _a.ShippingFirstName, ShippingLastName = _a.ShippingLastName, ShippingPhoneNumber = _a.ShippingPhoneNumber, ShippingPostalCode = _a.ShippingPostalCode, ShippingStateCode = _a.ShippingStateCode, ShippingStreetAddress1 = _a.ShippingStreetAddress1, ShippingStreetAddress2 = _a.ShippingStreetAddress2, ShippingEmailId = _a.ShippingEmailId;
                        var cardNumber = $("#div-CreditCard [data-payment='number']").val().split(" ").join("");
                        var IsAnonymousUser = $("#hdnAnonymousUser").val() == 0 ? true : false;
                        var guid = $('#GUID').val();
                        var discount = $('#Discount').val();
                        var ShippingCost = $('#ShippingCost').val();
                        var SubTotal = $('#SubTotal').val();
                        var cardType = Checkout.prototype.DetectCardType(cardNumber);
                        var orderNumber = response.orderNumber;
                        if (cardNumber != "") {
                            $("#hdnCreditCardNumber").val(cardNumber.slice(-4));
                        }
                        if ($("#addNewCreditCard-panel").attr("class").indexOf("active") != -1) {
                            if (cardType.toLowerCase() != $("input[name='PaymentProviders']:checked").val().toLowerCase()) {
                                Checkout.prototype.HidePaymentProcessDialog();
                                var message = ZnodeBase.prototype.getResourceByKeyName("SelectedCardType") + $("input[name='PaymentProviders']:checked").val().toLowerCase() + ZnodeBase.prototype.getResourceByKeyName("SelectCardNumberAndCardType");
                                if (message != undefined) {
                                    Checkout.prototype.ShowErrorPaymentDialog(message);
                                }
                                Checkout.prototype.HideLoader();
                                return false;
                            }
                        }
                        var paymentSettingId = $('#PaymentSettingId').val();
                        var paymentCode = $('#hdnPaymentCode').val();
                        var CustomerPaymentProfileId = $('#CustomerPaymentProfileId').val();
                        var CustomerProfileId = $('#CustomerProfileId').val();
                        var CardDataToken = $('#CardDataToken').val();
                        var gatewayCode = $("#hdnGatwayName").val();
                        if (gatewayCode.toLowerCase() == 'payflow') {
                            if ($("#hdnEncryptedTotalAmount").val() != undefined && $("#hdnEncryptedTotalAmount").val() != null) {
                                Total = $("#hdnEncryptedTotalAmount").val();
                            }
                        }
                        if (Total.indexOf(',') > -1) {
                            Total.replace(',', '');
                        }
                        //Get Payment model
                        var payment = Quote.prototype.GetPaymentModel(guid, gatewayCode, BillingCity, BillingCountryCode, BillingFirstName, BillingLastName, BillingPhoneNumber, BillingPostalCode, BillingStateCode, BillingStreetAddress1, BillingStreetAddress2, BillingEmailId, ShippingCost, ShippingCity, ShippingCountryCode, ShippingFirstName, ShippingLastName, ShippingPhoneNumber, ShippingPostalCode, ShippingStateCode, ShippingStreetAddress1, ShippingStreetAddress2, ShippingEmailId, SubTotal, Total, discount, cardNumber, CustomerPaymentProfileId, CustomerProfileId, CardDataToken, cardType, paymentSettingId, IsAnonymousUser, paymentCode, orderNumber);
                        //Validate Payment Profile And Proceed for Convert To Order
                        Quote.prototype.ValidatePaymentProfileAndConvertToOrder(payment, paymentSettingId, paymentCode, gatewayCode);
                    }
                });
            }
        }
    };
    Quote.prototype.ValidatePaymentProfileAndConvertToOrder = function (payment, paymentSettingId, paymentCode, gatewayCode) {
        payment["CardSecurityCode"] = payment["PaymentToken"] ? $("[name='SaveCard-CVV']:visible").val() : $("#div-CreditCard [data-payment='cvc']").val();
        $("#div-CreditCard").hide();
        var paymentOptionId = $("input[name='PaymentOptions']:checked").attr("id");
        var paymentType = Checkout.prototype.GetPaymentType(paymentOptionId);
        submitCard(payment, function (response) {
            if (response.GatewayResponse == undefined) {
                if (response.indexOf("Unauthorized") > 0) {
                    Quote.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessCreditCardPayment") + response + ZnodeBase.prototype.getResourceByKeyName("ContactUsToCompleteOrder"));
                    Checkout.prototype.HideLoader();
                    Checkout.prototype.isPayMentInProcess = false;
                }
            }
            else {
                var isSuccess = response.GatewayResponse.IsSuccess;
                if (isSuccess) {
                    Quote.prototype.ClosePopup();
                    var submitPaymentViewModel = Quote.prototype.GetSubmitPaymentViewModel(paymentSettingId, paymentCode, response, paymentType);
                    $.ajax({
                        type: "POST",
                        url: "/quote/ConvertQuoteToOrder",
                        async: true,
                        data: submitPaymentViewModel,
                        success: function (response) {
                            Checkout.prototype.isPayMentInProcess = false;
                            if (response.error != null && response.error != "" && response.error != 'undefined') {
                                var message = Checkout.prototype.GetPaymentErrorMsg(response);
                                Quote.prototype.ClearPaymentAndDisplayMessage(message);
                                Checkout.prototype.HideLoader();
                                return false;
                            }
                            else if (response.receiptHTML != null && response.receiptHTML != "" && response.receiptHTML != 'undefined') {
                                Quote.prototype.CanclePayment();
                                //This will focus to the top of screen.
                                $(this).scrollTop(0);
                                $('body, html').animate({ scrollTop: 0 }, 'fast');
                                $(".cartcount").html('0');
                                $("#messageBoxContainerId").hide();
                                $(".cartAmount").html('');
                                alert("Quote");
                                window.location.href = "/Checkout/OrderCheckoutReceipt";
                            }
                        },
                        error: function () {
                            Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessOrder"));
                            Checkout.prototype.HideLoader();
                            return false;
                        }
                    });
                }
                else {
                    Quote.prototype.PaymentFailedProcess(response, gatewayCode);
                }
            }
        });
    };
    Quote.prototype.PaymentFailedProcess = function (response, gatewayCode) {
        Checkout.prototype.isPayMentInProcess = false;
        var errorMessage = response.GatewayResponse.ResponseText;
        if (errorMessage == undefined) {
            errorMessage = response.GatewayResponse.GatewayResponseData;
        }
        if (errorMessage != undefined && errorMessage.toLowerCase().indexOf("missing card data") >= 0) {
            Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlacementCardDataMissing"));
        }
        else if (errorMessage != undefined && errorMessage.indexOf("Message=") >= 0) {
            Checkout.prototype.ClearPaymentAndDisplayMessage(errorMessage.substr(errorMessage.indexOf("=") + 1));
            $("#div-CreditCard").show();
        }
        else if (errorMessage != null && errorMessage != undefined && errorMessage.indexOf('customer') > 0) {
            Checkout.prototype.ClearPaymentAndDisplayMessage(errorMessage);
        }
        else {
            switch (gatewayCode.toLowerCase()) {
                case "payflow":
                    if (response.GatewayResponse.ResponseText)
                        Checkout.prototype.ClearPaymentAndDisplayMessage(response.GatewayResponse.ResponseText);
                    else
                        Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlacement"));
                    break;
                default:
                    Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlacement"));
            }
        }
        Checkout.prototype.HideLoader();
    };
    Quote.prototype.GetSubmitPaymentViewModel = function (paymentSettingId, paymentCode, response, paymentType) {
        return {
            OmsQuoteId: $("#QuoteId").val(),
            UserId: $("#hdnUserId").val(),
            PaymentDetails: {
                PaymentSettingId: paymentSettingId,
                PaymentCode: paymentCode,
                CustomerProfileId: response.GatewayResponse.CustomerProfileId,
                CustomerPaymentId: response.GatewayResponse.CustomerPaymentProfileId,
                CustomerShippingAddressId: response.GatewayResponse.CustomerShippingAddressId,
                CustomerGuid: response.GatewayResponse.CustomerGUID,
                PaymentToken: $("input[name='CCdetails']:checked").val(),
                paymentType: paymentType
            }
        };
    };
    Quote.prototype.GetPaymentModel = function (guid, gatewayCode, BillingCity, BillingCountryCode, BillingFirstName, BillingLastName, BillingPhoneNumber, BillingPostalCode, BillingStateCode, BillingStreetAddress1, BillingStreetAddress2, BillingEmailId, ShippingCost, ShippingCity, ShippingCountryCode, ShippingFirstName, ShippingLastName, ShippingPhoneNumber, ShippingPostalCode, ShippingStateCode, ShippingStreetAddress1, ShippingStreetAddress2, ShippingEmailId, SubTotal, Total, discount, cardNumber, CustomerPaymentProfileId, CustomerProfileId, CardDataToken, cardType, paymentSettingId, IsAnonymousUser, paymentCode, orderNumber) {
        return {
            "GUID": guid,
            "GatewayType": gatewayCode,
            "BillingCity": BillingCity,
            "BillingCountryCode": BillingCountryCode,
            "BillingFirstName": BillingFirstName,
            "BillingLastName": BillingLastName,
            "BillingPhoneNumber": BillingPhoneNumber,
            "BillingPostalCode": BillingPostalCode,
            "BillingStateCode": BillingStateCode,
            "BillingStreetAddress1": BillingStreetAddress1,
            "BillingStreetAddress2": BillingStreetAddress2,
            "BillingEmailId": BillingEmailId,
            "ShippingCost": ShippingCost,
            "ShippingCity": ShippingCity,
            "ShippingCountryCode": ShippingCountryCode,
            "ShippingFirstName": ShippingFirstName,
            "ShippingLastName": ShippingLastName,
            "ShippingPhoneNumber": ShippingPhoneNumber,
            "ShippingPostalCode": ShippingPostalCode,
            "ShippingStateCode": ShippingStateCode,
            "ShippingStreetAddress1": ShippingStreetAddress1,
            "ShippingStreetAddress2": ShippingStreetAddress2,
            "ShippingEmailId": ShippingEmailId,
            "SubTotal": SubTotal,
            "Total": Total,
            "Discount": discount,
            "PaymentToken": ($("#addNewCreditCard-panel").attr("class").indexOf("active") != -1) ? "" : $("input[name='CCListdetails']:checked").val(),
            "CardNumber": cardNumber,
            "CardExpirationMonth": $("#div-CreditCard [data-payment='exp-month']").val(),
            "CardExpirationYear": $("#div-CreditCard [data-payment='exp-year']").val(),
            "GatewayCurrencyCode": $('#hdnCurrencyCode').val(),
            "CustomerPaymentProfileId": CustomerPaymentProfileId,
            "CustomerProfileId": CustomerProfileId,
            "CardDataToken": CardDataToken,
            "CardType": cardType,
            "PaymentSettingId": paymentSettingId,
            "IsAnonymousUser": IsAnonymousUser,
            "IsSaveCreditCard": $("#SaveCreditCard").is(':checked'),
            "CardHolderName": $("#div-CreditCard [data-payment='cardholderName']").val(),
            "CustomerGUID": $("#hdnCustomerGUID").val(),
            "PaymentCode": paymentCode,
            "OrderId": orderNumber
        };
    };
    Quote.prototype.GetOrderDetails = function (response) {
        var BillingCity = response.Billing.CityName;
        var BillingCountryCode = response.Billing.CountryName;
        var BillingFirstName = response.Billing.FirstName;
        var BillingLastName = response.Billing.LastName;
        var BillingPhoneNumber = response.Billing.PhoneNumber;
        var BillingPostalCode = response.Billing.PostalCode;
        var BillingStateCode = response.Billing.StateName;
        if (response.Billing.StateCode != undefined && response.Billing.StateCode != null && response.Billing.StateCode != "") {
            BillingStateCode = response.Billing.StateCode;
        }
        var BillingStreetAddress1 = response.Billing.Address1;
        var BillingStreetAddress2 = response.Billing.Address2;
        var BillingEmailId = response.Billing.EmailAddress;
        var ShippingCity = response.Shipping.CityName;
        var ShippingCountryCode = response.Shipping.CountryName;
        var ShippingFirstName = response.Shipping.FirstName;
        var ShippingLastName = response.Shipping.LastName;
        var ShippingPhoneNumber = response.Shipping.PhoneNumber;
        var ShippingPostalCode = response.Shipping.PostalCode;
        var ShippingStateCode = response.Shipping.StateName;
        var ShippingStreetAddress1 = response.Shipping.Address1;
        var ShippingStreetAddress2 = response.Shipping.Address2;
        var ShippingEmailId = response.Shipping.EmailAddress;
        return { BillingCity: BillingCity, BillingCountryCode: BillingCountryCode, BillingFirstName: BillingFirstName, BillingLastName: BillingLastName, BillingPhoneNumber: BillingPhoneNumber, BillingPostalCode: BillingPostalCode, BillingStateCode: BillingStateCode, BillingStreetAddress1: BillingStreetAddress1, BillingStreetAddress2: BillingStreetAddress2, BillingEmailId: BillingEmailId, ShippingCity: ShippingCity, ShippingCountryCode: ShippingCountryCode, ShippingFirstName: ShippingFirstName, ShippingLastName: ShippingLastName, ShippingPhoneNumber: ShippingPhoneNumber, ShippingPostalCode: ShippingPostalCode, ShippingStateCode: ShippingStateCode, ShippingStreetAddress1: ShippingStreetAddress1, ShippingStreetAddress2: ShippingStreetAddress2, ShippingEmailId: ShippingEmailId };
    };
    Quote.prototype.IsValidCreditCardDetails = function () {
        var isValid = true;
        if (!$("#radioCCList").is(':visible')) {
            $('input[data-payment="number"],input[data-payment="exp-month"],input[data-payment="exp-year"],input[data-payment="cvc"]').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "border": "1px solid black",
                        "background": ""
                    });
                }
            });
            isValid = Checkout.prototype.ValidateCreditCardDetails();
        }
        else {
            isValid = Checkout.prototype.ValidateCVV();
        }
        if (isValid == false) {
            Checkout.prototype.isPayMentInProcess = false;
            return false;
        }
        return isValid;
    };
    Quote.prototype.ClearPaymentAndDisplayMessage = function (message) {
        Quote.prototype.CanclePayment();
        $("#errorPayment").html(message);
    };
    Quote.prototype.CanclePayment = function () {
        $("#div-CreditCard").hide();
        $("#div-CreditCard [data-payment='number']").val('');
        $("#div-CreditCard [data-payment='cvc']").val('');
        $("#div-CreditCard [data-payment='exp-month']").val('');
        $("#div-CreditCard [data-payment='exp-year']").val('');
        $("#div-CreditCard [data-payment='cardholderName']").val('');
        $("input[name='PaymentOptions']:checked").prop('checked', false);
    };
    return Quote;
}(ZnodeBase));
//# sourceMappingURL=Quote.js.map