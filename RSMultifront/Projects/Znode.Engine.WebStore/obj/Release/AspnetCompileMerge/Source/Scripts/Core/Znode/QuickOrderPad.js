var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var QuickOrderPad = /** @class */ (function (_super) {
    __extends(QuickOrderPad, _super);
    function QuickOrderPad() {
        return _super.call(this) || this;
    }
    QuickOrderPad.prototype.Init = function () {
        $('#btnQuickOrderPad').attr('disabled', 'disabled');
        QuickOrderPad.prototype.QuickOrderPadAutoComplete();
        QuickOrderPad.prototype.GenerateNewRow();
        QuickOrderPad.prototype.AddMultipleOrdersToCart();
        QuickOrderPad.prototype.ClearAll();
        QuickOrderPad.prototype.RemoveRow();
        QuickOrderPad.prototype.ShowRemoveItemBox();
        QuickOrderPad.prototype.SetQuantity();
    };
    QuickOrderPad.prototype.QuickOrderPadAutoComplete = function () {
    };
    QuickOrderPad.prototype.OnItemSelect = function (item) {
        var act = document.activeElement;
        Endpoint.prototype.GetAutoCompleteItemProperties(item.id, function (res) {
            $(act).val(res.DisplayText);
            $('.quick-order-pad-autocomplete').val(res.DisplayText);
            $(act).attr("data_qo_sku", res.DisplayText);
            $(act).attr("data_qo_product_id", res.Id);
            $(act).attr("data_qo_product_name", res.Properties.ProductName);
            $(act).attr("data_qo_cart_quantity", res.Properties.CartQuantity);
            $(act).attr("data_qo_quantity_on_hand", res.Properties.Quantity);
            $(act).attr("data_qo_product_type", res.Properties.ProductType);
            $(act).attr("data_qo_addon_product", res.Properties.AddOnProductSkus);
            $(act).attr("data_qo_retail_price", res.Properties.RetailPrice);
            $(act).attr("data_qo_group_product_sku", res.Properties.GroupProductSKUs);
            $(act).attr("data_qo_group_product_qty", res.Properties.GroupProductsQuantity);
            $(act).attr("data_qo_configurable_product_sku", res.Properties.ConfigurableProductSKUs);
            $(act).attr("data_qo_autoaddonskus", res.Properties.AutoAddonSKUs);
            $(act).attr("data_qo_inventorycode", res.Properties.InvetoryCode);
            $(act).attr("data_qo_isobsolete", res.Properties.IsObsolete);
            if (res.Properties.CallForPricing != undefined) {
                $(act).attr("data_qo_call_for_pricing", res.Properties.CallForPricing);
            }
            else {
                $(act).attr("data_qo_call_for_pricing", '');
            }
            if (res.Properties.TrackInventory != undefined) {
                $(act).attr("data_qo_track_inventory", res.Properties.TrackInventory);
            }
            else {
                $(act).attr("data_qo_track_inventory", '');
            }
            if (res.Properties.OutOfStockMessage != undefined) {
                $(act).attr("data_qo_out_stock_message", res.Properties.OutOfStockMessage);
            }
            if (res.Properties.MaxQuantity != undefined) {
                $(act).attr("data_qo_max_quantity", res.Properties.MaxQuantity);
            }
            if (item.properties.MinQuantity != undefined) {
                $(act).attr("data_qo_min_quantity", item.properties.MinQuantity);
            }
            $('.quickOrderPadAddToCart').prop('disabled', false);
        });
    };
    QuickOrderPad.prototype.GenerateNewRow = function () {
        var id = parseInt($('#indexId').val()) + 1;
        $('#defaultValue-add-new-row').on('click', function () {
            $("#quickorderdiv").append('<div class="form-group" id="form-group-' + id + '"><div class="col-xs-8 col-sm-9 nopadding"><input class="typeahead tt-input" data-autocomplete-id-field="Id" data-autocomplete-url="/Product/GetProductListBySKU" data-onselect-function="QuickOrderPad.prototype.OnItemSelect" data_autocomplete_url="/Product/GetProductListBySKU" data_is_first="true" data_onselect_function="QuickOrderPad.prototype.OnItemSelect" data_qo_call_for_pricing="" data_qo_cart_quantity="" data_qo_in_stock_message="" data_qo_max_quantity="" data_qo_min_quantity="" data_qo_out_stock_message="" data_qo_product_id="" data_qo_product_name="" data_qo_quantity_on_hand="" data_qo_sku="" data_qo_track_inventory="" id="Name" name="Name" placeholder="Enter SKU" type="text" value="" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><p id="inventoryMessage_' + id + '" for="txtQuickOrderPadSku_' + id + '" class="col-xs-12 nopadding error-msg"></p></div><div class="col-xs-3 col-sm-2"><input class="quantity quick-order-pad-quantity" parentcontrol= "txtQuickOrderPadSku_' + id + '"  id="txtQuickOrderPadQuantity_' + id + '" maxlength="4" name="txtQuickOrderPadQuantity_' + id + '" placeholder="Qty" type="text" value="1" /></div><div class="col-xs-1 nopadding"><div id="removeRow_' + id + '" class="remove_row remove-item" title="Clear"><i class="zf-close"></i></div></div></div>');
            id++;
            QuickOrderPad.prototype.QuickOrderPadAutoComplete();
            QuickOrderPad.prototype.SetQuantity();
            QuickOrderPad.prototype.RemoveRow();
        });
    };
    QuickOrderPad.prototype.ClearAll = function () {
        $("#btnQuickOrderClearAll").on("click", function () {
            var index = 0;
            $('#quick-order-pad-content [data-autocomplete-url]').each(function () {
                index++;
                $(this).val("");
                $(".quick-order-pad-quantity").val('1');
                $('p[for="' + this.id + '"]').html("");
                $('#btnQuickOrderPad').attr('disabled', 'disabled');
            });
        });
    };
    QuickOrderPad.prototype.RemoveRow = function () {
        $(document).off("click", ".remove_row");
        $(document).on("click", ".remove_row", function () {
            var removeId = $(this).attr("id");
            var selectedRow = removeId.split('_')[1];
            QuickOrderPad.prototype.ClearSelectedData(selectedRow);
            $(this).hide();
            if (selectedRow != '1') {
                $('#form-group-' + selectedRow).remove();
            }
            var _existVal = 0;
            $('#quick-order-pad-content [data-autocomplete-url]').each(function () {
                if ($(this).val() !== "") {
                    _existVal = 1;
                }
            });
            if (_existVal === 0) {
                $('#btnQuickOrderPad').attr('disabled', 'disabled');
            }
        });
    };
    QuickOrderPad.prototype.ShowRemoveItemBox = function () {
        $('#quick-order-pad-content [data-autocomplete-url]').on("focusout", function () {
            var removeId = $(this).attr("id");
            var selectedRow = removeId.split('_')[1];
            $('#txtQuickOrderPadQuantity_' + selectedRow + '').val('1');
            if ($(this).val() != "") {
                $("#removeRow_" + selectedRow).show();
            }
            else {
                QuickOrderPad.prototype.ClearSelectedData(selectedRow);
                $("#removeRow_" + selectedRow).hide();
                var _existVal = 0;
                $('#quick-order-pad-content [data-autocomplete-url]').each(function () {
                    if ($(this).val() !== "") {
                        _existVal = 1;
                    }
                });
                if (_existVal === 0) {
                    $('#btnQuickOrderPad').attr('disabled', 'disabled');
                }
            }
        });
    };
    QuickOrderPad.prototype.ClearSelectedData = function (selectedRow) {
        $('#txtQuickOrderPadSku_' + selectedRow).val("");
        $('#txtQuickOrderPadQuantity_' + selectedRow).val('1');
        $('#inventoryMessage_' + selectedRow).html("");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_sku", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_skuId", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_product_id", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_product_name", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_cart_quantity", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_quantity_on_hand", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_in_stock_message", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_out_stock_message", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_min_quantity", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_max_quantity", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_call_for_pricing", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_is_first", "true");
    };
    QuickOrderPad.prototype.AddMultipleOrdersToCart = function () {
        var cartItems = [];
        var isSuccess = true;
        $('#btnQuickOrderPad').on("click", function () {
            cartItems.length = 0;
            var index = 0;
            isSuccess = true;
            $('#quick-order-pad-content [data-autocomplete-url]').each(function () {
                var track_inventory = $(this).attr("data_qo_track_inventory");
                var quantity_on_hand = $(this).attr("data_qo_quantity_on_hand");
                var call_for_pricing = $(this).attr("data_qo_call_for_pricing");
                var product_type = $(this).attr("data_qo_product_type");
                var retail_price = $(this).attr("data_qo_retail_price");
                var inventorySettingQuantity = $(this).attr("data_qo_quantity_on_hand");
                var autoAddOnProductSkus = $(this).attr("data_qo_autoaddonskus");
                var inventoryCode = $(this).attr("data_qo_inventorycode");
                var isObsolete = $(this).attr("data_qo_isobsolete");
                index++;
                $('p[for="' + this.id + '"]').html("");
                if (!isNaN(parseInt($(this).attr("data_qo_product_id"))) && $(this).attr("data_qo_product_id") != "") {
                    var groupProductQty = void 0;
                    var groupProductSKUs = $(this).attr("data_qo_group_product_sku");
                    var configurableProductSKUs = $(this).attr("data_qo_configurable_product_sku");
                    if (groupProductSKUs != undefined) {
                        groupProductQty = new Array(groupProductSKUs.split(",").length + 1).join($('input[parentcontrol=' + this.id + ']').val() + "_").replace(/\_$/, '');
                    }
                    var quantity = parseFloat($("input[parentcontrol=" + this.id + "]").val());
                    if ($(this).attr("data_is_first") == "false") {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorValidSKU"));
                        isSuccess = false;
                        return false;
                    }
                    if ($(this).val() != $(this).attr("data_qo_sku")) {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorValidSKU"));
                        isSuccess = false;
                        return false;
                    }
                    if ((quantity % 1) != 0 || (quantity) <= 0) {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorValidQuantity"));
                        isSuccess = false;
                        return false;
                    }
                    if (isNaN(quantity) || quantity.toString() == "") {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorWholeNumber"));
                        isSuccess = false;
                        return false;
                    }
                    if (call_for_pricing == "true") {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("CallForPricing"));
                        isSuccess = false;
                        return false;
                    }
                    if ((track_inventory == "DisablePurchasing")) {
                        if (parseInt(quantity_on_hand) <= 0) {
                            $('p[for="' + this.id + '"]').html($(this).attr("data_qo_out_stock_message"));
                            isSuccess = false;
                            return false;
                        }
                    }
                    if (parseFloat($(this).attr("data_qo_max_quantity")) < quantity + parseFloat($(this).attr("data_qo_cart_quantity"))) {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectedQuantityExceedsMaxCartQuantity"));
                        isSuccess = false;
                        return false;
                    }
                    if (parseFloat($(this).attr("data_qo_min_quantity")) > quantity) {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectedQuantityLessThanMinSpecifiedQuantity"));
                        isSuccess = false;
                        return false;
                    }
                    if ((track_inventory == "DisablePurchasing")) {
                        if (parseInt(quantity_on_hand) == parseInt($(this).attr("data_qo_cart_quantity"))) {
                            $('p[for="' + this.id + '"]').html($(this).attr("data_qo_out_stock_message"));
                            isSuccess = false;
                            return false;
                        }
                    }
                    if ((track_inventory == "DisablePurchasing")) {
                        if ((quantity + parseInt($(this).attr("data_qo_cart_quantity"))) > parseInt($(this).attr("data_qo_quantity_on_hand"))) {
                            $('p[for="' + this.id + '"]').html("Only " + (parseInt(quantity_on_hand) - parseInt($(this).attr("data_qo_cart_quantity"))) + " quantity are available for Add to cart/Shipping");
                            isSuccess = false;
                            return false;
                        }
                    }
                    if ((retail_price == "" || retail_price == undefined) && (groupProductSKUs === undefined || groupProductSKUs.trim() === '')) {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorPriceNotSet"));
                        isSuccess = false;
                        return false;
                    }
                    if (isObsolete == "true") {
                        $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ObsoleteProductErrorMessage"));
                        isSuccess = false;
                        return false;
                    }
                    if (inventoryCode != "" && inventoryCode.toLowerCase().trim() == "donttrackinventory") {
                        isSuccess = true;
                    }
                    if (inventoryCode.toLowerCase().trim() != "donttrackinventory" && (inventorySettingQuantity == "" || inventorySettingQuantity == undefined || inventorySettingQuantity == "0")) {
                        $('p[for="' + this.id + '"]').html($(this).attr("data_qo_out_stock_message"));
                        isSuccess = false;
                        return false;
                    }
                    if (isSuccess) {
                        var quickOrderModel = {
                            "ProductId": $(this).attr("data_qo_product_id"),
                            "ProductName": $(this).attr("data_qo_product_name"),
                            "Sku": $(this).attr("data_qo_sku"),
                            "Quantity": quantity,
                            "ProductType": $(this).attr("data_qo_product_type"),
                            "GroupProductSKUs": groupProductSKUs,
                            "GroupProductsQuantity": groupProductQty,
                            "ConfigurableProductSKUs": configurableProductSKUs,
                            "AutoAddonSKUs": autoAddOnProductSkus,
                            "TemplateName": $("#TemplateName").val()
                        };
                        cartItems.push(quickOrderModel);
                    }
                }
                else if ($(this).val() != "") {
                    $('p[for="' + this.id + '"]').html(ZnodeBase.prototype.getResourceByKeyName("ErrorValidSKU"));
                    $("#removeRow_" + index).show();
                    isSuccess = false;
                    return false;
                }
            });
            if (window.location.pathname.toLowerCase().indexOf("/user/createtemplate") >= 0 || window.location.pathname.toLowerCase().indexOf("/user/edittemplate") >= 0 || window.location.pathname.toLowerCase().indexOf("/user/quickorderpadtemplate") >= 0) {
                if (isSuccess) {
                    $.ajax({
                        url: "/user/addmultipleproductstocarttemplate/",
                        type: "post",
                        data: JSON.stringify(cartItems),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.isSuccess) {
                                if (data.omsTemplateId > 0)
                                    window.location.href = "/user/edittemplate?omstemplateid=" + data.omsTemplateId + "";
                                else
                                    window.location.href = "/user/createtemplate/";
                            }
                            else {
                                $('#lblNotificationMessage').addClass('error-msg');
                                $('#lblNotificationMessage').html(data.message);
                            }
                        },
                        error: function (msg) {
                        }
                    });
                }
            }
            else {
                if (isSuccess) {
                    if ($("#isEnhancedEcommerceEnabled").val() == "True") {
                        GoogleAnalytics.prototype.SendAddToCartsFromMultipleQuickOrder(cartItems);
                    }
                    $.ajax({
                        url: "/product/addmultipleproductstocart/",
                        type: "post",
                        data: JSON.stringify(cartItems),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.isSuccess) {
                                window.location.href = "/cart/index";
                            }
                            else {
                                $('#lblNotificationMessage').addClass('error-msg');
                                $('#lblNotificationMessage').html(data.message);
                            }
                        },
                        error: function (msg) {
                        }
                    });
                }
            }
        });
    };
    QuickOrderPad.prototype.SetQuantity = function () {
        $(document).off("focusout", ".quick-order-pad-quantity");
        $(document).on("focusout", ".quick-order-pad-quantity", function (ev) {
            var removeId = $(this).attr("id");
            var selectedRow = removeId.split('_')[1];
            if ($("#" + removeId + "").val() != "") {
                if ($("#" + removeId + "").val() >= 0) {
                    $("#" + removeId + "").val(parseInt($("#" + removeId + "").val()));
                }
                else {
                    $("#" + removeId + "").val($("#" + removeId + "").val().replace(/[^\d].+/, ""));
                    if ((ev.which < 48 || ev.which > 57)) {
                        $("#" + removeId + "").val(1);
                    }
                }
            }
        });
    };
    return QuickOrderPad;
}(ZnodeBase));
$(window).on("load", function () {
    var quickOrderPad = new QuickOrderPad();
    quickOrderPad.Init();
});
//# sourceMappingURL=QuickOrderPad.js.map