﻿using log4net.Config;
using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.App_Start;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.WebStore
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            BsonDefaults.GuidRepresentation = GuidRepresentation.Standard;
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(APIConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //Nivi Code
            // BundleConfig.RegisterBundles(BundleTable.Bundles);
            RSBundleConfig.RegisterBundles(BundleTable.Bundles);
            XmlConfigurator.Configure();
            AutoMapperConfig.Execute();
            //Nivi Code
            RSAutoMapperConfig.Execute();
            StartUpTasks.RegisterDependencies();
            //To register the custom ActionSessionStateAttribute 
            ControllerBuilder.Current.SetControllerFactory(typeof(SessionStateBehaviorControllerFactory));
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new ThemedViewEngine());
            MvcHandler.DisableMvcResponseHeader = true;

            bool minifiedJsonResponse = ZnodeWebstoreSettings.MinifiedJsonResponse;

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DefaultValueHandling = minifiedJsonResponse ? DefaultValueHandling.Ignore : DefaultValueHandling.Include
            };

            AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
            // at least for dev and QA, trust any certificate
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
            // sets the default representation to be used in serialization of Guids to Standard
            BsonDefaults.GuidRepresentation = GuidRepresentation.Standard;
        }

        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");           //Remove Server Header   
            Response.Headers.Remove("X-AspNet-Version"); //Remove X-AspNet-Version Header
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            string currentUrl = $"~{Request.RawUrl.ToLower()}";

            UrlRedirectViewModel redirectUrl = UrlRedirectAgent.Has301Redirect(currentUrl);
            if (HelperUtility.IsNotNull(redirectUrl))
                Context.Response.RedirectPermanent($"~/{redirectUrl.RedirectTo.Replace($"http://{HttpContext.Current.Request.Url.Authority}/", string.Empty)}");
        }

        //Redirect to cart page if user added any cart item from from browser.
        private void Session_Start(object sender, EventArgs e)
        {
          IWebstoreHelper sessionStartHelper = GetService<IWebstoreHelper>();
            sessionStartHelper.Session_Start(sender, e);
        }
        //Create cookies for client localization.
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            CookieHelper.SetCookie("culture", CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
            SetGlobalLoggingSetting();
        }

        //Application Error.
        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = null;
            exception = Server.GetLastError().GetBaseException();
            try
            {
                if (exception.GetType() == typeof(ZnodeException))
                {
                    Response.Clear();
                    switch ((exception as ZnodeException).ErrorCode)
                    {
                        case ErrorCodes.StoreNotPublished:
                            Response.ContentType = "text/html;charset=UTF-8";
                            Response.Write(WebStore_Resources.ErrorStoreNotPublished);
                            break;
                        case ErrorCodes.InvalidElasticSearchConfiguration:
                        case ErrorCodes.InvalidSqlConfiguration:
                        case ErrorCodes.InvalidZnodeLicense:
                        case ErrorCodes.InvalidDomainConfiguration:
                            Response.ContentType = "text/html;charset=UTF-8";
                            Response.Write($"<p style=\"text-align:center;font-size:30;\">{ exception.Message }</p>");
                            break;
                        default:
                            Response.ContentType = "text/html;charset=UTF-8";
                            Response.Write(WebStore_Resources.GenericConfigurationError);
                            break;
                    }
                    Server.ClearError();
                }
                else
                {
                    RedirectToErrorPage(sender, exception);
                }
                LogMessage(exception);
            }
            catch (Exception ex) { LogMessage(ex); } 
        }

        private void RedirectToErrorPage(Object sender, Exception exception)
        {
            var controller = new ErrorPageController();
            var httpContext = ((MvcApplication)sender).Context;

            var routeData = new RouteData();
            httpContext.ClearError();
            httpContext.Response.Clear();

            SetErrorPage(exception, routeData);

            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
          
        }

        private void SetErrorPage(Exception exception, RouteData routeData)
        {
            routeData.Values.Add("controller", "ErrorPage");
            routeData.Values.Add("action", "PageNotFound");
            routeData.Values.Add("exception", exception);

            if (exception.GetType().Equals(typeof(HttpException)))
            {

                Response.StatusCode = ((HttpException)exception).GetHttpCode();
                routeData.Values.Add("statusCode", ((HttpException)exception).GetHttpCode());
            }
            else
            {
                Response.StatusCode = 500;
                routeData.Values.Add("statusCode", 500);
            }

            Response.TrySkipIisCustomErrors = true;
        }

        //Log error message.
        private void LogMessage(Exception exception)
        {
               ZnodeLogging.LogMessage(exception, "WebstoreApplicationError", TraceLevel.Error);
        }
        //Set Global Logging Settings.
        private void SetGlobalLoggingSetting()
        {
            //Store Global Default Logging Setting in cache
            if (HttpRuntime.Cache["DefaultLoggingConfigCache"] == null)
                ZnodeDependencyResolver.GetService<IPortalAgent>()?.SetGlobalLoggingSetting();
        }
    }
}
