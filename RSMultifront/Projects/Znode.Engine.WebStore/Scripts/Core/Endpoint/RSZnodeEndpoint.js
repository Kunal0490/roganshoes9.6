var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var RSZnodeEndpoint = /** @class */ (function (_super) {
    __extends(RSZnodeEndpoint, _super);
    function RSZnodeEndpoint() {
        return _super.call(this) || this;
    }
    /*Nivi code Start*/
    RSZnodeEndpoint.prototype.GetMapURLSuccess = function (state, sku, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetStoresForStoreLocatorSuccess = function (state, sku, _userCord, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.SaveinCookieSuccess = function (selectedstoreid, storename, storeaddress, callbackMethod) {
        //alert("SaveinCookieSuccess " + storename + ", " + storeaddress + ", " + selectedstoreid);
        _super.prototype.ajaxRequest.call(this, "/rshome/saveincookie/", Constant.Post, { "storeId": selectedstoreid, "storeName": storename, "storeAddress": storeaddress }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.RemoveStoreDetailsForCartOnShipClickSuccess = function (productId, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, "/rscart/UpdateDeliveryPreferenceToShip/", Constant.Post, { "productId": productId }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.AssignStoreDetailsForCartSuccess = function (selectedstoreid, storename, storeaddress, productId, callbackMethod) {
        // alert("selectedstoreid endpoint" + selectedstoreid)
        _super.prototype.ajaxRequest.call(this, "/rscart/UpdateDeliveryPreferenceToPickUp/", Constant.Post, { "storeId": selectedstoreid, "storeName": storename, "storeAddress": storeaddress, "productId": productId }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.ClickCartSuccess = function (selectedstoreid, storename, storeaddress, productId, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, "/rscart/UpdateDeliveryPreferenceToPickUp/", Constant.Post, { "storeId": selectedstoreid, "storeName": storename, "storeAddress": storeaddress, "productId": productId }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetStoresForCartSuccess = function (state, sku, _userCord, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetStockForSelectedStoreSuccess = function (state, sku, selectedstore, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetStoresSuccess = function (state, sku, _userCord, callbackMethod) {
        //   alert("endpoint");
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetStockForSelectedStoreClickCartSuccess = function (state, sku, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetStockForSelectedStoreCartSuccess = function (state, sku, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetStockForCheckoutSuccess = function (state, sku, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.GetCurrentSelectedStoreDetailsSuccess = function (state, sku, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.FillLocationDropdownsuccess = function (sku, callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/ALL/" + sku, Constant.GET, { "sku": sku }, callbackMethod, "json");
    };
    RSZnodeEndpoint.prototype.FillBrandsDropdownSuccess = function (callbackMethod) {
        _super.prototype.ajaxRequest.call(this, $("#apiURL").val() + "/Brand/List", Constant.GET, {}, callbackMethod, "json");
    };
    return RSZnodeEndpoint;
}(ZnodeBase));
//# sourceMappingURL=RSZnodeEndpoint.js.map