﻿class CaseRequest extends ZnodeBase {
    constructor() {
        super();
    }

    Init() {
        CaseRequest.prototype.ValidationForContactUsForm();
        CaseRequest.prototype.ValidationForCustomerFeedbackForm();
        //Nivi Code start
        CaseRequest.prototype.ValidationForShoeFinderForm();
        CaseRequest.prototype.ValidationForScheduleATruckForm();
        CaseRequest.prototype.ValidationForRegisterForm();
        //Nivi Code ENd
    }

    //Set validation for inputs
    ValidationForContactUsForm(): any {
        $("#contact-us").on("click", function () {
            var flag: boolean = true;
            /*NIVI CODE START*/
            //Set required field for first name
            var firstName: string = $("#valFirstName1").val();
            if (firstName.length < 1) {
                $("#valFirstNameErr1").html(ZnodeBase.prototype.getResourceByKeyName("RequiredFirstName"));
                flag = false;
            }
            else {
                $("#valFirstNameErr1").html("");
            }

            //Set required field for last name
            var lastName: string = $("#valLastName2").val();
            if (lastName.length < 1) {
                $("#valLastNameErr2").html(ZnodeBase.prototype.getResourceByKeyName("RequiredLastName"));
                flag = false;
            }
            else {
                $("#valLastNameErr2").html("");
            }
            /*NIVI CODE END*/
            //Set required field for comment
            /*Original Code */
            //var comment: string = $("#valComment").val();
            //if (comment.length < 1) {
            //    $("#valCommentErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredComment"));
            //    flag = false;
            //}
            //else {
            //    $("#valCommentErr").html("");
            //}

            ////Validate phone number
            //var phoneNum: string = $("#valPhoneNum").val();
            //if (phoneNum.length < 1) {
            //    $("#valPhoneNumErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredPhoneNumber"));
            //    flag = false;
            //}

            //Validate email address
            var email: string = $("#valEmail").val();
            if (email.length < 1) {
                $("#valEmailErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredEmailId"));
                flag = false;
            }
            else {
                $("#valEmailErr").html("");
                var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    $("#valEmailErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                    flag = false;
                }
            }
            return flag;
        });
    }

    //Set validation for Customer Feedback Form
    ValidationForCustomerFeedbackForm(): any {
        $("#customer-feedback").on("click", function () {
            var flag: boolean = true;
            var FirstName: string = $("#FirstName").val();
            if (FirstName.length < 1) {
                $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredFirstName"));
                flag = false;
            }
            var LastName: string = $("#LastName").val();
            if (LastName.length < 1) {
                $("#valLastNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredLastName"));
                flag = false;
            }
            //Validate email address
            var email: string = $("#valEmailAddress").val();
            if (email.length < 1) {
                $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredEmailId"));
                flag = false;
            }
            else {
                $("#valEmailAddressErr").html("");
                var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                    flag = false;
                }
            }
            return flag;
        });
    }
    //Nivi Code
    ValidationForShoeFinderForm(): any {
        var flag = true;
        $("#valFirstNameErr").html("");
        $("#valLastNameErr").html("");
        $("#valEmailErr").html("");
        var FirstName = $("#valFirstName").val();
        if (FirstName.length < 1) {
            $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterFirstName"));
            flag = false;
        }
        var LastName = $("#valLastName").val();
        if (LastName.length < 1) {
            $("#valLastNameErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterLastName"));
            flag = false;
        }
        //Validate email address
        var email = $("#valEmail").val();
        if (email.length < 1) {
            $("#valEmailErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredEmailId"));
            flag = false;
        }
        else {
            $("#valEmailErr").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $("#valEmailErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                flag = false;
            }
        }
        return flag;
    }

    //Nivi Code
    ValidationForScheduleATruckForm(): any {

        var flag: boolean = true;
        var CompanyName = $("#idCompanyName").val();
        if (CompanyName.length < 1) {
            $("#idCompanyNameErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterCompanyName"));
            flag = false;
        }
        else { $("#idCompanyNameErr").html(''); }
        var Address = $("#valueAddres").val();
        if (Address.length < 1) {
            $("#AddressErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterAddress"));
            flag = false;
        } else { $("#AddressErr").html(''); }
        var PhoneNumber = $("#valPhoneNum").val();
        if (PhoneNumber.length < 1) {
            $("#valPhoneNumErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterPhoneNumber"));
            flag = false;
        } else { $("#valPhoneNumErr").html(''); }
        var FirstName = $("#valFirstName").val();
        if (FirstName.length < 1) {
            $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterContactName"));
            flag = false;
        } else { $("#valFirstNameErr").html(''); }
        var NoOfEmployee = $("#NumberEmployee").val();
        if (NoOfEmployee.length < 1) {
            $("#ErrNumberEmployee").html(ZnodeBase.prototype.getResourceByKeyName("NumberOfEmployeesRrequired"));
            flag = false;
        } else { $("#ErrNumberEmployee").html(''); }
        var SteelRequired = $("#valSteelRequired").val();
        if (SteelRequired.length < 1) {
            $("#ErrorSteelRequired").html(ZnodeBase.prototype.getResourceByKeyName("FootwearRequirments"));
            flag = false;
        } else { $("#ErrorSteelRequired").html(''); }
        var PosterNeeded = $("#ValPosterNeeded").val();
        if (PosterNeeded.length < 1) {
            $("#ErrPosterNeed").html(ZnodeBase.prototype.getResourceByKeyName("PosterNeeded"));
            flag = false;
        } else { $("#ErrPosterNeed").html(''); }
        //Validate email address  
        $("#ErrEmailVal").html('');
        var email = $("#valEmail").val();
        if (email.length < 1) {
            $("#ErrEmailVal").html(ZnodeBase.prototype.getResourceByKeyName("ValidEmailRequired"));
            flag = false;
        }
        else {
            $("#ErrEmailVal").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $("#ErrEmailVal").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                flag = false;
            }
        }
        return flag;
    }

    //Nivi Code
    ValidationForRegisterForm(): any {
        $("#valFirstNameErr").html('');
        $("#valLastNameErr").html('');
        $("#ErrEmailAsUsernm").html('');
        $("#ErrPassword").html('');
        $("#ErrConfirmPwd").html('');
        var flag = true;
        var FirstName = $("#valFirstName").val();
        if (FirstName.length < 1) {
            $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterFirstName"));
            flag = false;
        }
        var LastName = $("#valLastName").val();
        if (LastName.length < 1) {
            $("#valLastNameErr").html(ZnodeBase.prototype.getResourceByKeyName("EnterLastName"));
            flag = false;
        }
        var EmailAsUsername = $("#ValEmailAsUsernm").val();
        if (EmailAsUsername.length < 1) {
            $("#ErrEmailAsUsernm").html(ZnodeBase.prototype.getResourceByKeyName("ValidEmailRequired"));
            flag = false;
        }
        else {
            $("#ErrEmailAsUsernm").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(EmailAsUsername)) {
                $("#ErrEmailAsUsernm").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                flag = false;
            }
        }
        var Password = $("#ValPassword").val();
        if (Password.length < 1) {
            $("#ErrPassword").html(ZnodeBase.prototype.getResourceByKeyName("RequiredPassword"));
            flag = false;
        }
        else {
            $("#ErrPassword").html("");
            var regex = /^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$/;
            if (!regex.test(Password)) {
                $("#ErrPassword").html(ZnodeBase.prototype.getResourceByKeyName("ValidatePassword"));
                flag = false;
            }
        }
        var ConfirmPassword = $("#ValConfirmPwd").val();
        if (ConfirmPassword.length < 1) {
            $("#ErrConfirmPwd").html(ZnodeBase.prototype.getResourceByKeyName("RequiredConfirmPassword"));
            flag = false;
        }
        if (Password != ConfirmPassword) {
            $("#ErrConfirmPwd").html(ZnodeBase.prototype.getResourceByKeyName("ValidatePasswordMatch"));
            return false;
        }
        //var email = $("#valEmail").val();
        //if (email.length < 1) {
        //    $("#ErrEmailVal").html(ZnodeBase.prototype.getResourceByKeyName("ValidEmailRequired"));
        //    flag = false;
        //}
        //else {
        //    $("#ErrEmailVal").html("");
        //    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        //    if (!regex.test(email)) {
        //        $("#ErrEmailVal").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
        //        flag = false;
        //    }
        //}
        //catch (err) {
        //    console.log(err.message);
        //}
        return flag;
        // });
    };
}


