﻿ //<reference path="../../views/themes/rogansshoes/scripts/lib/googlemap.js"/>
//<reference path="../typings/google.maps.d.ts"/>
var xhr = new XMLHttpRequest();
var calledfrom = '';
var storeId = '';
var productId = '';

class InStorePickUp extends ZnodeBase {
    constructor() {
        super();
    }

    /*Called from PDP,CART and STORELocator Use My LOCation Button CLick*/
    GetLocation(c) {
        try {
            var ipaddress = $("#hdnIPAddress").val();
            console.log("IPaddress: " + ipaddress);
            calledfrom = c;
            xhr.onreadystatechange = this.processRequest;
            //xhr.open('GET', "//ipinfo.io/json", true);
            xhr.open('GET', ipaddress, true);
            xhr.send();
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*Called from GetLocation()*/
    processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            $("#responseerror").text("");
            var response = JSON.parse(xhr.responseText);
            var sku = $("#sku").val();
            $("#storedetails").html('');
            /*StoreLocator Case*/
            if (typeof sku === 'undefined') {
                sku = '0';
                $("#storedetails").append("<br/>");
            }
            else/*PDP Case*/ {
                $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding margin-top'><h1>" + "Select a Store (" + response.region + ")</h1><br/></div>");
            }
            $("#storedetails").show();
            var sku = $("#sku").val();

            var array = response.loc.split(",");
            var _userCord = new google.maps.LatLng(array[0], array[1]);
            var response = JSON.parse(xhr.responseText);
            if (calledfrom == 'pdp') {
                InStorePickUp.prototype.GetStores(response.region, sku, _userCord);
            }
            else if (calledfrom == 'cart') {
                sku = $("#UseMyCurrentLocation").attr("data-sku");
                InStorePickUp.prototype.GetStoresForCart(response.region, sku, _userCord);
            }
            else if (calledfrom == 'storelocator') {
                InStorePickUp.prototype.GetStoresForStoreLocator(response.region, sku, _userCord);
            }


        }
        else {
            //  alert("xhr.status=" + xhr.status);
            //var sku = $("#sku").val();
            //var a = Number("21.1500");
            //var b = Number("79.1000");
            //var _userCord = new google.maps.LatLng(a, b);
           // var _userCord = new google.maps.LatLng(array[0], array[1]);
            //alert(_userCord);
            //InStorePickUp.prototype.GetStores("Maharashtra", sku, _userCord);
            //alert("calledfrom-" + calledfrom);
            //if (calledfrom == 'pdp') {
            //    InStorePickUp.prototype.GetStores("Maharashtra", sku, _userCord);
            //}
            //else if (calledfrom == 'cart') {
            //    InStorePickUp.prototype.GetStoresForCart("Maharashtra", sku, _userCord);
            //}
            //else if (calledfrom == 'storelocator') {
            //    InStorePickUp.prototype.GetStoresForStoreLocator("Maharashtra", sku, _userCord);
            //}---
            /*****/
           /* var sku = $("#sku").val();
            var a = Number("21.1500");
            var b = Number("79.1000");
            var _userCord = new google.maps.LatLng(a, b);*/
             /*****/
            //var _userCord = new google.maps.LatLng(array[0], array[1]);
            //alert(_userCord);
            // GetStores("Maharashtra", sku, _userCord);
            //alert("calledfrom-" + calledfrom);
           /* if (calledfrom == 'pdp') {
                
                InStorePickUp.prototype.GetStores("Maharashtra", sku, _userCord);
            }
            else if (calledfrom == 'cart') {
                sku = $("#UseMyCurrentLocation").attr("data-sku");
                InStorePickUp.prototype.GetStoresForCart("Maharashtra", sku, _userCord);
            }
            else if (calledfrom == 'storelocator') {
                InStorePickUp.prototype.GetStoresForStoreLocator("Maharashtra", sku, _userCord);
            }*/
            $("#responseerror").text("It seems currently this service is down, but no problem You can always search for nearest stores by entering your address.")
            return false;
          
        }

    }

    /*******************PDP/STORE LOCATOR METHODS START*****************************/
    /*Called from processRequest(e) for both PDP and Store Locator*/
    GetStores(state, sku, _userCord) {
        if (sku != '0') {
            sku = $("#dynamic-configurableproductskus").val();
        }
        try {
            RSZnodeEndpoint.prototype.GetStoresSuccess(state, sku, _userCord, function (data) {
                
                $("#loaderId").html("");
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {

                            //if (json[j].Quantity > 0) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;
                            // }

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            //if (json[j].Quantity > 0) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];

                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + storenamedetail + ">");
                            if (sku == '0') {
                                /*STORE LOCATOR CASE*/
                                $("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + storenamedetail + "</h4></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><button id=" + address['WarehouseId'] + " onclick=' return InStorePickUp.prototype.ShowPopUpSL(this,\"" + storenamedetail + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store'>Make this My Store</button></div>" + "</div>");
                            }
                            else {
                                /*PDP CASE*/
                                //var status = "AVAILABLE(" + address['Quantity']+")";
                                var status = "<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + address['Quantity'] + " left</span>";
                                if (address['Quantity'] == 0) {
                                    status = "Out of stock";
                                    //$("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4><label style='color:darkred;font-weight:bold;'>" + status + "</label></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><label id=" + address['WarehouseId'] + " data-qty=" + address['Quantity'] + " class='btn btn-danger' data-dismiss='modal' aria-hidden='true'>Not Available</label></div>" + "</div>");
                                    $("#storedetails").append("<div class='col-lg-6 col-xs-9 col-md-9 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + " <span class= 'error-msg'> " + status + "</span>" + "</div>");
                                }
                                else {
                                    //var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                                    $("#storedetails").append("<div class='col-lg-6 col-xs-9 col-md-9 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + "<span class='form-group'>" + status + "</span>" + "<div><button id=" + address['WarehouseId'] + " data-qty=" + address['Quantity'] + " onclick='InStorePickUp.prototype.Click(this,\"" + storenamedetail + "\",\"" + addressdetail + "\",\"" + address['Quantity'] + "\");' class='btn btn-primary popup-btnpickup' data-dismiss='modal' aria-hidden='true' style='text-transform: none;'>Pick Up here</button></div>" + "</div>");
                                }
                            }
                            $("#storedetails").append("<div class='col-lg-6 col-xs-3 col-md-3 nopadding text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> Miles Away" + "</div>");
                            $("#storedetails").append("</div><hr>");
                            $("#storedetails").show();
                            // }

                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*******************PDP/STORE LOCATOR METHODS End******************/

    /*******************PDP METHODS Start*****************************/

    /*Called from PDP on Make This My Store Click.
    Custom values are taken from $("#SelectedWarehouseId"),$("#SelectedWarehouseName"),$("#SelectedWarehouseAddress") and assigned in SetCartItemModelValuesmethod of Product.ts
    to be passed on to cart*/
    Click(obj, storename, storeaddress, stock) {
        try {
            
            var selectedstoreid = $(obj).attr('id');
            $("#headerstoreid").val(selectedstoreid);
            $("#headerstorename").val(storename);
            $("#headerstoreaddress").val(storeaddress);

            $("#SelectedWarehouseId").val(selectedstoreid);
            $("#SelectedWarehouseName").val(storename);
            //alert("Click: "+$("#SelectedWarehouseName").val(storename));
            $("#SelectedWarehouseAddress").val(storeaddress);
            $("#SelectedWarehouseStock").val(stock);

            $("#defaultstorename").html('');
            $("#defaultstorename").append("<b class='storename'>" + storename + "</b><p style='padding-left:20px;'><span id='status' style='display:none;' class='error-msg pdp-error-msg'>(NOT AVAILABLE)</span></p> <span class='select-store Tablet-view-changestore'><a style='text-decoration: underline;' onclick='InStorePickUp.prototype.ChangeStore();' href='JavaScript:Void(0);' title='Change Store'>Change Store</a>&nbsp;<span class='text-danger Tablet-view-changestore-redtxt' style='display:none;' id='showmsgpickup'>or select the ship option</span></span>");
            $('.select-store').show();

            $("#Pickup").prop('checked', true);
            $("#status").html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + stock + " left</span>");
            $("#status").show();

            $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
            /*newCall */
            var productType = $("#IsCallForPricing").val();           
            if (productType != "True") {
                $('[data-test-selector="btnAddToCart"]').prop("disabled", false);
            }
            InStorePickUp.prototype.SaveinCookie(selectedstoreid, storename, storeaddress);
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*Called from PDP on clicking Pick up in store Radio Button*/
    AssignStoreDetails(selectedstoreid, storename, storeaddress) {
        /*new*/
        $("#InstoreWarning").show();
        var IsSKUSlected = InStorePickUp.prototype.VerifySKU();
        if (IsSKUSlected == true) {

            $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
            InStorePickUp.prototype.GetStockForSelectedStore('pdp');
            $("#SelectedWarehouseId").val(selectedstoreid);
            $("#SelectedWarehouseName").val(storename);
            $("#SelectedWarehouseAddress").val(storeaddress);
        }

    }

    /*Called on  $('#btnGetStock').click(); in Product.ts whenever Color,Size or width is changed in PDP */
    ResetSKUAndStock() {
        var IsSKUSlected = InStorePickUp.prototype.VerifySKU();
        if (IsSKUSlected == true) {

            $("#loaderId").html("<div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
           // alert($("#loaderId").html());
            InStorePickUp.prototype.GetStockForSelectedStore('pdp');
        }

    }
    /*Called from PDP on clicking Ship Radio Button*/
    RemoveStoreDetails() {
        $("#SelectedWarehouseId").val('');
        $("#SelectedWarehouseAddress").val('');
        $("#SelectedWarehouseStock").val('');
        /*newCall */
        var productType = $("#IsCallForPricing").val();
        if (productType != "True") {
            $('[data-test-selector="btnAddToCart"]').prop("disabled", false);
        }
        $('#status').hide();
        /*new*/
        $("#InstoreWarning").show();
    }

    FitGuide(fitguideurl) {
        window.open(fitguideurl, "_blank", "toolbar=yes,width=1000,height=700");
    }

    /*Called from Add To cart button for mobile view */
    RedirectToCart() {
        var webstoreURL = $("#WebstoreURL").val();

        window.location.href = webstoreURL + "/cart";
    }

    /*Called internally from PickUpinStore.js to check SKU (color-size-width combination) is selected before starting Pick Up. */
    VerifySKU() {

        var IsAllAttributesSelected = "True";
        var IsProductWithWidth = $("#HaveWidth").val();
        var IsProductWithSize = $("#HaveSize").val();
        var json = $('#AssociatedProducts').val();

        ///*SWATCH/DDL check*/
        var Color = $("input[name='Color']:checked").val();

        var Size = "";
        var Width = "";
        var LayoutType = $('#LayoutType').val();

        if (LayoutType == 'Swatch') {
            Size = $("input[name='Size']:checked").val();
            Width = $("input[name='ShoeWidth']:checked").val();
        }
        else {
            Size = $('#Size :selected').text();
            Width = $('#ShoeWidth :selected').text();
        }
        if (IsProductWithWidth == "True") {
            if ((Size == "Select" || typeof (Size) == "undefined")) {
                $("#SizeError").show();
                IsAllAttributesSelected = "False";
            }
            else {
                $("#SizeError").hide();
            }

            if (Width == "Select" || typeof (Width) == "undefined") {
                $("#WidthError").show();
                IsAllAttributesSelected = "False";
            }
            else {
                $("#WidthError").hide();
            }
        }
        else if (IsProductWithSize == "True") {
            if ((Size == "Select" || typeof (Size) == "undefined")) {
                $("#SizeError").show();
                IsAllAttributesSelected = "False";
            }
            else {
                $("#SizeError").hide();
            }
        }
        if (IsAllAttributesSelected == "True") {
            $("#SizeError").hide();
            $("#WidthError").hide();
            var Sku = "";
            var iflag = 0;
            var columns = ['PublishProductId', 'SKU', 'OMSColorValue', 'Custom1', 'Custom2'];
            var result = JSON.parse(json).map(function (obj) {
                return columns.map(function (key) {
                    return obj[key];
                });
            });
            result.unshift(columns);
            for (var j = 0; j < result.length; j++) {

                if (IsProductWithSize == "False" && IsProductWithSize == "False") {
                    if (result[j][2] == Color) {
                        iflag = 1;
                        Sku = result[j][1];
                    }
                }
                else if (IsProductWithSize == "False") {
                    if (result[j][2] == Color) {
                        iflag = 1;
                        Sku = result[j][1];
                    }
                }
                else if (IsProductWithWidth == "False") {
                    if (result[j][2] == Color && result[j][3] == Size) {
                        iflag = 1;
                        Sku = result[j][1];
                    }
                }
                else if (result[j][2] == Color && result[j][3] == Size && result[j][4] == Width) {
                    iflag = 1;
                    Sku = result[j][1];
                }

            }
            if (iflag == 1) {
                $("#dynamic-configurableproductskus").val(Sku);
                return true;
            }
            else {
                $("#CombinationErrorMessage").html('This product is not available.');
                return false;
            }
        }
        else {
            $("#Ship").prop('checked', true);
            return false;
        }

    }

    ShowPopUp() {
        $("#InstoreWarning").show();
        var IsSKUSlected = InStorePickUp.prototype.VerifySKU();
       
        var defaultstorenamehtml = $(".storename").text();
        if (IsSKUSlected == true) {
            //if (typeof ($(".storename").val()) == "undefined") {
           // alert("defaultstorenamehtml=" + defaultstorenamehtml);
            if (defaultstorenamehtml == "") {
                $("#Ship").prop('checked', true);
                $("#storedetails").html('');
                $('#btnTrigger').click();
            }
            else {
                var IsSKUSlected = InStorePickUp.prototype.VerifySKU();
                if (IsSKUSlected == true) {

                    $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
                    InStorePickUp.prototype.GetStockForSelectedStore('pdp');
                }
            }

        }
    }

    /*Called on Change Store Link Click. Opens Change Store Confirmation Pop Up  */
    ChangeStore() {
        //trigger Pop Up Code
        $('#btnConfirmStorePopUp').click();
    }

    /*Called on Change Store Confirmation Pop Up's Proceed button Click. Opens Store Location pop up. */
    ShowStatus() {
        var IsSKUSlected = InStorePickUp.prototype.VerifySKU();
        if (IsSKUSlected == true) {
            InStorePickUp.prototype.GetStockForSelectedStore('pdpChangeStore');
            $("#storedetails").html('');
            $('#btnTrigger').click();
        }
    }

    /*Called internally from PickUpinStore.js to get stock of SKU in selected stores. */
    GetStockForSelectedStore(calledfrom) {
        var sku = $("#dynamic-configurableproductskus").val();

        var state = "ALL";
        var selectedstore = "";
        if (calledfrom == 'pdp') {
            selectedstore = $(".storename").text();
        }

        try {
            RSZnodeEndpoint.prototype.GetStockForSelectedStoreSuccess(state, sku, selectedstore, function (data) {
                var IsStoreActive = 0;

                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));

                        $.each(json, function (j, val) {
                            if (json[j].StoreName == selectedstore) {
                                IsStoreActive = 1;
                                $("#SelectedWarehouseStock").val(json[j].Quantity);
                                if (calledfrom == 'pdp') {
                                    if (json[j].Quantity == 0) {
                                        $('[data-test-selector="btnAddToCart"]').prop("disabled", true);
                                        $("#status").html('(NOT AVAILABLE)');
                                        $("#status").show();
                                        $("#showmsgpickup").show();
                                    }
                                    else {
                                        /*newCall */
                                        var productType = $("#IsCallForPricing").val();  
                                        if (productType != "True") {
                                            $('[data-test-selector="btnAddToCart"]').prop("disabled", false);
                                        }
                                        $("#status").html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                        $("#status").show();
                                        $("#showmsgpickup").hide();
                                    }
                                    
                                }
                            }
                        });
                    }
                });
                /*new*/
                $("#loaderId").html("");
              //  alert("selectedstore=" + selectedstore);
                /*BG*/
                if (IsStoreActive == 0 && selectedstore!="") {
                    $('[data-test-selector="btnAddToCart"]').prop("disabled", true);
                    $("#status").html('Currently this store is not available for Pick Up Please select another store');
                    $("#status").show();
                }
            });
            
            
        }
        catch (err) {
            console.log(err.message);
        }
    }
    /*******************PDP METHODS END*****************************/


    /*******************CART METHODS START*****************************/

    /*CART- called from processRequest(e) and GetLatLang()*/
    GetStoresForCart(state, sku, _userCord) {
        try {
            RSZnodeEndpoint.prototype.GetStoresForCartSuccess(state, sku, _userCord, function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $("#storedetails").html('');
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + storenamedetail + ">");

                            var status = "<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + address['Quantity'] + " left</span>";
                            if (address['Quantity'] == 0) {
                                status = "Out of stock";
                                $("#storedetails").append("<div class='col-lg-8 col-xs-9 col-md-4 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + " <span class= 'error-msg'> " + status + "</span>" + "</div>");
                            }
                            else {
                                $("#storedetails").append("<div class='col-lg-8 col-xs-9 col-md-4 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + "<span class='form-group'>" + status + "</span>" + "<div><button id='" + address['WarehouseId'] + "' data-qty=" + address['Quantity'] + " onclick='InStorePickUp.prototype.ClickCart(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\",\"" + sku + "\",\"" + address['Quantity'] + "\");' class='btn btn-primary popup-btnpickup' data-dismiss='modal' aria-hidden='true' style='text-transform: none;'>Pick Up here</button></div>" + "</div>");
                            }

                            $("#storedetails").append("<div class='col-lg-4 col-xs-3 col-md-4 text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</div>");
                            $("#storedetails").append("</div><hr/>");
                            $("#storedetails").show();

                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
               
            });
           
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*CART- Called on PickUp Here Click of Cart*/
    ClickCart(obj, storename, storeaddress, sku, stock) {
        try {
            //alert("productId:" + productId);
           // var productId = $("#ProductId").val();
            var selectedstoreid = $(obj).attr('id');
            $("[data-pickup='pickup']").each(function () {
                if ($(this).attr('data-sku') == sku) {
                    productId = $(this).attr('name');
                }
            });

            $("#headerstoreid").val(selectedstoreid);
            $("#headerstorename").val(storename);
            $("#headerstoreaddress").val(storeaddress);
           // alert("selectedstoreid=" + selectedstoreid);
            var clickedradiobutton = "#Pickup-" + productId;
            $(clickedradiobutton).prop('checked', true);

            $("#SelectedWarehouseId").val(selectedstoreid);
            $("#SelectedWarehouseName").val(storename);
            $("#SelectedWarehouseAddress").val(storeaddress);
            $(".storename").each(function () {
                $(this).html('');
                $(this).append(storename);
            });
            var availQtyErrorMsg = ("#avl_quantity_error_msg_" + productId);
            var pickupstockerror = $("#PickUpStockError").text();

            if (typeof (availQtyErrorMsg) == "undefined" || availQtyErrorMsg == "" || pickupstockerror != "") {

                $(".checkout").each(function () {
                    $(".checkout ").removeClass('disable-anchor');
                    $("#PickUpStockError").text('');
                    $("#status_" + productId).text('');
                });
            }
            
            //GetStockForSelectedStoreCart(sku, productId);
            InStorePickUp.prototype.GetStockForSelectedStoreClickCart(sku, productId);
            $("#status_" + productId).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + stock + " left</span>");
            $("#status_" + productId).show();
            // alert($("#status_" + productId).html());

            
            //alert("productId=" + productId);
            RSZnodeEndpoint.prototype.ClickCartSuccess(selectedstoreid, storename, storeaddress, productId, function (response) {

            });
            alert("ClickCart=" + storename);
            $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
            $("#hdnSelectedStoreID").val(storeId);
            $("." + storeId + ":first").prop('checked', true);
            
        }
        catch (err) {
            console.log(err.message);
        }


    }

    /*Called from Cart on clicking Pick up in store Radio Button*/
    AssignStoreDetailsForCart(selectedstoreid, storename, storeaddress, productId, sku) {
       // alert("selectedstoreid" + selectedstoreid + ",storeaddress=" + storeaddress);
        $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
        var pickupstockerror = $("#PickUpStockError").text();
        var shipoutofstockerror = $("#avl_quantity_error_msg_" + productId).text();
        if (pickupstockerror == 'Entire order must be SHIP or PICK UP in store' || shipoutofstockerror != "") {
            $(".checkout").each(function () {
                $(".checkout ").removeClass('disable-anchor');
                $("#PickUpStockError").text('');
            });
        }

        InStorePickUp.prototype.GetStockForSelectedStoreCart(sku, productId);
        $("#avl_quantity_error_msg_" + productId).html('');
        //if (selectedstoreid == null) {
        //    alert("selectedstoreid is null");
        //    selectedstoreid = '';
        //}
        //else {
        //    alert("else");
        //}
        RSZnodeEndpoint.prototype.AssignStoreDetailsForCartSuccess(selectedstoreid, storename, storeaddress, productId, function (response) {
           
        });
        
    }

    /*Called from Cart on clicking Ship Radio Button*/
    RemoveStoreDetailsForCartOnShipClick(productId) {
        $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
        var status = "status_" + productId;
        $("#" + status).hide();
        var msg = "showmsgpickup_" + productId;
        $("#" + msg).hide();
        $("#quantity_error_msg_" + productId).text('');
        $(".qty").each(function () {

            var qtyproductid = $(this).attr('data-cart-productid');
            if (productId == qtyproductid) {

                $(this).attr('data-IsPickUp', 'false');
                $(this).attr('data-inventory', '');
            }
        });
        var availQtyErrorMsg = $("#avl_quantity_error_msg_" + productId).text();
        var pickupstockerror = $("#PickUpStockError").text();
        //  alert("availQtyErrorMsg=" + availQtyErrorMsg + " ,pickupstockerror=" + pickupstockerror);
        // if ((typeof (availQtyErrorMsg) == "undefined" || availQtyErrorMsg == "" )&& pickupstockerror != "") {
        if (typeof (availQtyErrorMsg) == "undefined" || availQtyErrorMsg == "" || pickupstockerror != "") {
            $(".checkout").each(function () {
                $(".checkout ").removeClass('disable-anchor');
                $("#PickUpStockError").text('');
                $("#status_" + productId).text('');
                //$(".checkout ").addAttr('href');
            });
        }
        RSZnodeEndpoint.prototype.RemoveStoreDetailsForCartOnShipClickSuccess(productId, function (response) {
            $("#loaderId").html('');
        });
       
    }

    /*CART -(Previously GetPickUpRadio) Select Store Case, directly opens Store details Pop up*/
    //function OnCartSelectStoreClick(obj) {
    //    storeId = $(obj).attr("data-pickupTarget");
    //    productId = $(obj).attr("data-product");
    //    var sku = $(obj).attr("data-sku");
    //    $("#UseMyCurrentLocation").attr('data-sku', sku);
    //    $("#btnSubmit").attr('data-sku', sku);

    //    GetStockForSelectedStoreCart(sku, productId);
    //}

    /*CART- Change Store Case, On click opens Change Store confirmation Pop up*/
    OnCartChangeStoreClick(obj) {
        storeId = $(obj).attr("data-pickupTarget");
        //alert(storeId);
        productId = $(obj).attr("data-product");
        var sku = $(obj).attr("data-sku");
        $("#UseMyCurrentLocation").attr('data-sku', sku);
        $("#btnSubmit").attr('data-sku', sku);
        InStorePickUp.prototype.GetStockForSelectedStoreCart(sku, productId);
        //trigger Pop Up Code
        $('#btnConfirmStorePopUp').click();
    }


    /*CART- Confirmation Pop Up,Proceed button Click, Opens Store details Pop up*/
    OnConfirmationProceedClick() {
        // GetStockForSelectedStoreCart(sku, productId);   
        $("#storedetails").html('');
        $('#btnTrigger').click();
    }


    /*CART- Called internally from PickUpinStore.js to get stock of SKU in selected stores.*/
    GetStockForSelectedStoreCart(sku, productid) {

        //var sku = $("#dynamic-configurableproductskus").val();

        var state = "ALL";
        var selectedstore = $(".storename:first").text();
        try {
            RSZnodeEndpoint.prototype.GetStockForSelectedStoreCartSuccess(state, sku, function (data) {
                var IsStoreActive = 0;

                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            if (json[j].StoreName == selectedstore) {
                                IsStoreActive = 1;
                                $("#SelectedWarehouseStock").val(json[j].Quantity);
                                var status = "status_" + productid;
                                var showmsgpickup = "showmsgpickup_" + productid;
                                if (json[j].Quantity == 0) {
                                    //$("#Ship_" + productid).prop('checked', true);
                                    $(".qty").each(function () {
                                        // alert("inside .qty");
                                        var qtysku = $(this).attr('data-sku');
                                        var radioValue = $("input[id='Pickup-" + productid + "']:checked").val();
                                        if (sku == qtysku && radioValue == 2) {
                                            $(this).attr('data-IsPickUp', 'true');
                                            $("#" + status).html('Not Available');
                                            $("#" + status).show();
                                            $("#" + showmsgpickup).show();
                                        }
                                    });
                                }
                                else {
                                    $("#" + status).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                    $("#" + status).show();
                                    $("#showmsgpickup").hide();
                                    try {
                                        $(".qty").each(function () {

                                            var qtysku = $(this).attr('data-sku');
                                            var radioValue = $("input[id='Pickup-" + productid + "']:checked").val();
                                            if (sku == qtysku && radioValue == 2) {

                                                $(this).attr('data-IsPickUp', 'true');
                                                $(this).attr('data-inventory', json[j].Quantity);
                                            }
                                        });
                                    }
                                    catch (err) {
                                        console.log(err.message);
                                    }
                                }
                                

                            }
                        });
                    }
                });
                /*new*/
                $("#loaderId").html("");
                if (IsStoreActive == 0 && selectedstore != "") {
                    $(".checkout").each(function () {
                        $(".checkout ").addClass('disable-anchor');
                        $("#PickUpStockError").text('Currently ' + selectedstore + ' is not available for Pick Up Please select another store');
                        $("#PickUpStockError").show();
                    });
                }
            });
           
           
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*CART- Called from Pick Up Here Store locations Pop Up*/
    GetStockForSelectedStoreClickCart(sku, productid) {

        //var sku = $("#dynamic-configurableproductskus").val();
        var selectedStoreId = $("#SelectedWarehouseId").val();
        //alert("selectedStoreId=" + selectedStoreId);
        var state = "stock" + selectedStoreId;
        var selectedstore = $(".storename:first").text();
        sku = "";
        $("div.cart-products table tbody tr").each(function () {
            var qtyTextbox = $(this).find('input[name="Quantity"]');
            sku = sku + "," + $(qtyTextbox).attr("data-sku");
        });

        try {
            RSZnodeEndpoint.prototype.GetStockForSelectedStoreClickCartSuccess(state, sku, function (data) {
                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));

                        $.each(json, function (j, val) {
                            $("div.cart-products table tbody tr").each(function () {
                                var qtyTextbox = $(this).find('input[name="Quantity"]');
                                var productId = parseInt($(qtyTextbox).attr("data-cart-productId"));
                                var sku = $(qtyTextbox).attr("data-sku");
                                var radioValue = $("input[id='Pickup-" + productId + "']:checked").val();
                                var status = "status_" + productId;
                                var showmsgpickup = "showmsgpickup_" + productId;
                                if (radioValue == 2) {
                                    if (json[j].SKU == sku) {
                                        if (json[j].Quantity == 0) {
                                            $("#" + status).html('Not Available');
                                            $("#" + status).show();
                                            $("#" + showmsgpickup).show();
                                            $(qtyTextbox).attr('data-IsPickUp', 'true');
                                        }
                                        else {
                                            $("#" + status).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                            $("#" + status).show();
                                            $("#" + showmsgpickup).hide();
                                            $(qtyTextbox).attr('data-IsPickUp', 'true');
                                            $(qtyTextbox).attr('data-inventory', json[j].Quantity);
                                        }
                                    }
                                }
                            });
                            $("#loaderId").html("");
                        });
                    }
                });
            });
           
            /*new*/
            $("#loaderId").html("");
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*CART- Called from Pick Up click when first time customer is setting Default Store*/
    ShowPopUpCart(sku, productid) {
        var selectedstore = $(".storename:first").text();
        if (selectedstore != "") {
            this.AssignStoreDetailsForCart($("#headerstoreid").val(), selectedstore, $("#headerstoreaddress").val(), productid, sku);
        }
        else {
            $("#UseMyCurrentLocation").attr('data-sku', sku);
            $("#btnSubmit").attr('data-sku', sku);

            $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
            InStorePickUp.prototype.GetStockForSelectedStoreCart(sku, productid);
            $('#btnTrigger').click();
        }
    }

    /*CART-Called from Cart.ts EnableDisableCheckoutButton() where btnResetStock click is called in _Quantity.cshtml which calls this method*/
    ResetCartOnSubmit() {
        $(".qty").each(function () {
            var sku = $(this).attr('data-sku');
            var productId = $(this).attr('data-cart-productId');
            InStorePickUp.prototype.GetStockForSelectedStoreCart(sku, productId);
        });
    }

    /*******************CART METHODS END******************************/

    /*Called On Store Locator Make This My Store Click*/
    SaveinCookie(selectedstoreid, storename, storeaddress) {
        try {
            $("#headerstoreid").val(selectedstoreid);
            $("#headerstorename").val(storename);
            $("#headerstoreaddress").val(storeaddress);

            //alert("saveincookie " + storename + "," + storeaddress + "," + selectedstoreid);
            //var selectedstoreid = $(obj).attr('id');
           // alert("selectedstoreid" + selectedstoreid);
            $(".btn-primary-1").each(function () {
                $(this).removeClass('btn btn-primary-1');
                $(this).addClass('btn btn-primary');
                $(this).text('Make This My Store');
            });

            $("#" + selectedstoreid).removeClass('btn btn-primary');
            $("#" + selectedstoreid).addClass('btn btn-primary-1');
            $("#" + selectedstoreid).text('My Store');

            RSZnodeEndpoint.prototype.SaveinCookieSuccess(selectedstoreid, storename, storeaddress, function (response) {
               // alert("saveincookie " + storename + "," + storeaddress + "," +selectedstoreid);
                $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
               // alert("savesuccessincookie" + JSON.stringify($(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>")));
            });
            
            return false;
        }
        catch (err) {
            console.log(err.message);
            $("#" + selectedstoreid).removeClass('btn btn-primary');
            $("#" + selectedstoreid).addClass('btn btn-primary');
        }
        return false;
    }

    /*Called From Store Locator,PDP and Cart store location pop up Submit button click*/
    GetLatLng(calledfrom) {
        try {

            $("#responseerror").text("");
            var addr = $("#txtzipcitystate").val();
            if (addr == '') {
                $("#error").text("*This is a required field.")
                return false;
            }
            else {
                $("#error").text("");
            }
            var lat = '';
            var lng = '';
            var address = addr;
            $("#loaderId").html(" <div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = String(results[0].geometry.location.lat());
                    lng = String(results[0].geometry.location.lng());

                    var _userCord = new google.maps.LatLng(Number(lat), Number(lng));
                    var sku = $("#dynamic-configurableproductskus").val();
                    if (calledfrom == 'storelocator') {
                        InStorePickUp.prototype.GetStoresForStoreLocator('ALL', '0', _userCord);
                    }
                    else if (calledfrom == 'pdp') {
                        InStorePickUp.prototype.GetStores('ALL', sku, _userCord);
                    }
                    else {
                        sku = $("#btnSubmit").attr('data-sku');
                        InStorePickUp.prototype.GetStoresForCart('ALL', sku, _userCord);
                    }
                    // initMap();
                }
                else {
                    console.log("Geocode was not successful for the following reason: " + status);
                }
            });
            $("#loaderId").html("");

        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*Called from processRequest(e) and GetLatLang()*/
    GetStoresForStoreLocator(state, sku, _userCord) {

        try {
            var imagePath = $("#ImagePathURL").val();
            var webstoreUrl = $("#WebStoreUrl").val();
            var currentStore = $(".StoreLocatorLink span").html();
            RSZnodeEndpoint.prototype.GetStoresForStoreLocatorSuccess(state, sku, _userCord, function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {

                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            var storedetailshref = "";
                            if (address['seourl'] == null) {
                                storedetailshref = "#";
                            }
                            else {
                                storedetailshref = webstoreUrl + '/' + address['seourl'];
                            }
                           // alert("storedetailshref"+storedetailshref);
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];
                                var arr = address['StoreTiming'].split(',');
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                            var HtmlTags = $("#storedetails").html();
                            HtmlTags += "<div class='col-lg-12 col-md-12 nopadding storeLocationCoordinate' style='margin-bottom:20px !important' id=" + storenamedetail + " data-distance='0'  data-lng=" + address['Longitude'] + "  data-lat=" + address['Latitude'] + "  data-address=" + addr.replace(/^,/, '') + "  data-title=" + address['StoreName'] + ">"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding-mobile'><img alt='" + storenamedetail + "' title='" + storenamedetail + "' src='" + imagePath + address['ImageName'] + "'/></div>"
                            HtmlTags += "<div class='col-lg-8 col-md-8 col-sm-8 col-xs-12 nopadding'>" + "<div class='col-lg-5 col-md-5 col-sm-5 col-xs-12 nopadding-mobile'>" + "<div class='form-group'><h4 style='margin: 0px;'>" + storenamedetail + "</h4></div>"

                            HtmlTags += "<div style='margin-bottom: 15px !important;'><h4 style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>"
                            $.each(arr, function (j, val) {
                                HtmlTags += "<div class='form-group' style='line-height: 10px;'><p class='store-hours'>" + arr[j] + "</p></div>"
                            });
                            HtmlTags += "</div>"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 nopadding col-xs-12'><div class='form-group'>" + addr.replace(/^,/, '') + "</div>"
                            HtmlTags += "<div class='form-group'><p class='storeLocationCoordinate recent-order-reorder'><a href=" + address['MapLocationURL'] + " target='_blank' style='text-decoration: underline;' title='Map and Directions'>Map and Directions</a></p></div></div>"
                            HtmlTags += "<div class='col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center margin-bottom'><h1><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</h1></div>"
                            HtmlTags += "<div class='col-lg-12 col-xs-12 col-md-12 margin-top'><div class='form-inlineclass'><div>"

                            HtmlTags += "<a style='color: #fff !important;font-weight: normal;' class='btn red' href=" + storedetailshref + " target='_blank' title='Store Details'>Store Details</a></div>"
                            if (address['IsActive'] == true) {
                                if (currentStore == storenamedetail) {

                                    HtmlTags += "<div><button id=" + address['WarehouseId'] + " onclick=' return InStorePickUp.prototype.ShowPopUpSL(this,\"" + storenamedetail + "\",\"" + addressdetail + "\");' class='btn btn-primary-1' data-dismiss='modal' aria-hidden='true' value='Make this My Store' title='Make this My Store' style='margin-left: 15px;'>My Store</button></div></div></div>" + "</div>";
                                    // alert("HtmlTags store address: - " + HtmlTags);
                                }
                                else {
                                    HtmlTags += "<div><button id=" + address['WarehouseId'] + " onclick=' return InStorePickUp.prototype.ShowPopUpSL(this,\"" + storenamedetail + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store' title='Make this My Store' style='margin-left: 15px;'>Make this My Store</button></div></div></div>" + "</div>";
                                    //alert("HtmlTags store address2: - " + JSON.stringify(HtmlTags));
                                }
                            }
                            HtmlTags += "</div></div><br/>";

                            $("#storedetails").html(HtmlTags);
                           // alert("HtmlTags store address2 store details: - " + JSON.stringify($("#storedetails").html(HtmlTags)));
                            $("#storedetails").show();
                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            });
           
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*Not in use*/
    GetMapURL(storename, ctrl) {

        var state = "ALL";
        var sku = "0";
        var locationMapUrl = ""; //alert(ctrl.attr("onclick")); alert(ctrl.attr("href"));
        RSZnodeEndpoint.prototype.GetMapURLSuccess(state, sku, function (data) {
            $.each(data, function (i, item) {

                if (i == "StoreLocations") {
                    var json = JSON.parse(JSON.stringify(data[i]));
                    $.each(json, function (j, val) {
                        var address1 = JSON.stringify(json[j]);
                        var address = JSON.parse(address1);
                        var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                        if (storenamedetail == storename) {
                            locationMapUrl = address['MapLocationURL'];
                            //return window.open(locationMapUrl, '_blank');
                            window.location.href = locationMapUrl;
                            //locationMapUrl=locationMapUrl.replace("'", "");
                            //window.open(locationMapUrl, '_blank');
                            // alert("In");
                            // $(ctrl).closest("lnkMapDirection").attr("href", locationMapUrl);
                            // $(this).closest("lnkMapDirection").click();
                        }
                        //alert(address['StoreName']);
                    });
                }
            });
        });
       
    }

    ShowPopUpSL(obj, storename, storeaddress) {
        try {

            //  alert("ShowPopUpSL called storename=" + storename + ",storeaddress=" + storeaddress);
            $('#btnProceed').attr('data-storeid', $(obj).attr('id'));
            $('#btnProceed').attr('data-storename', storename);
          //  alert("showpopupsl - storename"+$('#btnProceed').attr('data-storename', storename));
            $('#btnProceed').attr('data-storeaddress', storeaddress);
            // alert("btnProceed=" + $(obj).attr('id') + "," + $('#btnProceed').attr('data-storename') + "," + $('#btnProceed').attr('data-storeaddress'));

            $('#btnConfirmStorePopUp').click();
            return false;
        }
        catch (err) {
            console.log(err.message);
        }
    }

    ShowStores(obj) {
        try {
            // alert("ShowStores called");
            var selectedstoreid = $(obj).attr('data-storeid');
            var storename = $(obj).attr('data-storename');
            var storeaddress = $(obj).attr('data-storeaddress');
            // alert(selectedstoreid + "," + storename + "," + storeaddress);
            InStorePickUp.prototype.SaveinCookie(selectedstoreid, storename, storeaddress)
        }
        catch (err) {
            console.log("ShowStores msg=" + err.message);
        }

    }
    /***************STORE LOCATOR METHODS END***************************/

   /**************************CheckOut Methods**********************************/

    GetStockForCheckout(sku, productid) {
        var state = "ALL";
        var selectedstore = $(".storename:first").text();
    // alert(selectedstore);
        try {
            RSZnodeEndpoint.prototype.GetStockForCheckoutSuccess(state, sku, function (data) {
                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));
                        //console.log(json);
                        $.each(json, function (j, val) {
                            if (json[j].StoreName == selectedstore) {

                                $("#SelectedWarehouseStock").val(json[j].Quantity);
                                var status = "status_" + productid;
                                var showmsgpickup = "showmsgpickup_" + productid;
                                var qty = "#qty_" + productid;
                                // alert("qty=" + $(qty).html());
                                if (json[j].Quantity == 0) {
                                    //$("#Ship_" + productid).prop('checked', true);
                                    $(".qty").each(function () {
                                        // alert("inside .qty");
                                        var qtysku = $(this).attr('data-sku');
                                        // var radioValue = $("input[id='Pickup-" + productid + "']:checked").val();
                                        if (sku == qtysku) {
                                            //$(this).attr('data-IsPickUp', 'true');
                                            $("#" + status).html('Not Available <br/> <span class="select-store"><a style="text-decoration: underline;padding: 0px;font-weight: 400" class="text-danger" href="/cart" title="Select the ship option">Select the ship option</a></span>');
                                            $("#" + status).show();
                                            //$("#btnCompleteCheckout").addClass('disable-anchor');
                                           // $("#paypal-express-checkout").addClass('disable-anchor');
                                        }
                                    });
                                }
                                else {
                                    if (json[j].Quantity < $(qty).html()) {
                                        $("#" + status).html("<b style='color:#000'>Out of stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left <br/><a href='/cart'>Update</a></span>");
                                       // $("#btnCompleteCheckout").addClass('disable-anchor');
                                       // $("#paypal-express-checkout").addClass('disable-anchor');
                                    }
                                    else {
                                        $("#" + status).html("<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + json[j].Quantity + " left</span>");
                                       // $("#btnCompleteCheckout").removeClass('disable-anchor');
                                       // $("#paypal-express-checkout").removeClass('disable-anchor');
                                    }
                                    $("#" + status).show();
                                    //  $("#" + showmsgpickup).hide();                                    
                                }
                                //$("#loaderId").html("");

                            }
                        });
                    }
                });
            });
            $(".pickupsku").each(function () {
                var value = $(this).text();
                // alert("value=" + value);
                var arr = value.split('-');
                var productid = arr[1];
                var status = "status_" + productid;
                if ($("#" + status).html().indexOf("Not Available") != -1) {
                    //   alert("inside not available");
                    $("#btnCompleteCheckout").addClass('disable-anchor');
                    $("#paypal-express-checkout").addClass('disable-anchor');

                }
            });
            /*new*/
            $("#loaderId").html("");
        }
        catch (err) {
            console.log(err.message);
        }
    }
    

//new method
    GetCurrentSelectedStoreDetails(storename) {
        var state = "ALL";
        var sku = '0';
        try {
            RSZnodeEndpoint.prototype.GetCurrentSelectedStoreDetailsSuccess(state, sku, function(data) {
                $.each(data, function (i, item) {
                    if (i == "StoreLocations") {

                        var json = JSON.parse(JSON.stringify(data[i]));
                        //console.log(json);
                        $.each(json, function (j, val) {
                            if (json[j].StoreName == storename) {
                                InStorePickUp.prototype.AssignStoreDetails(json[j].WarehouseId, json[j].StoreName, json[j].StoreAddress);
                            }
                        });
                    }
                });

                /*new*/
                $("#loaderId").html("");
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }

    FillLocationDropdown() {
        var sku = "0";
        try {
            RSZnodeEndpoint.prototype.FillLocationDropdownsuccess(sku, function (data) {

                $.each(data, function (i, item) {
                    var json = JSON.parse(JSON.stringify(data[i]));
                    $.each(json, function (j, val) {
                        var address1 = JSON.stringify(json[j]);
                        var t = JSON.parse(address1);
                        var r = t.StoreName.split("-");

                        $('#ddlLocation').append($("<option></option>").attr("value", t.WarehouseId).text(r[1]));
                    });

                });

            });

        } catch (n) {
            console.warn(n.message)
        }
    }

    FillBrandsDropdown() {
        try {
            RSZnodeEndpoint.prototype.FillBrandsDropdownSuccess(function (n) {
                $.each(n, function (a, e) {
                    var o = JSON.parse(JSON.stringify(n[a]));
                    $.each(o, function (n, a) {
                        var e = JSON.stringify(o[n]),
                            t = JSON.parse(e);
                        $("#ddlBrand").append($("<option></option>").attr("value", t.BrandId).text(t.BrandName))
                    })
                })

            });
        } catch (n) {
            console.warn(n.message)
        }
    }

    
}