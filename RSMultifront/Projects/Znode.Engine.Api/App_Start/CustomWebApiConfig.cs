﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Znode.Engine.Api
{
    public static class CustomWebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Custom Portal Detail Routes
            config.Routes.MapHttpRoute("custom-portal-list", "customportal/list", new { controller = "customportal", action = "getportallist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("custom-portal-detail-list", "customportaldetail/list", new { controller = "customportal", action = "getcustomportaldetaillist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("custom-portal-get", "customportal/getcustomportaldetail/{customPortalDetailId}", new { controller = "customportal", action = "getcustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), customPortalDetailId = @"^\d+$" });
            config.Routes.MapHttpRoute("custom-portal-create", "customportal/create", new { controller = "customportal", action = "insertcustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("custom-portal-update", "customportal/update", new { controller = "customportal", action = "updatecustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("custom-portal-delete", "customportal/delete", new { controller = "customportal", action = "deletecustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            //Nivi Code
            config.Routes.MapHttpRoute("RS-Order-LisByStatus", "rsorder/getorderlist", new { controller = "rsorder", action = "getorderlist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("RS-Order-UpdateOrderStatus", "rsorder/updateorderstatus", new { controller = "rsorder", action = "updateorderstatus" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("RS-Order-UpdateOrderStatusMany", "rsorder/updateorderstatusmany", new { controller = "rsorder", action = "updateorderstatusmany" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("RS-Order-ExecutePackage", "rsorder/executepackage", new { controller = "rsorder", action = "executepackage" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("rscustomizedform-shoefinder", "rscustomizedform/shoefinder", new { controller = "rscustomizedform", action = "shoefinder" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("rscustomizedform-scheduleatruck", "rscustomizedform/scheduleatruck", new { controller = "rscustomizedform", action = "scheduleatruck" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("custom-product-storelocation", "rspublishproduct/getstorelocationsdetails/{state}/{sku}", new { controller = "rspublishproduct", action = "getstorelocationsdetails" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("RSAllProductFeed-productfeed", "rsallproductfeed/productfeed", new { controller = "rsallproductfeed", action = "create" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("rssalesdetails-getsalesdetails", "rssalesdetails/getsalesdetails", new { controller = "rssalesdetails", action = "getsalesdetails" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
        }
    }
}