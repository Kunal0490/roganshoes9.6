var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Dashboard = /** @class */ (function (_super) {
    __extends(Dashboard, _super);
    function Dashboard() {
        var _this = _super.call(this) || this;
        _this._endPoint = new Endpoint();
        return _this;
    }
    Dashboard.prototype.GetSelectedPortal = function () {
        var portalIds = $("#ddlPortal").val();
        var durationId = $("#ddlDuration").val();
        if (portalIds == 0) {
            var values = $.map($('#ddlPortal option'), function (e) { return e.value; });
            portalIds = values.join(',');
        }
        return portalIds;
    };
    Dashboard.prototype.DisplayLowInventoryProductReport = function () {
        var url = window.location.protocol + "//" + window.location.host + "/MyReports/GetDashboardReport?reportPath=InventoryReorder&reportName=Inventory Re-order&portalIds=" + Dashboard.prototype.GetSelectedPortal() + "&durationId=" + $("#ddlDuration").val();
        window.open(url, '_blank');
    };
    Dashboard.prototype.GetSalesDetailsBasedOnSelectedPortal = function () {
        Endpoint.prototype.SalesDetailsBasedOnSelectedPortal(parseInt($("#ddlPortal").val()), function (response) {
            $("#salesDetailsOfSelectedPortal").html(response.html);
            $("#TopProducts").html(response.TopProduct);
            $("#TopSearches").html(response.TopSearch);
            $("#TopBrands").html(response.TopBrand);
        });
        //Get low inventory products on the basis of selected portal.
        Dashboard.prototype.GetDashboardLowInventoryProductCountOnSelectedPortal();
    };
    Dashboard.prototype.GetDashboardLowInventoryProductCountOnSelectedPortal = function () {
        Endpoint.prototype.DashboardLowInventoryProductCountOnSelectedPortal(parseInt($("#ddlPortal").val()), function (response) {
            $("#LowInventoryCount").html(response.html);
        });
    };
    Dashboard.prototype.SetLink = function () {
        var _newUrl = MediaManagerTools.prototype.UpdateQueryString("portalId", $("#ddlPortal").val(), window.location.href);
        window.history.pushState({ path: _newUrl }, '', _newUrl);
    };
    return Dashboard;
}(ZnodeBase));
//# sourceMappingURL=Dashboard.js.map